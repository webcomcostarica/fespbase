<?php
return [
    //Fechas
    
    'January' => 'Enero',
    'February' => 'Febrero',
    'March' => 'Marzo',
    'April' => 'Abril',
    'May' => 'Mayo',
    'June' => 'Junio',
    'July' => 'Julio',
    'August' => 'Agosto',
    'September' => 'Septiembre',
    'October' => 'Octubre',
    'November' => 'Noviembre',
    'December' => 'Diciembre',
    //lotes
    'Cant Inicial' => 'Cantidad inicial',
    'Create Lote' => 'Crear lote',
	//taxonomias     
    'Schortcut' => 'Acceso directo',   
    'Shortcuts' => 'Accesos directos',   
    'Create Shortcuts' => 'Crear acceso directo',
    
    'Search' => 'Buscar',   
    'Reset' => 'Limpiar filtros',   
    
    'ID' => 'Código',   
    'Name' => 'Nombre',   
    'Name' => 'Nombre',   
    'Link' => 'Enlace',   
    'Image' => 'Imagen',   
    'Pos' => 'Posición',   
    'Description' => 'Descripción', 
    
    
    'Create' => 'Crear',  
    'Update' => 'Actualizar',  
    'Delete' => 'Borrar',  


	//taxonomias
    'Create Taxonomia' => 'Crear taxonomía',
    'Taxonomias' => 'Taxonomías',
	

	//package
    'Categoria' => 'Categoría',
    'Descripcion' => 'Descripción',
    'Modificacion' => 'Modificación',
    'Num Factura' => 'Número Factura',
    'Creacion' => 'Creación',
    'Create Package' => 'Crear paca',
    'Packages' => 'Pacas',
    'Compra' => 'Fecha de compra',
    'Update Package:' => 'Actualizar paca: ',
	
    //usuarios
    'Create User' => 'Crear usuario',
    'Users' => 'Usuarios',
    'ID' => 'Código',
    'Username' => 'Usuario',
    'Auth Key' => 'Código de autorización',
    'Password Hash' => 'Máscara de seguridad',
    'Password Reset Token' => 'Llave de seguridad',
    'Email' => 'Correo',
    'Status' => 'Estado',
    'Rol ID' => 'Rol',
    'Created At' => 'Creado',
    'Updated At' => 'Modificado',    
    
    'Congratulations!' => 'Felicidades!',
    'password' => 'Contraseña',
    'rememberMe' => 'Recuerdame',
    'username' => 'Usuario',

    
    //Roles
    'ID' => 'Código',
    'Nombre' => 'Nombre',
    'Rols' => 'Roles',
    'Rigths' => 'Derechos',
    'Operations' => 'Operaciones',
    'Users' => 'Usuarios',
    'Allowed operations' => 'Operaciones Permitidas',
    
    //Operaciones
    'Operaciones' => 'Operaciones',
    'Operacions' => 'Operaciones',
    'Operacion' => 'Operación',
    
    //botones
    'Create' => 'Crear',
    'Update' => 'Actualizar',
    'Delete' => 'Borrar',
    
    //inicio
    'Home' => 'Inicio',
    'Login' => 'Entrar',
    
    //mensajes
    'Are you sure you want to delete this item?'=>'¿Está seguro de borrar este registro?',
    
    
    'About' => 'Acerca de',
    'Contact' => 'Contacto',
    'Logout' => 'Salir',
    'My Company' => 'Mi Empresa',
    'Sign In' => 'Entrar',
    'Sign Up' => 'Registrarse',
    'Status' => 'Estado'];


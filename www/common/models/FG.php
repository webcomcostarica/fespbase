<?php
namespace common\models;
use Yii;

class FG {    
    public static function logfile($mensaje){
        if(is_object($mensaje))
			$mensaje = serialize($mensaje);
		if(is_array($mensaje))	
			$mensaje = json_encode($mensaje);
		$url = Yii::getAlias('@webroot').'datos.log';
        $fp = fopen($url, 'a+');
            fputs($fp,"\r\n".date('Y:m:d h:i:s a').'-'.$mensaje.'\r\n \r\n \r\n');
        fgets($fp);
        
        /*
		if(is_object($mensaje))
			$mensaje = serialize($mensaje)
		
        $fp = fopen('datos.log', 'a+');
            fputs($fp,"\r\n".date('Y:m:d h:i:s a').'-'.$mensaje.'\r\n \r\n \r\n');
        fgets($fp);
		*/
       
    }
    public static function dc($number){   
       return round($number,2,PHP_ROUND_HALF_ODD);
       // $precision = 5;
       // return substr(number_format($number, $precision+1, '.', ''), 0, -1);
    }
    
    public static function ultimo_dia() { 
          $month = date('m');
          $year = date('Y');
          $day = date("d", mktime(0,0,0, $month+1, 0, $year));
     
          return date('d', mktime(0,0,0, $month, $day, $year));
      }
}


<?php

namespace common\components;

use backend\models\DgtTe;
use backend\models\DgtFe;
use backend\models\FirmadorDGT;
use backend\models\DgtToken;
use backend\models\Configuraciones;
use backend\models\NcDgtComprobante;
use Yii;
use backend\models\SisConfiguraciones;
use backend\models\base\SendMail;
use common\models\FG;

class FuncionesDGT {

    const ESTADOESTADO = [
        0 => 'Sin Enviar',
        1 => 'Aceptado',
        2 => 'Rechazado Parcialmente',
        3 => 'Rechazado',
    ];
    
    const ENCABEZADOS = [
        '01'=>'<?xml version="1.0" encoding="utf-8"?><FacturaElectronica 
            xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
            xsi:schemaLocation="https://tribunet.hacienda.go.cr/docs/esquemas/2017/v4.2/facturaElectronica https://tribunet.hacienda.go.cr/docs/esquemas/2016/v4.2/FacturaElectronica_V4.2.xsd" 
            xmlns="https://tribunet.hacienda.go.cr/docs/esquemas/2017/v4.2/facturaElectronica"></FacturaElectronica>',
        '03'=> '<?xml version="1.0" encoding="utf-8"?><NotaCreditoElectronica 
                xmlns="https://tribunet.hacienda.go.cr/docs/esquemas/2017/v4.2/notaCreditoElectronica" 
                xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"></NotaCreditoElectronica>',
        '05'=>'<?xml version="1.0" encoding="utf-8"?><MensajeReceptor 
                xmlns="https://tribunet.hacienda.go.cr/docs/esquemas/2017/v4.2/mensajeReceptor" 
                xmlns:xs="http://www.w3.org/2001/XMLSchema" 
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"></MensajeReceptor>',
        '06'=>'<?xml version="1.0" encoding="utf-8"?><MensajeReceptor 
                xmlns="https://tribunet.hacienda.go.cr/docs/esquemas/2017/v4.2/mensajeReceptor" 
                xmlns:xs="http://www.w3.org/2001/XMLSchema" 
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"></MensajeReceptor>',
        '07'=>'<?xml version="1.0" encoding="utf-8"?><MensajeReceptor 
                xmlns="https://tribunet.hacienda.go.cr/docs/esquemas/2017/v4.2/mensajeReceptor" 
                xmlns:xs="http://www.w3.org/2001/XMLSchema" 
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"></MensajeReceptor>',
    ];
    
  
   
   
    
    public static function sendComprobante($xmlFirmado, $token, $data, $numeracion,$ambiente) {
       $comprobante = true; // FE,TE,o notas
       $numeroReceptor = false;
       if(
            $numeracion->tipoComprobante == '05' || 
            $numeracion->tipoComprobante == '06' || 
            $numeracion->tipoComprobante == '07' 
       ){
           $comprobante = false; //Mensajes de receptor
       }
       if(!$comprobante){
            $tipoEmisor = $numeracion->tipoEmisor;
            $numeroEmisor = $numeracion->numeroCedulaEmisor;
            $fechaEmision = $numeracion->fechaEmisionDoc;
            $tipoReceptor = '01';
            $numeroReceptor = $numeracion->numeroCedulaReceptor;
       }else{
            $tipoEmisor = $data['Emisor']['Identificacion']['Tipo'];
            $numeroEmisor = $data['Emisor']['Identificacion']['Numero'];
            $fechaEmision = $data['FechaEmision'];
            if (isset($data['Receptor'])){
                $tipoReceptor = $data['Receptor']['Identificacion']['Tipo'];
                $numeroReceptor = $data['Receptor']['Identificacion']['Numero'];
            }
       }

        $mensaje = '{
          "clave": "' . $numeracion->clave . '",
          "fecha": "' . $fechaEmision . '", 
          "emisor": {
            "tipoIdentificacion": "' . $tipoEmisor . '",
            "numeroIdentificacion": "' . $numeroEmisor . '"
          },';

        if ($numeroReceptor) {
            $mensaje .='
              "receptor": {
                "tipoIdentificacion": "' . $tipoReceptor . '",
                "numeroIdentificacion": "' . $numeroReceptor . '"
              },';
        }
        if(!$comprobante)
            $mensaje .='
                "consecutivoReceptor":"'.$numeracion->numeroConcecutivo.'",';
    
        $mensaje .='
          "comprobanteXml": "' . base64_encode($xmlFirmado) . '"
        }';
            
        $header = array(
            'Authorization: bearer ' . $token->access_token,
            'Content-Type: application/json'
        );

        $curl = curl_init($ambiente['urlRecepcion']);
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, $mensaje);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);

        $respuesta = curl_exec($curl);
        $numeracion->envioEstado = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        $dataResponse = explode("\n", $respuesta);
        
        
        
        if ($numeracion->envioEstado == 202){
            if(isset($numeracion->urlConsulta))
            foreach ($dataResponse as $key => $currentkey) {               
                $pos = strpos($currentkey, 'Location');
                if ($pos !== false) {
                    $numeracion->urlConsulta = utf8_encode($dataResponse[$key]);
                    break;
                }
            }
			$numeracion->response = '';
			$numeracion->json = '';
            $numeracion->envioMensaje = 'Entregado correctamente';
        }else{
            
            foreach ($dataResponse as $key => $currentkey) {
                $pos = strpos($currentkey, 'X-Error-Cause');
                if ($pos !== false) {
                    $numeracion->envioMensaje = utf8_encode($dataResponse[$key]);
                    break;
                }
                $pos = strpos($currentkey, 'x-error-cause');
                if ($pos !== false) {
                    $numeracion->envioMensaje = utf8_encode($dataResponse[$key]);
                    break;
                }
            }
			$numeracion->response = 'SCE 01';
			$numeracion->json = 'SCE 01';
        }
         
        if ($numeracion->save()){
            Yii::$app->session->setFlash('success', 'Comprobante enviado.');
              
         return [
                'Numeracion' => $numeracion,
                'DatosDelComprobante' => $data,
                'Token' => $token,
            ];
        } else {
            var_dump($numeracion->errors);die;
            Yii::$app->session->setFlash('success', 'Ha ocurrido un error al envar el mensaje.');
            return false;
        }
    }

    public static function enviarCorreo($comprobante){
        $modelEmail = New SendMail();
        if(isset($comprobante->comprobante) && isset($comprobante->idColaboradors)){
            $dgt_compro = $comprobante->comprobante;
            $config = SisConfiguraciones::getParametrosGenerales();
            $modelEmail->desde = 'fe@webcomcr.cloud';
            $colaborador = $comprobante->idColaboradors[0];
            if($colaborador->id != $config['ClientePorDefecto']){
                $modelEmail->para = $colaborador->correo;
                if(empty($modelEmail->para)){                    
                    $modelEmail->para = $config['Correo'];
                }
                $modelEmail->asunto = 'INFO: Nuevo comprobante electrónico';
                $nombre = ($colaborador->id == $config['ClientePorDefecto'])? 'Usuario': $colaborador->nombreCompleto;
                $emisor = $config['NombreComercial'].'('.$config['Cedula'].')';
                $modelEmail->plantilla = " 
                <h1>Nuevo comprobante electrónico</h1>
                    <p>
                        Estimado, $nombre <br>
                    </p>
                    <p>
                        El emisor autorizado <?= $emisor ?>, ha creado un comprobante electrónico para usted.<br>
                        <br>
                        Verifique la información adjunta.
                        <ul>
                            <li>Comprobante PDF (De facil lectura)</li>
                            <li>Comprobante XML (Comprobante legal)</li>
                            <li>Mensaje de hacienda confirmado que todo está en orden</li>
                        </ul>
                    </p>

                    <p>
                        Saludos.
                    </p>
                ";
                
                $modelEmail->mensajeXml = $dgt_compro->pathMensajeXml;
                $modelEmail->comprobanteXml = $dgt_compro->pathComprobanteXml;
                $modelEmail->comprobantePdf = $dgt_compro->pathComprobantePdf;
                $dgt_compro->correoNotificacion = $modelEmail->send();
                $dgt_compro->correoNotificado = $modelEmail->para;
                $dgt_compro->save();
                echo 'Enviado '.$dgt_compro->correoNotificacion.'<br>';
            }else{
                echo '->>Defecto<br>';
                $dgt_compro->correoNotificacion = 1;               
                $dgt_compro->save();
            }
            return $dgt_compro->correoNotificacion;
        }else{
            echo '->>Falta algo<br>';
        }
       
    }

    public static function getEstado($datosFinales,$config) {
        $curl = curl_init();
        $ambiente = self::getAmbienteSistema($config);
        $token = FuncionesDGT::TokenGenerarNuevo($ambiente);
        $numeracion = $datosFinales['Numeracion'];
		
		if(is_object($token)){
			curl_setopt_array($curl, array(
				CURLOPT_URL => $ambiente['urlRecepcion'] . $numeracion->clave,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",CURLOPT_MAXREDIRS => 10,CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",
				CURLOPT_HTTPHEADER => array(
					"Authorization: Bearer " . $token->access_token,
					"Cache-Control: no-cache",
					"Content-Type: application/x-www-form-urlencoded",
				),
			));

			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			
			$response = curl_exec($curl);
			$err = curl_error($curl);
			$code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			$numeracion->responseEstado = $response;
					
			curl_close($curl);
			echo 'Dgt HHTP ('.$code.')';
			if( $code >= 200 && $code <= 299){                  
				$response = json_decode($response);
				if(isset($response->{'error'})){
					$numeracion->estadoMensaje = $response->{'error_description'};
					$numeracion->estadoEstado = $code;                
				}else{
					$array = get_object_vars($response);
					$comprobante = $numeracion->comprobante;
					$numeracion->pathMensajeXml = Yii::getAlias('@webroot') . $ambiente['paths'][$numeracion->tipoComprobante] . $numeracion->numConce . '_mensaje' . '.xml';    
				  
					if (isset($array['respuesta-xml'])) {
						$mensaje = base64_decode($array['respuesta-xml']);                                       
						try {
							file_put_contents($numeracion->pathMensajeXml, $mensaje);
						} catch (\Exception $e) {
							Yii::$app->session->setFlash('danger', 'No se genero el mensaje de aceptacion.');
							return false;
						}
						$xml = new \SimpleXMLElement($mensaje);
						$mensajeDetalle = str_replace("'", '', utf8_decode($xml->DetalleMensaje));
						$mensajeDetalle = utf8_encode($mensajeDetalle);
						$numeracion->estadoMensaje = str_replace("'", '', $mensajeDetalle);
						$numeracion->estadoEstado = $xml->Mensaje;
					} else {
						if(isset($array['ind-estado']))
							if($array['ind-estado'] == 'procesando')
								$numeracion->estadoMensaje = $array['ind-estado'];
						echo '<pre>';
						var_dump($array);
						echo '</pre>';
					}
					
				}
			}else{
				if($code == 500){
					Yii::$app->session->setFlash('danger', 'Hacienda no responde.');
					return false;
				}            
				$numeracion->estadoMensaje = $err;
				$numeracion->estadoEstado = $code;  
			}

			if ($numeracion->save()) {
				echo ' guardado ';
			} else {
				if($response){
					echo ' dgtNoResponde <br>';
					echo '<pre>';
					var_dump($response);
					echo '</pre>';
				}
				echo ' NoSeHaGuardado ';
			}
		}else{
			Yii::$app->session->setFlash('danger', 'Hacienda no responde.');							
		}
    }

    public static function getAmbienteSistema($configs){        
        if($configs['Ambiente'] == 0){           
           $data = array(
                'client_id' => 'api-stag',
                'client_secret' => '',
                'grant_type' => 'password',
                'username' => $configs['StgUsuarioDGT'],
                'password' => $configs['StgClaveDGT'] ,
                'scope' => ''
            );
        }else{
          $data = [
                'client_id' => 'api-prod',
                'client_secret' => '',
                'grant_type' => 'password',
                'username' => $configs['ProdUsuarioDGT'],
                'password' => $configs['ProdClaveDGT'],
                'scope' => ''
          ];
        }
        
        $ambientes = [
                SisConfiguraciones::PRUEBAS => [ //Pruebas
                'pinP12' => $configs['StgPinArchivoP12'],
                'urlP12' => '/crt/pruebas/'.$configs['StgArchivoP12'],
                'urlRecepcion' => 'https://api.comprobanteselectronicos.go.cr/recepcion-sandbox/v1/recepcion/',
                'urlToken' => 'https://idp.comprobanteselectronicos.go.cr/auth/realms/rut-stag/protocol/openid-connect/token',
                'paths'=>[
                    '01'=>'/crt/pruebas/fe/',
                    '02'=>'/crt/pruebas/nd/',
                    '03'=>'/crt/pruebas/nc/',
                    '04'=>'/crt/pruebas/te/',
                    '05'=>'/crt/pruebas/mr/',
                    '06'=>'/crt/pruebas/mr/',
                    '07'=>'/crt/pruebas/mr/'
                ],
                'jsonSend' => $data,
                'jsonSendParamsCurl' => http_build_query($data)
            ], 

            SisConfiguraciones::PRODUCCION => [
                'pinP12' => $configs['ProdPinArchivoP12'],
                'urlP12' => '/crt/produccion/'.$configs['ProdArchivoP12'],
                'urlRecepcion' => 'https://api.comprobanteselectronicos.go.cr/recepcion/v1/recepcion/',
                'urlToken' => 'https://idp.comprobanteselectronicos.go.cr/auth/realms/rut/protocol/openid-connect/token',
                'paths'=>[
                    '01'=>'/crt/produccion/fe/',
                    '02'=>'/crt/produccion/nd/',
                    '03'=>'/crt/produccion/nc/',
                    '04'=>'/crt/produccion/te/',
                    '05'=>'/crt/produccion/mr/',
                    '06'=>'/crt/produccion/mr/',
                    '07'=>'/crt/produccion/mr/'
                ],
                'jsonSend' => $data,
                'jsonSendParamsCurl' => http_build_query($data)
            ]];
         
        return $ambientes[(int)$configs['Ambiente']];
    }

    public static function ValidarAmbiente($amb){
        if( trim($amb['pinP12'])!='' && trim($amb['urlP12'])!='' &&
            trim($amb['jsonSend']['username'])!='' && 
            trim($amb['jsonSend']['password'])!='')
            return true;
    }

    public static function TokenGenerarNuevo($ambiente){
        $token = DgtToken::findOne(1);
        
        $ahora = strtotime(date('Y-m-d H:i:s'));	        
        $bdHora = $token->refresh_token_limit;
		
        if((int)$ahora > (int)($bdHora)){
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $ambiente['urlToken'],
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $ambiente['jsonSendParamsCurl'],
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "content-type: application/x-www-form-urlencoded",
                    "postman-token: c1016240-cf6f-fe54-67d6-587ad9b11c39"
                ),
            ));
            
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            
            $response = curl_exec($curl);
            $err = curl_error($curl);
            $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);
            $token->access_token = '...';
            //$token->creacion = date('Y-m-d h:i:s');
            
            if ($err) {
                 if(!$token->save()){
                        var_dump($model->erros);die;
                    }
                return  'Hacienda no responde ('.$code.'-'.$err.').';
            } else {
                $response = json_decode($response); 
                if(isset($response->{'error'})){
                    if(!$token->save()){
                        var_dump($token->errors);die;
                    }
                    return 'Credenciales incorrectos ('.$response->{'error_description'}.').';
                }
                
                $refresh_token_limit = strtotime(date('Y-m-d H:i:s')) + $response->{'expires_in'};
                
                $token->refresh_token_limit = $refresh_token_limit;                
                $token->access_token = $response->{'access_token'};
                $token->expires_in = $response->{'expires_in'};
                $token->refresh_expires_in = $response->{'refresh_expires_in'};
                $token->refresh_token = $response->{'refresh_token'};
                $token->token_type = $response->{'token_type'};
                $token->id_token = $response->{'id_token'};
                $token->session_state = $response->{'session_state'};
                if ($token->save()) {
                   // Yii::$app->session->setFlash('success', 'Nuevo token con hacienda.');
                     return $token;
                } else {
                    return 'No se ha guardado el token.';
                }            
            }
        }else{       
            return $token;
        }
    }
    
            
    public static function getToken() {
        if (self::ValidarConexionHacienda()) {
            $configs = SisConfiguraciones::getParametrosGenerales();
            $ambiente = self::getAmbienteSistema($configs);     
            return self::TokenGenerarNuevo($ambiente);
        }
        return false;
    }
   
    
    
    
    
    
    
    
    
    
    
    
    public static function MrFirmarEnviarComprobante($xmlString,$config,$numeracion){
        $ambiente = self::getAmbienteSistema($config);        
        $numeracion->pathMensajeXml = $pathComprobante =  $ambiente['paths'][$numeracion->tipoComprobante] . $numeracion->clave . '_mr.xml';
        $pathComprobante = Yii::getAlias('@webroot') . $pathComprobante;
        $numeracion->save();

        file_put_contents($pathComprobante, $xmlString);
        $pathp12 = Yii::getAlias('@webroot') . $ambiente['urlP12'];
        $firmador = new \backend\models\FirmadorDGT();
        $firmador->firmar(
                $pathp12,               //uri del cert.p12
                $ambiente['pinP12'].'', //clave para el cert.p12
                $pathComprobante,       //URI XML sin fimar              
                $pathComprobante,       //URI XML fimardo              
                $numeracion->tipoComprobante
        );
        
        $xml = file_get_contents($pathComprobante);
        
        return FuncionesDGT::sendComprobante(
            $xml,    //xmlFirmado
            FuncionesDGT::getToken(),               //Token
            $xmlString,                                  //Objeto del comprobante
            $numeracion,
            $ambiente
        );
    }
    
    
    
    
        
    
    public static function ValidarConexionHacienda() {
        $validar = @fsockopen('idp.comprobanteselectronicos.go.cr', 80, $errno, $errstr, 15);
        if ($validar) {
            fclose($validar);
            return true;
        }
        Yii::$app->session->setFlash('warning', 'No hay conexion con hacienda');
        return false;
    }
    
    
     public static function toXml($config,$data,$numeracion,$detallesXml){        
        $ambiente = self::getAmbienteSistema($config);  
        $xml_data = new \SimpleXMLElement(self::ENCABEZADOS[$config['tipoComprobante']]);
        $numeracion->pathComprobanteXml = $pathComprobante = Yii::getAlias('@webroot') . $ambiente['paths'][$config['tipoComprobante']] .$data['NumeroConsecutivo'] . '_comprobante'.  '.xml';
        $firmador = new FirmadorDGT();
        $firmador->array_to_xml($data, $xml_data);
        $dom = new \DOMDocument("1.0");
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $dom->loadXML($xml_data->asXML());
        $result = $dom->saveXML();
        $result = str_replace('ListaDetalles', $detallesXml, $result);
        file_put_contents($pathComprobante, $result);
        $pathP12 = Yii::getAlias('@webroot') . $ambiente['urlP12'];
        
        $firmador->firmar(
                $pathP12, //uri del cert.p12
                $ambiente['pinP12'].'', //clave para el cert.p12
                $pathComprobante, //URI XML sin fimar
                $pathComprobante, //URI donde va caer el xmlFirmardo
                $config['tipoComprobante']
        );                                          //IMP: Parametros de tipo doc
/*
        if (FuncionesDGT::ValidarConexionHacienda()) {
            return FuncionesDGT::sendComprobante(
                file_get_contents($pathComprobante), //xmlFirmado
                FuncionesDGT::getToken(), //Token
                $data, //Objeto del comprobante
                $numeracion,
                $ambiente
            );
        }
        */
    }
    
    static function detalleXml($row) {
        $xml_data_linea = new \SimpleXMLElement('<LineaDetalle></LineaDetalle>');
        $linea_to_xml = new FirmadorDGT();
        $linea_to_xml->array_to_xml($row, $xml_data_linea);
        $dom = new \DOMDocument("1.0");
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $dom->loadXML($xml_data_linea->asXML());
        return preg_replace("/<\\?xml.*\\?>/", '', $dom->saveXML());
    }

    static function getPostGetParams() {
        return array_merge(Yii::$app->request->queryParams, Yii::$app->request->post());
    }

    public static function ClaveNumerica($datos, $config, $comprobante) {
        $numConce = sprintf("%03d", $config['Sucursal']) .
                sprintf("%05d", $config['Terminal']) .
                sprintf("%02d", $config['tipoComprobante']);

        if ($config['tipoComprobante'] == '01')
            $nuevo = new DgtFe();
        if ($config['tipoComprobante'] == '03')
            $nuevo = new NcDgtComprobante();
        if ($config['tipoComprobante'] == '04')
            $nuevo = new DgtTe();

        $nuevo->tipoComprobante = $config['tipoComprobante'];    
        $nuevo->fechaEmision = self::getFormatoFecha($comprobante->fecha);
        $nuevo->normativa = $config['Resolucion'];
        $nuevo->fechaNormativa = $config['FechaResolucion'];
        $nuevo->idComprobante = $comprobante->id;
        $nuevo->scenario = 'Generacion';
        $nuevo->correoNotificacion = 0;
        if (!$nuevo->save()) {
			FG::logfile('Detalle');          
            Yii::$app->session->setFlash('danger', 'ENC 01: Error al crear la <b>numeración</b> del comprobante');
            return $nuevo;
        }

        
        $nuevo->numConce = $numConce . sprintf("%010d", $nuevo->id);
        $clave = $config['CodPais'];
        $clave .= date('dmy', strtotime($comprobante->fecha));
        $clave .= str_pad($datos['emi_numeroId'], 12, '0', STR_PAD_LEFT);
        $clave .= $nuevo->numConce;
        $clave .= '1'; //Estado del comprobante
        $clave .= self::codigoAzar();
        $nuevo->clave = $clave;

        if (!$nuevo->save()) {
            Yii::$app->session->setFlash('danger', 'Error al crear la <b>clave</b> del comprobante');
        }
        return $nuevo;
    }

    public static function codigoAzar() {
        $digits = 8;
        return rand(pow(10, $digits - 1) - 1, pow(10, $digits) - 1);
    }

    public static function valoresDgt($valor) {
        return number_format((float) $valor, 5, '.', '');
    }

    public static function getFormatoFecha($x = null) {
        date_default_timezone_set('America/Costa_Rica');
        $fecha = new \DateTime($x);
        if (!isset($x)) {
            $fecha = new \DateTime(date('Y-m-d H:i:s', strtotime('now')));
        }
        return $fecha->format('c');
    }
    
     public static function ComprobanteXML($datos, $config, $numeracion) {
        $data = array(
            'Clave' => $datos['clave'],
            'NumeroConsecutivo' => $datos['numConce'],
            'FechaEmision' => $datos['fechaEmision'],
            'Emisor' => [
                'Nombre' => $datos['emi_nombre'],
                'Identificacion' => [
                    'Tipo' => $datos['emi_tipo'],
                    'Numero' => $datos['emi_numeroId'],
                ],
                'Ubicacion' => [
                    'Provincia' => $datos['emi_provincia'],
                    'Canton' => sprintf("%02d", $datos['emi_canton']),
                    'Distrito' => sprintf("%02d", $datos['emi_distrito']),
                    'Barrio' => sprintf("%02d", $datos['emi_barrio']),
                    'OtrasSenas' => $datos['emi_otrasenas'],
                ],
                'Telefono' => [
                    'CodigoPais' => $datos['emi_codPais'],
                    'NumTelefono' => $datos['emi_numTel'],
                ],
                'CorreoElectronico' => $datos['emi_correo'],
            ]
        );

        if (isset($datos['rec_tipo']))
            if ($datos['rec_tipo'] !== 999) {
                    $receptor = [
                            'Nombre' => $datos['rec_nombre'],
                            'Identificacion' => [
                                'Tipo' => $datos['rec_tipo'],
                                'Numero' => $datos['rec_numeroId'],
                            ]
                        ];                        
                        
                    if ($datos['rec_telefono'] !== '') { 
                        $receptor['Telefono'] = [
                            'CodigoPais' => $datos['rec_codPais'],
                            'NumTelefono' => $datos['rec_telefono'],
                        ];                                
                    }
                    if ($datos['rec_correo'] !== '') {
                        $receptor['CorreoElectronico'] = $datos['rec_correo'];                       
                    }
                                
                    
                $data['Receptor'] = $receptor;
            }           
        
        $data['CondicionVenta'] = $datos['com_condicionVenta'];

        if ($datos['com_condicionVenta'] === '02') {
            $data['PlazoCredito'] = $datos['com_plazoCredito'];
        }

        $data['MedioPago'] = $datos['com_medioPago'];
        $detallesXml = '';
        foreach ($datos['detalles'] as $detalle) {
            $detallesXml .= FuncionesDGT::detalleXml($detalle);
        }


        $data['DetalleServicio'] = 'ListaDetalles';


        $data['ResumenFactura'] = [
                    'CodigoMoneda' => $datos['com_tipMoneda'],
                    'TipoCambio' => $datos['com_tipCambio'],
                    'TotalServGravados' => $datos['com_totSerGra'],
                    'TotalServExentos' => $datos['com_totSerExe'],
                    'TotalMercanciasGravadas' => $datos['com_totMerGra'],
                    'TotalMercanciasExentas' => $datos['com_totMerExe'],
                    'TotalGravado' => $datos['com_totGravado'],
                    'TotalExento' => $datos['com_totExento'],
                    'TotalVenta' => $datos['com_totVenta'],
                    'TotalDescuentos' => $datos['com_totDescu'],
                    'TotalVentaNeta' => $datos['com_totVenNet'],
                    'TotalImpuesto' => $datos['com_monImp'],
                    'TotalComprobante' => $datos['com_totCompro'],
        ];

        if (isset($datos['ref_clave']))
            $data['InformacionReferencia'] = [
                        'TipoDoc' => $datos['ref_tipoDoc'],
                        'Numero' => $datos['ref_clave'],
                        'FechaEmision' => $datos['ref_fechaEmision'],
                        'Codigo' => $datos['ref_codigoComprobante'],
                        'Razon' => $datos['ref_razon'],
            ];
        
        $data['Normativa'] = [
            'NumeroResolucion' => 'DGT-R-48-2016',
            'FechaResolucion' => '07-10-2016 12:00:00',
        ];

        $xmlFirmado = self::toXml($config,$data,$numeracion,$detallesXml);
    }

}

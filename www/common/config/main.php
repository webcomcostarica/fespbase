<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language' => 'es-ES',
    'timeZone'=>'America/Costa_Rica',
    'components' => [
		'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=wc_base',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],     
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
         'mail' => [
             'class' => 'yii\swiftmailer\Mailer',
             'transport' => [
                  'class' => 'Swift_SmtpTransport',
                'encryption' => 'tls',
                'host' => 'mail.webcomcr.cloud',
                'port' => '465', 
                'username' => 'fe@webcomcr.cloud',
                'password' => 'T7Y+vI%hQVkG',
                // 'encryption' => 'tls', // It is often used, check your provider or mail server specs
             ],
         ],
         'i18n' => [
            'translations' => [
                'frontend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
                'backend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
            ],
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            // Hide index.php
            'showScriptName' => false,
            // Use pretty URLs
            'enablePrettyUrl' => true,
            'rules' => [
                '<alias:\w+>' => 'site/<alias>',
            ],
        ],
    ],
];

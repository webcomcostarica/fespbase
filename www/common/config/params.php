<?php
use kartik\datecontrol\Module;

return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'cuentaNombre' => 'Jarvis Base'
];

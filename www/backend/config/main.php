<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/params.php')
);

return [
    'id' => 'app-backend',
    'name' => 'Inicio',
    'basePath' => dirname(__DIR__),
    'language'=>'es-ES',
    'timeZone' => 'America/Costa_Rica',
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'aliases' => [
		'@adminlte/widgets'=>'@vendor/adminlte/yii2-widgets'
    	],
    'modules' => [
         'gridview' => [
            'class' => '\kartik\grid\Module',
            // enter optional module parameters below - only if you need to
            // use your own export download action or custom translation
            // message source
            //'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ]
    ],
    
    'components' => [
		'request' => [
            'cookieValidationKey' => 'JtZon2lnq0XN6NxstMt51McG_0jZjKun',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            
        ],
   
        
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'formatter' => [
            'dateFormat' => 'dd.MM.yyyy',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            'currencyCode' => '₡',
       ],
    ],
    'params' => $params,
];

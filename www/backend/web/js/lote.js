/*
	calcula el precio de venta tomando en cuenta el campo de entrada
*/
function cal_precioVenta(e){
	var x = event.keyCode;
	if(x==9)return false;
	let tipo = $(e).attr('tipo')
	let tipo_iva = $('#lote-imp_ven_i').val()
	let c,f,u,utilidad,subtotal,total,iva,dif
	c = $('#lote-preciocompra').val()
	//c = c.replace(",", ".");

	if(tipo == 'compra' || tipo == 'iva' || tipo == 'utilidad'){
		u = $('#lote-utilidad').val()		
		utilidad = dc(u*c/100);
		total = subtotal = dc(c)+dc(utilidad)
	}
	
	if(tipo == 'precio'){
		f = dc($('#lote-precio-calc').val())
		dif = dc(f)-dc(c);
		utilidad = dif * 100 / c;							
		total = subtotal = dc(f) + dc(utilidad)
	}
	
	if(tipo_iva == 1){
		iva = dc(13 * subtotal / 100,1);	
		total = subtotal + iva
	}
	
	if(tipo == 'compra') actualizarValores(0,subtotal,total,0,iva)
	if(tipo == 'utilidad') actualizarValores(0,subtotal,total,0,iva)	
	if(tipo == 'iva') actualizarValores(u,subtotal,total,0,iva)			
	if(tipo == 'precio') actualizarValores(utilidad,subtotal,total,1,iva)		
			
	$('.field-lote-utilidad').removeClass('has-error')
	if(utilidad <= 0)
		$('.field-lote-utilidad').addClass('has-error')
	$('.iva_display').addClass('hidden')
	if(tipo_iva == 1){
		$('.iva_display').removeClass('hidden')	
	}
}
function actualizarValores(u,p,f,t,i){	
	if(u != 0){
		$("#lote-utilidad").val(dc(u))
	}
	if(p != 0){
		$("#lote-precio").val(dc(p))
		if(t==0)
			$("#lote-precio-calc").val(dc(p))
	}	
	$("#span_sub_total").html(dc(p))
	$("#span_total_final").html(dc(f))
	$("#span_iva").html(dc(i,4))
}

function dc(valor,i=0){
	return parseFloat(parseFloat(valor).toFixed(i))
}
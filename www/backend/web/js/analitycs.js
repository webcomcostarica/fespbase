


    
    
var MASTERCHART = null;
var canvas="canvas_semana";// "canvas_dias";
var etiquetas_grafico_actual;
var datos_grafico_actual;
var tipo='Barras';

var MasterData = {
    labels: etiquetas_grafico_actual,
    datasets: [
        {
            fillColor: "rgba(0,153,204,0.5)",
            strokeColor: "rgba(0,102,153,0.8)",
            highlightFill: "rgba(220,220,220,0.75)",
            highlightStroke: "rgba(220,220,220,1)",
            data: datos_grafico_actual
        }
    ]
}

function charinit(){
    MasterData = {
    labels: etiquetas_grafico_actual,
    datasets: [
                {
                    fillColor: "rgba(0,153,204,0.5)",
                    strokeColor: "rgba(0,102,153,0.8)",
                    highlightFill: "rgba(220,220,220,0.75)",
                    highlightStroke: "rgba(220,220,220,1)",
                    data: datos_grafico_actual
                }
            ]
    }
}

function Limpiar() {
  if(MASTERCHART){
        MASTERCHART.stop();
        MASTERCHART.clear();
        MASTERCHART.destroy();
    }
}
function panel(id){    
    if(id===1){
        datos_grafico_actual=datos_dias_semana
        etiquetas_grafico_actual=["Lunes","Martes","Miercoles","Jueves","Viernes","Sabado"]
//        canvas='canvas_semana'
    }
    if(id===2){
        datos_grafico_actual=datos_dias_mes  
        etiquetas_grafico_actual=etiquetas_dias_mes
//        canvas='canvas_dias'
    }
    if(id===3){
        datos_grafico_actual=datos_meses_ano  
        etiquetas_grafico_actual=etiquetas_mes_ano 
//        canvas='canvas_meses'
    }
    pintarGrafico()
}

function graficotipo(pTipo){
    tipo=pTipo;
    pintarGrafico();
}


function pintarGrafico(){
    charinit();
    var CANVAS = document.getElementById(canvas).getContext("2d");
    if(tipo=='Barras'){
        Limpiar();
        MASTERCHART = new Chart(CANVAS).Bar(MasterData, {
            responsive: true
        });}
    if(tipo=='Radar'){
        Limpiar();
         MASTERCHART = new Chart(CANVAS).Radar(MasterData, {
            responsive: true
        });}
    if(tipo=='Lineas'){
        Limpiar();
         MASTERCHART = new Chart(CANVAS).Line(MasterData, {
            responsive: true
        });}
}
   
pintarGrafico();

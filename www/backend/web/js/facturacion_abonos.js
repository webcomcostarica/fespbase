
function abono_validar() {
    $("#abo_monto").removeClass("has-error");
    $("#abo_fecha").removeClass("has-error");
    $("#abo_saldo_actual").removeClass("has-error");
    var error = 0;

    //vacios
    if ($("#abo_fecha").val().toString().trim() === "") {
        $("#abo_fecha").val().addClass("has-error");
        error = 1;
    }
    if ($("#abo_monto").val().toString().trim() === "") {
        $("#abo_monto").addClass("has-error");
        error = 1;
    }

    if (parseFloat($("#abo_monto").val()) <= 0) {
        $("#abo_monto").addClass("has-error");
        error = 1;
    }
    if (parseFloat($("#abo_saldo_actual").val()) < 0) {
        $("#abo_saldo_actual").addClass("has-error");
        error = 1;
    }

    if (error) {
        return false;
    }
    return true;

}

function abono_limpiar() {
    $("#abo_saldo_anterior").val("Abono efectivo");
    $("#abo_monto").val("");
    $("#abo_saldo_actual").val("");
    $("#abo_fecha").val("");
    $("#abo_tipo").val("");
}

function abono_aceptar() {
    var a = $("#abo_saldo_anterior").val();
    var b = $("#abo_monto").val();
    var c = $("#abo_saldo_actual").val();
    var d = $("#abo_fecha").val();
    var f = $("#abo_tipo").val();
    var btn = $("#td_abono_aceptar").html();
    if (abono_validar()) {
        $.ajax({
            beforeSend: function () {
                $("#td_abono_aceptar").html(img_esperando);
            },
            success: function (data_s) {
                data_c = jQuery.parseJSON(data_s);
                if (data_c.msn === "agregar") {
                    $("#tbl_detalle_abonos").append(data_c.html);
                    if (parseFloat($("#abo_saldo_actual").val()) <= 0) {
                        alert("Este apartado ya está listo para retirar")
                    }
                } else {
                    console.log(data_c.msn);
                }
                $("#td_abono_aceptar").html(btn);//abono_limpiar();
                window.location.reload();
            },
            error: function (data) {
                $("#td_abono_aceptar").html(btn);
                console.log("Ocurrio un error, al intentar agregar este abono.");
            },
            type: "post",
            async: true,
            url: "/factura/abono",
            data: {
                "id": id_factura_actual,
                "sal_ant": a,
                "monto": b,
                "sal_act": c,
                "fecha": d,
                "tipo": f,
                "accion": 'agregar',
            },
            cache: false,
            dataType: "html"
        });

    }
}

function abono_quitar(id) {
    $.ajax({
        success: function (data_s) {
            data_c = jQuery.parseJSON(data_s);
            if (data_c.msn === "quitar") {
                window.location.reload();
            } else {
                console.log(data_c.msn);
            }
        },
        error: function (data) {
            $("#td_abono_aceptar").html(btn);
            console.log("Ocurrio un error, al intentar quitar este abono.");
        },
        type: "post",
        async: true,
        url: "/factura/abono",
        data: {
            "id": id,
            "accion": 'quitar',
        },
        cache: false,
        dataType: "html"
    });
}

function abono_monto(element) {
    var monto = parseFloat($("#abo_saldo_anterior").val()) - parseFloat($(element).val());
    $("#abo_saldo_actual").val(monto);
    abono_validar();
}
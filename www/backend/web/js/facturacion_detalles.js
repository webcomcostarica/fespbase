

function factura_calcular() {
    total_facturado = 0;
    sub_facturado = 0;
    ahorro = 0;
    $("#tbl_detalle tr").each(function () {
        var cant, cost, desc = 0;
        $.each(this.children, function (trs) {
            if (trs === 2) {
                cant = $(this).html();
            }
            if (trs === 3) {
                cost = $(this).html();
            }
            if (trs === 4) {
                desc = $(this).html();
                ahorro += parseFloat(desc);
            }
            if (trs === 5) {
                var sub = parseFloat(cant * cost);
                var des = parseFloat((cant * cost) - desc);
                total_facturado += des;
                sub_facturado += sub;
                $(this).html(des);
            }

        });
    });
    $("#idSub").html(sub_facturado);
    $("#tdTot").html(total_facturado);
    $("#tdDes").html(ahorro);
}

function detalle_validar(pTipo) {
    if (pTipo == 0) {
        var error = 0;
        $("#inp_des_lot").removeClass("has-error-fact");
        $("#inp_can_lot").removeClass("has-error-fact");
        $("#inp_cos_lot").removeClass("has-error-fact");
        $("#inp_desc_lot").removeClass("has-error-fact");

        //vacios
        if ($("#inp_des_lot").val().toString().trim() === "") {
            $("#inp_des_lot").addClass("has-error-fact");
            error = 1;
        }
        if ($("#inp_can_lot").val().toString().trim() === "") {
            $("#inp_can_lot").addClass("has-error-fact");
            error = 1;
        }
        if ($("#inp_cos_lot").val() === "Sin precio vigente") {
            $("#inp_cos_lot").addClass("has-error-fact");
            error = 1;
        }
        if ($("#inp_desc_lot").val().toString().trim() === "") {
            $("#inp_desc_lot").addClass("has-error-fact");
            error = 1;
        }

		 if ($("#inp_des_lot").val().length > 160) {
            $("#inp_des_lot").addClass("has-error-fact");
            error = 1;
        }
		
        //numeros negativos
        if (parseFloat($("#inp_can_lot").val()) <= 0) {
            $("#inp_can_lot").addClass("has-error-fact");
            error = 1;
        }
        if (parseFloat($("#inp_cos_lot").val()) <= 0) {
            $("#inp_cos_lot").addClass("has-error-fact");
            error = 1;
        }
        if (parseFloat($("#inp_desc_lot").val()) < 0) {
            $("#inp_desc_lot").addClass("has-error-fact");
            error = 1;
        }



        if (error) {
            return false;
        }
        return true;
    }
    if (pTipo == 1) {
        var error = 0;
        $("#inp_des_lot_1").removeClass("has-error-fact");
        $("#inp_can_lot_1").removeClass("has-error-fact");
        $("#inp_cos_lot_1").removeClass("has-error-fact");
        $("#inp_desc_lot_1").removeClass("has-error-fact");

        //vacios
        if ($("#inp_des_lot_1").val().toString().trim() === "") {
            $("#inp_des_lot_1").addClass("has-error-fact");
            error = 1;
        }
        if ($("#inp_can_lot_1").val().toString().trim() === "") {
            $("#inp_can_lot_1").addClass("has-error-fact");
            error = 1;
        }
        if ($("#inp_cos_lot_1").val().toString().trim() === "") {
            $("#inp_cos_lot_1").addClass("has-error-fact");
            error = 1;
        }
        if ($("#inp_desc_lot_1").val().toString().trim() === "") {
            $("#inp_desc_lot_1").addClass("has-error-fact");
            error = 1;
        }
		 if ($("#inp_des_lot_1").val().length > 160) {
             $("#inp_des_lot_1").addClass("has-error-fact");
            error = 1;
        }
        //numeros negativos
		let cantidad = parseFloat($("#inp_can_lot_1").val());
		let costo = parseFloat($("#inp_cos_lot_1").val());
		let descuento = $("#inp_desc_lot_1").val();
		
        if (cantidad <= 0) {
            $("#inp_can_lot_1").addClass("has-error-fact");
            error = 1;
        }
        if (costo <= 0) {
            $("#inp_cos_lot_1").addClass("has-error-fact");
            error = 1;
        }
       		
		if (descuento !== "") {
			if (descuento.indexOf("%") > 0) {
				descuento = descuento.toString().replace('%', '');
				descuento = (descuento * ((costo * cantidad))) / 100;
			}
		}	
		
		if(descuento < 0 || descuento > costo){
			$("#inp_desc_lot_1").addClass("has-error-fact");
			error = 1;
		}	
		
        if (error) {
            return false;
        }
        return true;
    }
}

function detalle_quitar(pId) {
    var tr_esperando = $("#tbl_detalle #tr_lote_" + pId).html();
    $.ajax({
        beforeSend: function () {
            $("#tr_lote_borrar_" + pId).html(img_esperando);
        },
        success: function (data_s) {
            if (data_s.msn === "quitar") {
                location.reload();  
            } else {
                console.log(data_s.msn);
            }
        },
        error: function (data) {
            console.log("Ocurrio un error, al intentar quitar este detalle.");
            $("#tbl_detalle #tr_lote_" + pId).html(tr_esperando);
        },
        type: "post",
        async: true,
        url: "/factura/detalle",
        data: {
            "id": pId,
            "accion": 'quitar',
            cache: false,
            dataType: "html"
        }
    });
}

function localstorage_validar() {
    if (typeof (Storage) !== "undefined") {
        return true;
    } else {
        alert("No se puede facturar, este explorador no soporta localstorage");
        return false;
    }
}

function detalle_aceptar(pTipo) {
        if (pTipo == 0) { 
            if (detalle_validar(0)) {
                var btn = $("#td_detalle_aceptar_id").html();
                var descuento = $("#inp_desc_lot").val();
                var tipo_des = "C";
                if (descuento !== "") {
                    if (descuento.indexOf("%") > 0) {
                        descuento = descuento.toString().replace('%', '');
                        descuento = (descuento * (($("#inp_cos_lot").val() * $("#inp_can_lot").val()))) / 100;
                        tipo_des = "%";
                    }
                } else {
                    descuento = 0;
                }

                $.ajax({
                    beforeSend: function () {
                        $("#td_detalle_aceptar_id").html(img_esperando);
                    },
                    success: function (data_s)
                    {
                        data_c = jQuery.parseJSON(data_s);
                        if (data_c.msn == "agregar") {
                            location.reload();                          
                        } else {
                            console.log(data_c.msn);
                        }
                        $("#td_detalle_aceptar_id").html(btn);
                    },
                    error: function (data) {
                        $("#td_detalle_aceptar_id").html(btn);
                        console.log("Ocurrio un error, al intentar agregar este detalle.");
                    },
                    type: "post",
                    async: true,
                    url: "/factura/detalle",
                    data: {
                        "id": id_factura_actual,
                        "codigo_lote": parseInt($("#inp_cod_int_lot").attr("id_lote")),
                        "tipo": 0,
                        "descripcion": $("#inp_des_lot").val(),
                        "cantidad": parseFloat($("#inp_can_lot").val()),
                        "costo": parseFloat($("#inp_cos_lot").val()),
                        "descuento": parseFloat(descuento),
                        "tipo_descuento": tipo_des,
                        "impuesto": $("#inp_imp_lot").val(),
                        "unidad": $("#data-titunidad").html(),
                        "accion": 'agregar',
                    },
                    cache: false,
                    dataType: "html"
                });
            }
        }
        if (pTipo == 1) { 
            if (detalle_validar(1)) {
                var btn = $("#td_detalle_aceptar_id_uno").html();
                var descuento = $("#inp_desc_lot_1").val();
                var tipo_des = "C";
                if (descuento !== "") {
                    if (descuento.indexOf("%") > 0) {
                        descuento = descuento.toString().replace('%', '');
                        descuento = (descuento * ($("#inp_cos_lot_1").val() * $("#inp_can_lot_1").val())) / 100;
                        tipo_des = "%";
                    }
                } else {
                    descuento = 0;
                }

                $.ajax({
                    type: "post",
                    async: true,
                    url: "/factura/detalle",
                    data: {
                        "id": id_factura_actual,
                        "tipo": 1,
                        "descripcion": $("#inp_des_lot_1").val(),
                        "cantidad": parseFloat($("#inp_can_lot_1").val()),
                        "unidad": $("#inp_uni_lot_1").val(),
                        "costo": parseFloat($("#inp_cos_lot_1").val()),
                        "impuesto": $('#inp_imp_lot_1').val(),
                        "descuento": parseFloat(descuento),
                        "tipo_descuento": tipo_des,
                        "accion": 'agregar',
                    },
                    cache: false,
                    dataType: "html",
                    beforeSend: function () {
                        $("#td_detalle_aceptar_id_uno").html(img_esperando);

                    },
                    success: function (data_s)
                    {
                        data_c = jQuery.parseJSON(data_s);
                        if (data_c.msn == "agregar") {
                            location.reload();                            
                        } else {
                            console.log(data_c.msn);
                        }
                        $("#td_detalle_aceptar_id_uno").html(btn);
                        detalle_limpiar(1);
                    },
                    error: function (data) {
                        $("#td_detalle_aceptar_id_uno").html(btn);
                        console.log("Ocurrio un error, al intentar agregar este detalle.");
                    },
                });
            }
        }
}

function detalle_limpiar(pTipo) {
    if (pTipo !== 3) {
        $("#inp_cod_int_lot").val("");
    }
    $("#inp_cod_int_lot").attr("id_lote", "");
    $("#inp_des_lot").val("");
    $("#inp_can_lot").val("");
    $("#inp_cos_lot").val("");
    $("#inp_desc_lot").val("");
    $("#inp_des_lot_1").val("");
    $("#inp_can_lot_1").val("");
    $("#inp_cos_lot_1").val("");
    $("#inp_desc_lot_1").val("");
}

function detalle_buscar(input_buscar) {
	let valor =$('#inp_cod_int_lot').val()
	let idInput=$('#inp_cod_int_lot').attr('id')
	if(input_buscar=='bar'){
		valor =$('#inp_cod_bar_lot').val()
		idInput=$('#inp_cod_bar_lot').attr('id')
	}
	$("#btn_modificar_lote_venta").addClass('hidden');
        $.ajax({
			beforeSend: function () {
				$("#spa_buscando_lote").html(img_esperando_simple);
			},
            success: function (data_s) {
				$("#btn_modificar_lote_venta").removeClass('hidden')
                data = jQuery.parseJSON(data_s);
                if (data.id) {
                    $("#inp_cod_int_lot").attr("id_lote", data.id);
                    $("#btn_modificar_lote_venta").attr("href", 
					'/lote/update?id='+data.id+'&venta='+id_factura_actual)
                    $("#inp_desc_lot").val(0);
                    $("#inp_can_lot").val(1);
                    $("#inp_cos_lot").val(data.precio);
                    if (data.imp_ven_i == 0)
                        data.imp_ven_i = 'E';
                    else
                        data.imp_ven_i = 'G';
                    $("#inp_imp_lab").html(data.imp_ven_i);
                    $("#inp_imp_lot").val(data.imp_ven_i);

                    
					
                    $("#data-titunidad").html(data.unidad);
                    if (!data.unidad)
                        $("#data-titunidad").html('Unidad');
                    $("#inp_des_lot").val(data.descripcion);
                } else {
                    detalle_limpiar(3);
                }   
				$("#spa_buscando_lote").html('')
            },
            error: function (data) {               
                detalle_limpiar();
                console.log("Ocurrio un error, al intentar cargar por producto por código.");
            },
            type: "get",
            async: true,
            url: "/lote/codigo",
            data: {
                "cod": valor,
                "tipo": idInput,
            },
            cache: false,
            dataType: "html"
        });  
	
}


        $("#inp_cod_int_lot").keyup(function (e) {
            detalle_buscar('cod');
        }); 
		 $("#inp_cod_int_lot").on('change',function (e) {
            detalle_buscar('cod');
        }); 
		$("#inp_cod_bar_lot").keyup(function (e) {
            detalle_buscar('bar');
        });
		$("#inp_cod_bar_lot").on('change',function (e) {
            detalle_buscar('bar');
        });
		 $('#modelButton').click(function(){
                $('.modal').modal('show')
                    .find('#modelContent')
                    .load($(this).attr('value'));
            });
            $('#modelButton').click(function(){
                $('.modal').modal('show')
                    .find('#modelContent')
                    .load($(this).attr('value'));
            });
        

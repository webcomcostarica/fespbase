
	function revisarEstadoDgt(ambiente){
		$.ajax({
			success: function (data) {
				if(data.estado)
					$('#respuesta_pruebas').html('El ambiente de pruebas está correctamente configurado.')
				else{
					$('#respuesta_pruebas').html('Los datos no permiten activar el sistema.')
					window.location.reload();
					}
			},
			error: function (data) {
				window.location.reload();
			},
			type: 'post',
			url: '/sistema/revisar-estado',
			data: {
				ambiente,
				cache: false,
				dataType: 'html'
			}
		});
	}

	function setTab(tab){
		$.ajax({      
			success: function (data) {
				
			},
			error: function (data) {
				alert('Error al cambiar tab')
			},
			type: "post",
			async: true,
			url: "/sistema/set-tab",
			data: {
				tab,
				cache: false,
				dataType: "html"
			}
		});
	}

	function cambiarAmbiente(e){
        $("#div_produccion").addClass('hidden')
        $("#div_pruebas").addClass('hidden')
        if($(e).val() == 0)
            $("#div_pruebas").removeClass('hidden')
        else
            $("#div_produccion").removeClass('hidden')
    }

	
	function activarInventario(e){
			$("#conf_inventario").addClass('hidden');
		if($(e).val() == 1){
			$("#conf_inventario").removeClass('hidden');
		}
	}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function calculadora(ele) {
	let monto = parseFloat($(ele).val())
    console.log('Total: ' + total_facturado)
    console.log('Paga con: ' + monto)
    $("#vuelto").html(total_facturado - monto);
	$.ajax({        
        success: function (data_s) {            
           console.log('error al modificar el vuelto.')
        },
        error: function (data) {
           console.log('error al modificar el vuelto.')
        },
        type: "post",
        async: true,
        url: "/factura/vuelto",
        data: {
            id: pId,
            monto: monto,
        }
    });	
}

function factura_eliminar_sa(pId) {
    $.ajax({
        beforeSend: function () {
            $(".factura-update").html(img_esperando);
        },
        success: function (data_s) {
            window.location.reload();
        },
        error: function (data) {
            window.location.reload();
        },
        type: "post",
        async: true,
        url: "/safacturacion/delete",
        data: {
            id: pId,
            cache: false,
            dataType: "html"
        }
    });
}

function factura_eliminar(pId) {
    $.ajax({
        beforeSend: function () {
            $(".factura-update").html(img_esperando);
        },
        success: function (data_s) {
            location.reload();
        },
        error: function (data) {
            location.reload();
        },
        type: "get",
        async: true,
        url: "/factura/delete",
        data: {
            "id": pId,
            cache: false,
            dataType: "html"
        }
    });
}

function factura_imprimir(id) {
    var ventimp = window.open(' ', 'popimpr');
    ventimp.document.write($("#"+id).html());
    ventimp.document.close();
    ventimp.print();
    ventimp.close();
}

/* Detalles de factura */
function CalcFechCierre() {
    if ($("#factura-forma_pago").val() === "Apartado") {
        $("#lab_vence").html(img_esperando);
        $.ajax({
            success: function (data_s) {
                $("#lab_vence").html(' Vence: ' + data_s.fecha);

            },
            error: function (data) {
                console.log("Ocurrio un error, al intentar calcular la fecha de vencimiento.");
            },
            type: "post",
            async: true,
            url: "/factura/fechaproxima",
            data: {
                "pFecha": $("#factura-fecha").val(),
                cache: false,
                dataType: "html"
            }
        });
    } else {
        $("#lab_vence").val('');
    }
}

function cambiar_opciones() {
    if ($("#factura-dgt_condicion").val() === "04") {
        $("#btn_facturar").addClass("hidden");
        $("#btn_cobrar").removeClass("hidden");
        CalcFechCierre();
    } else {
        $("#lab_vence").html("");
        $("#btn_facturar").removeClass("hidden");
        $("#btn_cobrar").addClass("hidden");
    }
}

function factura_actualizar(ele) {
    var component = $(ele);
	var recargar = false;
	if($(ele).val()=='USD' || $(ele).val()=='CRC'){
		 recargar = true;
	}
    $.ajax({
        success: function (data_s) {
            console.log(data_s.msj)
			 if(recargar)
				 window.location.reload();
			
        },
        error: function (data) {
            console.log('Ocurrio un error, al intentar cambiar tipo de factura.');
        },
        type: 'post',
        async: true,
        url: '/factura/actualizar',
        data: {
            'id': id_factura_actual,
            'condicion': $('#factura-dgt_condicion').val(),
            'medioPago': $('#factura-dgt_mediopago').val(),
            'fecha': $("#factura-fecha").val(),
            'usuario': $("#factura-idusua").val(),
            'moneda': $("#factura-dgt_moneda").val(),
            'receptorRapido': $("#col_receptor_rapido").val(),
        },
        cache: false,
    });
}


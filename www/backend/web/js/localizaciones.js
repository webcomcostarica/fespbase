
   
function buscar_ubicacion(nivel) {
    $.ajax({
        beforeSend: function () {			
            if(nivel=='cantones'){				
				$('#sisconfiguraciones-canton').attr('disabled',true)
				$('#sisconfiguraciones-canton').find('option').remove()
				$('#sisconfiguraciones-distrito').attr('disabled',true)
				$('#sisconfiguraciones-distrito').find('option').remove()
				$('#sisconfiguraciones-barrio').attr('disabled',true)
				$('#sisconfiguraciones-barrio').find('option').remove()			
				$('#sisconfiguraciones-canton').removeAttr('disabled')
			}
            if(nivel=='distritos'){					
				$('#sisconfiguraciones-distrito').attr('disabled',true)
				$('#sisconfiguraciones-distrito').find('option').remove()
				$('#sisconfiguraciones-barrio').attr('disabled',true)
				$('#sisconfiguraciones-barrio').find('option').remove()
				$('#sisconfiguraciones-distrito').removeAttr('disabled')
			}
            if(nivel=='barrios'){
			$('#sisconfiguraciones-barrio').attr('disabled',true)
			$('#sisconfiguraciones-barrio').find('option').remove()
				$('#sisconfiguraciones-barrio').removeAttr('disabled')
			}
        },
        success: function (data) {
				$('#sisconfiguraciones-canton').append(data.cantones)
				$('#sisconfiguraciones-distrito').append(data.distritos)
				$('#sisconfiguraciones-barrio').append(data.barrios)
        },
        error: function (data) {
			
        },
        type: "post",
        async: true,
        url: "/sistema/ubicaciones",
        data: {
            nivel,
			provincia:$('#sisconfiguraciones-provincia').val(),
			canton:$('#sisconfiguraciones-canton').val(),
			distrito:$('#sisconfiguraciones-distrito').val(),
			barrio:$('#sisconfiguraciones-barrio').val(),           
            cache: false,
            dataType: "html"
        }
    });
}


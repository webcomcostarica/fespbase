 
 
/* Colaboradores*/

function actualizar_correo(e) {
	$.ajax({			
        success: function (data_s) {
            console.log(data_s.mensaje)
        },
        error: function (data) {
            console.log("Ocurrio un error al cambiar el correo.");
        },
        type: "post",
        async: true,
        url: "/colaborador/correo",
        data: {
            "cor": $(e).val(),
            "id": $("#receptor-nombre").val()
        },
        cache: false,
        dataType: "html"
    });
}

function activar_btn_cambiar(){
	let letras = $('#colaborador-cedula').val().length
	$("#btn_cambiar_cedula").removeClass('btn-info')
	if(letras < 9){
		$("#btn_cambiar_cedula").addClass('btn-info')
		$("#btn_cambiar_cedula").attr('disabled',true)
		return false;
	}
	else{
		$("#btn_cambiar_cedula").addClass('btn-success')
		$("#btn_cambiar_cedula").attr('disabled',false)
	}
}
function colaborador_cedula_nuevo(tipo) {
    if(tipo=='cedula')
        $("#receptor-nombre").val('')
    else
        $("#colaborador-cedula").val('')
	
	$("#btn_cambiar_cedula").addClass('btn-info')
	$("#btn_cambiar_cedula").attr('disabled',true)
	let formulario = $('#section_receptor').html()

    $.ajax({
		beforeSend : function(){
			$('#section_receptor').html(img_esperando);
		},	
        success: function (data_s) {
            data = jQuery.parseJSON(data_s);            
            if (data.model != undefined) {
				$('#section_receptor').html(data.html)
				          
            }else{
				alert('Esta cédula no es correcta.')
                $("#section_receptor").html(formulario);
            }
        },
        error: function (data) {
            console.log("Ocurrio un error, al intentar cargar por cédula.");
        },
        type: "post",
        async: true,
        url: "/colaborador/cedula",
        data: {
            "ced": $("#colaborador-cedula").val(),
            "id": $("#receptor-nombre").val(),
            "fac": id_factura_actual
        },
        cache: false,
        dataType: "html"
    });
}

function colaborador_cedula() {
    $.ajax({
        success: function (data_s) {
            data = jQuery.parseJSON(data_s);
            if (data.id !== 0) {
                $("#col_resultado").val(data.nombre + ' ' + data.apellido + ' ' + data.sapellido);
                $("#col_resultado").attr("id_cliente", data.id);
            } else {
                $("#col_resultado").val('No existe...');
                $("#col_resultado").attr("id_cliente", 0);
            }
        },
        error: function (data) {
            console.log("Ocurrio un error, al intentar cargar por cédula.");
        },
        type: "post",
        async: true,
        url: "/colaborador/cedula",
        data: {
            "ced": $("#col_cedula").val(),
            "nom": $("#col_nombre").val(),
            "pap": $("#col_papell").val(),
            "sap": $("#col_sapell").val(),
        },
        cache: false,
        dataType: "html"
    });
}

function colaborador_aceptar() {           
        $.ajax({
            beforeSend: function(data_b){
                $("#tr_titulo_resultado_colaborador").addClass('hidden');
                $("#tr_cuerpo_resultado_colaborador").addClass('hidden');
            },
            success: function (data_s) {
                data_c = jQuery.parseJSON(data_s);
                console.log(data_c.msn);
                $("#col_cedula").val("");
                $("#col_nombre").val("");
                $("#col_papell").val("");
                $("#col_sapell").val("");
                $("#col_resultado").val("");
            },
            error: function (data) {
                console.log("Ocurrio un error, al intentar cargar por cédula.");
            },
            type: "post",
            async: true,
            url: "/factura/colaborador",
            data: {
                "id": id_factura_actual,
                "id_col": data.id,
                "accion": 'agregar',
            },
            cache: false,
            dataType: "html"
        });    
}

function colaborador_quitar(id) {
    var tr_col = $("#tr_colaborador_" + id).html();
    $("#tbl_cola_buscar").toggleClass("hidden");
    $.ajax({
        beforeSend: function () {
            $("#tr_colaborador_" + id).html(img_esperando);
        },
        success: function (data_s) {
            data_c = jQuery.parseJSON(data_s);
            if (data_c.msn == "quitar") {
                
                $("#tr_colaborador_" + id).remove();
            } else {
                console.log(data_c.msn);
            }
        },
        error: function (data) {
            $("#tr_colaborador_" + id).html(tr_col);
            console.log("Ocurrio un error, al intentar quitar este colaborador.");
        },
        type: "post",
        async: true,
        url: "/factura/colaborador",
        data: {
            "id": id_factura_actual,
            "id_col": id,
            "accion": 'quitar',
        },
        cache: false,
        dataType: "html"
    });
}

function colaborador_validar_formulario() {
    return true;
}


function colaborador_agregar() {
    if (colaborador_validar_formulario()) {
        $.ajax({
            success: function (data_s) {
                data = data_s;
                if (data_s.id) {
                    $("#col_resultado").val(data_s.nombre + ' ' + data_s.apellido + ' ' + data_s.sapellido);
                    $("#col_resultado").attr("id_cliente", data_s.id);
                    colaborador_aceptar();
                    $("#btn_cerrar_modal").click();
                } else {
                    var errores = "";
                    if (data_s.nombre) {
                        errores += "<br>" + data_s.nombre;
                    }
                    if (data_s.apellido) {
                        errores += "<br>" + data_s.apellido;
                    }
                    if (data_s.sapellido) {
                        errores += "<br>" + data_s.sapellido;
                    }
                    if (data_s.cedula) {
                        errores += "<br>" + data_s.cedula;
                    }
                    if (data_s.tipo) {
                        errores += "<br>" + data_s.tipo;
                    }
                    if (data_s.nacimiento) {
                        errores += "<br>" + data_s.nacimiento;
                    }
                    $("#form_new_col_err").html(errores);
                }
            },
            error: function (data) {
                alert('Error al crear el cliente');
            },
            type: 'post',
            url: "/colaborador/createmodal",
            data: {
                "Colaborador[nombre]": $("#colaborador-nombre").val(),
                "Colaborador[apellido]": $("#colaborador-apellido").val(),
                "Colaborador[sapellido]": $("#colaborador-sapellido").val(),
                "Colaborador[cedula]": $("#colaborador-cedula").val(),
                "Colaborador[tipo]": $("#colaborador-tipo").val(),
                "Colaborador[nacimiento]": $("#colaborador-nacimiento").val(),
                "Colaborador[telefono]": $("#colaborador-telefono").val(),
                "Colaborador[correo]": $("#colaborador-correo").val(),
            },
        });



    }
}
 
	function cambiarPersona(e){
		$('#colaborador-nombre').addClass('hidden')
		$('#colaborador-nombrecompleto').addClass('hidden')
		$('.field-colaborador-apellido').addClass('hidden')
		$('.field-colaborador-sapellido ').addClass('hidden')
		if($(e).val() == 1){
			$('#colaborador-nombre').removeClass('hidden')
			$('.field-colaborador-apellido').removeClass('hidden')
			$('.field-colaborador-sapellido').removeClass('hidden')		
		}
		if($(e).val() == 2){
			$('#colaborador-nombrecompleto').removeClass('hidden')	
			
		}
	}
	
	function mostrarCodPro(e){
		$('.field-colaborador-cod_provedor').addClass('hidden')
		if($(e).val() == 4){
			$('.field-colaborador-cod_provedor').removeClass('hidden')		
		}
	}
	
	function consultar_cedula(e){
		$.get("https://apis.gometa.org/cedulas/" + $("#colaborador-cedula").val() , function(data, status){
			if(status == 'success'){
				var resultado = data.results[0]				
				var tipoIde = '1'; //Fisica
				if(resultado.type == 'J'){
					var fullname = resultado.fullname;
					$("#col_nombre_label").html(fullname)
					$("#col_nombre_label").removeClass('hidden')
					var array = fullname.split(" ");
					$("#colaborador-nombre").val(array[0])
					$("#colaborador-apellido").val(array[1])
					$("#colaborador-sapellido").val(array[2])
					tipoIde = '2';
				}else{
					$("#col_nombre_label").addClass('hidden')
					$("#colaborador-nombre").val(resultado.firstname1)
					$("#colaborador-apellido").val(resultado.lastname1)
					$("#colaborador-sapellido").val(resultado.lastname2)					
				}
				$("#colaborador-tipoidentificacion").val(tipoIde)
				$("#colaborador-tipo").val(1)
			}
		});
	}
	
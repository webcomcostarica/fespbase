<?php

use backend\models\Caja;
use backend\models\Factura;
use backend\models\search\FacturaSearch;
use common\components\Moneda;


                
   
    $this->title = 'Resumen diario';    
    $this->params['breadcrumbs'][] = ['label' => 'Reportes', 'url' => ['/reporte/index']];
$this->params['breadcrumbs'][] = $this->title;
   
        
     ?>
  
 
    <div class="row">
        <div class="col-lg-12">

            <h1><?php echo $this->title; ?></h1>

        </div>
    </div>
    
    <div class="row">
        <?= $this->render('_desde'); ?> 
    </div>
       
        <?php
            $hoy = date('Y-m-d',strtotime('now'));
            
            if(isset($_POST['Factura']['desde'])){
                $hoy = $_POST['Factura']['desde'];
            }
               
            
              /*
               
                $caja = Caja::find()->where(['like','fecha',$hoy])->
                andWhere(['tipo'=>'Abrir'])->one();
                $cajas = [1=>['A'=>0,'C'=>0]];

                if(!empty($caja))
                    $cajas[1]['A'] = $caja->total;
                

                $caja = Caja::find()->where(['like','fecha',$hoy])->
                andWhere(['=','tipo','Cerrar'])->one();

                if(!empty($caja))
                    $cajas[1]['C'] = $caja->total;
*/

                $facturasDeHoy =Factura::find()
                ->where(['like','fecha',$hoy])->
                andWhere(['estado'=>'Facturada'])->all();
                
                /*
                    '01'=>'Efectivo',
                    '02'=>'Tarjeta',
                    '03'=>'Cheque',
                    '04'=>'Transferencia - depósito bancario',
                */

                $tots = [
                    'sg'=>0,
                    'mg'=>0,
                    'tg'=>0,
                    'se'=>0,
                    'me'=>0,
                    'te'=>0,
                    'de'=>0,
                    'im'=>0,
                    'to'=>0
                ];

                $totales = [
                    'USD'=>[
                        '01'=>$tots,
                        '02'=>$tots,
                        '03'=>$tots,
                        '04'=>$tots,
                        'sg'=>0,
                        'mg'=>0,
                        'tg'=>0,
                        'se'=>0,
                        'me'=>0,
                        'te'=>0,
                        'de'=>0,
                        'im'=>0,
                        'to'=>0
                    ],
                    'CRC'=>[
                        '01'=>$tots,
                        '02'=>$tots,
                        '03'=>$tots,
                        '04'=>$tots,
                        'sg'=>0,
                        'mg'=>0,
                        'tg'=>0,
                        'se'=>0,
                        'me'=>0,
                        'te'=>0,
                        'de'=>0,
                        'im'=>0,
                        'to'=>0
                    ]
                ];
                $total = 0;
                foreach($facturasDeHoy as $fact){
                    $totales[$fact->dgt_moneda][$fact->dgt_medioPago]['to'] += $fact->dgt_total	;

                    $totales[$fact->dgt_moneda]['sg'] += $fact->dgt_serviciosGravados;
                    $totales[$fact->dgt_moneda]['mg'] += $fact->dgt_mercaGravada;
                    $totales[$fact->dgt_moneda]['tg'] += $fact->dgt_gravado;

                    $totales[$fact->dgt_moneda]['se'] += $fact->dgt_serviciosExentos;
                    $totales[$fact->dgt_moneda]['me'] += $fact->dgt_mercaExenta;
                    $totales[$fact->dgt_moneda]['te'] += $fact->dgt_totExento;

                    $totales[$fact->dgt_moneda]['de'] += $fact->dgt_totDescuento;
                    $totales[$fact->dgt_moneda]['im'] += $fact->dgt_totImpuesto;
                    $total += $fact->dgt_total;
                }
                ?>
     
     
    <div class="row">
        <div class="col-lg-5" id="resumen_dia">
            <table class="table table-results">
                <tbody>
                <thead>
                    <tr>
                        <th class="">Detalle</th>
                        <th class="">Monto</th>
                    </tr>
                </thead>
                
                    <tr>
                        <td class="">Cantidad de ventas</td>                      
                        <td><?= count($facturasDeHoy) ?></td>
                    </tr> 


                  
                    <?= $this->render('_hoy_tr_resumen',[
                        'titulo'=>'Efectivo',
                        'monto'=>$totales['CRC']['01']['to']
                    ]) ?>

                    <?= $this->render('_hoy_tr_resumen',[
                        'titulo'=>'Tarjeta',
                        'monto'=>$totales['CRC']['02']['to']
                    ]) ?>

                    <?= $this->render('_hoy_tr_resumen',[
                        'titulo'=>'Cheques',
                        'monto'=>$totales['CRC']['03']['to']
                    ]) ?>

                    <?= $this->render('_hoy_tr_resumen',[
                        'titulo'=>'Transferencias',
                        'monto'=>$totales['CRC']['04']['to']
                    ]) ?>

                </tbody>
                <tfoot>
                    <?= $this->render('_hoy_tr_resumen',[
                        'titulo'=>'Total Gravado',
                        'monto'=>$totales['CRC']['tg']
                    ]) ?>

                    <?= $this->render('_hoy_tr_resumen',[
                        'titulo'=>'Total Exento',
                        'monto'=>$totales['CRC']['te']
                    ]) ?>

                    <?= $this->render('_hoy_tr_resumen',[
                        'titulo'=>'Total Descuentos',
                        'monto'=>$totales['CRC']['de']
                    ]) ?>

                    <?= $this->render('_hoy_tr_resumen',[
                        'titulo'=>'Total Impuestos',
                        'monto'=>$totales['CRC']['im']
                    ]) ?>

                    <?= $this->render('_hoy_tr_resumen',[
                        'titulo'=>'Total ventas',
                        'monto'=>$total
                    ]) ?>
                        
                
                </tfoot>
            </table>
                
        </div>
    </div>
            <a 
            class="btn btn-warning" 
            href="#" onclick="factura_imprimir('resumen_dia')">Imprimir resumen</a>

        <script>
            function factura_imprimir(id) {
                var ventimp = window.open(' ', 'popimpr');
                ventimp.document.write($("#"+id).html());
                ventimp.document.close();
                ventimp.print();
                ventimp.close();
            }
        </script>
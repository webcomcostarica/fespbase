<?php

    use kartik\grid\GridView;
    use common\components\Botones;
    use common\components\Moneda;
    use backend\models\CodigosComprobantes;
    

 echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showPageSummary' => true,
        'layout' => '
            <div class="row">
                <div class="col-xs-12">
                    <div class="text-right">
                        {export}{toggleData}
                    </div>
                </div>
            </div>
            {items}{pager}',
            'exportConfig' => [
                GridView::PDF => [],
                GridView::EXCEL => [],
            ],
        'columns' => [
                   //['class' => 'yii\grid\SerialColumn'],
                   [
                        'attribute' => 'concecutivo',
                        'contentOptions' => ['style' => 'width: 8%;text-align:left'],
                        'value'=>function($model){
                            return $model->comprobante->numConce;
                        }
                    ], /*
                    [
                        'label'=>'ID File',
                        'contentOptions' => ['style' => 'width: 8%;text-align:left'],
                        'value'=>function($model){
                            return $model->comprobante->id;
                        }
                    ],
                     [
                        'label'=>'ID Comprobante',
                        'contentOptions' => ['style' => 'width: 8%;text-align:left'],
                        'value'=>function($model){
                            return $model->comprobante->idComprobante;
                        }
                    ],
                    [
                        'attribute' => 'Estado',
                        'contentOptions' => ['style' => 'width: 8%;text-align:left'],
                        'value'=>function($model){
                            return $model->comprobante->estadoEstado;
                        }
                    ],*/
                    [
                        'attribute' => 'fecha',
                        'contentOptions' => ['style' => 'width: 8%;text-align:left'],
                    ],
                    [
                        'label' => 'Receptor',
                        'contentOptions' => ['style' => 'width: 16%;text-align:left'],
                        'attribute' => 'colaborador_nombre',
                        'format' => 'raw',
                        'value' => function ($model, $key, $index) {
                            return $model->receptores;
                        },                      
                    ],
                    [
                        'attribute'=>'dgt_medioPago',
                        'label'=>'Medio',
                        'contentOptions' => ['style' => 'width: 5%;text-align:left'],
                        'value'=>function($model){
                            return CodigosComprobantes::MEDIOSDEPAGO[$model->dgt_medioPago];   
                        }                        
                    ],
                    [
                    'attribute'=>'dgt_gravado',
                    'label'=>'Gravado',
                    'contentOptions' => ['style' => 'width: 5%;text-align:left'],
                    'pageSummary' => true,
                    'value' => function($model){
                            return Moneda::getreporte($model->dgt_gravado);
                        }
                    ],
                    [
                    'attribute'=>'dgt_totExento',
                    'label'=>'Exento',
                    'contentOptions' => ['style' => 'width: 5%;text-align:left'],
                    'pageSummary' => true,
                    'value' => function($model){
                            return Moneda::getreporte($model->dgt_totExento);
                        }
                    ],
                    [
                    'attribute'=>'dgt_totDescuento',
                    'label'=>'Descuentos',
                    'contentOptions' => ['style' => 'width: 5%;text-align:left'],
                    'pageSummary' => true,
                    'value' => function($model){
                            return Moneda::getreporte($model->dgt_totDescuento);
                        }
                    ],
                    
                    [
                    'attribute'=>'dgt_totImpuesto',
                    'label'=>'Impuestos',
                    'contentOptions' => ['style' => 'width: 5%;text-align:left'],
                    'pageSummary' => true,
                    'value' => function($model){
                            return Moneda::getreporte($model->dgt_totImpuesto);
                        }
                    ],
                    [
                    'attribute'=>'dgt_total',
                    'pageSummary' => true,
                    'value' => function($model){
                            return Moneda::getreporte($model->dgt_total);
                        }
                    ],              
                    [
                        'label' => 'Consultar',
                        'format' => 'raw',
                        'value' => function($model) {
                            return Botones::Consultar('/factura/comprobante',"id={$model->id}",'Consultar');
                        },
                    ],
                ],
            ]);
            ?>
<?php
   
    use backend\models\Caja;
    use backend\models\Factura;
    use backend\models\search\FacturaSearch;
    use common\components\Moneda;
                  
    $model = new Factura();
    $this->title = 'Todas las ventas del mes';
    $model->desde = date('Y-m-d', strtotime('now'));
    $model->hasta = date('Y-m-d', strtotime('now'));
    $searchModel = new FacturaSearch();
    $dataProvider = $searchModel->diarioDeVentasTipo('mes');
    
    $this->params['breadcrumbs'][] = ['label' => 'Reportes', 'url' => ['/reporte/index']];
$this->params['breadcrumbs'][] = $this->title;
   
        
     ?>
  
 
        <div class="col-lg-12">

        <h1><?php echo $this->title; ?></h1>
        <p>
            Reporte de todas las ventas realizadas en el mes, cada linea es una venta efectuada.<br>
            Puedes exportar utilizando el componente superior derecho de la tabla.
        </p>
        </div>
    <?= $this->render('_desdehasta') ?>
         <div class="col-lg-12">
           <?= $this->render('_lista',[
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
            ]) ?>
        
        </div>

                   
        
           

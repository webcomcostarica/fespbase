<?php
   
    use backend\models\Caja;
    use backend\models\Factura;
    use backend\models\search\FacturaSearch;
    use common\components\Moneda;
    use kartik\grid\GridView;
    use common\components\Botones;
    use backend\models\CodigosComprobantes;
    
                  
    $this->title = 'Reporte de gastos deducibles';     
    $this->params['breadcrumbs'][] = ['label' => 'Reportes', 'url' => ['/reporte/index']];
    $this->params['breadcrumbs'][] = $this->title;
   
        
     ?>
  
 
        <div class="col-lg-12">

        <h1><?php echo $this->title; ?></h1>
        <p>
            Reporte de todas los gastos realizados entre las fechas seleccionadas.
        </p>
        </div>
    <?= $this->render('_desdehasta') ?>
         <div class="col-lg-12">
         
         <?php
         
          echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,       
        'showPageSummary' => true,
        'layout' => '
            <div class="row">
                <div class="col-xs-12">
                    <div class="text-right">
                        {export}{toggleData}
                    </div>
                </div>
            </div>
            {items}{pager}',
            'exportConfig' => [
                GridView::PDF => [],
                GridView::EXCEL => [],
            ],
        'columns' => [
           // ['class' => 'yii\grid\SerialColumn'],

         //   'id',
            'numeroConcecutivo:ntext',
          //  'tipoComprobante:ntext',
            'clave:ntext',
          //  'numeroCedulaEmisor:ntext',
            'fechaEmisionDoc',
            // 'mensaje',
             'detalleMensaje',
              [
                'label' => 'FE Envio',
                'contentOptions' => function($model) {
                    $clases = [
                        100 => 'info',
                        200 => 'success',
                        300 =>'warning', 
                        400 => 'danger',
                        500  => 'danger', 
                        0  => 'warning', 
                    ];
                    $id = $model->getClase();
                    
                    return [
                        'class' => $clases[$id],
                        'code' => $id
                        ];
                },
                    'format' => 'raw',
                    'value' => function($model) {
                            return CodigosComprobantes::ESTADOENTREGA[$model->getClase()];
                        },
                    ],
                /*
                 [
                        'label' => 'FE Estado',
                        'format' => 'raw',
                        'contentOptions' => function($model) {
                            $clases = [
                            0 => 'warning', 
                            1 => 'success', 
                            2 => 'danger', 
                            3 => 'danger', 
                            800 => 'danger',
                            100 => 'info',
                            200 => 'success',
                            300 =>'warning', 
                            400 => 'danger',
                            500  => 'danger',
                            null => 'warning'
                            ];
                            $id = $model->getClaseEstado();
                            return [
                                'class' => $clases[$id],
                                'code' => $id
                            ];
                        },
                        'value' => function($model) {                            
                            return CodigosComprobantes::ESTADOVALIDACION[$model->getClaseEstado()];
                        },
                    ],
                    */
                    [
                        'pageSummary' => true,
                        'attribute'=>'totalFactura',
                        'label'=>'Total',
                    ],
        ],
            ]);
            
         
        
        
         ?>
        
        </div>

                   
        
           

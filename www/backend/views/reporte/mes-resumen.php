<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use common\components\Botones;
use common\components\Moneda;
use backend\models\CodigosComprobantes;



$this->title = 'Ventas mensuales x dia';
$this->params['breadcrumbs'][] = ['label' => 'Reportes', 'url' => ['/reporte/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ventas-diarias-index">

<h1><?= Html::encode($this->title) ?></h1>
     
 <?= $this->render('_desdehasta') ?>
            
<?php
 echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showPageSummary' => true,
        'layout' => '
            <div class="row">
                <div class="col-xs-12">
                    <div class="text-right">
                        {export}{toggleData}
                    </div>
                </div>
            </div>
            {items}{pager}',
            'exportConfig' => [
                GridView::PDF => [],
                GridView::EXCEL => [],
            ],
        'columns' => [
           // ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            'fecha:ntext',
           // 'condicion',
           // 'medioPago',
           // 'tipoCambio',
            //'totGravado',
            
            [
                'label' => 'Exento',
                'format' => 'raw',
                'value' => function($model) {
                     return $model->moneda_reporte($model->totExento);
                },
                'pageSummary' => true,
               
            ], 
            [
                'label' => 'Gravado',
                'format' => 'raw',
                'value' => function($model) {
                     return $model->moneda_reporte($model->totGravado);
                },
                'pageSummary' => true,
               
            ],
            [
                'label' => 'Descuento',
                'format' => 'raw',
                'value' => function($model) {
                     return $model->moneda_reporte($model->totDescuento);
                },
               'pageSummary' => true,
            ],            
            [
                'label' => 'Impuesto',
                'format' => 'raw',
                'value' => function($model) {
                     return $model->moneda_reporte($model->totImpuesto);
                },
               'pageSummary' => true,
            ],
            [
                'label' => 'Total',
                'format' => 'raw',
                'value' => function($model) {
                     return $model->moneda_reporte($model->total);
                },
               'pageSummary' => true,
            ]
            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

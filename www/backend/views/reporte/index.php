<?php
	
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
    use dosamigos\datepicker\DatePicker;
		
    $model = new backend\models\Factura();
        
    $this->title = "Reportes";
    $this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['/reporte-index']];
    $this->params['breadcrumbs'][] = ['label' => 'Inicio', 'url' => ['/']];
    $tam_image=30;
    
    $tab = (isset($_GET['tab']))? $_GET['tab'] : 'res';
?>

<div class="row"> 
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <h1><?php echo $this->title; ?></h1>
   </div>
        <div style="text-align: center;" class="col-xs-6 col-sm-4 col-md-4" id="acc_factura">
            <div class="panel panel-default">
              <div class="panel-heading">Diario</div>
              <div class="panel-body">
                 <p><?php echo Html::img('/img/servicios/excel.png', ['height' => '' . $tam_image]) ?></p>             
                    <?= Html::a(
                        Yii::t('app', 'Detallado'), 
                        ['diario-detalle?_toga654c069=all'],
                        [
                            'class' => 'btn btn-success',
                            'title' => 'Cada linea del reporte es una venta.'
                        ]
                    ) ?>
                    
                    <?= Html::a(
                        Yii::t('app', 'Resumido'), 
                        ['diario-resumen'],
                        [
                            'class' => 'btn btn-info',
                            'title' => 'Tabla con resumen del dia.'
                        ]
                    ) ?>
                    
              </div>
            </div>
    
            
            
        </div>
        
        <div style="text-align: center;" class="col-xs-6 col-sm-4 col-md-4" id="acc_factura">
            <div class="panel panel-default">
              <div class="panel-heading">Mensual</div>
              <div class="panel-body">
               <p><?php echo Html::img('/img/servicios/excel.png', ['height' => '' . $tam_image]) ?></p>             
                 <?= Html::a(
                        Yii::t('app', 'Detallado'), 
                        ['mes-detalle?_toga654c069=all'],
                        [
                            'class' => 'btn btn-success',
                            'title' => 'Cada linea del reporte es una venta.'
                        ]
                    ) ?>
                    
                    <?= Html::a(
                        Yii::t('app', 'Resumido'), 
                        ['mes-resumen?_toga654c069=all'],
                        [
                            'class' => 'btn btn-info',
                            'title' => 'Cada linea es el total del dia.'
                        ]
                    ) ?>
             </div>
            </div>
        </div>
        <!--
          <div style="text-align: center;" class="col-xs-6 col-sm-4 col-md-4" id="acc_factura">
            <div class="panel panel-default">
              <div class="panel-heading">Anual</div>
              <div class="panel-body">
                <p><?php echo Html::img('/img/servicios/excel.png', ['height' => '' . $tam_image]) ?></p>             
                 <?= Html::a(
                        Yii::t('app', 'Detallado'), 
                        ['anual-detalle'],
                        [
                            'class' => 'btn btn-success',
                            'title' => 'Cada linea del reporte es un mes.'
                        ]
                    ) ?>
                    <!--
                    <?= Html::a(
                        Yii::t('app', 'Siguiente'), 
                        ['anual-detalle'],
                        [
                            'class' => 'btn btn-info',
                            'title' => 'Tabla con resumen del dia.'
                        ]
                    ) ?>
                    <p class="info">Seleccione un desde hasta resumido por dia, mes y año.</p>
              -- ></div>
            </div>
        </div>-->
        
    </div>
    

 <?php
    use backend\models\Factura;
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
    use dosamigos\datepicker\DatePicker;


    $form = ActiveForm::begin();
        
        $model = new backend\models\Factura();
        if(isset($_POST['Factura'])){            
            if(isset($_POST['Factura']['desde'])){
                $model->desde = $_POST['Factura']['desde'];                
            }else{
                $model->desde = date('Y-m-d', strtotime('now'));
            } 
         }
        ?>
        <div class="col-lg-2">
        <?php
            echo Html::label('Fecha');
            echo DatePicker::widget([
                'model' => $model,
                'attribute' => 'desde',
                'language' => 'es',
                'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd',
                ]
            ]); ?>
        </div> 
              

        <div class="col-lg-2">
        <?php
            echo Html::label('Accion');
        ?>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Enviar'), ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
                
    <?php ActiveForm::end(); ?>

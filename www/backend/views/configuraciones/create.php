<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\configuraciones */

$this->title = Yii::t('backend', 'Create Configuraciones');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Configuraciones'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="configuraciones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

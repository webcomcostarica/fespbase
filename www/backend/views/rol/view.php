<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Rol */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend','Rols'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rol-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('backend','Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend','Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend','Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nombre',
        ],
    ]) ?>
    
    <h2><?= Yii::t('backend','Allowed operations') ?></h2>
 
    <ol>
        <?php foreach ($model->operacionesPermitidasList as $operacionPermitida) { ?>
        <li><?= $operacionPermitida['nombre']?> </li>
         <?php }?>
    </ol>

</div>

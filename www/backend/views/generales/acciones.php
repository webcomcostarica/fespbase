<?php

use yii\helpers\Html;
?>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">    
    <div class="panel panel-primary">
      <div class="panel-heading">Acciones</div>
      <div class="panel-body">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
            <h3>Cajas</h3>
            <ul>
                <li>
            <?= Html::a('<span class="glyphicon glyphicon-open"></span> Abrir Caja',
                '/caja/abrir'
                //['class'=>'btn btn-success']
                ); ?>
                </li>
                <li>

            <?= Html::a('<span class="glyphicon glyphicon-save"></span> Cerrar Caja',
                '/caja/cerrar'
                //['class'=>'btn btn-success']
                ); ?>
                </li>
        </ul>

           
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
            <h3>Facturación</h3>
           <ul>
               <li>
            <?= Html::a('<span class="glyphicon glyphicon-th-list"></span> Lista de facturas',
                '/factura/index'
                //['class'=>'btn btn-success']
                ); ?>
                </li>

                <li>
                    <?= Html::a('<span class="glyphicon glyphicon-file"></span> Nueva Factura',
                        '/factura/create'
                        //['class'=>'btn btn-success']
                    ); ?>
                </li>
           
        </div>
         <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
            <h3>Otros</h3>
            <ul>
                <li>
                    <?= Html::a('<span class="glyphicon glyphicon-file"></span> Clientes','/factura/create'); ?>
                </li>
                <li>
                    <?= Html::a('<span class="glyphicon glyphicon-file"></span> Inventario','/factura/create'); ?>
                </li>
                <li>
                    <?= Html::a('<span class="glyphicon glyphicon-file"></span> Reportes','/factura/create'); ?>
                </li>
                <li>
                    <?= Html::a('<span class="glyphicon glyphicon-file"></span> Confirmar gastos','/factura/create'); ?>
                </li>
            </ul>
         </div>
            
        </div>
  
      </div>
</div>
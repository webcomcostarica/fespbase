<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Horas */

$this->title = Yii::t('app', 'Modificar {modelClass} ', [
    'modelClass' => 'Horas',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Horas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="horas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

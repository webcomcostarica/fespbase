<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\HorasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Horas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="horas-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Agregar Horas'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
	$acciones = '{view} {update} {delete}';
	$horas = [];
	for($i=1;$i<24;$i++){
		$horas[$i] = "$i:00";
		if($i < 12){
			$horas[$i] = "$i:00 am";		
			$horas[($i+0.5)] = "$i:30 am";		
		}
		if($i == 12){
			$horas[$i] = "$i:00 md";		
			$horas[($i+0.5)] = "$i:30 pm";		
		}
		if($i > 12){
			$hora = $i-12;
			$horas[$i] = "$hora:00 pm";		
			$horas[($i+0.5)] = "$hora:30 pm";		
		}
	}
	
	
	echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
          //  ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            'dia',
           // 'usuario',
		   [
                'label'=>'Empleado',		   
                'attribute' => 'usuario',                
                'filter' => [
                    1 => 'cmena',
                    2 => 'otro',
                ]
            ],
			 [
                'label'=>'Desde',		   
                'attribute' => 'desde',                
                'filter' => $horas
            ],
			 [
                'label'=>'Hasta',		   
                'attribute' => 'hasta',                
                'filter' => $horas
            ],
			[
                'label'=>'Estado',		   
                'attribute' => 'estado',                
                'filter' => [0=>'Pendientes',1=>'Aprobadas',2=>'Pagadas']				
			],
            'cantidad',
            'costo',
            'total',

            [
			'class' => 'yii\grid\ActionColumn',
			'template'=>$acciones
			],
        ],
    ]); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Horas */
/* @var $form yii\widgets\ActiveForm */

use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DatePicker;
use common\models\User;

?>

<div class="row">
	<div class="col-lg-4">
			<?php $form = ActiveForm::begin(); ?>

			<?php
				echo Html::label('Dia laborado');
				echo DatePicker::widget([
					'model' => $model,
					'attribute' => 'dia',
					'language' => 'es', 
					'clientOptions' => [
						'autoclose' => true,
						'format' => 'yyyy-mm-dd'
					]
				]);
				?><br>
			<?php         
			$usuarios = User::find()->where(['status'=>'1'])->all();   

			echo $form->field($model, 'usuario')
			->dropDownList(			
				ArrayHelper::map($usuarios,'id','username'),           
				['prompt'=>'Usuario']				
			); 
			
			$horas = [];
			for($i=1;$i<24;$i++){
				$horas[$i] = "$i:00";
				if($i < 12){
					$horas["$i"] = "$i:00 am";		
					$horas["".($i+0.5)] = "$i:30 am";		
				}
				if($i == 12){
					$horas[$i] = "$i:00 md";		
					$horas["".($i+0.5)] = "$i:30 pm";		
				}
				if($i > 12){
					$hora = $i-12;
					$horas[$i] = "$hora:00 pm";		
					$horas["".($i+0.5)] = "$hora:30 pm";		
				}
			}
			$horas[24] = "12:00 mn";
				
			echo $form->field($model, 'desde')
			->dropDownList(			
				$horas,           
				['prompt'=>'Desde']				
			);

			echo $form->field($model, 'hasta')
			->dropDownList(			
				$horas,           
				['prompt'=>'Desde']				
			); 
			?>

	</div>
		
		<div class="col-lg-4">
			<?php
			echo $form->field($model, 'estado')
			->dropDownList(			
				[0=>'Pendientes',1=>'Aprobadas',2=>'Pagadas'],           
				['prompt'=>'Estado']				
			);
			?>

			<?= $form->field($model, 'costo')->textInput() ?>

			
			<?php //$form->field($model, 'total')->textInput() ?>

			<div class="form-group">
				<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Agregar') : Yii::t('app', 'Modificar'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
			</div>

			<?php ActiveForm::end(); ?>

		</div>
		<div>
		<?php
			if(!empty($model->errors))
				print_r($model->errors);
		?>
		<div>
</div>

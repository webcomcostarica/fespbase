<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Horas */

$this->title = 'Modificar horas';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Horas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
	<div class="col-lg-4">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Actualizar'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Borrar'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'dia',
            'usuario',
            'desde',
            'hasta',
           [
                'label'=>'Estado',		   
                'attribute' => 'estado',
				/*'value'=>function($data){
					if($data->estado == 0)
						echo 'Pendientes';
					if($data->estado == 1)
						return 'Aprobadas';
					if($data->estado == 2)
						return 'Pagadas';					
				},
				'format'=>'raw'*/
				
			],
            'cantidad',
            'costo',
            'total',
        ],
    ]) ?>

</div>
</div>

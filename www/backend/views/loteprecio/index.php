<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Precios');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lote-precio-index">

    

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

           // 'id',
            'estado',
            'fecha',
            'usuario0.username',
           // 'lote',
           // 'descripcion:ntext',
            'costo',

            ['class' => 'yii\grid\ActionColumn',
             'template'=>'{update}'],
        ],
    ]); ?>

<!--
    <p>
        <?= Html::a(Yii::t('backend', 'Create').' precio', ['create','idlote'=>$lote_id], ['class' => 'btn btn-success']) ?>
    </p>-->
</div>

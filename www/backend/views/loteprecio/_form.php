<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\LotePrecio */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
    
    <?php $form = ActiveForm::begin(); ?>
    <div class="col-lg-6">
    <?php // $form->field($model, 'vigente')->textInput() ?>
    
    <?= $form->field($model, 'vigente')->dropDownList(
                [0=>'Inactivo',1=>'Vigente'],           
                ['prompt'=>'Seleccione por favor']    
        ); ?>
    
    <?= $form->field($model, 'costo')->textInput() ?>

    </div>
    <div class="col-lg-6">


    <?php //$form->field($model, 'id')->textInput() ?>

    <?php // $form->field($model, 'fecha')->textInput() ?>

    <?php // $form->field($model, 'usuario')->textInput() ?>

    <?php // $form->field($model, 'lote')->textInput() ?>

    <?= $form->field($model, 'descripcion')->textarea(['rows' => 4]) ?>
    </div>
</div>    
    <div class="form-group">
        <p>Nota: Un precio VIGENTE, desactiva los demas y el actual rige para el paquete</p>
        <?php 
        echo Html::submitButton(
                $model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), 
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
                                    ]
                ); 
                ?>
    </div>
    

    <?php ActiveForm::end(); ?>



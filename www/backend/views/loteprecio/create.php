<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\LotePrecio */

$this->title = Yii::t('backend', 'Crear precio para lote');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Todas las pacas'), 'url' => ['package/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Paca').' '.$model->lote0->paca0->id, 'url' => ['/lote/index','id'=>$model->lote0->paca0->id]];
//$this->params['breadcrumbs'][] = ['label' => 'Lote '.$model->lote, 'url' => ['view', 'id' => $model->lote]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Lote').' '.$model->lote, 'url' => ['/lote/update','id'=>$model->lote]];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="lote-precio-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

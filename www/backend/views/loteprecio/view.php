<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\LotePrecio */

$this->title = Yii::t('backend', 'Ver').' costo '.$model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Todas las pacas'), 'url' => ['package/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Paca').' '.$model->lote0->paca0->id, 'url' => ['/lote/index','id'=>$model->lote0->paca0->id]];
//$this->params['breadcrumbs'][] = ['label' => 'Lote '.$model->lote, 'url' => ['view', 'id' => $model->lote]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Lote').' '.$model->lote, 'url' => ['/lote/update','id'=>$model->lote]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lote-precio-view">


    <p>
        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           // 'id',
            'fecha',
            'usuario0.username',
            'lote',
            'descripcion:ntext',
            'estado',
            'costo',
            'piezas',
        ],
    ]) ?>

</div>

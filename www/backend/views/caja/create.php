<?php

use yii\helpers\Html;
use backend\models\Package;
use common\components\Moneda;
use backend\models\PackagePi;


/* @var $this yii\web\View */
/* @var $model backend\models\Caja */

$this->title = $titulo.' Caja';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Reportes'), 'url' => ['/reporte/diario-de-ventas']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cajas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="caja-create">

    <h1><?= Html::encode($titulo). ' Caja' ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'config'=>$config
    ]) ?>
	
	
	

</div>

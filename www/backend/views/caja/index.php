<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CajaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Cajas');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Reportes'), 'url' => ['/reporte/diario-de-ventas']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
    <h1><?= Html::encode($this->title) ?></h1>
      
   
  
    </div> 
    <br>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">    
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                //['class' => 'yii\grid\SerialColumn'],

            //  'id',
            // 'cincuentamil',
            // 'veitemil',
            // 'diesmil',
            // 'cincomil',
                // 'dosmil',
                // 'mil',
                // 'quinientos',
                // 'cien',
                // 'cincuenta',
                // 'vienticinco',
                // 'dies',
                // 'cinco',
                'fecha',
                'total',
                'tipo',
                // 'usuario',

                ['class' => 'yii\grid\ActionColumn',
                'template'=>' {update} {view} '
                ],
            ],
        ]); ?>

    </div>

</div>

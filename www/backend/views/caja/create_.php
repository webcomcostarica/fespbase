<?php

use yii\helpers\Html;
use backend\models\Package;
use common\components\Moneda;
use backend\models\PackagePi;


/* @var $this yii\web\View */
/* @var $model backend\models\Caja */

$this->title = $titulo.' Caja';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cajas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="caja-create">

    <h1><?= Html::encode($titulo). ' Caja' ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
	
	
	<div class="col-lg-4 col-sm-4">		
	<?php
		date_default_timezone_set('America/Costa_Rica'); 
		$date = date('Y-m-d',strtotime('-1 day'));		
		$ayer = PackagePi::find()->andWhere(['=','fecha',$date])->one();
		
		$paca = Package::findOne(233);
		$venEfectivo = $paca->ValorDeVentaEfe;
		$venApartados = $paca->ValorDeVentaAparVendidos;
		$cobApartados = $paca->ValorDeVentaApar;
		
		$totVenPI = $venEfectivo+$venApartados;
	?>
	<h2>Sobres</h2>
	<p>	
		<b>Sobre Tienda: </b><?= Moneda::get( (($totVenPI*$paca->porcentajeTienda)/100 )-$ayer->tienda); ?><br>
		<b>Sobre Inversionista: </b><?= Moneda::get(( ($totVenPI*$paca->porcentajeInversor)/100)-$ayer->inversor) ?>
	</p>
	</div>

</div>

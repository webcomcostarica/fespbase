<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\CajaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="caja-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'cincuentamil') ?>

    <?= $form->field($model, 'veitemil') ?>

    <?= $form->field($model, 'diesmil') ?>

    <?= $form->field($model, 'cincomil') ?>

    <?php // echo $form->field($model, 'dosmil') ?>

    <?php // echo $form->field($model, 'mil') ?>

    <?php // echo $form->field($model, 'quinientos') ?>

    <?php // echo $form->field($model, 'cien') ?>

    <?php // echo $form->field($model, 'cincuenta') ?>

    <?php // echo $form->field($model, 'vienticinco') ?>

    <?php // echo $form->field($model, 'dies') ?>

    <?php // echo $form->field($model, 'cinco') ?>

    <?php // echo $form->field($model, 'fecha') ?>

    <?php // echo $form->field($model, 'usuario') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

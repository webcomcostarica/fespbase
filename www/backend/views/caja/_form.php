<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\Caja */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="caja-form row">
	
	<div class="col-lg-4 col-sm-4">
    <?php $form = ActiveForm::begin(); ?>
	
	<?php
		echo Html::Label('Fecha');
		echo DatePicker::widget([
			'model' => $model,
			'attribute' => 'fecha',
			'language' => 'es', 
			'clientOptions' => [
				'autoclose' => true,
				'format' => 'yyyy-mm-dd',
				'startDate' => date('Y-m-d',strtotime('-2 day')),
			]
		]);
       
	?>
	</div>
    <div class="col-lg-4 col-sm-4">
   
    <?= $form->field($model, 'terminal')->textInput(['value'=>$config['Terminal'],'type'=>'number','min'=>0,'max'=>99]) ?>
    </div>
    <div class="col-lg-4 col-sm-4">
   
    <?= $form->field($model, 'sucursal')->textInput(['value'=>$config['Sucursal'],'type'=>'number','min'=>0,'max'=>99]) ?>
	</div>
    </div>
    
    <div class="row">
    <div class="col-lg-12">
	<p>Anote la cantidad correspondiente de cada moneda</p>
	</div>
    
<div class="col-lg-4 col-sm-4">
       
    <?= $form->field($model, 'cincuentamil')->textInput() ?>

    <?= $form->field($model, 'veitemil')->textInput() ?>

    <?= $form->field($model, 'diesmil')->textInput() ?>

    <?= $form->field($model, 'cincomil')->textInput() ?>
	</div>
<div class="col-lg-4 col-sm-4">
    <?= $form->field($model, 'dosmil')->textInput() ?>

    <?= $form->field($model, 'mil')->textInput() ?>
	


    <?= $form->field($model, 'quinientos')->textInput() ?>

    <?= $form->field($model, 'cien')->textInput() ?>
	</div>
	<div class="col-lg-4 col-sm-4">
    <?= $form->field($model, 'cincuenta')->textInput() ?>

    <?= $form->field($model, 'vienticinco')->textInput() ?>
	
    <?= $form->field($model, 'veinte')->textInput() ?>

    <?= $form->field($model, 'dies')->textInput() ?>

    <?= $form->field($model, 'cinco')->textInput() ?>

   

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Crear') : Yii::t('app', 'Actualizar'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
	</div>
    <?php ActiveForm::end(); ?>
	
	<div class="col-lg-4 col-sm-4">
		
	</div>
</div>
</div>

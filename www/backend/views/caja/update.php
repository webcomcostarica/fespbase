<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Caja */

$this->title = Yii::t('app', 'Actualizar {modelClass}: ', [
    'modelClass' => 'Caja',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Reportes'), 'url' => ['/reporte/diario-de-ventas']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cajas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Actualizar');
?>
<div class="caja-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'config' => $config
    ]) ?>

</div>

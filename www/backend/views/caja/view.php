<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Caja */

$this->title = 'Caja de '.$model->fecha;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Reportes'), 'url' => ['/reporte/diario-de-ventas']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cajas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$admin = (Yii::$app->user->identity->rol_id == 2 || Yii::$app->user->identity->rol_id == 3)?1:0;

?>
<div class="caja-view">

    <h1><?= Html::encode($this->title) ?></h1>

	<?php if($admin){ ?>
    <p>
        <?= Html::a(Yii::t('app', 'Actualizar'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Borrar'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
	<?php } ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
        //    'id',
            'fecha',
			'sucursal',
			'terminal',
			'total',
			'tipo',
            
            'cincuentamil',
            'veitemil',
            'diesmil',
            'cincomil',
            'dosmil',
            'mil',
            'quinientos',
            'cien',
            'cincuenta',
            'vienticinco',
            'dies',
            'cinco',
            'usuario',
        ],
    ]) ?>

</div>

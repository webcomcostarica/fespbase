<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ContactosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Contactos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contactos-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php // Html::a(Yii::t('backend', 'Crear Contactos'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
        //    ['class' => 'yii\grid\SerialColumn'],

          //  'id',
            'colaborador',
            'colaborador0.nombre',
            'colaborador0.apellido',
            'colaborador0.cedula',
            //'titulo:ntext',
            'datos:ntext',
           // 'tipo:ntext',

             [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{view} {update} {delete}',
            ],
        ],
    ]); ?>

</div>

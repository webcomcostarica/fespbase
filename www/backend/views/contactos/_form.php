<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Contactos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contactos-form row">
    <div class="col-lg-6">
        <?php $form = ActiveForm::begin(); ?>

        <?php // $form->field($model, 'colaborador')->textInput() ?>

        <?= $form->field($model, 'titulo'); ?>
        
        <?= $form->field($model, 'datos'); ?>
        
        <?php 
        echo $form->field($model, 'tipo')
                ->dropDownList(
                    [   'tel'=>'Teléfono',
                        'correo'=>'Correo',
                        'direccion'=>'Dirección',
                        'otro'=>'Otro',]    
                );
        ?>
        
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
         
    </div>
    
    <div class="col-lg-6">


    </div>    

    <?php ActiveForm::end(); ?>
    
</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Contactos */

$this->title = Yii::t('backend', 'Crear Contacto');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Colaboradores'), 'url' => ['index','sort'=>'nombre']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', $colab->nombre.' '.$colab->apellido), 'url' => ['colaborador/view','id'=>$colab->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contactos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Contactos */

$this->title = 'Contacto de '.$model->colaborador0->nombre.' '.$model->colaborador0->apellido;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', $model->colaborador0->nombre.' '.$model->colaborador0->apellido), 'url' => ['colaborador/view','id'=>$model->colaborador]];
//$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="contactos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

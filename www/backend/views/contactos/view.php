<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Contactos */

$this->title = $model->titulo;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Colaboradores'), 'url' => ['index','sort'=>'nombre']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', $model->colaborador0->nombre.' '.$model->colaborador0->apellido), 'url' => ['colaborador/view','id'=>$model->colaborador]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contactos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
      
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'colaborador0.nombre',
            'colaborador0.apellido',
            'titulo:ntext',
            'datos:ntext',
            'tipo:ntext',
        ],
    ]) ?>

</div>

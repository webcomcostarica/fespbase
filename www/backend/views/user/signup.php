<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

use backend\models\Rol;

$this->title = Yii::t('backend', 'Create User');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?= $form->field($model, 'username') ?>
            
                <?= $form->field($model, 'rol')->dropDownList(
                    [1=>'Caja',2=>'Admin',3=>'SuperUsuario',5=>'Contabilidad']
                           //['prompt'=>'Seleccione por favor']    
                   ); ?>

                <?= $form->field($model, 'email') ?>

                <?= $form->field($model, 'password')->passwordInput() ?>
            
                 
                <div class="form-group">
                    <?= Html::submitButton('Crear', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="nc-detalles-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'descripcion')->textarea(['rows' => 6])->label('Razón de la anulación') ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Crear') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

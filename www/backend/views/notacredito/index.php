<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = Yii::t('app', 'Comprobantes');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nc-comprobante-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            'id',
             [
                'label' => 'Comprobante Anulado',                
                'format' => 'raw',
                'value' => function($model) {          
                        return  Html::a($model->comprobanteAnulado->comprobante->numConce,
                                ['/factura/imprimir'],
                                [
                                    'data' => [
                                        'method' => 'get',
                                        'params' => ['id' => $model->comprobanteAnulado->id],
                                    ],                                   
                                ]
                            );
                },
            ],            
            'fecha:ntext',
            [
                'label' => 'FE Envio',
                'contentOptions' => function($model) {
                    $clases = [0 => 'warning', 202 => 'success'];
                    $id = (!empty($model->comprobante)) ? $model->comprobante->envioEstado : 0;
                    return ['class' => $clases[$id]];
                },
                'format' => 'raw',
                'value' => function($model) {
                    return $model->comprobante->envioMensaje;
                },
            ],
            [
                'label' => 'FE Estado',
                'format' => 'raw',
                'contentOptions' => function($model) {
                    $clases = [0 => 'warning', 1 => 'success', 2 => 'danger', 3 => 'danger', 800 => 'danger',400=>'warning'];
                    $id = (!empty($model->comprobante)) ? $model->comprobante->estadoEstado : 0;
                    return ['class' => $clases[$id]];
                },
                'value' => function($model) {
                    $clases = [0 => 'Esperando', 1 => 'Aceptado', 2 => 'Rechazado Parcialmente', 3 => 'Rechazado',400=>'warning'];
                    return $clases[$model->comprobante->estadoEstado];
                },
            ],
                            // 'vence',
                            // 'iv',
                            // 'idUsua',
                            // 'creacion',
                            // 'modificacion',
                            // 'tipo:ntext',
                            // 'forma_pago:ntext',
                            // 'estado:ntext',
                            // 'colaboradores',
                            // 'dgt_condicion',
                            // 'dgt_plazoCredito:ntext',
                            // 'dgt_medioPago',
                            // 'dgt_moneda',
                            // 'dgt_tipoCambio',
                            // 'dgt_serviciosGravados',
                            // 'dgt_serviciosExentos',
                            // 'dgt_mercaGravada',
                            // 'dgt_mercaExenta',
                            // 'dgt_gravado',
                            // 'dgt_totExento',
                            // 'dgt_totVenta',
                            // 'dgt_totDescuento',
                            // 'dgt_totVentaNeta',
                            // 'dgt_totImpuesto',
                           // 'dgt_total',
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{imprimir}',
                                'buttons' => [
                                    'imprimir' => function ($url, $model) {
                                        $btn = '<a href="/notacredito/imprimir?id=' . $model->id . '">Consultar <span class="glyphicon glyphicon-pencil"></span></a>';
                                        return $btn;
                                    }
                                ]
                            ],
                        ],
                    ]);
                    ?>

</div>

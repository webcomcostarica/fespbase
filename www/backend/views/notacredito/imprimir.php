<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use common\models\AccessHelpers;

use backend\models\SisConfiguraciones;
$configuraciones = SisConfiguraciones::getParametrosGenerales();
use backend\models\CodigosComprobantes;
use common\components\FuncionesDGT;
$Numeracion = $model->comprobante;
$this->title = $model->estado . ' :' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Facturas'), 'url' => ['index', 'sort' => '-id']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="row">
    <ul class="nav nav-tabs">
        <li class="active">
            <a href="#panel-1" data-toggle="tab"   aria-expanded="false">Hacienda</a>
        </li>                
        <li class="">
            <a href="#panel-2" data-toggle="tab"   aria-expanded="false">Ver comprobante</a>
        </li>
        <li class="">
            <a href="#panel-3" data-toggle="tab"   aria-expanded="false">Descargar XML's</a>
        </li>
    </ul>
    
    <div class="tab-content">
            <div class="tab-pane active" id="panel-1">
                <?php if(isset($Numeracion)){
                        echo DetailView::widget([
                            'model' => $Numeracion,
                            'attributes' => [
                                'clave',
                                'numConce',
                                'envioEstado',
                                'envioMensaje',
                                'estadoEstado',
                                'estadoMensaje',
                            ],
                        ]);
                      
                     if($Numeracion->getClase() == 500){
                         echo Html::a('Enviar de nuevo',
                                '/notacredito/enviar-comprobante-generado', 
                                [
                                'class' => 'btn btn-primary',
                               
                                'data' => [
                                            'method' => 'post',
                                            'params' => ['id' => $model->id],                                               
                                        ]]
                            );
                    }
                    
                    if(
                        $Numeracion->envioEstado == 202 || 
                        $Numeracion->estadoEstado == 0 || 
                        isset($_GET['all'])
                        ){
                            echo Html::a('Consultar Estado',
                                '/notacredito/estado?id='.$model->id, [
                                'class' => 'btn btn-primary',
                            ]);                          
                    }
                    if(
                        $Numeracion->getClase() == 0 || //No se ha enviado
                        $Numeracion->getClase() == 500 || //No recibido por hacienda
                        isset($_GET['all'])             //todas las opciones
                    ){
                        echo Html::a('Enviar de nuevo',
                    '/notacredito/enviar-comprobante-generado', 
                    [
                    'class' => 'btn btn-primary',
                   
                    'data' => [
                                'method' => 'post',
                                'params' => ['id' => $model->id],                                               
                            ]]
                );
                    }

                    
                }         
            ?>

            </div>
            
            
            <div class="tab-pane" id="panel-2">            
                <div class="factura-update <?= $model->estado . ' ' . $model->forma_pago; ?>">
                    <br>
                       <?php 
                            echo $this->render('/factura/documento_pdf',[
                                'model' => $model,
                                'configuraciones' => $configuraciones,
                            ]);
                        ?>
       <!--
                   <button type="button" class="btn btn-warning" title="Imprimir" onclick="factura_imprimir_final();">
                        <span class="glyphicon glyphicon-print"></span>
                    </button>  -->                      
                </div>            
            </div>

            <div class="tab-pane" id="panel-3">
            <?php 
                  $ambiente = FuncionesDGT::getAmbienteSistema($configuraciones);  
            ?>
               <h2>Comprobante XML</h2>
               <a target="_blank" href="<?= $ambiente['paths'][$model->comprobante->tipoComprobante] . $model->comprobante->numConce . '_comprobante' . '.xml' ?>" class="btn btn-info">Ver</a>
               <a 
                download="<?= $model->comprobante->numConce . '_comprobante' . '.xml' ?>" 
                target="_blank" 
                href="<?= $ambiente['paths'][$model->comprobante->tipoComprobante] . $model->comprobante->numConce . '_comprobante' . '.xml' ?>" class="btn btn-warning">Descargar</a>
               
               <h2>Mensaje de aceptación de hacienda</h2>
               <a target="_blank" href="<?= $ambiente['paths'][$model->comprobante->tipoComprobante] . $model->comprobante->numConce . '_mensaje' . '.xml' ?>" class="btn btn-info">Ver</a>
               <a  download="<?= $model->comprobante->numConce . '_mensaje' . '.xml' ?>"
               target="_blank" 
               href="<?= $ambiente['paths'][$model->comprobante->tipoComprobante] . $model->comprobante->numConce . '_mensaje' . '.xml' ?>" class="btn btn-warning">Descargar</a>
                              
               <h2>PDF factura</h2> 
               <?php 
                $url = $ambiente['paths'][$model->comprobante->tipoComprobante] . 'pdf_'.$model->comprobante->numConce .'.pdf';
             ?>
             <a target="_blank" href="<?= $url ?>" class="btn btn-info">Ver PDF</a>
              
               <a target="_blank" href="<?= $url ?>" class="btn btn-warning">Descargar</a>
                           
            </div>

            <div class="tab-pane" id="panel-4">
                
            
            </div>



    </div>


<?php $this->registerJsFile('/js/facturacion.js'); ?>  
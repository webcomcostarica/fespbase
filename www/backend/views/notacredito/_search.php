<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\NcComprobanteSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nc-comprobante-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'fecha') ?>

    <?= $form->field($model, 'vence') ?>

    <?= $form->field($model, 'iv') ?>

    <?= $form->field($model, 'idUsua') ?>

    <?php // echo $form->field($model, 'creacion') ?>

    <?php // echo $form->field($model, 'modificacion') ?>

    <?php // echo $form->field($model, 'tipo') ?>

    <?php // echo $form->field($model, 'forma_pago') ?>

    <?php // echo $form->field($model, 'estado') ?>

    <?php // echo $form->field($model, 'colaboradores') ?>

    <?php // echo $form->field($model, 'dgt_condicion') ?>

    <?php // echo $form->field($model, 'dgt_plazoCredito') ?>

    <?php // echo $form->field($model, 'dgt_medioPago') ?>

    <?php // echo $form->field($model, 'dgt_moneda') ?>

    <?php // echo $form->field($model, 'dgt_tipoCambio') ?>

    <?php // echo $form->field($model, 'dgt_serviciosGravados') ?>

    <?php // echo $form->field($model, 'dgt_serviciosExentos') ?>

    <?php // echo $form->field($model, 'dgt_mercaGravada') ?>

    <?php // echo $form->field($model, 'dgt_mercaExenta') ?>

    <?php // echo $form->field($model, 'dgt_gravado') ?>

    <?php // echo $form->field($model, 'dgt_totExento') ?>

    <?php // echo $form->field($model, 'dgt_totVenta') ?>

    <?php // echo $form->field($model, 'dgt_totDescuento') ?>

    <?php // echo $form->field($model, 'dgt_totVentaNeta') ?>

    <?php // echo $form->field($model, 'dgt_totImpuesto') ?>

    <?php // echo $form->field($model, 'dgt_total') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

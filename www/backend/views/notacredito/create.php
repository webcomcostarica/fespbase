<?php

use yii\helpers\Html;


$this->title = 'Nueva anulación de factura';
$this->params['breadcrumbs'][] = ['label' => 'Factura', 'url' => ['/factura/imprimir','id'=>$model->factura]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nc-detalles-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

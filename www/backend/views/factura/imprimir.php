<?php

use yii\helpers\Html;
use backend\models\DgtFe;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use common\models\AccessHelpers;
use common\components\FuncionesDGT;
use backend\models\SisConfiguraciones;
use backend\models\CodigosComprobantes;
use backend\models\base\SendMail;

$this->title = $model->estado . ' :' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Facturas'), 'url' => ['index', 'sort' => '-id']];
$this->params['breadcrumbs'][] = $this->title;

$configuraciones = SisConfiguraciones::getParametrosGenerales();
$ambiente = FuncionesDGT::getAmbienteSistema($configuraciones);  
$Numeracion = $model->comprobante;

?>
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12">
    <ul class="nav nav-tabs">             
        <li class="active">
            <a href="#panel-2" data-toggle="tab"   aria-expanded="false">Ver comprobante</a>
        </li>  
        <li class="">
            <a href="#panel-1" data-toggle="tab"   aria-expanded="false">Hacienda</a>
        </li> 
        <li class="">
            <a href="#panel-3" data-toggle="tab"   aria-expanded="false">Descargar XML's</a>
        </li>
   
    </ul>
    </div> 
    <div class="tab-content">
    
                   
         <div class="tab-pane" id="panel-1">
                    <?php
                       echo $this->render('imprimir/_tab_1',[
                            'Numeracion'=>$Numeracion,
                            'model'=>$model
                        ]);
                    ?>
            </div>
            
            
            <div class="tab-pane active" id="panel-2">    

           

            <?php if (AccessHelpers::getAcceso('factura-imprimir-pos')) { ?>
             <div class="col-xs-12 col-sm-12 col-md-12"> 
             <?php
                $url = $ambiente['paths'][$model->comprobante->tipoComprobante] . 'pdf_'.$model->comprobante->numConce .'.pdf';
            ?>
                <a 
                class="btn btn-warning pull-right" 
                href="#" onclick="factura_imprimir('factura_pos')">Imprimir POS</a>
                
                <a 
                target="_blank" 
                href="<?= $url ?>" 
                class="btn btn-info pull-right">Imprimir en A4</a>

            </div>
            <?php } ?>

            <div class="col-xs-12 col-sm-12 col-md-12"> 
                <div class="factura-update <?= $model->estado . ' ' . $model->forma_pago; ?>">
                    <br>
                       <?php 
                            echo $this->render($configuraciones['PlantillaPdf'],[
                                'model' => $model,
                                'configuraciones' => $configuraciones,
                            ]);
                        ?>
       <!--
                   <button 
                   type="button" 
                   class="btn btn-warning" 
                   title="Imprimir" 
                   onclick="factura_imprimir_final();">
                        <span class="glyphicon glyphicon-print"></span>
                    </button>  -->                      
                </div>            
            </div>
            </div>

            <div class="tab-pane" id="panel-3">
            
            <div class="col-xs-12 col-sm-12 col-md-12">
               <h2>Comprobante XML</h2>
               <a target="_blank" href="<?= $ambiente['paths'][$model->comprobante->tipoComprobante] . $model->comprobante->numConce . '_comprobante' . '.xml' ?>" class="btn btn-info">Ver XML</a>
               <a 
                download="<?= $model->comprobante->numConce . '_comprobante' . '.xml' ?>" 
                target="_blank" 
                href="<?= $ambiente['paths'][$model->comprobante->tipoComprobante] . $model->comprobante->numConce . '_comprobante' . '.xml' ?>" class="btn btn-warning">Descargar XML</a>
               
               <?php
                    
                    if(!empty($model->comprobante))
                    if(!$model->comprobante->getConsultarDeNuevo()) { ?>
                       <h2>Mensaje de aceptación de hacienda</h2>
                       <a target="_blank" href="<?= $ambiente['paths'][$model->comprobante->tipoComprobante] . $model->comprobante->numConce . '_mensaje' . '.xml' ?>" class="btn btn-info">Ver XML</a>
                       <a  download="<?= $model->comprobante->numConce . '_mensaje' . '.xml' ?>"
                       target="_blank" 
                       href="<?= $ambiente['paths'][$model->comprobante->tipoComprobante] . $model->comprobante->numConce . '_mensaje' . '.xml' ?>" class="btn btn-warning">Descargar XML</a>
                <?php }else{ ?>
                   <br>
                   <br>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">Aun falta un paso</h3>
                        </div>
                        <div class="panel-body">
                        Este comprobante aun no ha sido aceptado por Hacienda, ellos tienen 3 horas parar revisarlo.<br>
                        <br>
                        Puedes esperar que el sistema lo consulte de forma automática o utilizar la fuerta bruta con este boton : 
                            <?=  Html::a('Consultar Estado',
                                    '/factura/estado?id='.$model->id, [
                                    'class' => 'btn btn-primary',
                                ]); ?> <br>                       
                        </div> 
                    </div>     
                <?php } ?>  
                
               <h2>PDF factura</h2>
              
             <?php 
                $url = $ambiente['paths'][$model->comprobante->tipoComprobante] . 'pdf_'.$model->comprobante->numConce .'.pdf';
             ?>
             <a target="_blank" href="<?= $url ?>" class="btn btn-info">Ver PDF</a>
              <a target="_blank" download="<?= 'pdf_'.$model->comprobante->numConce .'.pdf' ?>" href="<?= $url ?>" class="btn btn-warning">Descargar PDF</a>
                           
            </div>
            </div>

            <div class="hidden">
                <?php 
                    echo $this->render('imprimir_pos',[
                        'model' => $model
                    ]);
                ?>
            
            </div>



    </div>
    </div>


<?php $this->registerJsFile('/js/facturacion.js'); ?>  
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\DgtFe;
use yii\widgets\DetailView;

$this->title = $model->estado . ' :' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Facturas'), 'url' => ['index', 'sort' => '-id']];
$this->params['breadcrumbs'][] = $this->title;

$Numeracion = DgtFe::find()->where(['idFactura'=>$model->id])->one();
?>
<div class="row">
    <div id="impri_header"> 
        <p style='text-align: center;'><strong><?= Yii::$app->params['empresa']; ?> </strong><br>		
            CED: <?= Yii::$app->params['cedjuridica']; ?></p>
        <?php if (isset(Yii::$app->params['slogan'])) { ?>
            <p style='text-align: center;'><?= Yii::$app->params['slogan']; ?></p>
        <?php } ?>		
        <p >FACTURA DE <?= strtoupper($model->forma_pago); ?> </BR>
            NUMERO <?= $model->id; ?> </BR>
            FECHA <?= $model->fecha; ?></BR>
            TELÉFONO <?= Yii::$app->params['telefono']; ?></BR>
            CLIENTE <?php
        foreach ($model->idColaboradors as $data) {
            $ced = ($data->cedula != 111111111 ) ? $data->cedula . ' / ' : '';
            echo $ced . $data->nombre . ' ' . $data->apellido;
        }
        ?>
            CAJA 1</BR>


        <div id="impr_detalle">
            <table  style="width:100%" class="table table-striped table-bordered">
                <thead>
                    <tr>			
                        <th class="success" id="data-titunidad">Unidad</th> 
                        <th class="success">Precio U.</th> 						
                        <th class="success">Des.</th> 
                        <th style="width: 100px;"  class="success">TOTAL</th> 				 
                    </tr>
                </thead> 
                <tbody id="tbl_detalle">
                    <?php
                    $total_e = 0;
                    $total_g = 0;
                    $subtotal = 0;
                    $descuento = 0;
                    $cont = 0;
                    foreach ($model->detalleFacturas as $data) {
                        ?>
                        <tr id="tr_lote_<?= $cont; ?>">
                            <?php
                            $iva_det = ($data->impuesto == "G") ? '*' : '';
                            ?>
                            <?php if ($data->lote != 0) { ?> 
                                <td colspan=5><?php echo 'Lot ' . $data->lote . ' / ' . $data->descripcion . $iva_det; ?></td>				
    <?php } else { ?>
                                <td colspan=5><?php echo $data->descripcion . $iva_det; ?></td>					
    <?php } ?> 
                        </tr>
                        <tr id="tr_lote_<?= $cont; ?>">
                            <td><?php echo $data->cantidad . ' ' . $data->unidad; ?></td>				

                            <td><?= moneda($data->lote_precio); ?></td>
                            <td title="<?= $data->tipoDescuento ?>"><?= moneda($data->montoDescuento) ?></td>
                            <td><?= moneda($data->montoTotalLinea) ?></td>

                        </tr> <?php                       
                    }
                    ?>			

                </tbody>
                <tfoot>

                <table class="table table-results" >
                    <tbody>
                       
<?php if ($model->dgt_moneda !== 'CRC') { ?>
                <tr>
                    <td class="success" colspan="2">MONEDA</td>
                    <td><?= $model->dgt_moneda; ?></td>
                </tr>
                <tr>
                    <td class="success" colspan="2">CAMBIO</td>
                    <td><?= $model->dgt_tipoCambio; ?></td>
                </tr>                            
<?php }/*
if ($model->dgt_serviciosGravados != 0) {
    ?>
                <tr>
                    <td class="success" colspan="2">TOTAL SERVICIOS GRAVADOS</td>
                    <td><?= moneda($model->dgt_serviciosGravados); ?></td>
                </tr>
<?php }
if ($model->dgt_serviciosExentos != 0) {
    ?>
                <tr>
                    <td class="success" colspan="2">TOTAL SERVICIOS EXENTOS</td>
                    <td><?= moneda($model->dgt_serviciosExentos); ?></td>
                </tr>                    
<?php }
if ($model->dgt_mercaGravada != 0) {
    ?>
                <tr>
                    <td class="success" colspan="2">TOTAL MERCANCIAS GRAVADAS</td>
                    <td><?= moneda($model->dgt_mercaGravada); ?></td>
                </tr>                    
            <?php }
            if ($model->dgt_mercaExenta != 0) {
                ?>
                <tr>
                    <td class="success" colspan="2">TOTAL MERCANCIAS EXENTAS</td>
                    <td><?= moneda($model->dgt_mercaExenta); ?></td>
                </tr>
<?php } */
            if ($model->dgt_gravado != 0) {
                ?>

            <tr>
                <td class="success" colspan="2">TOTAL GRAVADO</td>
                <td><?= moneda($model->dgt_gravado); ?></td>
            </tr>
            <?php }
            if ($model->dgt_totExento != 0) {
                ?>
            <tr>
                <td class="success" colspan="2">TOTAL EXENTO</td>
                <td><?= moneda($model->dgt_totExento); ?></td>
            </tr>   <?php
            }
                ?>
            <tr>
                <td class="success" colspan="2">TOTAL VENTA</td>
                <td><?= moneda($model->dgt_totVenta); ?></td>
            </tr>
            <tr>
                <td class="success" colspan="2">DESCUENTOS</td>
                <td><?= moneda($model->dgt_totDescuento); ?></td>
            </tr>
            <?php 
            /*
            if ($model->dgt_totVentaNeta != 0) { ?>
                <tr>
                    <td class="success" colspan="2">TOTAL VENTA NETA</td>
                    <td id="idSub"><?= moneda($model->dgt_totVentaNeta); ?></td>
                </tr>
            <?php }*/
            if ($model->dgt_totImpuesto != 0) {
                ?>
                <tr>
                    <td class="success" colspan="2">TOTAL IMPUESTO</td>
                    <td id="idSub"><?= moneda($model->dgt_totImpuesto); ?></td>
                </tr>  
            <?php } ?>
            <tr>
                <td class="success" colspan="2">TOTAL</td>
                <td id="idSub"><?= moneda($model->dgt_total); ?>
                    <script>total_facturado =<?= $model->dgt_total; ?></script></td>
            </tr>
            <!-- TODO Impuesto General -->                    

<?php if ($model->dgt_condicion === '04') { ?>
                <tr>
                    <td class="success" colspan="2">SALDO</td>
                    <td><?php echo moneda($model->calcsaldo); ?></td>
                </tr>
<?php } ?>
            <tr>
                <td></td>
                <td></td>
            </tr>         
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>                       
                </table>	
                </tfoot>
            </table>


        </div>

        <div id="impr_pie">  
            <p>Los detalles con * representan los productos con IVA (Gravados)</p>		
            <p style='text-align: center;'>Autorizado Mediante Oficio #<?= Yii::$app->params['oficio']; ?></p>
            <p style='text-align: center;'>del dia <?= Yii::$app->params['fechaoficio']; ?></p>
        </div>


        <button type="button" class="btn btn-warning" title="Imprimir" onclick="factura_imprimir_final();">
            <span class="glyphicon glyphicon-print"></span>
        </button>
            
            </div>
          
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Factura Grupo Comercial Tectronic</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body style="">
    <div style="padding:150px; position:absolute; width:2550px; height:3300px; ">
        <img src="logo.png" alt="logo">
        <div style="float: right;" align="right">
            <p style="font-size: 50px;">
                Factura
            </p>
            <p style="color:brown; font-size: 40px;">
                N° 00001
            </p>
        </div>
        <div>
            <div style="width: 500px;">
                <p style="padding-top: 50px">
                    <strong>
                        Centro Costarricense de Emergencias
                    </strong>
                </p>
                <p>
                    <strong>
                        Cordero Mora Roy.
                    </strong>
                </p>
                <p>
                    <strong>
                        Cédula Juridica:
                    </strong>
                     0000000000.
                </p>
                <p>
                    <strong>
                        Telefono:
                    </strong>
                    2222 2222.
                </p>
                <p>
                    <strong>
                        Dirección:
                    </strong>
                    De la esquina oeste de los tribunales de Guadalpe
                    100 metros oeste y 100 metros sur. 
                </p>
                <table style="width: 300px; position: relative;
                              left: 2250px; bottom: 245px;"
                        cellpadding="0" cellspacing="0">
                    <tr>
                        <th style="border-radius: 10px 0 0 0;">
                            DÍA
                        </th>
                        <th>
                            MES
                        </th>
                        <th style="border-radius: 0 10px 0 0;">
                            AÑO
                        </th>
                    </tr>
                    <tr>
                        <td style="border-radius: 0 0 0 10px;">   </td>
                        <td>   </td>
                        <td style="border-radius: 0 0 10px 0;">   </td>
                    </tr>
                </table>
            </div>
        </div>
        <div style="position:relative; bottom:200px;">
            <table style="width:100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <th style="border-radius: 15px 0 0 0;">
                        Señor(a): 
                    </th>
                    <td style="border-radius: 0 15px 0 0;"></td>
                </tr>
                <tr>
                    <th>
                        Dirección:
                    </th>
                    <td></td>
                </tr>
                <tr>
                    <th style="width:200px; border-radius: 0 0 0 15px;">
                        Teléfono: 
                    </th>
                    <td style="border-radius: 0 0 15px 0;"></td>
                </tr>
            </table>

            <table class="cantidadTotal" style="width:100%;
            padding-top:20px;" cellpadding="1" cellspacing="0" >
                <tr>
                    <th style="width:10%; border-radius: 15px 0 0;">
                        Cantidad
                    </th>
                    <th>
                        Descripción
                    </th>
                    <th style="width:487px; border-radius: 0 15px 0 0;">
                        Monto
                    </th>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
            <table style="width:100%;" cellpadding="1" cellspacing="0">
                <tr>
                    <th style="width:80.8%; padding-right:10px; border-radius: 0 0 0 15px;"
                    align="right">
                        TOTAL:&nbsp;
                    </th>
                    <td style="border-radius: 0 0 15px; font-size: 50px">
                        &nbsp;₡
                    </td>
                </tr>
            </table>
            <table style="width:100%; padding-top:20px;"
            cellpadding="1" cellspacing="0" >
                <tr>
                    <th style="width:10%; border-radius: 15px 0 0 15px;">
                        HECHO POR
                    </th>
                    <td></td>
                    <th style="width:18%;">
                        FECHA DE VENCIMIENTO
                    </th>
                    <td style="border-radius: 0 15px 15px 0;"></td>
                </tr>
            </table>
            <table style="padding-top:20px;" cellpadding="0" cellspacing="0" >
                <tr>
                    <th style="width:10%; border-radius: 15px 0 0 15px;">
                        RECIBIDO CONFORME
                    </th>
                    <td style="width:2350px; border-radius: 0 15px 15px 0;"></td>
                </tr>
            </table>
            <p align="center" >
               FIRMA/NOMBRE/CEDULA
            </p>
        </div>
    </div>
</body>
</html>
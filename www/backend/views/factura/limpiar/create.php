<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Factura */

$this->title = $model->estado.'-'.$model->id ;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Facturas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="factura-create">
    <?= $this->render('_form', [
        'model' => $model,
        'personas' => $personas,
    ]) ?>

</div>

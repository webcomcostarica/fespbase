<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


$this->title = $model->idFactura;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Facturas Electronicas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dgt-token-view">

   <p><b>Factura Electrónica:</b> <?= Html::encode($this->title) ?></p>

   <div class="row" > 
       <div class="col-sm-6" > 
            <h3>Estado de envío</h3>
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'envioEstadoD:ntext',
                    'envioMensaje:ntext',
                ],
            ]) ?>
            
            <?= Html::a(Yii::t('app', 'Enviar'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        </div>
        <div class="col-sm-6" > 
            <h3>Estado de verificación</h3>
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'estadoEstadoD:ntext',
                    'estadoMensaje:ntext',
                ],
            ]) ?>
            
            <?= Html::a(Yii::t('app', 'Verificar'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
         </div>
    </div><br>        
    <h3>Detalles</h3>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idFactura',
            'clave:ntext',
            'numConce:ntext',
            'fechaEmision:ntext',
            'normativa:ntext',
            'fechaNormativa:ntext',
        ],
    ]) ?>

</div>

<?php
use backend\models\CodigosComprobantes;
$operacion = str_replace("/", "-", Yii::$app->controller->route);
?>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

    <div class="row">
        <div class="col-xs-12 col-md-12 col-sm-12">
    		<div class="invoice-title">
    			<h3 class=""><?= $configuraciones['NombreComercial']; ?> <?= $configuraciones['Cedula']; ?></h3>
                    <p><?= $configuraciones['OtrasSenas'] ?><br>
                    <b>Correo: </b> <?= $configuraciones['Correo'] ?><br>
                    <b>Teléfono: </b> <?= $configuraciones['Telefono'] ?><br>
                </p>
            </div>
    		<hr>
    		<div class="row">
    			<div class="<?= ($operacion=='factura-imprimir')? 'col-xs-8 col-sm-8 col-lg-8' : 'col-xs-7';?>">
    				<address>
                        <b>Numeración</b><br>
                        Consecutivo: <br>
    					<?= $model->comprobante->numConce; ?><br>
    					Clave: <br>
    					<span style="font-size:12px;"><?= $model->comprobante->clave; ?></span><br>
                    </address>
    			</div>
    			<div class="col-xs-12 col-md-3 col-sm-3 <?= ($operacion == 'factura-imprimir')? '' : 'text-right' ?>">
    				<address>
        			<strong>Emisión:</strong><br>
                        <?= $model->fecha; ?><br>
                        <b>Pago:</b><br>
    					Condición: <?= CodigosComprobantes::CONDICIONES[$model->dgt_condicion]; ?><br>
    					Medio: <?= CodigosComprobantes::MEDIOSDEPAGO[$model->dgt_medioPago]; ?><br>
                        Terminal <?= $configuraciones['Terminal'] ?> Sucursal <?= $configuraciones['Sucursal'] ?>    
    				</address>
    			</div>
    		</div>
            <?php if($configuraciones['ClientePorDefecto'] != $model->idColaboradors[0]->id){ ?>
    		<div class="row">
    			<div class="col-xs-12 col-md-6 col-sm-6">
    				<address>
    					<strong>Receptor</strong><br>
    					<?php
                            foreach ($model->idColaboradors as $data) {
                                $ced = ($data->cedula != $configuraciones['ClientePorDefecto'] ) ? $data->cedula : '';
                                echo $ced .' '. $data->getNombreCompleto().'<br>';
                                echo $data->getDireccion();
                            }
                        ?>
    				</address>
    			</div>
    		</div>
            <?php }
            echo $model->receptorRapido;
            if(!empty($model->receptorRapido)){ ?>
                <div class="row">
    			<div class="col-xs-12 col-md-6 col-sm-6">
    				<address>
    					<strong>Receptor</strong><br>
    					<?php
                            echo $model->receptorRapido;
                        ?>
    				</address>
    			</div>
    		</div>
                <?php
            }
            ?>
    	</div>
    </div>
    
    <div class="row">
    	<div class="col-md-12">
    		<div class="panel panel-default">
    			<div class="panel-heading" style="
    color: white !important;
    background-color: black;
    border-color: #ddd;">
    				<h3 class="panel-title" style="color:white;"><strong>Detalles</strong></h3>
    			</div>
    			<div class="panel-body">
    				<div class="table-responsive">
    					<table class="table table-condensed">
    						<thead>
                                <tr>
                                    <?php if(isset($_GET['all'])) { ?>
                                        <td>COD</td>
                                    <?php } ?>
        							<td><strong>Detalle</strong></td>
        							<td class="text-center"><strong>Cantidad</strong></td>
        							<td class="text-center"><strong>Precio</strong></td>
        							<td class="text-center"><strong>Sub</strong></td>
        							<td class="text-center"><strong>IVA</strong></td>
        							<td class="text-right"><strong>TotLinea</strong></td>
                                </tr>
    						</thead>
    						<tbody>
                                <?php
                                foreach ($model->detalleFacturas as $data) {?>
                                    <tr >
                                    <?php if(isset($_GET['all'])) { ?>
                                        <td><?php echo $data->lote; ?></td>
                                    <?php } ?>					
                                        <td><?php echo $data->descripcion; ?></td>					
                                        <td class="text-center"><?php echo $data->cantidad . ' ' . $data->unidad; ?></td>
                                        <td class="text-center"><?= $model->moneda($data->lote_precio); ?></td>
                                        <td class="text-center"><?= $model->moneda($data->montoTotal) ?></td>
                                        <td class="text-center"><?= $model->moneda($data->impuesto) ?></td>
                                        <td class="text-right"><?= $model->moneda($data->montoTotalLinea) ?></td>
                                    </tr> <?php                       
                                } ?>
                                <tr>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                </tr>
                                
    						</tbody>
                        </table>
                        <br>
                        <table class="table table-condensed">
    						<tbody>
                             <?php
                                if ($model->dgt_moneda !== 'CRC') { 
                                    echo $this->render('documento_pdf_fila_total', [
                                        'titulo' => 'Moneda',
                                        'valor' => $model->dgt_moneda
                                        ]);
                                     echo $this->render('documento_pdf_fila_total', [
                                        'titulo' => 'Cambio',
                                        'valor' => $model->dgt_tipoCambio
                                        ]);
                                ?>                         
                            <?php }
                          
                                echo $this->render('documento_pdf_fila_total', [
                                        'titulo' => 'Tot Gravado',
                                        'valor' => $model->moneda($model->dgt_gravado)
                                        ]);
                           
                            
                       
                                echo $this->render('documento_pdf_fila_total', [
                                        'titulo' => 'Tot Exento',
                                        'valor' => $model->moneda($model->dgt_totExento)
                                        ]);
                          
                            
                            echo $this->render('documento_pdf_fila_total', [
                                'titulo' => 'Total Venta',
                                'valor' => $model->moneda($model->dgt_totVenta)
                            ]);
                                        
                             
                            echo $this->render('documento_pdf_fila_total', [
                                'titulo' => 'Descuentos',
                                'valor' => $model->moneda($model->dgt_totDescuento)
                            ]);
                            
                         
                            echo $this->render('documento_pdf_fila_total', [
                                'titulo' => 'Tot Neto',
                                'valor' => $model->moneda($model->dgt_totVentaNeta)
                            ]); 
                            
                            echo $this->render('documento_pdf_fila_total', [
                                'titulo' => 'Tot Impuesto',
                                'valor' => $model->moneda($model->dgt_totImpuesto)
                            ]); 
                            
                            
                             echo $this->render('documento_pdf_fila_total', [
                                'titulo' => 'TOTAL',
                                'valor' => $model->moneda($model->dgt_total)
                            ]);                            
                            ?> 
                            </tbody>
    					</table>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    
    <div class="row">
    	<div class="col-md-12">
            <p class="text-right" style="font-size:12px;">
                Versión 4.2<br>
                Autorizada mediante resolución Nº DGT-R-48-2016 del 7 de octubre de 2016<br>
            </p>
        </div>
    </div>
        
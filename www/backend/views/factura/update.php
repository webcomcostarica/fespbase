<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\SisConfiguraciones;

$this->title = $model->estado.' :'. $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Facturas'), 'url' => ['index','sort'=>'-id']];
$this->params['breadcrumbs'][] = $this->title;

    
?>

<div class="factura-update <?= $model->estado.' '.$model->forma_pago; ?>">
  
   <?php
    $configuraciones = SisConfiguraciones::getParametrosGenerales();
    echo $this->render('factura/a_variables', ['model' => $model]); 
    $form = ActiveForm::begin(); ?>
    <div class="factura-form row">

        <?php echo $this->render('factura/c_datos_de_factura', [
            'model' => $model,
            'form'=>$form,
            'config'=>$config,
        ]); ?>

       
        <div class="col-lg-6" id="section_receptor">
        <?php echo $this->render('factura/d_colaboradores', [
            'model' => $model,
            'form'=>$form,
            'config' => $configuraciones
            ]); ?>       
        </div>
		<div class="col-lg-2">
        <?php echo $this->render('factura/d_usuarios', [
            'model' => $model,
            'form'=>$form,
            'config'=>$config
            ]); ?>       
        </div>

    </div>
    <div id="impr_detalle">
        <?php echo $this->render('factura/e_detalle', [
            'model' => $model,
            'form'=>$form,
            'configs' => $configuraciones
            ]); ?>    
    </div>

<?php ActiveForm::end(); ?>

<?php if($model->estado === "Facturada" && $model->forma_pago === "Apartado") {?>
<?php echo $this->render('factura/f_abonos_dos', ['model' => $model]); ?>    
<?php } ?>



<?php echo $this->render('factura/h_botones', ['model' => $model]); ?>    
  

<?php $this->registerJsFile('/js/facturacion.js'); ?>  

<?php  if($model->estado!="Facturada"){
        echo $this->render('factura/i_calc');
    } ?>

</div>

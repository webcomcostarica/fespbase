<?php
use backend\models\CodigosComprobantes;
$operacion = str_replace("/", "-", Yii::$app->controller->route);
?>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

    <div class="row">
        <div class="col-xs-12 col-md-12 col-sm-12">
    		<div class="invoice-title">
    			<h3 class=""><?= $configuraciones['NombreComercial']; ?> <?= $configuraciones['Cedula']; ?></h3>
                    <p><?= $configuraciones['OtrasSenas'] ?><br>
                    <b>Correo: </b> <?= $configuraciones['Correo'] ?><br>
                    <b>Teléfono: </b> <?= $configuraciones['Telefono'] ?><br>
                </p>
            </div>
    		<hr>
    		<?php
                if(isset($model->comprobante)){
            ?>
            <div class="row">
            
    			<div class="<?= ($operacion=='factura-imprimir')? 'col-xs-8 col-sm-8 col-lg-8' : 'col-xs-7';?>">
    				<address>
                        <b>Numeración</b><br>
                        Consecutivo: <br>
    					<?= $model->comprobante->numConce; ?><br>
    					Clave: <br>
    					<span style="font-size:12px;"><?= $model->comprobante->clave; ?></span><br>
                    </address>
    			</div>
                
    			<div class="col-xs-3 <?= ($operacion == 'factura-imprimir')? '' : 'text-right' ?>">
    				<address>
        			<strong>Emisión:</strong><br>
                        <?= $model->fecha; ?><br>
                        <b>Pago:</b><br>
    					Condición: <?= CodigosComprobantes::CONDICIONES[$model->dgt_condicion]; ?><br>
    					Medio: <?= CodigosComprobantes::MEDIOSDEPAGO[$model->dgt_medioPago]; ?><br>
                        Terminal <?= $configuraciones['Terminal'] ?> Sucursal <?= $configuraciones['Sucursal'] ?>    
    				</address>
    			</div>
    		</div>
            <?php }else{ ?>
                Factura de proforma, válida por 30 días.<br>
                <strong>Emisión:</strong><br>
                        <?= $model->fecha; ?><br>
            <?php } ?>
            <?php if($configuraciones['ClientePorDefecto'] != $model->idColaboradors[0]->id){ ?>
    		<div class="row">
    			<div class="col-xs-6">
    				<address>
    					<strong>Receptor</strong><br>
    					<?php
                            foreach ($model->idColaboradors as $data) {
                                $ced = ($data->cedula != $configuraciones['ClientePorDefecto'] ) ? $data->cedula : '';
                                echo $ced .' '. $data->getNombreCompleto().'<br>';
                                echo $data->getDireccion();
                            }
                        ?>
    				</address>
    			</div>
    		</div>
            <?php }
            if(!empty($model->receptorRapido)){ ?>
                <div class="row">
    			<div class="col-xs-12 col-md-6 col-sm-6">
    				<address>
    					<strong>Receptor</strong><br>
    					<?php
                            echo $model->receptorRapido.' (Este comprobante no es deducible)';
                        ?>
    				</address>
    			</div>
    		</div>
                <?php
            }
            ?>
            </div>
    </div>
    
    <div class="row">
    	<div class="col-md-12">
    		<div class="panel panel-default">
    			<div class="panel-heading" style="
    color: white !important;
    background-color: black;
    border-color: #ddd;">
    				<h3 class="panel-title" style="color:white;"><strong>Detalles</strong></h3>
    			</div>
    			<div class="panel-body">
    				<div class="table-responsive">
    					<table class="table table-condensed">
    						<thead>
                                <tr>
        							<td><strong>Detalle</strong></td>
        							<td class="text-center"><strong>Cantidad</strong></td>
        							<td class="text-center"><strong>Precio U</strong></td>
        							<td class="text-center"><strong>Descuento</strong></td>
        							<td class="text-center"><strong>Subtotal</strong></td>
        							<td class="text-center"><strong>Impuestos</td>
        							<td class="text-right"><strong>Monto</strong></td>
                                </tr>
    						</thead>
    						<tbody>
                                <?php
                                foreach ($model->detalleFacturas as $data) {
                                    $codigo = '';
                                    if($data->lote){
                                        $codigo = $data->lote0->cod_inte_producto.' ';
                                    }
                                ?>
                                    <tr >                        
                                        <td colspan='7'><?php echo $codigo.$data->descripcion; ?></td>	
                                    </tr>                                        
                                    <tr>      
                                        <td></td>
                                        <td class="text-center"><?php echo $data->cantidad . ' ' . $data->unidad; ?></td>                                        
                                        <td class="text-center"><?= $model->moneda($data->lote_precio); ?></td>
                                        <td class="text-center"><?= $model->moneda($data->montoDescuento); ?></td>
                                        <td class="text-center"><?= $model->moneda($data->subTotal); ?></td>
                                        <td class="text-center"><?= $model->moneda($data->impuesto); ?></td>
                                        <td class="text-right"><?= $model->moneda($data->montoTotal) ?></td>
                                    </tr> <?php                       
                                } ?>
                                <tr>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                </tr>
                                
    						</tbody>
                        </table>
                        <br>
                        <table class="table table-condensed">
    						<tbody>
                            <?php
                                if ($model->dgt_moneda !== 'CRC') { 
                                    echo $this->render('documento_pdf_fila_total', [
                                        'titulo' => 'Moneda',
                                        'valor' => $model->dgt_moneda
                                        ]);
                                     echo $this->render('documento_pdf_fila_total', [
                                        'titulo' => 'Cambio',
                                        'valor' => $model->dgt_tipoCambio
                                        ]);
                                ?>                         
                            <?php }
                          if($model->dgt_gravado != 0)
                                echo $this->render('documento_pdf_fila_total', [
                                        'titulo' => 'Tot Gravado',
                                        'valor' => $model->moneda($model->dgt_gravado)
                                        ]);
                           
                            
                            if($model->dgt_totExento!=0)
                                echo $this->render('documento_pdf_fila_total', [
                                        'titulo' => 'Tot Exento',
                                        'valor' => $model->moneda($model->dgt_totExento)
                                        ]);
                          
                           /* 
                            echo $this->render('documento_pdf_fila_total', [
                                'titulo' => 'Total Venta',
                                'valor' => $model->moneda($model->dgt_totVenta)
                            ]);
                            */
                                        
                             
                            echo $this->render('documento_pdf_fila_total', [
                                'titulo' => 'Descuentos',
                                'valor' => $model->moneda($model->dgt_totDescuento)
                            ]);
                            
                         
                            echo $this->render('documento_pdf_fila_total', [
                                'titulo' => 'Tot Neto',
                                'valor' => $model->moneda($model->dgt_totVentaNeta)
                            ]); 
                            
                            echo $this->render('documento_pdf_fila_total', [
                                'titulo' => 'Tot Impuesto',
                                'valor' => $model->moneda($model->dgt_totImpuesto)
                            ]); 
                            
                            
                             echo $this->render('documento_pdf_fila_total', [
                                'titulo' => 'TOTAL',
                                'valor' => $model->moneda($model->dgt_total)
                            ]);                            
                            ?> 
                            </tbody>
    					</table>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    
    <div class="row">
    	<div class="col-md-12">
            <p class="text-right" style="font-size:12px;">
                Versión 4.2<br>
                Autorizada mediante resolución Nº DGT-R-48-2016 del 7 de octubre de 2016<br>
            </p>
        </div>
    </div>
        
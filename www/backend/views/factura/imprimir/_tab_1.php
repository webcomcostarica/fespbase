 <?php 
 use yii\helpers\Html;
 use backend\models\DgtFe;
 use yii\widgets\ActiveForm;
 use yii\widgets\DetailView;
 use common\models\AccessHelpers;
 use common\components\FuncionesDGT;
 use backend\models\SisConfiguraciones;
 use backend\models\CodigosComprobantes;
 ?>
 <div class="col-xs-12 col-sm-12 col-md-12">
                <?php if(isset($Numeracion)){
                        echo DetailView::widget([
                            'model' => $Numeracion,
                            'attributes' => [
                                'clave',
                                'numConce',
                                'envioEstado',
                                'envioMensaje',
                                'estadoEstado',
                                'estadoMensaje',
                            ],
                        ]);?>

              
             <?php
                    if(
                        $Numeracion->getClase() == 0 || //No se ha enviado
                        $Numeracion->getClase() == 500 || //No recibido por hacienda
                        isset($_GET['all'])             //todas las opciones
                    ){
                         echo Html::a('Enviar de nuevo',
                                '/factura/enviar-comprobante-generado', 
                                [
                                'class' => 'btn btn-primary',
                               
                                'data' => [
                                            'method' => 'post',
                                            'params' => ['id' => $model->id],                                               
                                        ]]
                            );
                    }
                    
                    if(
                        $Numeracion->envioEstado == 202 || //recibido
                        isset($_GET['all'])
                    ){
                        if(
                            $Numeracion->envioEstado == 202 || 
                            $Numeracion->estadoEstado == 0 || 
                            isset($_GET['all'])
                            )
                                echo Html::a('Consultar Estado',
                                    '/factura/estado?id='.$model->id, [
                                    'class' => 'btn btn-primary',
                                ]);  

                       /* if(!empty($model->idColaboradors)){
                            if($Numeracion->estadoEstado == 1 || isset($_GET['all']))
                                echo Html::a('Enviar datos al emisor',
                                    '/factura/envidar-receptor?id='.$model->id, [
                                    'class' => 'btn btn-info',
                                ]);        
                        }
                    */
                        if(AccessHelpers::getAcceso('factura-nota-debito'))
                        echo Html::a('Nueva Nota Débito Cliente <span class="glyphicon glyphicon-plus"></span>',
                            '/factura/nota-debito?id='.$model->id, [
                            'class' => 'btn btn-success',
                            'style' => 'float: right',
                            'data' => [
                                'confirm' => '¿Desea aumentar la cantidad que cobras a este cliente?'
                            ]
                            
                        ]);
                        
                        if(AccessHelpers::getAcceso('factura-nota-credito')){
                            $nota = $Numeracion->getNotaCredito();    
                            if(empty($nota)){
                                echo Html::a('Nueva Nota Crédito Cliente <span class="glyphicon glyphicon-minus"></span>',
                                        ['/factura/nota-credito'],
                                        [
                                            'data' => [
                                                'method' => 'post',
                                                'params' => ['id' => $model->id],
                                                'confirm' => '¿Desea reducir la cantidad que cobras a este cliente?'
                                            ],
                                            'class' => 'btn btn-danger',
                                            'style' => 'float: right',
                                        ]
                                    );
                            }else{ ?>
                            
                                <?php
                                echo Html::a('Consultar nota de anulación <span class="glyphicon glyphicon-eye-open"></span>',
                                        ['/notacredito/imprimir'],
                                        [
                                            'data' => [
                                                'method' => 'get',
                                                'params' => ['id' => $nota->id],
                                            ],
                                            'class' => 'btn btn-warning',
                                            'style' => 'float: right',
                                        ]
                                    );
                                ?>
                               
                                <?php

                            } 
                        }                            
                    }
                       
                }         
            ?>

            </div>
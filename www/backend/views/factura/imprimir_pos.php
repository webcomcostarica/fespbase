<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\DgtFe;
use yii\widgets\DetailView;
use common\models\AccessHelpers;

use backend\models\SisConfiguraciones;
$configuraciones = SisConfiguraciones::getParametrosGenerales();
use backend\models\CodigosComprobantes;
use common\components\FuncionesDGT;
$Numeracion = $model->comprobante;

?>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12"> 

        <?php if (AccessHelpers::getAcceso('factura-imprimir-pos')) { ?>
            <!--<a class="btn btn-warning" href="/factura/imprimir-pos?id=<?= $model->id; ?>">Imprimir</a>
        --><?php } ?>
        </div>

    <div id="factura_pos">
                   


  <h2>Factura Electrónica</h2>
        <h5 class=""><?= $configuraciones['NombreComercial']; ?> <?= $configuraciones['Cedula']; ?></h3>
            <p><?= $configuraciones['OtrasSenas'] ?><br>
                <b>Correo: </b> <?= $configuraciones['Correo'] ?><br>
                <b>Teléfono: </b> <?= $configuraciones['Telefono'] ?><br>
            </p>           
        <hr>
        
          <?php if($configuraciones['ClientePorDefecto'] != $model->idColaboradors[0]->id){ ?>
            
            <strong>Receptor</strong><br>
            <?php
                foreach ($model->idColaboradors as $data) {
                    $ced = ($data->cedula != $configuraciones['ClientePorDefecto'] ) ? $data->cedula : '';
                    echo $ced .' '. $data->getNombreCompleto().'<br>';
                    echo $data->getDireccion();
                } ?>
            <?php  }
            if(!empty($model->receptorRapido)){ ?>
                <div class="row">
    			<div class="col-xs-12 col-md-6 col-sm-6">
    				<address>
    					<strong>Receptor</strong><br>
    					<?php
                            echo $model->receptorRapido.'(Ete comprobante no es deducible)';
                        ?>
    				</address>
    			</div>
    		</div>
                <?php
            }
            ?>  
            
        <address>
            <b>Numeración</b><br>
            Consecutivo: <br>
            <?= $model->comprobante->numConce; ?><br>
            Clave: <br>
            <span style="font-size:12px;"><?= $model->comprobante->clave; ?></span><br>
        </address>
               
        <address>
            <strong>Emisión:</strong><br>
            <?= $model->fecha; ?><br>
            <b>Pago:</b><br>
            Condición: <?= CodigosComprobantes::CONDICIONES[$model->dgt_condicion]; ?><br>
            Medio: <?= CodigosComprobantes::MEDIOSDEPAGO[$model->dgt_medioPago]; ?><br>
            Terminal <?= $configuraciones['Terminal'] ?> Sucursal <?= $configuraciones['Sucursal'] ?>    
        </address>


       
        
    
        <h3 class="panel-title" style="color:white;"><strong>Detalles</strong></h3>
        <table class="table table-condensed">
            <thead>
                <tr>
                    <td class="text-center"><strong>Cantidad</strong></td>
                    <td class="text-center"><strong>Precio</strong></td>
                    <td class="text-right"><strong>Monto</strong></td>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($model->detalleFacturas as $data) {
                    $codigo = '';
                    if($data->lote){
                        $codigo = $data->lote0->cod_inte_producto.' ';
                    }
                    ?>
                    <tr >
                        <td colspan="3"><?php echo $codigo.$data->descripcion; ?></td>  
                    </tr>
                    <tr>               
                        <td class="text-center"><?php echo $data->cantidad . ' ' . $data->unidad; ?></td>
                        <td class="text-center"><?= $model->moneda($data->lote_precio); ?></td>
                        <td class="text-right"><?= $model->moneda($data->montoTotalLinea) ?></td>
                    </tr> <?php                       
                } ?>
                <tr>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                </tr>
                
            </tbody>
        </table>
        <br>
        <table class="table table-condensed">
            <tbody>
            <?php
                if ($model->dgt_moneda !== 'CRC') { 
                    echo $this->render('documento_pdf_fila_total', [
                        'titulo' => 'Moneda',
                        'valor' => $model->dgt_moneda
                        ]);
                     echo $this->render('documento_pdf_fila_total', [
                        'titulo' => 'Cambio',
                        'valor' => $model->dgt_tipoCambio
                        ]);
                ?>                         
            <?php }
            if ($model->dgt_totExento != 0) {
                echo $this->render('documento_pdf_fila_total', [
                        'titulo' => 'Tot Exento',
                        'valor' => $model->moneda($model->dgt_totExento)
                        ]);
            }
            
            echo $this->render('documento_pdf_fila_total', [
                'titulo' => 'Sub total',
                'valor' => $model->moneda($model->dgt_totVenta)
            ]);
                        
            if($model->dgt_totDescuento != 0)            
            echo $this->render('documento_pdf_fila_total', [
                'titulo' => 'Descuentos',
                'valor' => $model->moneda($model->dgt_totDescuento)
            ]);
            if ($model->dgt_totImpuesto != 0) {
                echo $this->render('documento_pdf_fila_total', [
                    'titulo' => 'Tot Impuesto',
                    'valor' => $model->moneda($model->dgt_totImpuesto)
                ]); 
            } 
            
             echo $this->render('documento_pdf_fila_total', [
                'titulo' => 'TOTAL',
                'valor' => $model->moneda($model->dgt_total)
            ]);
            
            ?> 
            </tbody>
        </table>
  
        <div class="col-md-12">
            <p class="text-right" style="font-size:12px;">
                Versión 4.2<br>
                Autorizada mediante resolución Nº DGT-R-48-2016 del 7 de octubre de 2016<br>
            </p>
        </div>
    </div>
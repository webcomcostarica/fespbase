<?php

use common\models\AccessHelpers;

$factura_en_proceso = ($model->estado == "Procesando") ? true : false;
$inventario = AccessHelpers::getAcceso('lote-index');
?>


    <?php
        echo $this->render('/factura/factura/detalles/03_detallesFormulario',[
            'factura_en_proceso' => $factura_en_proceso,
            'inventario' => $inventario,
            'model' => $model,
            'configs' => $configs
        ]);
    ?>



<table  style="width:100%" class="table table-striped table-bordered">
    <?php 
        echo $this->render('/factura/factura/detalles/01_encabezado',[
            'factura_en_proceso' => $factura_en_proceso,
            'inventario' => $inventario
        ]);
    ?>
    <tbody id="tbl_detalle">
    <?php
         echo $this->render('/factura/factura/detalles/02_detallesActuales',[
            'factura_en_proceso' => $factura_en_proceso,
            'inventario' => $inventario,
            'model' => $model
        ]);
    ?>
    </tbody>
</table>

 
<table  style="width:100%" class="table table-striped table-bordered">   
    <table class="table table-results" >
        <tbody>
<?php if ($model->dgt_moneda !== 'CRC') { ?>
                <tr>
                    <td class="success" colspan="2">MONEDA</td>
                    <td><?= $model->dgt_moneda; ?></td>
                </tr>
                <tr>
                    <td class="success" colspan="2">CAMBIO</td>
                    <td><?= $model->dgt_tipoCambio; ?></td>
                </tr>                            
            <?php
            }/*
              if ($model->dgt_serviciosGravados != 0) {
              ?>
              <tr>
              <td class="success" colspan="2">TOTAL SERVICIOS GRAVADOS</td>
              <td><?= moneda($model->dgt_serviciosGravados); ?></td>
              </tr>
              <?php }
              if ($model->dgt_serviciosExentos != 0) {
              ?>
              <tr>
              <td class="success" colspan="2">TOTAL SERVICIOS EXENTOS</td>
              <td><?= moneda($model->dgt_serviciosExentos); ?></td>
              </tr>
              <?php }
              if ($model->dgt_mercaGravada != 0) {
              ?>
              <tr>
              <td class="success" colspan="2">TOTAL MERCANCIAS GRAVADAS</td>
              <td><?= moneda($model->dgt_mercaGravada); ?></td>
              </tr>
              <?php }
              if ($model->dgt_mercaExenta != 0) {
              ?>
              <tr>
              <td class="success" colspan="2">TOTAL MERCANCIAS EXENTAS</td>
              <td><?= moneda($model->dgt_mercaExenta); ?></td>
              </tr>
              <?php } */
            if ($model->dgt_gravado != 0) {
                ?>

                <tr>
                    <td class="success" colspan="2">TOTAL GRAVADO</td>
                    <td><?= $model->moneda($model->dgt_gravado); ?></td>
                </tr>
<?php
}
if ($model->dgt_totExento != 0) {
    ?>
                <tr>
                    <td class="success" colspan="2">TOTAL EXENTO</td>
                    <td><?= $model->moneda($model->dgt_totExento); ?></td>
                </tr>   <?php
}
?>
<!--
            <tr>
                <td class="success" colspan="2">TOTAL VENTA</td>
                <td><?= $model->moneda($model->dgt_totVenta); ?></td>
            </tr>-->
            <?php if($model->dgt_totDescuento > 0){ ?>
            <tr>
                <td class="success" colspan="2">DESCUENTOS</td>
                <td><?= $model->moneda($model->dgt_totDescuento); ?></td>
            </tr>
            <?php
            }
            /*
              if ($model->dgt_totVentaNeta != 0) { ?>
              <tr>
              <td class="success" colspan="2">TOTAL VENTA NETA</td>
              <td id="idSub"><?= moneda($model->dgt_totVentaNeta); ?></td>
              </tr>
              <?php } */
            if ($model->dgt_totImpuesto != 0) {
                ?>
                <tr>
                    <td class="success" colspan="2">TOTAL IMPUESTO</td>
                    <td id="idSub"><?= $model->moneda($model->dgt_totImpuesto); ?></td>
                </tr>  
<?php } ?>
            <tr>
                <td class="success" colspan="2">TOTAL</td>
                <td id="idSub"><?= $model->moneda($model->dgt_total); ?></td>
            </tr>
            <!-- TODO Impuesto General -->                    

           
            <tr>
                <td></td>
                <td></td>
            </tr>
        </tbody>                       
    </table>	
</tbody>
</table>


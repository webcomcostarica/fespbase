 <?php 
 
use yii\helpers\ArrayHelper;
use common\models\User;


	if ($model->estado == "Procesando") {  	    
	$usuarios = User::find()->where(['status'=>'1'])->all();
    if($config['facturacion_vendedor'] != 'hidden'){
	?>
     <h3>Vendedor</h3>
	<?php 
    }
			echo $form->field($model, 'idUsua')
				->dropDownList(
				ArrayHelper::map($usuarios,'id','username'),           
				[
				'prompt'=>'Vendedor',
				'onChange'=>'factura_actualizar(this)',
                'class' => $config['facturacion_vendedor']
				]    
				)->label(false);		
	}
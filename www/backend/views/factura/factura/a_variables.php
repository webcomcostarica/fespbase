<?php
use yii\helpers\Html;
use backend\models\SisConfiguraciones;
$configuraciones = SisConfiguraciones::getParametrosGenerales();
?>
<script>
	var id_factura_actual=<?php echo $model->id; ?>;
	var img_esperando = '<p style="text-align: center;"><?php echo Html::img('/img/esperando.gif', ['alt' => "esperando"]) ?></p>';
    var total_facturado = <?php echo $model->dgt_total; ?>;
</script>

 <?php
 
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\models\Colaborador;

$correo = $colaborador = $idReceptor = '';
    
if(isset($model->facturaColaboradores[0])){
    $colaborador = $model->facturaColaboradores[0];    
    $idReceptor = $colaborador->id_colaborador;
    $correo = $colaborador->idColaborador->correo;
} 

 ?>
 <h3>Receptor</h3>
   <ul class="nav nav-tabs">             
        <li class="active">
            <a 
            href="#panel-1" 
            data-toggle="tab"   
            aria-expanded="false">Buscar</a>
        </li>
        <li class="">
            <a 
            href="#panel-2" 
            data-toggle="tab"
            aria-expanded="false">Rápido</a>
        </li>  
        <li class="">
            <a 
            href="#panel-3" 
            data-toggle="tab"
            aria-expanded="false">Notificar</a>
        </li> 
    </ul>
    
     <div class="tab-content">
        <div class="tab-pane active" id="panel-1">
        
        <table style="width:100%" class="table table-striped table-bordered">
             
           
            <tfoot id="tbl_cola_buscar" >
                <?php if ($model->estado == "Procesando") { ?>
             
                        
                     
                        <tr>
                            <td>                                 
                                <?php 
                                 echo Select2::widget([
                                    'name' => 'Factura[receptor-nombre]',
                                    'value' => $idReceptor,
                                    'data' => ArrayHelper::map(
                                            Colaborador::find()->all(), 
                                            'id', 
                                            'NombreCompleto'
                                    ),
                                    'options' => [
                                        'placeholder' => 'Receptor',
                                        'onChange' => 'colaborador_cedula_nuevo("id")',
                                        'id'=>'receptor-nombre'
                                    ],
                                    'pluginOptions' => [
                                        'tags' => true,
                                        'tokenSeparators' => [',', ' '],
                                        'maximumInputLength' => 10
                                    ],
                                ]);
                                ?>
                                </td>
                                <td style="width:25%;">
                                
                                <?php if($idReceptor != '' && $idReceptor != $config['ClientePorDefecto']){ ?>
                                
                                    <a 
                                        id="btn_modificar_receptor_venta" 
                                        type="button" 
                                        class="btn btn-success" 
                                        title="Actualizar cliente" 
                                        href="/colaborador/update?id=<?= $idReceptor ?>&venta=<?= $model->id ?>">
                                            <i class="fas fa-user-edit"></i>
                                    </a>
                                        <?php } ?>
                                    <a 
                                        id="btn_buscar_receptor_venta" 
                                        type="button" 
                                        class="btn btn-success" 
                                        title="Buscar cliente" 
                                        href="/colaborador/index?venta=<?= $model->id ?>&anterior=<?= $idReceptor ?>">
                                            <i class="fas fa-search"></i>
                                </a>
                                
                                </td>
            
                                </tr>
                              


                        <tr id="tr_btn_more">
                            <td>
                            <div class="input-group">
                              <input 
                              onkeyup="activar_btn_cambiar()"
                              id="colaborador-cedula" 
                              type="text" 
                              class="form-control" 
                              placeholder="Cédula">
                              <span class="input-group-btn">
                                    <button 
                                    id="btn_cambiar_cedula"
                                    disabled="true"
                                    class="btn btn-info" 
                                    onclick="colaborador_cedula_nuevo('cedula')" 
                                    type="button">Cambiar</button>
                              </span>
                            </div>
                          </td>                          
                        </tr>
                        
<tr id="tr_titulo_resultado_colaborador" class="<?= ($idReceptor != 0) ? 'hidden':''  ?>">
    <td><b>Resultado</b></td>
</tr>                         
                           <tr id="tr_cuerpo_resultado_colaborador" class="<?= ($idReceptor != 0) ? 'hidden':''  ?>">                        
                        
                            <td>
                            <div class="well well-lg" id="span_resultado" >                                
                                
                            </div>
                                
                            <button 
                                    class="btn btn-success" 
                                    type="button"
                                    id='btn-aceptar-receptor'
                                    onclick="colaborador_aceptar();">
                                        <span class="glyphicon glyphicon-ok"></span> Aplicar
                            </button>
                                   
                            </td>
                        </tr>
 
                        
                                
                <?php } ?>                       
            </tfoot>
        </table>
        
        </div>   
        <div class="tab-pane" id="panel-2">
        
         <table style="width:100%" class="table table-striped table-bordered">
             
            <tbody>		  			
               
                        <tr>
                            <td colspan="2">

                                <div class="input-group full-width">
                                    <span class="input-group-addon warning"><i class="glyphicon glyphicon-user"></i></span>
                                    <input 
                                    onkeyup="factura_actualizar(this)"
                                    type="text" 
                                    id="col_receptor_rapido" 
                                    value="<?= $model->receptorRapido ?>"
                                    class="form-control has-warning"  
                                    placeholder="Nombre Completo">
                                    </div>
  <div class="hint-block success">Anotar solo el nombre sin cédula, puede generar problemas de recepción en el gasto.</div>
                                
                            </td>
                        </tr>
            </tbody>
            
        </table>
        
        </div>
      <div class="tab-pane" id="panel-3">
        <div class="input-group">
          <span class="input-group-addon" id="basic-addon1">@</span>
          <input 
                                  id="colaborador-correo" 
                                  type="text" 
                                  class="form-control" 
                                  placeholder="Correo"
                                  value="<?= $correo ?>"
                                  onChange="actualizar_correo(this)"
                                  > 
          <!--                        
          <input 
                value="<?= $correo ?>" 
                id="receptor-correo" 
                type="email" class="form-control" 
                placeholder="Correo" 
                aria-describedby="basic-addon1"
            >-->
        </div>
        </div>

    </div>
 

    
    
        
        
<?php $this->registerJsFile('/js/colaboradores.js'); ?> 


 <?php
use yii\helpers\Html;
use dosamigos\datepicker\DatePicker;
use backend\models\CodigosComprobantes;
?>

<div class="col-lg-4" >
        <h3>Datos de facturación</h3>

        <?php
       
        if ($model->estado == 'Procesando') {
            $label = ($config['facturacion_condiciones']=='hidden')?false:'Condición de venta';
            echo $form
                ->field($model, 'dgt_condicion')
                ->dropDownList(
                CodigosComprobantes::CONDICIONES
                ,
                [
                    'onChange' => 'factura_actualizar();',
                    'class'=>$model->dgt_condicion.' form-control'.' '.$config['facturacion_condiciones']
                ]
                )->label($label);
             
                echo $form
                ->field($model, 'dgt_medioPago')
                ->dropDownList(
                CodigosComprobantes::MEDIOSDEPAGO
                ,
                [
                    'onChange' => 'factura_actualizar();',
                    'class'=>$model->dgt_medioPago.' form-control'
                ]
                );
                
                  echo $form
                ->field($model, 'dgt_moneda')
                ->dropDownList(
                [
                    'CRC'=>'CRC(₡)',
                    'USD'=>'USD($)',
                ],
                [
                    'onChange' => 'factura_actualizar(this);',
                    'class'=>$model->dgt_moneda.' form-control'
                ]
                );
                
            ?>
        <label id="lab_vence" class="lab_aparta"><?php if($model->forma_pago==='04'){
                 $i=strtotime($model->fecha);	
                $fechaFin="";
                $Cantador=45;//Busca desde la fecha inicial hasta mes y medio para calcular el dia de cierre de apartado
                for(;$Cantador>0;$i+=86400){		
                    $fechIndex=date("d-m-Y", $i);
                    $fechaFin=date($fechIndex);//dia de que finaliza en caso de ser jornada Acumulativa
                    $Cantador--;
                }
                echo 'Vence: '.$fechaFin;
                } ?></label>
            <?php
            
        } else {
            ?>
            <table style="width:100%" class="table table-striped table-bordered">                     
                <tbody >		  			
                    <tr >
                        <td>Tipo</td>
                        <td id="impri_tipo_fact"><?= $tipos[$model->tipo]; ?></td>
                    </tr>                   
                    <tr >
                        <td >Forma de pago</td>
                        <td id="impri_forma_pago"><?= $model->forma_pago; ?></td>
                    </tr>                   
                </tbody>                 
            </table>
    <?php } ?>              
    </div>


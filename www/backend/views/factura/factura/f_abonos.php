
<?php if ($model->forma_pago === "Apartado" && $model->estado=="Cobrando" ) { ?>  
    <div class="row">
        <div class="col-lg-12">    
            <h1>Abonos</h1>
             <label id="lab_vence" class="lab_aparta"><?php 
                 $i=strtotime($model->fecha);	
                $fechaFin="";
                $Cantador=45;//Busca desde la fecha inicial hasta mes y medio para calcular el dia de cierre de apartado
                for(;$Cantador>0;$i+=86400){		
                    $fechIndex=date("d-m-Y", $i);
                    $fechaFin=date($fechIndex);//dia de que finaliza en caso de ser jornada Acumulativa
                    $Cantador--;
                }
                echo 'Vence: '.$fechaFin;
                ?></label>
            <br>
            <label id="lab_porce" class="lab_aparta">                
                <?php echo '30%   : '.moneda((30*$model->total)/100); ?>
            </label>
            <table style="width:100%" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th class="success">Tipo</th>  
                        <th class="success">Fecha</th> 
                        <th class="success">Saldo Anterior</th> 
                        <th class="success">Monto</th>
                        <th class="success">Saldo Actual</th>
                        <th class="success"></th> 
                    </tr>
                </thead> 
                <tbody id="tbl_detalle_abonos">	  			
                    <?php foreach ($model->abonos as $abono) { ?>
                        <tr id="tr_abono_<?= $abono->id; ?>">
                            <td ><?= $abono->tipo ?></td>
                            <td ><?= $abono->fecha ?></td> 
                            <td ><?= moneda($abono->saldoAnterior) ?></td> 
                            <td ><?= moneda($abono->monto) ?></td>
                            <td ><?= moneda($abono->saldoActual) ?></td> 
                            <td><?php if ($model->estado !== "Eliminado"){  ?>
                                <button type="button" class="btn btn-danger" title="Quitar" 
                                        onclick="abono_quitar(<?= $abono->id; ?>);">
                                    <span class="glyphicon glyphicon-trash"></span>
                                </button>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
                    <?php if ($model->estado === "Cobrando" && $model->calcSaldo != 0) { ?>
                    <tfoot>
                        <tr>
                            <td>
                                <select id="abo_tipo" class="form-control">
                                     <?php if(count($model->abonos)===0){ ?>
                                        <option value="Prima efectivo">Prima efectivo</option>
                                        <option value="Prima tarjeta">Prima tarjeta</option>
                                    <?php }else{ ?>
                                        <option value="Abono efectivo">Abono efectivo</option>
                                        <option value="Abono tarjeta">Abono tarjeta</option>
                                    <?php } ?>
                                </select>
                            </td>
                            <td><input id="abo_fecha"  value="<?php echo date("Y-m-d"); ?>" type="date" class="form-control" placeholder="Fecha"></td>
                            <td><input id="abo_saldo_anterior" value="<?php echo $model->calcSaldo; ?>" data-monto="<?php echo $model->calcSaldo; ?>" readonly=""  type="number" class="form-control" placeholder="Saldo anterior"></td>
                            <td><input id="abo_monto" type="number" onchange="abono_monto(this);" class="form-control" placeholder="Monto cancelado"></td>
                            <td><input id="abo_saldo_actual" readonly="" type="number" class="form-control" placeholder="Saldo actual"></td>


                            <td id="td_abono_aceptar">
                                <button type="button" class="btn btn-success" title="Aceptar abono" 
                                        onclick="abono_aceptar();">
                                    <span class="glyphicon glyphicon-ok"></span>
                                </button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-danger" title="Limpiar" 
                                        onclick="abono_limpiar();">
                                    <span class="glyphicon glyphicon-trash"></span>
                                </button>
                            </td>
                        </tr>
                    </tfoot>
    <?php } ?>
            </table>
        </div>
    </div>
<?php } ?>

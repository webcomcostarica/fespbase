<?php
foreach ($model->detalleFacturas as $key => $data) { ?>
    <tr id="tr_lote_<?= $data->id; ?>">
        <?php if ($factura_en_proceso && $inventario) { 
            $codigo = $data->lote;
            if ($data->lote != 0) {
                $codigo = $data->lote0->cod_inte_producto;
            }
        ?>
            <td><?= $codigo ?></td>
            <td><?= $data->descripcion ?></td>
        <?php
        } else {
            if ($data->lote != 0) {
                $codigo = $data->lote0->cod_inte_producto;
                ?> 
                <td><?php echo 'Lot ' . $codigo . ' / ' . $data->descripcion ?></td>				
            <?php } else { ?>
                <td><?php echo $data->descripcion; ?></td>					
            <?php } 
            } ?> 
        <td><?php echo $data->cantidad; ?></td>				
        <td><?php echo $data->unidad; ?></td>				

        <td><?= $model->moneda($data->lote_precio); ?></td>
        <!--<td title="<?= $data->tipoDescuento ?>"><?= $model->moneda($data->montoDescuento) ?></td>
        --><td><?= $model->moneda($data->impuesto); ?></td>
        <td><?= $model->moneda($data->montoTotalLinea) ?></td>
        <?php if ($factura_en_proceso) { ?>
            <td id="tr_lote_borrar_<?= $data->id; ?>">
                <button type="button" class="btn btn-danger" onclick="detalle_quitar(<?= $data->id ?>);">
                    <span class="glyphicon glyphicon-trash"></span>
                </button>
            </td>
        <?php } ?>
    </tr> <?php }
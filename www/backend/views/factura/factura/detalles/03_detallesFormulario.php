 <?php if ($factura_en_proceso) { ?>
 
 <?php if ($inventario) { ?> 

    <h3>Agregar detalles</h3>
    <table  style="width:100%" class="table table-striped table-bordered">
    <tbody>
            <tr>
                <td colspan="8">
                    <span><b>Agregar un producto desde el inventario</b></span>
                </td>
            </tr>
            <tr>
                <td>
					<input
						id="inp_cod_barras" 
						type="checkbox" 
						name="vehicle1" 
						value="1"> Lector de barras                   
                </td>
                <td>
                <input 
                id="inp_cod_barras_id" 
                type="text"  
                class="form-control" 
                placeholder="Cod Barras">
                </td>
				
            </tr>
			
			<tr>
                <td>
                    <input 
                    id="inp_cod_int_lot" 
                    type="number" class="form-control" 
                    placeholder="Cod Interno"
                    disabled="true" readonly='true'
                    >
                </td>
                <td>
                <input 
                id="inp_cod_bar_lot" 
                type="text"  
                class="form-control" 
                placeholder="Cod Barras">
                </td>
                <!--
                <td><input id="inp_cod_pro_lot" disabled="true" readonly='true' type="text"  class="form-control" placeholder="Cod Proveedor"></td>
               --><td>
                     <a 
                        id="btn_modificar_lote_venta"
                        type="button" 
                        class="btn btn-success" 
                        title="Actualizar producto" 
                        href="/lote/update?id=12&venta=<?= $model->id ?>">
                            <span class="glyphicon glyphicon-pencil"></span>
                    </a>
                    <span id="spa_buscando_lote"></span>
               </td>
            </tr>
            
            <tr>
                
                <?php if(isset($_SESSION['loteSet'])){ ?>
                    <script>
                        var loteSet = <?= $_SESSION['loteSet'] ?>                        
                    </script>
                <?php 
                $_SESSION['loteSet'] = null;
                } ?>

                <td colspan="2"><input id="inp_des_lot" type="text" class="form-control" placeholder="Descripcion"></td>
                <td><input id="inp_can_lot" type="number" class="form-control" placeholder="Cantidad"></td>
                <td><input id="inp_cos_lot" type="text"  class="form-control" placeholder="Precio"></td>

                <td><input id="inp_desc_lot" type="text" class="form-control" placeholder="Descuento"></td>
                <td><label id="inp_imp_lab">IVA</label>
                    <input id="inp_imp_lot" type="text" readonly="true" class="hidden">
                </td>
                <td id="td_detalle_aceptar_id">
                    <button type="button" 
                    class="btn btn-success" 
                    title="Aceptar actual" 
                    onclick="detalle_aceptar(0);">
                        <span class="glyphicon glyphicon-ok"></span>
                        </button></td>
                <td ><button type="button" class="btn btn-danger" title="Limpiar" onclick="detalle_limpiar(0); $('#inp_cod_int_lot').val('');"><span class="glyphicon glyphicon-trash"></span></button></form></td>
            </tr>
            <tr id="tr_agregar_producto_nuevo">
                <td colspan="8">
                    <span><b><a href="/lote/create/?id=0&venta=<?= $model->id; ?>"><span class="glyphicon glyphicon-plus"></span> Agregar producto nuevo</a></b></span><br>
                    <span><b><a href="/lote/index/?id=0&venta=<?= $model->id; ?>"><span class="glyphicon glyphicon-search"></span> Busqueda avanzada </a></b></span>
                </td>
            </tr>

    </tbody>
</table>
 <?php } ?>
 
  <table  style="width:100%" class="table table-striped table-bordered">
    <tbody>
            
            <tr>
                <td colspan="8">
                    <span><b>Nuevo detalle personalizado</b></span>
                </td>
            </tr>
            
            <tr>  

                    
                <td colspan="8">
                    <input id="inp_des_lot_1" type="text" class="form-control"  
                    placeholder="Descripcion">
                </td>
            </tr>
            <tr>
                <td>
                    <input value="1" id="inp_can_lot_1" type="number" class="form-control"  placeholder="Cantidad"></td>
                <td colspan="2">
                    <select id="inp_uni_lot_1" class="form-control" name="Lote[unidad]">
                        <option value="Unid" selected>Unidades</option>
                        <option value="kg" >Kilogramos</option>
                        <option value="Sp"  >Serv Profesionales</option>                        
                    </select>
                    <!--
                        Sp, m, kg, s, A, K, mol, cd, m², m³, m/s, m/s², 1/m, kg/m³, A/m², A/m, mol/m³, cd/m², 1, rad, sr, Hz, N, Pa, J, W, C, V, F, ?, S, Wb, T, H, °C, lm, lx, Bq, Gy, Sv, kat, Pa·s, N·m, N/m, rad/s, rad/s², W/m², J/K, J/(kg·K), J/kg, W/(m·K), J/m³, V/m, C/m³, C/m², F/m, H/m, J/mol, J/(mol·K), C/kg, Gy/s, W/sr, W/(m²·sr), kat/m³, min, h, d, º, ´, ´´, L, t, Np, B, eV, u, ua, Unid, Gal, g, Km, ln, cm, mL, mm, Oz, Otros
                    -->
                </td>
                <td>
                    <input 
                    id="inp_cos_lot_1" type="number" class="form-control"  
                    placeholder="Costo"></td>
                <td class="" >
                    <input id="inp_desc_lot_1" value="0" type="text" class="form-control"  placeholder="Descuento"></td>                       
                <td class="">					
                    <select id="inp_imp_lot_1" class="form-control" name="Lote[unidad]">
                        <option value="E" selected>Exento</option>
                        <option value="G" >Gravado</option>
                    </select>					
                </td>
                <td id="td_detalle_aceptar_id_uno"><button type="button" class="btn btn-success" title="Aceptar actual" onclick="detalle_aceptar(1);"><span class="glyphicon glyphicon-ok"></span></button></form></td>
                <td><button type="button" class="btn btn-danger" title="Limpiar" onclick="detalle_limpiar(1);"><span class="glyphicon glyphicon-trash"></span></button></td>
            </tr>
            
    </tbody>
</table>
            <?php } ?>
            
<?php 
    $this->registerJsFile(
    '@web/js/facturacion_detalles.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
<?php
use yii\helpers\Html;
?>
<div class="row">
    <!-- Si se esta procesando NO ES es un apartado -->
    <!-- Si se esta procesando ES es un apartado -->
<?php if ($model->estado == "Procesando") { ?>
        <?php
        $classes = ($model->forma_pago !== "Apartado") ? 'btn btn-success' : 'btn btn-success hidden';
       
        echo Html::a(Yii::t('backend', 'Facturar'),
                        ['/factura/facturar'],
                        [
                            'data' => [
                                'method' => 'post',
                                'params' => ['id' => $model->id],
                            ],
                            'class' => $classes,
                            'id' => 'btn_facturar',  
                            'confirm' => Yii::t('backend', '¿Desea aplicar este comprobante?'),                           
                        ]
                    );
        ?>

       
        <?=
        Html::a(Yii::t('backend', 'Eliminar'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', '¿Desea anular esta venta?'),
            ],
        ])
        ?>
        <?php 
               $url = '/factura/generar-pdf-id?id='.$model->id;
       ?>
        <a 
        target="_blank" 
        href="<?= $url ?>" 
        class="btn btn-info pull-right">Imprimir en A4</a>
        <br>
        <p><b>Nota:</b> Esta factura aun se procesa, entonces se puede eliminar</p>



<?php } ?>
    
<?php if ($model->estado == "Facturada") { 
    if(Yii::$app->user->identity){
    if(Yii::$app->user->identity->rol_id===3){
        echo Html::a(Yii::t('backend', 'Vista detallada'), ['view', 'id' => $model->id], [
            'class' => 'btn btn-warning',
        ]);
    }}
        ?>
        <button type="button" class="btn btn-warning" title="Imprimir" onclick="factura_imprimir('ventanormal');">
            <span class="glyphicon glyphicon-print"></span>
        </button>
<?php } ?> 
        
<?php if ($model->estado == "Cobrando") {
        if($model->calcSaldo<=0){
              echo Html::a(Yii::t('backend', 'Facturar'), ['facturar', 'id' => $model->id], [
                'class' => 'btn btn-success',
                'id' => 'btn_facturar',
                'title' => 'Cobrar esta factura y REBAJAR existencias del INVENTARIO',
                'data' => [
                    'confirm' => Yii::t('backend', '¿Desea cobrar esta factura y REBAJAR existencias del INVENTARIO?'),
                ],
            ]);
        }
        if(Yii::$app->user->identity){
    if(Yii::$app->user->identity->rol_id===3){
        echo Html::a(Yii::t('backend', 'Vista detallada'), ['view', 'id' => $model->id], [
            'class' => 'btn btn-warning',
        ]);
        }}
        ?>
        <button type="button" class="btn btn-danger" title="Eliminar este apartado" onclick="factura_eliminar(<?= $model->id; ?>);">
            <span class="glyphicon glyphicon-trash"></span>
        </button>
        <button type="button" class="btn btn-warning" title="Imprimir" onclick="factura_imprimir('ventaapartado');">
            <span class="glyphicon glyphicon-print"></span>
        </button>
<?php } ?> 
</div>     


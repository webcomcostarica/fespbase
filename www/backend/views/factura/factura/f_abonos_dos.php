
    <div class="row">
        <div class="col-lg-12">    
            <h1>Abonos</h1>
            <table style="width:100%" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th class="success">Tipo</th>  
                        <th class="success">Fecha</th> 
                        <th class="success">Saldo Anterior</th> 
                        <th class="success">Monto</th>
                        <th class="success">Saldo Actual</th>
                    </tr>
                </thead> 
                <tbody id="tbl_detalle_abonos">	  			
                    <?php foreach ($model->abonos as $abono) { ?>
                        <tr id="tr_abono_<?= $abono->id; ?>">
                            <td ><?= $abono->tipo ?></td>
                            <td ><?= $abono->fecha ?></td> 
                            <td ><?= $abono->saldoAnterior ?></td> 
                            <td ><?= $abono->monto ?></td>
                            <td ><?= $abono->saldoActual ?></td>                             
                        </tr>
                    <?php } ?>
                </tbody>
                    
            </table>
        </div>
    </div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use backend\models\Factura;
use backend\models\CodigosComprobantes;

$this->title = 'Facturas';
$this->params['breadcrumbs'][] = [
    'label' => $this->title,
    'url' => ['index', 'sort' => '-id']
];
?>
<div class="factura-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?=
        Html::a(Yii::t('backend', 'Crear Factura'), ['create'], [
            'data' => [
                'method' => 'post',
                'params' => [
                    'tipo' => 'venta'
                ],
            ],
            'class' => 'btn btn-success'
                ]
        );
        ?>
        
    </p>
    <?php $ftt = Yii::$app->formatter; ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'label' => 'Cod interno',
                'contentOptions' => ['style' => 'width: 7%;text-align:left'],
            ],
          
            [
                'label' => 'Colaborador',
                'format' => 'raw',
                'value' => function($model) {
                    foreach ($model->idColaboradors as $data) {
                        if($data->id === 3){
                            return 'Anónimo';
                        }else{
                            $nombre = $data->nombre . ' ' . $data->apellido . ' ' . $data->sapellido;
                            if($data->tipoIdentificacion == 2)
                                $nombre = $data->nombreCompleto;
                            return '<a  href="/colaborador/view?id=' . $data->id . '">' . $nombre . '</a>';
                        }
                    }
                },
                'attribute' => 'colaborador_nombre'
            ],
            [
                'label'=>'Concecutivo',
                'format'=>'raw',
                'value' => function($model){
                        if(!empty($model->comprobante)){
                            return $model->comprobante->numConce;
                        }
                        return '...';
                },
                'attribute' => 'concecutivo'
            ],
            [
                'label' => 'FE Envio',
                'contentOptions' => function($model) {
                    $clases = [
                        100 => 'info',
                        200 => 'success',
                        300 =>'warning', 
                        400 => 'warning',
                        500  => 'danger', 
                        0  => 'warning', 
                    ];
                    $id = (!empty($model->comprobante)) ? $model->comprobante->getClase() : 0;
                    
                    return [
                        'class' => $clases[$id],
                        'code' => $id
                        ];
                },
                        'format' => 'raw',
                        'value' => function($model) {
                            if ($model->estado == Factura::Facturada || $model->estado == Factura::Anulada) {
                                if (!empty($model->comprobante)){
                                    if($model->comprobante->envioEstado != 0)
                                        return $model->comprobante->envioEstadoL;
                                }
                                return Html::a('Enviar', ['/factura/enviar-comprobante-generado'], [
                                            'data' => [
                                                'method' => 'post',
                                                'params' => ['id' => $model->id,'destination'=>'index'],
                                            ],
                                                ]
                                );
                            }
                            
                    return 'Procesando';
                },
                'attribute' => 'feEnvio'
                    ],
                    [
                        'label' => 'FE Estado',
                        'format' => 'raw',
                        'contentOptions' => function($model) {
                            $clases = [
                            0 => 'warning', 
                            1 => 'success', 
                            2 => 'danger', 
                            3 => 'danger', 
                            800 => 'danger',
                            100 => 'info',
                            200 => 'success',
                            300 =>'warning', 
                            400 => 'warning',
                            500  => 'danger',
                            null => 'warning'
                            ];
                            $id = (!empty($model->comprobante)) ? $model->comprobante->getClaseEstado() : 0;
                            return [
                                'class' => $clases[$id],
                                'code' => $id
                            ];
                        },
                                'value' => function($model) {
                            if (!empty($model->comprobante))
                                return $model->comprobante->estadoEstadoL;
                            return 'Pendiente';
                        },
                        'attribute' => 'feEstado'
                            ],
                            [
                                'label'=>'Correo',
                                'format'=>'raw',
                                'value'=>function($model){                                   
                                    if(!empty($model->comprobante)){
                                         foreach ($model->idColaboradors as $data) {
                                            if($data->id !== 3){                            
                                                if($model->comprobante->correoNotificacion ==1)
                                                    return '<a 
                                                        data-toggle="tooltip"
                                                        title="Correo enviado a '.$model->comprobante->correoNotificado.'" 
                                                        href="/factura/enviar-correo?id='.$model->id.'" 
                                                        class="btn btn-success">
                                                            <span class="glyphicon glyphicon-envelope"></span>
                                                        </a>';
                                                else                                            
                                                    return '<a 
                                                        title="Enviar correo a '.$model->comprobante->correoNotificado.'" 
                                                        href="/factura/enviar-correo?id='.$model->id.'" 
                                                        class="btn btn-warning">
                                                            <span class="glyphicon glyphicon-envelope"></span>
                                                        </a>';
                                            }else{
                                                return 'Anónimo';
                                            }
                                         }
                                    }
                                    return '...';
               
                                }
                            ],   
                            [
                                'label' => 'Detalles',
                                'format' => 'raw',
                                'value' => function($model) {
                                    return Html::a('Ver Detalle <span class="glyphicon glyphicon-eye-open"></span>', ['/factura/comprobante', 'id' => $model->id]
                                    );
                                },
                                    ],
                                ],
                            ]);
                            ?>


                        </div>
<?php
if($config['contingencia'] == 0){ 
    $js = "
        $(document).ready(function () {
            $.ajax({
                
                success: function (data_s) {
                    console.log('Consulta de estados')
                },
                error: function (data) {
                    console.log('ERROR en la consulta de estados')
                },
                type: 'post',
                async: true,
                url: '/factura/estados',
                
            });
        });
    ";     
    $this->registerJs($js, yii\web\View::POS_READY); 
}


<link href="/css/lite_on.css" rel="stylesheet" id="bootstrap-css">
<table style="border-collapse: collapse;" class="borde">
		<tr>
			<td colspan="2" style="font-weight: 700; font-size: 16px; font-family: Cambria, Georgia, serif;">
				<span><?= $configuraciones['NombreComercial']; ?>.</span><br>
				<span>Cédula: <?= $configuraciones['Cedula']; ?>.</span><br>
				<span><?= $configuraciones['OtrasSenas']; ?></span><br>
				<span>Cel. + (506)<?= $configuraciones['Telefono']; ?>.</span><br>
			</td>
			<td style="text-align: right;">
				<img  src="/img/logo.png" width="180px" height="170px">
			</td>
		</tr>
		<tr>
			<td colspan="2" class="borde" style="width:50%; font-weight: 700; font-family: Cambria, Georgia, serif;">
				<span>FACTURA ELECTRONICA Nº</span>				 
			</td>
			<td  class="borde" style="width:50%;  background-color: #E0E6F8;">
				<span><?= $model->comprobante->numConce; ?></span>				
			</td>
		</tr>
		<tr>
			<td colspan="2" class="borde" style="font-weight: 700; font-family: Cambria, Georgia, serif;" >
				<span>CLAVE Nº</span>				 
			</td>
			<td class="borde" style="background-color: #E0E6F8;">
				<span><?= $model->comprobante->clave; ?></span>				
			</td>
		</tr>
       <?php if($configuraciones['ClientePorDefecto'] != $model->idColaboradors[0]->id){ ?>
        <tr>
			<td colspan="2" style="border-right: 1px solid #80FF00; " >
				<?php
					foreach ($model->idColaboradors as $data) { ?>	
						<span>Cliente: </span><span><?= $data->getNombreCompleto() ?></span><br>
						<span>Cédula: </span><span><?= $data->cedula ?></span><br>
						<?php if(!empty($data->telefono)){ ?>				
							<span>Teléfono: </span><span>+ (506) 2231-5450.</span><br>
						<?php }
					}
				?>
						
			</td>
			<td>
				<span>Pagado a: </span><span>LiteON Costa Rica.</span><br>
				<span>Representante: </span><span><?= $configuraciones['NombreComercial']; ?>.</span><br>
			</td>
		</tr>
        
	<?php } ?>
        
	</table>
	

	<table style="border-collapse: collapse; background: url(/img/fondo.JPG) no-repeat; background-position: 50px 50px;" class="borde">
		<tr>
			<td colspan="2" style="font-weight: 700; font-family: Cambria, Georgia, serif; padding-right: 500px;">
				DESCRIPCIÓN
			</td>
			<td style="border-left:1px solid #80FF00;"></td>
		</tr>
	<?php
			foreach ($model->detalleFacturas as $data) { ?>
		<tr>
			<td colspan="2" style="font-weight: 700; font-family: Cambria, Georgia, serif; ">
				
				<b>
					<?php echo $data->descripcion; ?></b>
				
			</td>
			<td class="borde" style="border-top:0;border-bottom:0;padding-right: 10px; text-align: right;">
				<span><?php echo $model->moneda($data->montoTotalLinea); ?>.</span>        
			</td>
		</tr>

			 <?php                       
			} ?>
								
	
		
			
		<tr>
			<td style="border-top:1px solid #80FF00;">
				<span class="styleTransferencia">Transferencia bancaria al</span> 
			</td>
			<td class="borde" style="text-align: right; padding-right: 10px;width: 150px;">
				<span>SUBTOTAL</span>	
			</td>
			<td class="borde totales">
				<span><?= $model->moneda($model->dgt_totVenta) ?>.</span>
			</td>
		</tr>
		<tr >
			<td>
				 <span class="styleTransferencia">número de cuenta del Banco Nacional</span>
			</td>
			<td class="borde" style="text-align: right; padding-right: 10px;width: 150px;">
				<span>DESCUENTOS</span>
			</td>
			<td class="borde totales">
				<span><?= $model->moneda($model->dgt_totDescuento) ?></span>
			</td>
		</tr>
		<tr>
			<td>
				 <span class="styleTransferencia">200-01-038-018782-5</span>
			</td>
			<td class="borde" style="text-align: right; padding-right: 10px;width: 150px;">
				<span>IMPUESTO</span>
			</td>
			<td class="borde totales">
				<span><?= $model->moneda($model->dgt_totImpuesto) ?></span>
			</td>
		</tr>
		<tr>
			<td></td>
			<td class="borde" style="text-align: right; padding-right: 10px;width: 150px;">
				<span>TOTAL</span>
			</td>
			<td class="borde totales">
				<span><?= $model->moneda($model->dgt_total) ?></span>
			</td>
		</tr>
	</table>	
	<table style="border-collapse: collapse; border-top:0;" class="borde">
		<tr>
			<td style="width: 50%; background-color: #E0E6F8; border-right:1px solid #80FF00;">
				<span>Fecha:</span>	<br>
				<span><?= $model->fecha; ?>.</span>
			</td>
			<td colspan="2" style="background-color: #F2F2F2;">
				<span></span><br>
			</td>
		</tr>		
	</table>
	<table style="margin-top: 50px;">
		<tr>
			<td style="text-align: center;font-weight: 700; font-family: Cambria, Georgia, serif; font-size: 15px;">
				Versión 4.2<br>
                Autorizada mediante resolución Nº DGT-R-48-2016 del 7 de octubre de 2016<br>
			</td>
		</tr>
	</table>
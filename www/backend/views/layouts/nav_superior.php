<?php
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;



use common\models\AccessHelpers;

$local = 0;
$clas_nav = 'navbar-inverse navbar-fixed-top';
$label = 'JARVIS ('.Yii::$app->params['cuentaNombre'].')';

if($local==1){
    $clas_nav = 'navbar-default navbar-fixed-top';
    $label = 'JARVIS <span id="brand_tipo">(Local)</span>';
}

            NavBar::begin([
                'brandLabel' => $label,
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => $clas_nav
                ],
            ]);
            $menuItems = [
                ['label' => Yii::t("backend", 'Home'), 'url' => ['/site/index']],
            ];
            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => Yii::t("backend", 'Login'), 'url' => ['/site/login']];
            } else {
                $menuItems[] = [
                    'label' => Yii::t("backend", 'Rigths'),
                    'visible' => AccessHelpers::getAcceso('user-index'),
                    'submenuOptions' => [],
                    'items' => [
                        ['label' => Yii::t("backend", 'Users'), 'url' => ['/user/index'], 'visible' => AccessHelpers::getAcceso('user-index')],
                        ['label' => Yii::t("backend", 'Rols'), 'url' => ['/rol/index'], 'visible' => AccessHelpers::getAcceso('rol-index')],
                        ['label' => Yii::t("backend", 'Operations'), 'url' => ['/operacion/index'], 'visible' => AccessHelpers::getAcceso('operacion-index')],
                    ],
                ];
								
                $menuItems[] = [
                    'label' => Yii::t("backend", 'Facturacion'),
                    'visible' => AccessHelpers::getAcceso('factura-index'),
                    'submenuOptions' => [],
                    'items' => [
                        ['label' => Yii::t("backend", 'Buscar Facturas'), 'url' => ['/factura/index','sort'=>'-id'], 'visible' => AccessHelpers::getAcceso('factura-index')],
                        ['label' => Yii::t("backend", 'Colaboradores'), 'url' => ['/colaborador/index','sort'=>'nombre'], 'visible' => AccessHelpers::getAcceso('factura-index')],
                        [
                            'label' => Yii::t("backend", 'Nueva VENTA'), 
                            'url' => ['/factura/create','tipo'=>'venta'],
                            'type'=>'post',
                            'visible' => AccessHelpers::getAcceso('factura-create')
                        ],
                        [
                            'label' => Yii::t("backend", 'Confirmar Gastos'), 
                            'url' => ['/recibir-factura/index'],
                            'type'=>'post',
                            'visible' => AccessHelpers::getAcceso('recibir-factura-index')
                        ],
                    ],
                ];
                
                $menuItems[] = [
                    'label' => Yii::t("backend", 'Inventario'),
                    'visible' => AccessHelpers::getAcceso('lote-index'),
                    'submenuOptions' => [],
                    'items' => [
                        ['label' => Yii::t("backend", 'Lista'), 'url' => ['/lote/index?id=0','sort'=>'-id'], 'visible' => AccessHelpers::getAcceso('factura-index')],
                        /*['label' => Yii::t("backend", 'Colaboradores'), 'url' => ['/colaborador/index','sort'=>'nombre'], 'visible' => AccessHelpers::getAcceso('factura-index')],
                        [
                            'label' => Yii::t("backend", 'Nueva VENTA'), 
                            'url' => ['/factura/create','tipo'=>'venta'],
                            'type'=>'post',
                            'visible' => AccessHelpers::getAcceso('factura-create')
                        ],
                        [
                            'label' => Yii::t("backend", 'Confirmar Gastos'), 
                            'url' => ['/recibir-factura/index'],
                            'type'=>'post',
                            'visible' => AccessHelpers::getAcceso('recibir-factura-index')
                        ],*/
                    ],
                ];

                $menuItems[] = [
                    'label' => Yii::t("backend", 'Logout') . '(' . Yii::$app->user->identity->username . ')',
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ];
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
            ]);
            NavBar::end();
            ?>

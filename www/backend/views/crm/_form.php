<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use yii\helpers\ArrayHelper;
use common\models\User;
use backend\models\Colaborador;
use kartik\select2\Select2;

?>

<div class="crm-oportunidades-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput()->label('Titulo') ?>

    <?= $form->field($model, 'monto',[
        'template' => '
            {label}
            <div class="input-group">
            <span class="input-group-addon" id="basic-addon1">$</span>
            {input}
            </div>
            {error}
        '
    ])->textInput() ?>
    
    <?= $form->field($model, 'etapa')->dropDownList(
                [
                    0 => 'Nueva',
                    1 => 'Estimar',
                    2 => 'Seguimiento',

                    3 => 'Trabajando',
                    4 => 'Entregar',
                    5 => 'Pendiente',
                ],           
                ['prompt'=>'Seleccione por favor']    
        ); ?>

    <?= $form->field($model, 'detalles')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'prioridad')->dropDownList(
                [
                    0 => '*',
                    1 => '**',
                    2 => '***'
                ],           
                ['prompt'=>'Seleccione por favor']    
        ); ?>

   
     <?php /* $form->field($model, 'estado')->dropDownList(
                [
                    0 => 'Negociando',
                    1 => 'Ganada',
                    2 => 'Perdida'
                ],           
                ['prompt'=>'Seleccione por favor']    
        ); */?>

  
    <?php
        echo Html::label('Cierre estimado');
            echo DatePicker::widget([
                'model' => $model,
                'attribute' => 'cierreProyectado',
                'language' => 'es',
                'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd',
                ]
            ]);
    ?>

    <br>
    <?php
   
    $usuarios = User::find()->where(['status'=>'1'])->all();
     
        echo $form->field($model, 'encargado')
        ->dropDownList(
        ArrayHelper::map($usuarios,'id','username'),           
        [
            'prompt'=>'Encargado'
        ]    
        );
    
        echo Html::label('Cliente');
            echo Select2::widget([
                'name' => 'CrmOportunidades[cliente]',                
                'data' => ArrayHelper::map(Colaborador::find()->where(['tipo'=>1])->all(), 'id', 'nombre'),
                'value' => $model->cliente,
                'options' => ['placeholder' => 'Cliente'],
                'pluginOptions' => [
                    'tags' => true,
                    'tokenSeparators' => [',', ' '],
                    'maximumInputLength' => 10
                ],
            ]);

    ?>
    <br>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <div class="row">
        <div class="col">
            <?= $form->errorSummary($model); ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>

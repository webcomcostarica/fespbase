<?php
use kartik\sortable\Sortable;
use common\components\Moneda;
use yii\web\View;

$this->title = 'Oportunidades';
$this->params['breadcrumbs'][] = ['label' => 'CRM', 'url' => ['/crm/panel']];
//$this->params['breadcrumbs'][] = $this->title;

$tab = (isset($_SESSION['tab_crm'])) ? $_SESSION['tab_crm'] : 1;


?>
   

<div class="row">
    <div class="col-md-10">
        <h1><?= $this->title; ?></h1>
    </div>

    <div class="col-md-2"><br>
        <a class="btn btn-success" href="/crm/create">Nueva oportunidad</a>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li class="<?= ($tab==1)?'active':'' ?>">
                <a onclick="setTab(1)" href="#panel-1" data-toggle="tab"   aria-expanded="false">Nuevas</a>
            </li>                
            <li class="<?= ($tab==2)?'active':'' ?>">
                <a onclick="setTab(2)" href="#panel-2" data-toggle="tab"   aria-expanded="false">Ganadas</a>
            </li>
            <li class="<?= ($tab==3)?'active':'' ?>">
                <a onclick="setTab(3)" href="#panel-3" data-toggle="tab"   aria-expanded="false">Lista</a>
            </li>
        </ul>
    </div> 
</div> 

        <div class="tab-content">
            <?php
                echo $this->render('panel_one',[
                    'datos'=>$datos,
                    'tab' => $tab
                ]);

                echo $this->render('panel_two',[
                    'datos'=>$datos,
                    'tab' => $tab
                ]);
                
                 echo $this->render('panel_tree',[
                    'datos'=>$datos,
                    'tab' => $tab,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider
                ]);
                
                ?>
          
        </div><!--Tab Content-->
<script type="text/javascript">
    
    function actualizar(e){
        let ids = ''
        $(e).children('li').each(function() {
            let oportunidad = $(this).children('.opo_sort')         
            ids = ids.concat( $(oportunidad).data('id')+',' );
        });
       
        ids = ids.slice(0, -1) + '';
        etapa = $(e).data('id')

        $.ajax({
            statusCode: {
                200: function() {
                    location.reload();
                },
                304: function() {
                    console.log('error al modificar');
                }
              },
            type: 'get',
            url: "/crm/cambiar-etapa",
            data: {
                "ids":ids,
                'etapa':etapa
            }
        });


        
   }

</script>
<?php

$this->registerJsFile(
    '@web/js/crm.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
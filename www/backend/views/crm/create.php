<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\CrmOportunidades */

$this->title = 'Create Crm Oportunidades';
$this->params['breadcrumbs'][] = ['label' => 'Crm Oportunidades', 'url' => ['panel']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-oportunidades-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

              
                

        
               
<?php 
use common\components\Moneda;


if(isset($model)){ ?>

            <div class="row opo_sort" data-id="<?= $model->id ?>">
                <div class="col-sm-8">
                   <span id="item_<?= $model->id ?>_nombre"><?= $model->nombre ?></span><span id="item_<?= $model->id ?>_cliente">
                   <br>
                    <a href="/colaborador/view?id=<?= $model->cliente ?>"><?= $model->cliente0->nombre.' '.$model->cliente0->apellido ?></a></span>
                   <br>
                   <span id="item_<?= $model->id ?>_precio">
                     <?= Moneda::gettm($model->monto,'$') ?>
                   </span> 

                   <!--| WP <span class="glyphicon glyphicon-tag"> </span>-->

                   <br>

                   <?php for( $i = 1; $i <= $model->prioridad; $i++){ ?>
                      <a href="javascript:void(0)" onclick="prioridad(<?= $i ?>)"><span class="glyphicon glyphicon-star gold"></span></a>
                  <?php } ?>
                  <?php for($i = $model->prioridad ; $i < 3; $i++){ ?>
                      <a href="javascript:void(0)" onclick="prioridad(<?= $i ?>)"><span class="glyphicon glyphicon-star-empty "></span></a> 
                  <?php } ?>

                   <!-- <span class="glyphicon glyphicon-time warning"></span>-->

                </div>

                <div class="col-sm-4">
                    <div class="pull-right">
                          <button type="button" class="btn  btn-xs" data-toggle="collapse" data-target="#collapseExample_<?= $model->id ?>" aria-expanded="false" aria-controls="collapseExample_<?= $model->id ?>">
                              <span class="glyphicon glyphicon-chevron-down"></span> 
                          </button>
                    </div>
                </div>
              </div>


                <div class="collapse" id="collapseExample_<?= $model->id ?>">
                  <div class="well">
                    
                     <?= $model->detalles ?> 
                
                                           <!--
                        <div class="alert alert-info" role="alert">Información</div>
                        <div class="alert alert-warning" role="alert">Alerta</div>
                        <div class="alert alert-danger" role="alert">Muy encuenta</div>
                    -->
                    <br>
                    <hr><br>
                    <a href="/crm/update?id=<?= $model->id ?>"
                      class="btn btn-primary btn-xs">Modificar</a>
                  </div>
                </div>

<?php } ?>
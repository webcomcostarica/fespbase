<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CrmOportunidadesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Crm Oportunidades';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="tab-pane <?= ($tab==3)?'active':'' ?>" id="panel-3">  

    <div class="row"> 
                    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [           
            'id',
            'nombre:ntext',
            'monto',
            'etapa',
            'detalles:ntext',
            'prioridad',
            // 'orden',
            // 'estado',
            // 'creacion',
            // 'modificacion',
             'cierreProyectado',
            // 'encargado',
             'cliente0.NombreCompleto',

            ['class' => 'yii\grid\ActionColumn',
            'template'=>'{update}'],
        ],
    ]); ?>
</div>
</div>
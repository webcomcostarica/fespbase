<?php
use kartik\sortable\Sortable;
use common\components\Moneda;
use yii\web\View;

$etapas = [
        0=>['titulo'=>'Nueva',      'items'=>[],'total'=>0],
        1=>['titulo'=>'Estimar',   'items'=>[],'total'=>0],
        2=>['titulo'=>'Seguimiento',    'items'=>[],'total'=>0],
     //   3=>['titulo'=>'Entregar','items'=>[],'total'=>0],
    ];


foreach($datos as $oportunidad){
    if($oportunidad->etapa<3){
	    array_push($etapas[$oportunidad->etapa]['items'],
	        [
	            'content'=>$this->render('_ficha_oportunidad',
	                [
	                    'model'=>$oportunidad
	                ]),
	            'id'=>$oportunidad->id
	        ]);

	    $etapas[$oportunidad->etapa]['total'] += $oportunidad->monto;
	}
} ?>


<div class="tab-pane <?= ($tab==1)?'active':'' ?>" id="panel-1">  

                    <div class="row"> 

                    <?php
                        $conta=0;
                        foreach ($etapas as $key=>$etapa) { ?>
                            <div class="col-xs-3">
                            <h3><?= $etapa['titulo'] ?> <span class="label label-info btn-xs"><?= Moneda::gettm($etapa['total'],'$'); ?></span></h3>
                            <?php
                            echo Sortable::widget([
                                'connected'=>true,
                                'items'=>$etapa['items'],
                                'id' => 'opo_sort_'.$key,
                                'options' => [
                                    'data-id'=>$key
                                ],
                                'pluginEvents' => [
                                    'sortupdate' => '
                                    function() { 
                                        actualizar(this);
                                     }',
                                ]
                            ]);?>
                            </div>
                            <?php
                        }
                    ?>                    
                        
                    </div><!--row panel-->

                </div><!--Panel-->
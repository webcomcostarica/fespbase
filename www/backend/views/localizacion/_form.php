<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Localizaciongeografica */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="localizaciongeografica-form">

    <?php $form = ActiveForm::begin(); ?>
	
<div class="row">
    <div class="col-lg-6">
	<?php 
		if($model->errors){
			echo $form->errorSummary($model);
		}
	?>
	
	<?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

	 
    <?= $form->field($model, 'codigo')->textInput() ?>	
	
	
    <?= $form->field($model, 'detalle')->textInput(['maxlength' => true]) ?>
	

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script src="/js/localizaciones.js" type="text/javascript"></script>
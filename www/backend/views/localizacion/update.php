<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Localizaciongeografica */

$this->title = 'Update Localizaciongeografica: ' . ' ' . $model->idlocalizacion;
$this->params['breadcrumbs'][] = ['label' => 'Localizaciongeograficas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idlocalizacion, 'url' => ['view', 'id' => $model->idlocalizacion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="localizaciongeografica-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

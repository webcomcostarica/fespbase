<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Localizaciongeografica */

$this->title = $model->idlocalizacion;
$this->params['breadcrumbs'][] = ['label' => 'Localizaciongeograficas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="localizaciongeografica-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->idlocalizacion], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->idlocalizacion], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idlocalizacion',
            'nombre',
            'codigo',
            'idpadre',
            'detalle',
        ],
    ]) ?>

</div>

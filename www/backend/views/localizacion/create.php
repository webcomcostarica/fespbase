<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Localizaciongeografica */

$this->title = 'Nueva Localización Geográfica';
$this->params['breadcrumbs'][] = ['label' => 'Localizaciongeograficas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="localizaciongeografica-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

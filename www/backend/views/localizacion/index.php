<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\grid\GridView;
use backend\models\Localizaciongeografica;

$this->title = 'Localizaciones Geográficas';
$this->params['breadcrumbs'][] = [
    'label' => 'Lista',
    'url' => ['index', 'sort' => '-id']
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="localizaciongeografica-index">

    <h1><?= Html::encode($this->title) ?></h1>
	
   

  <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [      
         
        [
			'label' => 'Superior',
            'attribute' => 'nivelSuperior',
            'value' => function($model){
                if(isset($model->localizacionsuperior))
                {
                    $nivel = Localizaciongeografica::NIVELES[$model->localizacionsuperior->nivel];
                    $btn = '<a href="/localizacion/index?padre='.$model->localizacionsuperior->idPadre.'" ><span class="glyphicon glyphicon-circle-arrow-left"></span> '.$nivel.'</a>';
                }else
                    $btn = 'N/A';
                return $btn;
            },
            'format'=>'raw'
		 ],
         [
			'label' => 'Tipo',
			'attribute' => 'nivel',
            'filter'=>[
                1=>'País',
                2=>'Provincia',
                3=>'Cantón',
                4=>'Distrito',
            ],
            'value'=>function($model){
                return $model->tipo;
            }
		 ],
         'nombre',
		 [
			'class' => 'yii\grid\ActionColumn',
			'template' => '{hijo} ',
			'buttons'=>[
					'hijo'=>function($url,$model){
                        $nivel = Localizaciongeografica::NIVELES[$model->nivel+1];																							
						$btn = '<a href="/localizacion/index?padre='.$model->idLocalizacion.'" > <span class="	glyphicon glyphicon-circle-arrow-right"></span> '.$nivel.' de '.$model->nombre.'</a>';
					return $btn;						
				},
			]
		],
        [
			'class' => 'yii\grid\ActionColumn',
			'template' => '{update}',
		]
		]]);
	?>

</div>

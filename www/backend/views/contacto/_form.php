<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker; 

/* @var $this yii\web\View */
/* @var $model backend\models\Contacto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contacto-form row">
    <div class="col-lg-6">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'nombre'); ?>

            <?php //echo $form->field($model, 'correo'); ?>
             <?php 
             $model->correo="cmena@igd.cr";
//                    echo Html::label('Fecha');
//                    echo DatePicker::widget([
//                        'model' => $model,
//                        'attribute' => 'fecha',
//                        'language' => 'es',
//                        'clientOptions' => [
//                            'autoclose' => true,
//                            'format' => 'dd-mm-yyyy'
//                        ]
//                    ]);
                    ?>

            <?php //$form->field($model, 'tipo')->textarea(['rows' => 6]) ?>
             <?php   
                  echo $form->field($model,'tipo')
                        ->dropDownList(
                                [
                                    "Factura"=>"Ajuste de facturas",
                                    "Solicitud"=>"Solicitud",
                                    "Reporte de errores"=>"Reporte de errores",
                                    "Otro"=>"Otro",
                                    ]);
             ?>
            <?= $form->field($model, 'mensaje')->textarea(['rows' => 3]) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Enviar') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <div class="col-lg-6">   
        <div class="jumbotron">
            <h2>Contáctenos</h2>
            <p>Siempre es bienvenido un mensaje para hacer cotizaciones, un mensaje de retroalimentación, mejoras, alertar un error o plantear una mejora de negociacíon.</p>
            <p><strong>¡</strong>Ayudenos a mejorar<strong>!</strong></p>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>

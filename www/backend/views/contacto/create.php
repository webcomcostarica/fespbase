<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Contacto */

$this->title = ($msn==="Listo")?"Listo":Yii::t('backend', 'Enviar mensaje');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Contactos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contacto-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if($msn==="Listo"){ ?>
        <div class="contacto-form row">
            <div class="col-lg-6">
                <div class="jumbotron">
                    <p>Su mensaje está guardado, gracias.</p>
                   <?php 
                     date_default_timezone_set('America/Costa_Rica'); 
                     $hora=date("g");
                     $ampm=date("a");
                     if($ampm==="am"){ ?>
                        <p>Te deseamos un buen día.</p>
                    <?php }else{ ?>                    
                        <?php if($hora<'6'){ ?>                        
                                <p>Te deseamos muy buenas tardes.</p>                        
                            <?php }else{ ?>                        
                                <p>¡Buenas noches!</p>                        
                            <?php } ?>
                    <?php } ?>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="jumbotron">
                    <?php echo Html::img('/img/mail.png', ['alt' => "Correo enviado"]) ?>
                </div>
            </div>
        </div>
    <?php }else{ ?>
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    <?php } ?>

</div>

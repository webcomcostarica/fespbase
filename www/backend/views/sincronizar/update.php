<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\DgtColas */

$this->title = 'Update Dgt Colas: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Dgt Colas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dgt-colas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

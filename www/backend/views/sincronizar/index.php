<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\DgtColasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dgt Colas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dgt-colas-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Dgt Colas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'comprobante',
            'tipo',
            'estado',
            'terminal',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

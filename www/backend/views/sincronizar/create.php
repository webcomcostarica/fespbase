<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\DgtColas */

$this->title = 'Create Dgt Colas';
$this->params['breadcrumbs'][] = ['label' => 'Dgt Colas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dgt-colas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

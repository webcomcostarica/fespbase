<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\CodigosComprobantes;

$this->title = Yii::t('app', 'Recibir facturas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mr-comprobante-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Nuevo deducible'), ['nuevo'], ['class' => 'btn btn-success']) ?>
     </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
           // ['class' => 'yii\grid\SerialColumn'],

            'id',
            'numeroConcecutivo:ntext',
          //  'tipoComprobante:ntext',
            'clave:ntext',
          //  'numeroCedulaEmisor:ntext',
            'fechaEmisionDoc',
            // 'mensaje',
             'detalleMensaje',
              [
                'label' => 'FE Envio',
                'contentOptions' => function($model) {
                    $clases = [
                        100 => 'info',
                        200 => 'success',
                        300 =>'warning', 
                        400 => 'danger',
                        500  => 'danger', 
                        0  => 'warning', 
                    ];
                    $id = $model->getClase();
                    
                    return [
                        'class' => $clases[$id],
                        'code' => $id
                        ];
                },
                    'format' => 'raw',
                    'value' => function($model) {
                            return CodigosComprobantes::ESTADOENTREGA[$model->getClase()];
                        },
                    ],
                
                 [
                        'label' => 'FE Estado',
                        'format' => 'raw',
                        'contentOptions' => function($model) {
                            $clases = [
                            0 => 'warning', 
                            1 => 'success', 
                            2 => 'danger', 
                            3 => 'danger', 
                            800 => 'danger',
                            100 => 'info',
                            200 => 'success',
                            300 =>'warning', 
                            400 => 'danger',
                            500  => 'danger',
                            null => 'warning'
                            ];
                            $id = $model->getClaseEstado();
                            return [
                                'class' => $clases[$id],
                                'code' => $id
                            ];
                        },
                        'value' => function($model) {                            
                            return CodigosComprobantes::ESTADOVALIDACION[$model->getClaseEstado()];
                        },
                    ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}'
            ],
        ],
    ]); ?>

</div>

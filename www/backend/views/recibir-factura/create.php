<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Registrar un gasto deducible');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mr Comprobantes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mr-comprobante-create">

   
    <div class="col-lg-12">  
        <h2>Pasos</h2>
    </div>
    
    <ul>
    
        <li class="<?= ($model->paso > 1)?'success':''; ?>">
             <?=  ($model->paso == 1)?'<b>':'' ?> 
                Cargar mensaje XML de Hacienda(DGT).
            <?=  ($model->paso == 1)?'</b>':'' ?>
        </li>
        <li class="<?= ($model->paso > 2)?'success':''; ?>">
             <?=  ($model->paso == 2)?'<b>':'' ?> 
                Revisar datos.
            <?=  ($model->paso == 2)?'</b>':'' ?>
        </li> 
        <li class="<?= ($model->paso > 2)?'success':''; ?>">
             <?=  ($model->paso == 2)?'<b>':'' ?> 
                Cargar comprobante XML de la compra.
            <?=  ($model->paso == 2)?'</b>':'' ?>
        </li>
        
         <li class="<?= ($model->paso > 3)?'success':''; ?>">
             <?=  ($model->paso == 3)?'<b>':'' ?> 
                Confirmar el gasto.
            <?=  ($model->paso == 3)?'</b>':'' ?>
        </li>
        
         <li class="<?= ($model->paso > 3)?'success':''; ?>">
             <?=  ($model->paso == 4)?'<b>':'' ?> 
                Consultar estado.
            <?=  ($model->paso == 4)?'</b>':'' ?>
        </li>
    </ul>
    
    
    
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

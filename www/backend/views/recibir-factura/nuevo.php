<?php
    
use yii\helpers\Html;

$this->title = Yii::t('app', 'Nuevo deducible');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Confirmaciones'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$tam_image = '80px';
?>

    <div class="col-lg-12"> 
    <h2>Eliga la forma de registro</h2>
        
        <div style="text-align: center;" class="col-xs-6 col-sm-4 col-md-4" id="acc_factura">
            <div class="panel panel-success">
              <div class="panel-heading">Tengo el Mensaje y el Comprobante XML</div>
              <div class="panel-body">
                  <p><?php echo Html::img('/img/servicios/2ce.png', ['height' => '' . $tam_image]) ?></p>             
                
                    <?= Html::a(
                        Yii::t('app', 'Siguiente'), 
                        ['crear-mc'],
                        ['class' => 'btn btn-success']
                    ) ?>
                    <p class="info"><b>Recomendado</b></p>
              </div>
            </div>
    
            
            
        </div>
        
        <div style="text-align: center;" class="col-xs-6 col-sm-4 col-md-4" id="acc_factura">
            <div class="panel panel-default">
              <div class="panel-heading">Tengo solo el Comprobante XML</div>
              <div class="panel-body">
                <p><?php echo Html::img('/img/servicios/1ce.png', ['height' => '' . $tam_image]) ?></p>             
                <?= Html::a(
                    Yii::t('app', 'Siguiente'), 
                    ['crear-cc'],
                    ['class' => 'btn btn-success']
                ) ?>
                    <p class="info">No sabes si el comprobante fue aprobado.</p>
              </div>
            </div>
        </div>
        
        
    </div>
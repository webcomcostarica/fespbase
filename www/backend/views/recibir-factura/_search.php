<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\MrComprobanteSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mr-comprobante-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'tipoComprobante') ?>

    <?= $form->field($model, 'clave') ?>

    <?= $form->field($model, 'numeroConcecutivo') ?>

    <?= $form->field($model, 'numeroCedulaEmisor') ?>

    <?php // echo $form->field($model, 'fechaEmisionDoc') ?>

    <?php // echo $form->field($model, 'mensaje') ?>

    <?php // echo $form->field($model, 'detalleMensaje') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

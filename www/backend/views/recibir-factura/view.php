<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\components\FuncionesDGT;
use backend\models\SisConfiguraciones;
$configuraciones = SisConfiguraciones::getParametrosGenerales();

$this->title = 'Detalles';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Confirmaciones'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="row">
    <ul class="nav nav-tabs">
        <li class="active">
            <a href="#panel-1" data-toggle="tab"   aria-expanded="false">Hacienda</a>
        </li>                
        <li class="">
            <a href="#panel-2" data-toggle="tab"   aria-expanded="false">Detalles</a>
        </li>
        <li class="">
            <a href="#panel-3" data-toggle="tab"   aria-expanded="false">Descargar XML's</a>
        </li>
    </ul>
    
    <div class="tab-content">
        <div class="tab-pane active" id="panel-1">
             <?php
                echo Html::tag('h2','Estado con DGT');    
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'numeroConcecutivo',
                        'fechaEmisionDoc',
                        'envioEstado',
                        'envioMensaje',
                        'estadoEstado',
                        'estadoMensaje',
                    ],
                ]);
                    echo Html::a('Consultar Estado',
                        '/recibir-factura/consultar-estado?id='.$model->id, [
                        'class' => 'btn btn-primary',
                    ]);       
                    
            ?>
        </div>
        <div class="tab-pane" id="panel-2">
            <h2><?= Html::encode($this->title) ?></h2>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'detalleMensaje',
                    'clave:ntext',
                    'numeroConcecutivo:ntext',
                    'numeroCedulaEmisor:ntext',
                    'numeroCedulaReceptor:ntext',
                  //  'tipoComprobante:ntext',
                   // 'mensaje',
                    'montoTotalImpuesto',
                    'totalFactura',
                    'normativa',
                    'fechaNormativa',
                    'fechaEmisionDoc',
                ],
            ]) ?>
        </div>
        <div class="tab-pane" id="panel-3">
        <?php 
             $ambiente = FuncionesDGT::getAmbienteSistema($configuraciones);  
        ?>
            <h2>Comprobante Emisor</h2>
            <?php 
                $url = $model->pathComprobanteXml;
            ?>
               <a target="_blank" 
               href="<?= $url ?>" class="btn btn-info">Ver XML</a>
              
              <a 
                download="<?= $url ?>" 
                target="_blank" 
                href="<?= $url ?>" class="btn btn-warning">Descargar XML</a>
               
            
            <h2>Mensaje de respuesta</h2>
                <?php 
                $url = $model->pathMensajeXml;
                ?>
               <a target="_blank" 
               href="<?= $url ?>" class="btn btn-info">Ver XML</a>
              
              <a 
                download="<?= $url ?>" 
                target="_blank" 
                href="<?= $url ?>" class="btn btn-warning">Descargar XML</a>
               
               
               
            <h2>Respuesta de hacienda</h2>
                <?php 
                $url = $ambiente['paths'][$model->tipoComprobante] . $model->clave . '_mensaje_hacienda.xml';
            ?>
               <a target="_blank" 
               href="<?= $url ?>" class="btn btn-info">Ver XML</a>
              
              <a 
                download="<?= $url ?>" 
                target="_blank" 
                href="<?= $url ?>" class="btn btn-warning">Descargar XML</a>
               
               
               
        </div>
    </div>
    <?php 
    if(isset($_GET['all'])){
        echo '<pre>';
        var_dump($model);
        echo '<pre>';
    }
    ?>
</div>
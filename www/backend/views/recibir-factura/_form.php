<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

?>

<div class="mr-comprobante-form">

    <?php $form =  ActiveForm::begin(
            [
                'options' => [
                    'enctype' => 'multipart/form-data'
                ]
            ]
        ); ?>
 <div class="col-lg-12">
    <?= $form->errorSummary($model); ?>
 </div>
    <?php 
    //cargar mensaje
    if($model->paso == '1'){ 
    ?>
        <div class="col-lg-12">   
        <h2>Paso 1 </h2>                 
             <?php                    
                echo $form->field($model, 'mensajeDGT')->fileInput()->label('Busque el archivo.xml del comprobante'); ?>
        </div>
        <?php
    }
    
    if($model->paso == '2'){ 
        ?>
        <div class="col-lg-6">   
            <h2>Paso 2 </h2>                 
             <?php                    
                echo $form->field($model, 'adjunto')->fileInput()->label('Busque el archivo.xml del comprobante'); ?>
        </div>
       
        <?php
    }
     if($model->paso == '3'){ ?>
         <div class="col-lg-6"> 
<h2>Paso 3 </h2>           
            <?php
     
        echo Html::tag('h2','Enviar confirmación');
        echo $form
                ->field($model, 'tipoRespuesta')
                ->dropDownList(
                [
                    '05'=>'Confirmar la aceptación',
                    '06'=>'Confirmar la aceptar parcial',
                    '07'=>'Confirmar el rechazo',
                ])->label('Seleccione la confirmación que desea aplicar');
                ?>
        </div><?php
     }
     
    if($model->paso >= 2 && $model->paso <=3){ ?>
         <div class="col-lg-6">    
            <?php
            echo Html::tag('h2','Revisar datos');
            echo DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'clave:ntext',
                    'numeroCedulaEmisor:ntext',
                    'numeroCedulaReceptor:ntext',
                    'montoTotalImpuesto',
                    'totalFactura',
                ],
            ]);  ?>
        </div><?php
    }
     if($model->paso == 4){
    ?>
        
        <div class="col-lg-6"> <?php
        
             echo Html::tag('h2','Estado con DGT');    
             echo DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'numeroConcecutivo',
                    'fechaEmisionDoc',
                    'envioEstado',
                    'envioMensaje',
                    'estadoEstado',
                    'estadoMensaje',
                ],
            ]); ?>
        </div>
     <?php }
    if($model->paso <= 4){ ?>
     <div class="col-lg-12"> 
        <div class="form-group">
            <?= Html::submitButton(
                    Yii::t('app', 'Validar'), 
                    [
                        'class' => ($model->paso == 'adjuntar') ? 
                            'btn btn-success' 
                                : 
                            'btn btn-primary'
                    ]) ?>
        </div>
    </div>

    <?php 
    }else{
        
    }
    
    
    ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Lote */

$this->title = 'Lote '.$model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Inventario'), 'url' => ['lote/index','sort'=>'-id','id'=>0]];
//$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Lotes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lote-view">
<?php  if(Yii::$app->user->identity){
    if(Yii::$app->user->identity->rol_id===3){ ?>
    <p>
        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
<?php }else{ ?>
    <h1>Consulta del <?php echo $this->title; ?></h1>
<?php } } ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'proveedor0.NombreCompleto',
            'cod_barras',
          //  'paca',
            'precio',
			'impGetText:ntext',
          //  'calidad:ntext',
			'unidad',
            'cantidad',
            'alerta',
            'minimo',
            'descripcion:ntext',
            'creacion',
            'modificacion',
        ],
    ]) ?>
    <h2><?= Yii::t('backend','Precios') ?></h2>
    <ol>
        <?php foreach ($model->lotePrecios as $precio) { 
            date_default_timezone_set('America/Costa_Rica');
            $fecha =  date('d-m-Y',strtotime($precio->fecha));  
            $vigente=($precio->vigente==1)?'  <strong> [Precio vigente]</strong>':'';
            ?>
        <li><?= '&#162;'.$precio['costo'].':  Creado el '.$fecha .' por '.$precio->usuario0->username.$vigente; ?> </li>
         <?php }?>
    </ol>

</div>

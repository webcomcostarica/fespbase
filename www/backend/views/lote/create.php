<?php

use yii\helpers\Html;

$this->title = Yii::t('backend', 'Create').' '.$config['NombreDeLotes'];

$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Inventario'), 'url' => ['lote/index','sort'=>'-id','id'=>0]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lote-create">
    <h1><?= Html::encode($this->title) ?></h1>

	<?php 
		$model->imp_ven_i = 0;
	?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>

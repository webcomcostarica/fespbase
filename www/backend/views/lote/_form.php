<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Moneda;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\models\Colaborador;
?>

<div class="lote-form row">
    <div class="col-lg-6">

        <?php $form = ActiveForm::begin(); ?>

        <?php
      

        echo $form->field($model,'id')->textInput(['readonly'=>true]);
          // echo '<label class="control-label">Proveedor</label>';
        echo $form->field($model,'cod_provedor_a')->textInput();
               
        ?><!--
        <table class="table table-striped table-bordered">        
            <tbody>
            <tr data-key="23">
                <td><?php
/*
                echo Select2::widget([
                    'name' => 'Lote[cod_provedor]',
                    'value' => $model->cod_provedor,
                    'data' => ArrayHelper::map(Colaborador::getProveedores(), 'cod_provedor', 'NombreCompleto'),
                    'options' => ['placeholder' => $model->cod_provedor],
                    'pluginOptions' => [
                        'tags' => true,
                        'tokenSeparators' => [',', ' '],
                        'maximumInputLength' => 10
                    ],
                ]);
            
                */
                ?></td>
                <td style="width:30%;">
                    <span><b><a href="/colaborador/create?lote=<?= $model->id; ?>"><span class="glyphicon glyphicon-plus"></span> Agregar proveedor</a></b></span>
                    <br><span>
                    <b>
                    <a href="/colaborador/index?ColaboradorSearch%5Btipo%5D=4&sort=nombre"><span class="glyphicon glyphicon-search"></span> Ver proveedores </a></b>
                    </span>
                </td>
                
            </tr>
            </tbody>
        </table>-->

<?php
        
        
        echo $form->field($model,'cod_barras')->textInput();
        
       
        
          $form->field($model, 'calidad')
          ->dropDownList(
          [
          'PRIMERA'=>'PRIMERA',
          'SEGUNDA'=>'SEGUNDA',
          'TERCERA'=>'TERCERA'
          ],
          ['prompt'=>'Seleccione por favor',
          'onChange'=>'' ]
          ); 
        ?>


        <?php
        echo $form->field($model, 'unidad')
                ->dropDownList(
                        [
                            'kg' => 'Masa ( kg )',
                            'Unid' => 'Cantidad ( Unid )',
                        // 'Longitud'=>'Longitud ( Metro )',
                        // 'Capacidad'=>'Capacidad ( Litro )',
                        // 'Tiempo'=>'Tiempo ( H:M:S )',
                        ]
        );
?>
</div>
<div  class="col-lg-2"> 
<?php
        echo $form->field($model, 'cantidad')->textInput();
        ?>
</div>
<div  class="col-lg-2"> 
<?php
        echo $form->field($model, 'alerta')->textInput()->hint('Alerta amarilla.');
        ?>
</div>
<div  class="col-lg-2"> 
<?php
        echo $form->field($model, 'minimo')->textInput()->hint('Alerta en rojo.');
?>
</div>
<div  class="col-lg-2"> 
<?php
        echo $form->field($model, 'precioCompra')->textInput([
            'onkeyup'=>'cal_precioVenta(this)',
            'tipo'=>'compra',
            'type'=>'number',
            'step'=>'0.1',
            'min'=>0,
        ]);
     ?>
</div>
<div class="col-lg-2"> 
    <?php
        echo $form->field($model, 'utilidad')->textInput([
            'onkeyup'=>'cal_precioVenta(this)',
            'type'=>'number',
            'step'=>'0.1',
            'min'=>0,
            'tipo'=>'utilidad'
        ]);
     ?>
</div>
<div  class="col-lg-2"> 
<?php
   echo $form->field($model, 'imp_ven_i')
                ->dropDownList(
                        [
                            '0' => 'EXENTO',
                            '1' => 'GRAVADO',
                        ],
                        [
                            'onChange'=>'cal_precioVenta(this)',
                            'tipo'=>'iva'
                        ]
        )->label('IVA');
    ?>
       
</div>


<div class="col-lg-6"> 
    
    <div class="form-group field-lote-precio-calc required">

        <input 
            type="number" 
            id="lote-precio-calc" 
            class="form-control" 
            value="<?= $model->precio ?>" 
            onkeyup="cal_precioVenta(this)" 
            step="0.1" 
            min="0" 
            tipo="precio" 
            aria-required="true">
    </div>

    <?php  
        echo $form->field($model, 'precio')->textInput([
            'onkeyup'=>'cal_precioVenta(this)',
            'type'=>'number',
            'step'=>0.1,
            'min'=>0,
            'tipo'=>'precio',
            'class'=>'hidden'
        ])->label(false);
       
       $precioFinal = $model->precio;
       $iva = $precioFinal*0.13;
        
    ?>
    <div class="alert alert-success " role="alert">
        <b>Precio sin IVA: </b> <span id="span_sub_total"><?= $model->precio; ?></span>
        <br>
        <span class="iva_display <?= ($model->imp_ven_i==0)?'hidden':''?>">
            <b>IVA: </b> 
                <span 
                    style="color: red;"
                    id="span_iva">
                        <?= $iva ?>
                </span>
            <br>
            <b>Precio venta final: </b> 
                <span 
                    style="color: blue;"
                    id="span_total_final">
                        <?= $model->precio+$iva ?>
                </span>
        </span>
    </div>
</div>

</div>
<div class="row">
<div  class="col-lg-6"> 

        <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>

</div>
<div  class="col-lg-12"> 


    <?php
    echo Html::submitButton(
            $model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',]);
    ?>


</div>
</div>
    <?php ActiveForm::end(); ?>
    <?php $this->registerJsFile('/js/lote.js'); ?> 
    


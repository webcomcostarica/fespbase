<?php

use yii\helpers\Html;


$this->title = Yii::t('backend', 'Update');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Todas las pacas'), 'url' => ['package/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Paca').' '.$model->paca, 'url' => ['index','id'=>$model->paca]];
$this->params['breadcrumbs'][] = ['label' => 'Lote '.$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lote-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\LoteSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lote-search hidden">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'paca') ?>

    <?= $form->field($model, 'precio') ?>

    <?= $form->field($model, 'calidad') ?>

    <?= $form->field($model, 'cantidad') ?>

    <?php // echo $form->field($model, 'descripcion') ?>

    <?php // echo $form->field($model, 'creacion') ?>

    <?php // echo $form->field($model, 'modificacion') ?>

    <?php // echo $form->field($model, 'cant_inicial') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('backend', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

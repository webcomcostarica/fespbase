<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = Yii::t('backend', $config['NombreDeLotes']);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Inventario'), 'url' => ['lote/index','sort'=>'-id','id'=>0]];

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lote-index">

    <h1><?= Html::encode($this->title) ?></h1>
	
	<p>
		<?= Html::a(Yii::t('backend', 'Nuevo'), ['create','id'=>$_GET["id"]], ['class' => 'btn btn-success']) ?>
	</p>
	
    <?php 
    if($_GET["id"]){
        if((int)$_GET["id"] > 0){ ?>        
            <p>
                <?= Html::a(Yii::t('backend', 'Create Lote'), ['create','id'=>$_GET["id"]], ['class' => 'btn btn-success']) ?>
            </p>
        <?php
        }
    }
        ?>


    <?php 
    
        $acciones = [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}',
            ];
        if(isset($_GET['venta'])){
             $acciones = [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{consultar}',
                'buttons'=>[
                       'consultar'=>function($url,$model){ 
                            $btn = '<a href="/lote/index?id=0&venta='.$_GET['venta'].'&loteSet='.$model->id.'#tbl_terminos" > <span class="glyphicon glyphicon-ok"></span></a>';
                        return $btn;						
                    },
                ]
            ];
        }

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'cod_barras',
            'cod_provedor_a',
            'precio',
			[
				'attribute'=>'imp_ven_i',
				'label'=>'IVA',
				'value'=>function($model){
					if(isset($model->imp_ven_i))
						if($model->imp_ven_i==1)
							return 'GRAVADO';
						else
							return 'EXENTO';
					else
						return 'N/D';
					
				},
				'format'=>'raw'
			],
			'unidad',
          //  'cantidad',
            [
                'attribute' => 'cantidad',
                'label' => 'Vitrina',
                 'contentOptions'=>function($model){
                     $class = [
                            0 => 'success',
                            1 => 'warning',
                            2 => 'danger',
                     ];
                     $id = 0;
                     $actual = $model->cantidad;
                     $minimo = $model->minimo;
                     $alerta = $model->alerta;                     
                     if($actual < $minimo)
                        $id = 2;
                     elseif($actual >= $model->minimo && $actual <= $alerta)
                        $id = 1;
                     
                     return [
                        'style' => 'width: 10%;text-align:left',
                        'class' => $class[$id]
                    ];
                 },
                'value' => function($model){
                    if($model->unidad == 'mL')
                        return ($model->cantidad/40).' tragos';
                        
                   return $model->cantidad.' '.$model->unidad;                      
                }
            ],
            
             'descripcion:ntext',

            $acciones
            ,
        ],
    ]); ?>

    
    
    
</div>


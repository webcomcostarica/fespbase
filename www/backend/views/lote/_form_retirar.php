<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Lote */

$this->title = Yii::t('backend', 'Retirar Etiquetas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lote-create">
    <h1><?= Html::encode($this->title) ?></h1>

    <form id="form-signup" action="/index.php?r=lote%2Fretirar" method="post" role="form">
        <input type="hidden" name="_csrf" value="ZjI2bTJpd1olCk4dRjsEOTFeYgFURA1vFVR/OFkqEhkMewIsai4OHw==">
        <div class="form-group field-signupform-username required">
            <label class="control-label" for="lotes">Anote los códigos entre comas</label>
            <input type="text" id="signupform-username"  class="form-control" name="retirar[lotes]">
            <p class="help-block help-block">ejemplo: 1413,1212,1022</p>
        </div> 
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="signup-button">Sacar del inventario</button>                
        </div>
    </form>
    <?php if(!empty($resultados)){ ?>
    <h2>Resultado</h2>
    <table class="table table-striped table-bordered">
        <thead>
            <tr><th>Lote</th>
                <th>Cantidad Inicial</th>
                <th>Cantidad Final</th>
                <th>Se quitaron</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($resultados as $resultado){ ?>
            <tr>
                <td><?= $resultado['lote'] ?></td>
                <td><?= $resultado['inicial'] ?></td>
                <td><?= $resultado['final'] ?></td>
                <td><?= $resultado['mns'] ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    <?php } ?>
</div>




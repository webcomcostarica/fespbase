<?php use yii\helpers\Html; ?>

<tr id="tr_term_<?= $termino->id; ?>">
	<td ><?= $termino->nombre; ?></td>
	<td class="btnCate">
	 <?= Html::button('-', [
	 'class' => 'btn btn-danger',
	 'onClick'=>'
			   $.ajax({
					success: function(data) {								
						 $(data).remove()
					},
					error:function(data){	
						alert("Error al crear borrar el término");
					},
					type: "get",
					url: "/index.php?r=taxonomia%2Fdeletetermino",
					data: {
						"pIdTax":'.$termino->id.'
						},
					cache: false,
					dataType: "html"
				});'
	]) ?></td> 
</tr>	
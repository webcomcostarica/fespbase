<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Taxonomia */

$this->title = Yii::t('backend', 'Create Taxonomia');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Taxonomias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="taxonomia-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

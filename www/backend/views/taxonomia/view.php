<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Taxonomia */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Taxonomias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="taxonomia-view">

    <h1><?= Html::encode($this->title) ?></h1>

	<div class="col-lg-6">
    <p>
        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'contenido:ntext',
            'nombre:ntext',
            'descripcion:ntext',
        ],
    ]) ?>
	</div>
	<div class="col-lg-6">
	<p>Lista de términos que serán listados al seleccionar la categoría.</p>
		<table  style="width:100%" class="table table-striped table-bordered">
		<thead>
		  <tr>
			<th>Lista de terminos</th>
			<th></th> 
		  </tr>
		</thead>  
		<tbody id="tbl_terminos">
		  <?php foreach($model->diccionarios as $termino ){ ?>			
			<tr id="tr_term_<?= $termino->id; ?>">
				<td ><?= $termino->nombre; ?></td>
				<td class="btnCate">
				 <?= Html::button('-', [
				 'class' => 'btn btn-danger',
				 'onClick'=>'
						   $.ajax({
								success: function(data) {								
									 $(data).remove()
								},
								error:function(data){	
									alert("Error al crear borrar el término");
								},
								type: "get",
								url: "/index.php?r=taxonomia%2Fdeletetermino",
								data: {
									"pIdTax":'.$termino->id.'
									},
								cache: false,
								dataType: "html"
							});'
				]) ?></td> 
			</tr>	
		  <?php }	?>
		  <tr id="tr_btn_more">			
			<td><input type="text"  id="termino_nombre" class="form-control" placeholder="Nombre"></td>
			<td class="btnCate">
        <?= Html::button('+', [
		 'class' => 'btn btn-success',
		 'onClick'=>'						  
				   $.ajax({
						success: function(data) {
							$("#termino_nombre").val("")
							var btnMore=$("#tr_btn_more");
							$("#tr_btn_more").remove();			
							$("#tbl_terminos").append(data);
							$("#tbl_terminos").append(btnMore);
						},
						error:function(data){	
							alert("Error al crear el término");
						},
						type: "get",
						url: "/index.php?r=taxonomia%2Fcreatetermino",
						data: {
							"pNombre":$("#termino_nombre").val(),
							"pIdTax":'.$model->id.'
							},
						cache: false,
						dataType: "html"
					});'
		]) ?>
		</td> 
		  </tr>
		  </tbody>
		</table>
	</div>
</div>

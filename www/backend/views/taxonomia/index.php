<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\TaxonomiaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Taxonomias');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="taxonomia-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend', 'Create Taxonomia'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
        //    ['class' => 'yii\grid\SerialColumn'],

          //  'id',
            'contenido:ntext',
            'nombre:ntext',
            'descripcion:ntext',

            [
				'class' => 'yii\grid\ActionColumn',
				'template' => ' {view} ',
			],
        ],
    ]); ?>

</div>

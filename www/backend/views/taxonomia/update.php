<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Taxonomia */

$this->title = Yii::t('backend', 'Update').' taxonomía:'. ' ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Taxonomias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="taxonomia-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

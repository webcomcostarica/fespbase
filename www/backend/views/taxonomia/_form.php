<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


use backend\models\Diccionario;

/* @var $this yii\web\View */
/* @var $model backend\models\Taxonomia */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="taxonomia-form row">
	<div class="col-lg-6">

    <?php $form = ActiveForm::begin(); ?>

	<p>Configure el donde desea utilizar esta taxonomía.</p>
	<?= $form->field($model, 'contenido')->dropDownList(
                ['Pacas'=>'Pacas','Lotes'=>'Lotes','Facturas'=>'Facturas'],           
                ['prompt'=>'Seleccione por favor']    
        ); ?>
		
    <?php // $form->field($model, 'contenido')->textInput() ?>

    <?= $form->field($model, 'nombre')->textInput() ?>

    <?= $form->field($model, 'descripcion')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php if(!$model->isNewRecord){ ?>
	<div class="col-lg-6">
	<p>Lista de términos que serán listados al seleccionar la categoría.</p>
		<table  style="width:100%" class="table table-striped table-bordered">
		<thead>
		  <tr>
			<th>Lista de terminos</th>
			<th></th> 
		  </tr>
		</thead>  
		<tbody id="tbl_terminos">
		  <?php foreach($model->diccionarios as $termino ){ ?>			
			<tr id="tr_term_<?= $termino->id; ?>">
				<td ><?= $termino->nombre; ?></td>
				<td class="btnCate">
				 <?= Html::button('-', [
				 'class' => 'btn btn-danger',
				 'onClick'=>'
						   $.ajax({
								success: function(data) {								
									 $(data).remove()
								},
								error:function(data){	
									alert("Error al crear borrar el término");
								},
								type: "get",
								url: "/index.php?r=taxonomia%2Fdeletetermino",
								data: {
									"pIdTax":'.$termino->id.'
									},
								cache: false,
								dataType: "html"
							});'
				]) ?></td> 
			</tr>	
		  <?php }	?>
		  <tr id="tr_btn_more">			
			<td><input type="text"  id="termino_nombre" class="form-control" placeholder="Nombre"></td>
			<td class="btnCate">
        <?= Html::button('+', [
		 'class' => 'btn btn-success',
		 'onClick'=>'						  
				   $.ajax({
						success: function(data) {
							$("#termino_nombre").val("")
							var btnMore=$("#tr_btn_more");
							$("#tr_btn_more").remove();			
							$("#tbl_terminos").append(data);
							$("#tbl_terminos").append(btnMore);
						},
						error:function(data){	
							alert("Error al crear el término");
						},
						type: "get",
						url: "/index.php?r=taxonomia%2Fcreatetermino",
						data: {
							"pNombre":$("#termino_nombre").val(),
							"pIdTax":'.$model->id.'
							},
						cache: false,
						dataType: "html"
					});'
		]) ?>
		</td> 
		  </tr>
		  </tbody>
		</table>
	</div>
<?php } ?>
</div>



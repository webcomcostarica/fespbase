<?php

use yii\helpers\Html;
use common\models\AccessHelpers;


$tam_image = '80px';
$this->title = 'JARVIS';
?>
<div class="site-index"> 
    <div class="body-content">
        <?php if (Yii::$app->user->identity) {
            ?>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-header">
                            <h1>
                                <small>Escritorio</small>
                            </h1>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
    <?php 
    if (AccessHelpers::getAcceso('factura-index')) { ?>
        <div style="text-align: center;" class="col-xs-6 col-sm-4 col-md-4" id="acc_factura">
            <p><?php echo Html::img('/img/servicios/factura.png', ['height' => '' . $tam_image]) ?></p>             
            <h2><a class="btn btn-default" href="/factura/index?sort=-id">Facturación</a></h2>
        </div>
    <?php } ?>
	  <?php 
    if (AccessHelpers::getAcceso('creditos-index')) { ?>
        <div style="text-align: center;" class="col-xs-6 col-sm-4 col-md-4" id="acc_factura">
            <p><?php echo Html::img('/img/servicios/creditos.png', ['height' => '' . $tam_image]) ?></p>             
            <h2><a class="btn btn-default" href="/creditos/index?sort=-id">Creditos</a></h2>
        </div>
    <?php } 
    if (AccessHelpers::getAcceso('notacredito-index')) { ?>
        <div style="text-align: center;" class="col-xs-6 col-sm-4 col-md-4" id="acc_factura">
            <p><?php echo Html::img('/img/servicios/notaCredito.png', ['height' => '' . $tam_image]) ?></p>             
            <h2><a class="btn btn-default" href="/notacredito/index?sort=-id">Notas</a></h2>
        </div>
    <?php } 
    if (AccessHelpers::getAcceso('recibir-factura-index')) { ?>
        <div style="text-align: center;" class="col-xs-6 col-sm-4 col-md-4" id="acc_factura">
            <p><?php echo Html::img('/img/servicios/confirmarGasto.png', ['height' => '' . $tam_image]) ?></p>             
            <h2><a class="btn btn-default" href="/recibir-factura/index?sort=-id">Confirmar Gastos</a></h2>
        </div>
    <?php } 
    if (AccessHelpers::getAcceso('colaborador-index')) { ?>        
        <div style="text-align: center;" class="col-xs-6 col-sm-4 col-md-4" id="acc_clientes">
            <p><img src="/img/servicios/clientes.png" height="80px" alt="Colaboradores"></p>             
            <h2><a class="btn btn-default" href="/colaborador/index?sort=nombre">Clientes</a></h2>
        </div>
    <?php } 
    if (AccessHelpers::getAcceso('sistema-update')) { ?>
        <div style="text-align: center;" class="col-xs-6 col-sm-4 col-md-4">
            <p><img src="/img/servicios/configurar.png" height="80px" alt="Configurar Jarvis"></p>             
            <h2><a class="btn btn-default" href="/sistema/update">Configurar Jarvis</a></h2>
        </div>
    <?php } 
    if (AccessHelpers::getAcceso('lote-index')) { ?>        
        <div style="text-align: center;" class="col-xs-6 col-sm-4 col-md-4" id="acc_lotes">
            <p><img src="/img/servicios/lote.png" height="80px" alt="Lotes"></p>             
            <h2><a class="btn btn-default" href="/lote/index?id=0?sort=-id">Inventario</a></h2>
        </div>
    <?php }  
    if (AccessHelpers::getAcceso('reporte-diario-de-ventas')) { ?>        
        <div style="text-align: center;" class="col-xs-6 col-sm-4 col-md-4" id="acc_lotes">
            <p><img src="/img/servicios/reportes.png" height="80px" alt="Lotes"></p>             
            <h2><a class="btn btn-default" href="/reporte/index">Diario de ventas</a></h2>
        </div>
    <?php }    
    if (AccessHelpers::getAcceso('crm-panel')) { ?>        
        <div style="text-align: center;" class="col-xs-6 col-sm-4 col-md-4" id="acc_lotes">
            <p><img src="/img/servicios/crm.png" height="80px" alt="Lotes"></p>             
            <h2><a class="btn btn-default" href="/crm/panel">CRM</a></h2>
        </div>
    <?php }  ?>
                        </div>
                    </div>                    
                </div>

                <?php
                if (Yii::$app->user->identity->rol_id === 2 || Yii::$app->user->identity->rol_id === 3) { ?>
                
                
<div class="row">
    <div class="col-md-12">
        <div class="page-header">
            <h1>
                <small>Sistema y Configuración</small>
            </h1>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">


       
        <?php if (AccessHelpers::getAcceso('rol-index')) { ?>                          
            <div style="text-align: center;" class="col-xs-6 col-sm-4 col-md-4">
                <p><img src="/img/servicios/1.png" height="80px" ></p>             
                <h2><a class="btn btn-default" href="/rol/index">Seguridad</a></h2>
            </div>
        <?php } ?>

        <?php if (AccessHelpers::getAcceso('user-index')) { ?>
            <div style="text-align: center;" class="col-xs-6 col-sm-4 col-md-4">
                <p><img src="/img/servicios/usuarios.png" height="80px" alt="Usuarios"></p>             
                <h2><a class="btn btn-default" href="/user/index">Usuarios</a></h2>
            </div>
        <?php } ?>
        <?php if (AccessHelpers::getAcceso('gii-index')) { ?>
            <div style="text-align: center;" class="col-xs-6 col-sm-4 col-md-4">
                <p><img src="/img/servicios/5.png" height="80px" alt="Gii"></p>             
                <h2><a class="btn btn-default" href="/gii">Gii</a></h2>
            </div>
        <?php } ?>

    </div>
</div>				

                
                <?php
                }
                ?>


            </div>


            <?php
        } else {
            echo $this->render('visitante');
        }
        ?>
    </div>
</div>

<?php $this->registerJsFile('/js/index.js'); ?>  
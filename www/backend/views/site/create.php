<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Contacto */

$this->title = Yii::t('backend', 'Create Contacto');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Contactos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contacto-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>


<div class="jumbotron">
      <div class="container">
        <h1 class="display-3">Reportar ajustes!</h1>
        <p>Hola, guía para reportar rapidamente un problema o un ajuste en nuestro sistema.</p>
        <p>
        	<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Ver video »</button>
        </p>
      </div>
    </div>
    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-4">
          <h2>Web Whatsapp</h2>
          <p>Te permite mandarnos un mensaje desde el mismo computador directo a nuestro teléfono.</p>
          <p>
          	<a class="btn btn-secondary" target="_blank" href="https://web.whatsapp.com/" role="button">Configurar »</a>
          </p>
          <img src="/img/contenido/webws.JPG" class="img-thumbnail" alt="Cinque Terre">
        </div>
        <div class="col-md-4">
          <h2>Recortes de windows</h2>
          <p>Esta aplicación te permite tomar pantallazos del sistema y reportar tus dudas. </p>
          <p><a class="btn btn-secondary" href="#" role="button">Aprender a usar »</a></p>
          <img src="/img/contenido/recortes.JPG" class="img-thumbnail" alt="Cinque Terre">
       </div>
        <div class="col-md-4">
          <h2>¿Porque de esta forma?</h2>
          <p>
          	Le permite a usted utilizar herramientas faciles para marcar errores o crear solicitudes a travez de nuestro whatsapp Buisness donde será atendido en seguida...
          	<ul>
          		<li>Uso del mismo computador.</li>
          		<li>Evita tomar fotografias.</li>
          		<li>Comunicación más fluida y clara</li>
          	</ul>
          </p>
          
        </div>
      </div>

      <hr>



<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Como hacer un caso.</h4>
      </div>
      <div class="modal-body">
       <div class="embed-responsive embed-responsive-16by9">
		  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


      <footer>
        <p>© GRUPO WEBCOM COSTA RICA SA <?= date('Y') ?></p>
      </footer>
    </div>
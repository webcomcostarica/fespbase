<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\packageSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="package-search hidden">
    <div class="row">
    <div class="col-lg-4">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'categoria') ?>

    <?= $form->field($model, 'compra') ?>
</div>
    <div class="col-lg-4">
    <?= $form->field($model, 'desempacada') ?>

    <?= $form->field($model, 'proveedor') ?>

    <?php // echo $form->field($model, 'estado') ?>

    <?php // echo $form->field($model, 'precio') ?>

    <?php // echo $form->field($model, 'calidad') ?>

    <?php // echo $form->field($model, 'descripcion') ?>

    <?php // echo $form->field($model, 'creacion') ?>

    <?php // echo $form->field($model, 'modificacion') ?>

    <?php // echo $form->field($model, 'num_factura') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('backend', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>
</div>
</div>
    <?php ActiveForm::end(); ?>

</div>

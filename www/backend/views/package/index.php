<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\AccessHelpers; 
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\packageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Todas las '.Yii::$app->params['activo'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="package-index">

    <h1 id="title"><?= Html::encode($this->title) ?></h1>
   
	<div class="row">
		<div class="col-sm-6">
			<?php 
			$bd= \Yii::$app->db; 
			$desde=date('Y-m',strtotime("now")).'-01';
			$hasta=date("Y-m-d",  strtotime('now'));
				$usuarios=['1','2','3'];		
			?>
			<script type="text/javascript">    
				var datos_dias_mes=[
				<?php 
				$total = 0;
				foreach ($usuarios as $usuario) {
					echo '"'.$usuario.'",';        
				} ?>
				]; 
				
				
				var etiquetas_mes_ano=[    
				<?php
				$usuarios=['Costo','Tienda','Vendido'];	
				foreach ($usuarios as $usuario) {
					echo '"'.$usuario.'",';        
				} ?>
				];  
				
				var datos_grafico_actual=datos_dias_mes
				var etiquetas_grafico_actual=etiquetas_mes_ano  
				var canvas='canvas_dias';    
			</script>
			<canvas class="hidden" id="canvas_semana" data="<?= $total; ?>"></canvas>
		 
		</div>
	</div>
   
    <?php 
      if(AccessHelpers::getAcceso('package-create')){ 
        $template='{lotes} {view} {update} {consultar}';
    ?>     
        <p>
            <?= Html::a('Agregar Mercadería', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

    <?php
      }
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
     //   'filterModel' => $searchModel,
        'columns' => [

            'id',
            'desempacada',
            'compra',
            /*  [
                'label'=>'Categoría',
                'value'=>'laCategoria.nombre',
            ],     
            [
                'label'=>'Estado',
                'value'=>'elEstado.nombre',
            ],
            [
                'label'=>'Calidad',
                'value'=>'laCalidad.nombre'
            ],  */ 
			[
				'label'=>'Costo',
                'value'=>'precio'
			],
			[
				'label'=>'Tienda',
                'value'=>'ValorDeTienda'
			],
			[
				'label'=>'Vendido',
                'value'=>'ValorDeVenta'
			],

            ['class' => 'yii\grid\ActionColumn',
				'template'=>$template,
				'buttons'=>[
				  'consultar' => function ($url, $model) {     
					$btn = '<a href="#title" onclick="consultarpaca('.$model->precio.','.$model->ValorDeTienda.','.$model->ValorDeVentaInt.');" ><span class="glyphicon glyphicon-plus"></span></button>';
					return $btn;                           

				  }
				]                            
			]
        ],
    ]); ?>

</div>
  




<?php $this->registerJsFile('/js/Chart.min.js'); ?>  
<?php $this->registerJsFile('/js/analitycs.js'); ?> 
<?php $this->registerJsFile('/js/pacas.js'); ?>

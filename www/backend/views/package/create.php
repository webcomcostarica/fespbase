<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Package */

$this->title = 'Nueva solicitud de '.Yii::$app->params['activo'];
$this->params['breadcrumbs'][] = ['label' => Yii::$app->params['activo'], 'url' => ['index','sort'=>'-id']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="package-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'categorias' => $categorias,
        'calidad' => $calidad,
        'estado' => $estado,
        'proveedor' => $proveedor,
    ]) ?>

</div>

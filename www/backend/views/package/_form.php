<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DatePicker;
    

/* @var $this yii\web\View */
/* @var $model backend\models\Package */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="package-form row">
	<div class="col-lg-6">
		<?php $form = ActiveForm::begin(); ?>

		<?php // $form->field($model, 'categoria')->textInput() ?>
            
                <?php            
                echo $form->field($model, 'categoria')
                ->dropDownList(
                        ArrayHelper::map($categorias,'id','nombre'),           
                        ['prompt'=>'Seleccione por favor',
                        'onChange'=>'' ]    
                ); 
                ?>
            
		<?= $form->field($model, 'descripcion')->textarea(['rows' => 2]) ?>
            
		<?php //$form->field($model, 'estado')->textInput() ?>
                  <?php            
                echo $form->field($model, 'estado')
                ->dropDownList(
                        ArrayHelper::map($estado,'id','nombre'),           
                        ['prompt'=>'Seleccione por favor',
                        'onChange'=>'' ]    
                ); 
                ?>
            
                 <?php            
                echo $form->field($model, 'calidad')
                ->dropDownList(
                        ArrayHelper::map($calidad,'id','nombre'),           
                        ['prompt'=>'Seleccione por favor',
                        'onChange'=>'' ]    
                ); 
                ?>

            <?php // echo $form->field($model, 'compra')->textInput();
                echo Html::label('Compra');
                echo DatePicker::widget([
                    'model' => $model,
                    'attribute' => 'compra',
                    'language' => 'es', 
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'dd-mm-yyyy'
                    ]
                ]);
            ?>
            
		<?php //$form->field($model, 'desempacada')->textInput() 
            
                echo Html::label('Desempacada');
                echo DatePicker::widget([
                    'model' => $model,
                    'attribute' => 'desempacada',
                    'language' => 'es', 
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'dd-mm-yyyy'
                    ]
                ]);
                ?>

                <?php            
                echo $form->field($model, 'proveedor')
                ->dropDownList(
                        ArrayHelper::map($proveedor,'id','nombre'),           
                        ['prompt'=>'Seleccione por favor',
                        'onChange'=>'' ]    
                ); 
                ?>
            
		<?php 
                //echo $form->field($model, 'proveedor')->textInput() ?>
            
		<?= $form->field($model, 'precio')->textInput() ?>
		
		<?php //$form->field($model, 'calidad')->textInput() ?>
            
            

		<?php //$form->field($model, 'creacion')->textInput() ?>

		<?php // $form->field($model, 'modificacion')->textInput() ?>

		<?= $form->field($model, 'num_factura')->textInput() ?>

		<div class="form-group">
			<?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
			<?php //($model->isNewRecord)? '' : 
                             // Html::a(Yii::t('backend', 'Lotes'), ['lote/index', 'id' => $model->id], ['class' => 'btn btn-success']); ?>
		</div>

		<?php ActiveForm::end(); ?>
	</div>
    <?php if(!$model->isNewRecord){ ?>
    <div class="col-lg-6" id="div_lotes" onload="cargarLotes();">
        
    </div>
    
    <?php } ?>
</div>
<script>
    function cargarLotes(){
         $.ajax({
                success: function(data) {								
                         $("#div_lotes").html(data)
                },
                error:function(data){							
                         $("#div_lotes").html("Error al cargar los lotes")
                },
                type: &quot;get&quot;,
                url: &quot;/index.php?r=lotes%2Findex,
                data: {
                        "id":<?php $model->id; ?>
                        },
                cache: false,
                dataType: &quot;html&quot;
        });
    }
</script>

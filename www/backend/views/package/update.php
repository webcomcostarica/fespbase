<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Package */

$this->title = Yii::t('backend', 'Update Package:') . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Todas las '.Yii::t('backend', 'Packages'), 'url' => ['index','sort'=>'-id']];
$this->params['breadcrumbs'][] = ['label' => 'Paca '.$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="package-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'categorias' => $categorias,
        'calidad' => $calidad,
        'estado' => $estado,
        'proveedor' => $proveedor,
    ]) ?>

</div>

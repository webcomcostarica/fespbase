<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Package */

$this->title = 'Ver';
$this->params['breadcrumbs'][] = ['label' => 'Todas las '.Yii::t('backend', 'Packages'), 'url' => ['index','sort'=>'-id']];
$this->params['breadcrumbs'][] = ['label' => 'Paca '.$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="package-view">
<?php  if(Yii::$app->user->identity){
    if(Yii::$app->user->identity->rol_id===3){ ?>
    <p>
        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend', 'Lotes'), ['lote/index', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
<?php } } ?>
    <?php 
    
    echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'num_factura',
             [
                'label'=>'Categoría',
                'value'=>$model->laCategoria->nombre,
            ],            
            'compra',
            'desempacada',
            [
                'label'=>'Proveedor',
                'value'=>$model->elProveedor->nombre
            ],
            'precio',   
            [
                'label'=>'Valor de venta',
                'value'=>$model->ValorDeVenta,
            ],   
            [
                'label'=>'Estado',
                'value'=>$model->elEstado->nombre,
            ],
            [
                'label'=>'Calidad',
                'value'=>$model->laCalidad->nombre
            ],
            'descripcion:ntext',
            'modificacion',
            'creacion',
            'cantPacas',
        ],
    ]) ?>

</div>

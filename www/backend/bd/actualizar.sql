/* Campos de path para archivos en fe_dgt_comprobante */
ALTER TABLE `fe_dgt_comprobante` ADD `pathComprobanteXml` TEXT NULL AFTER `responseEstado`, 
ADD `pathComprobantePdf` TEXT NULL AFTER `pathComprobanteXml`,
 ADD `pathMensajeXml` TEXT NULL AFTER `pathComprobantePdf`, 
 ADD `pathMensajePdf` TEXT NULL AFTER `pathMensajeXml`;

/* Campos de path para archivos en nc_dgt_comprobante */
ALTER TABLE `nc_dgt_comprobante` ADD `pathComprobanteXml` TEXT NULL AFTER `response`, ADD `pathComprobantePdf` TEXT NULL AFTER `pathComprobanteXml`, ADD `pathMensajeXml` TEXT NULL AFTER `pathComprobantePdf`, ADD `pathMensajePdf` TEXT NULL AFTER `pathMensajeXml`;

/* Campos de path para archivos en mr_comprobante */
ALTER TABLE `mr_comprobante` ADD `pathComprobanteXml` TEXT NULL AFTER `json`, ADD `pathComprobantePdf` TEXT NULL AFTER `pathComprobanteXml`, ADD `pathMensajeXml` TEXT NULL AFTER `pathComprobantePdf`, ADD `pathMensajePdf` TEXT NULL AFTER `pathMensajeXml`;

/*Correo notificados*/
ALTER TABLE `fe_dgt_comprobante` ADD `correoNotificado` TEXT NULL AFTER `pathMensajePdf`, ADD `correoNotificacion` BOOLEAN NULL AFTER `correoNotificado`;

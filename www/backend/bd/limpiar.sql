DELETE FROM `fe_colaboradores_comprobante` WHERE 1;
DELETE FROM `fe_dgt_comprobante` WHERE 1;
ALTER TABLE fe_dgt_comprobante AUTO_INCREMENT = 1;
DELETE FROM `fe_detalles` WHERE  1;
ALTER TABLE fe_detalles AUTO_INCREMENT = 1;


DELETE FROM `nc_colaboradores_comprobante` WHERE 1;
DELETE FROM `nc_dgt_comprobante` WHERE 1;
ALTER TABLE nc_dgt_comprobante AUTO_INCREMENT = 1;
DELETE FROM `nc_detalles` WHERE  1;
ALTER TABLE nc_detalles AUTO_INCREMENT = 1;
DELETE FROM `nc_comprobante` WHERE  1;
ALTER TABLE nc_comprobante AUTO_INCREMENT = 1;

DELETE FROM `fe_comprobante` WHERE  1;
ALTER TABLE fe_comprobante AUTO_INCREMENT = 1;

DELETE FROM `mr_comprobante` WHERE  1;
ALTER TABLE mr_comprobante AUTO_INCREMENT = 1;

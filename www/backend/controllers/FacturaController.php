<?php

namespace backend\controllers;

use Yii;
use backend\models\Factura;
use backend\models\search\FacturaSearch;
use yii\web\NotFoundHttpException;
use backend\models\FacturaColaboradores;
use backend\models\NcComprobante;
use backend\models\DetalleFactura;
use backend\models\LotePrecio;
use backend\models\Lote;
use backend\models\Abono;
use yii\web\Response;
use backend\models\DgtFe;
use common\components\FuncionesDGT;
use kartik\mpdf\Pdf;
use backend\models\CodigosComprobantes;
use backend\models\DgtColas;
use backend\models\base\DgtComprobante;
use common\models\FG;
use common\models\AccessHelpers;
use backend\models\SisConfiguraciones;

class FacturaController extends BaseController {


    public function actionEnviarCorreo($id){
		FG::logfile('mensaje');
       // Yii::$app->response->format = Response::FORMAT_JSON;
        $comprobante = Factura::findOne($id);
        if($comprobante){
            if(FuncionesDGT::enviarCorreo($comprobante)){
                echo 'Se envio el correo corretamente...';
                Yii::$app->session->setFlash('success', 'Correo enviado.');
                if(!isset($_GET['all']))
                    return $this->redirect(['/factura/index']);
                    
            }else{
                echo 'Ha ocurrido un error al enviar el correo.';
            }
                 
        }else{
            echo 'No hay un comprobante asociado...';
        }
        //sleep(5);
        //return $this->redirect(['index','sort'=>'-id']);
    }
    
    public function actionEnviarCorreos(){
        //correos
        $comprobantes_pendientes = DgtFe::find()
        ->andWhere(['!=','correoNotificacion',1])
        ->andWhere(['or',
            ['envioEstado'=>202],
            ['envioEstado'=>400]
        ])
        ->andWhere(['=','estadoEstado',1])
        ->all();
       
        echo 'Cantidad de correos: '.count($comprobantes_pendientes);
        foreach($comprobantes_pendientes as $compro){
            echo $comprobante->comprobante->id . '<br>';
            if(
                isset($compro->comprobante) && 
                $compro->comprobante->idColaboradors
            ){
                FuncionesDGT::enviarCorreo($compro->comprobante);
            }else{
                if(!isset($compro->comprobante)){
                    echo '->Sin comprobante<br>';
                }
                if(!$compro->comprobante->idColaboradors){
                    echo '->Sin colaborador<br>';
                }
                $modelCompro = $compro->comprobante;
                $modelCompro->correoNotificacion = 0;
                $modelCompro->save(); 
            }
        }       
    }
    
	
	public function ejecutarUrl($url){
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true, 
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_HTTPHEADER => array(
			'Cache-Control: no-cache',
			'Content-Type: application/x-www-form-urlencoded'
			),
		));

		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		$response = curl_exec($curl);		
		$err = curl_error($curl);
		$code = curl_getinfo($curl, CURLINFO_HTTP_CODE);		
		curl_close($curl);
		if($code == 200)
			return json_decode($response);
		return false;
	}

	
    public function actionEstados(){
		$respuesta = $this->ejecutarUrl('http://apis.gometa.org/status/status.json');
		if($respuesta->{'api-prod'}->{'status'} != 'BAD'){		
			//consultar estados
			$comprobantes_pendientes = DgtFe::find()
			 ->andWhere(['or',
			   ['estadoEstado'=>0],
			   ['estadoEstado'=>200],
			   ['estadoEstado'=>400],
			   ['estadoEstado'=>403],
			   ['estadoEstado'=>404]
			])
			 ->andWhere(['or',
			 ['envioEstado'=>202],
			 ['envioEstado'=>400],
			 ])
			->all();
			
			echo 'Estados Cantidad '.count($comprobantes_pendientes).'<br>';       
			foreach($comprobantes_pendientes as $compro){
			   echo 'ID:'.$compro->idComprobante.' EE:'.$compro->estadoEstado.' ';
				
			   FuncionesDGT::getEstado([
					'Numeracion' => $compro            
				],$this->config);
			   
			   echo '<br>';
			} 
		}else{
			return 'DGT no esta estable';
		}		
    }
    
    
    
    public function actionImprimirPos($id){
        $model = $this->findModel($id);
         return $this->render('imprimir_pos', [
            'model' => $model
        ]);
    }
        
    public function actionNotaCredito() {
        $params = Yii::$app->request->post();
        return $this->redirect(['/notacredito/anular', 'id' => $params['id']]);
    }

   public function actionIndex() {
        $searchModel = new FacturaSearch();
       // $searchModel->consultarEstados($this->config);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $date = date('Y-m-d', strtotime('now'));
       
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'config' => $this->config,
        ]);
    }

    public function actionComprobante() {
        $model = $this->findModel(FuncionesDGT::getPostGetParams()['id']);

        if ($model->estado == 'Facturada' || $model->estado == 'Anulada')
            return $this->redirect(['imprimir', 'id' => $model->id]);
        
        $personas = [];
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $model->VerFechas();
            return $this->render('update', [
                'model' => $model,
                'personas' => $personas,
                'config'=>$this->config
            ]);
        }
    }

    public function actionFacturar(){
        
        $params = Yii::$app->request->post();
        $model = $this->findModel($params['id']);
        $detalles = DetalleFactura::find()->where(['factura' => $params['id']])->all();
        $model->fecha= Factura::getFormatoFecha();
        if (count($detalles) > 0 && count($model->idColaboradors) > 0) {
        
            $model->estado = 'Facturada';
            if ($model->update()) {
                $model->actualizarInventario($detalles);                
                
                if($this->config['AmbienteTerminal'] === 'Local')
                    DgtColas::setItem($model,$this->config['Terminal'],Factura::CODFE);
        
                Yii::$app->session->setFlash('success', 'Se ha generado un nuevo comprobante');
                return $this->redirect(['enviar-comprobante', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('danger', 'Error al facturar ; Codigo 001');
            }
        } else {
            if (count($detalles) == 0) {
                Yii::$app->session->setFlash('danger', 'Comprobante sin detalles, debes agregar al menos un detalle.');
            }
            if (count($model->idColaboradors) == 0) {
                Yii::$app->session->setFlash('danger', 'No hay receptores asociados a este comprobante, debes agregar a un receptor.');
            }
        }
        
            
        return $this->redirect(['comprobante', 'id' => $model->id]);
    }

    public function actualizarTipoCambio(){
        try {
            $fecha = date('d/m/Y');
            $url ="https://gee.bccr.fi.cr/indicadoreseconomicos/WebServices/wsIndicadoresEconomicos.asmx/ObtenerIndicadoresEconomicos?tcIndicador=318&tcFechaInicio=$fecha&tcFechaFinal=$fecha&tcNombre=Jarvis&tnSubNiveles=N";
            $datos = [
                'tcIndicador'=>'318',
                'tcFechaInicio'=>date('d/m/Y'),
                'tcFechaFinal'=>date('d/m/Y'),
                'tcNombre'=>'Jarvis FE',
                'tnSubNiveles'=>'N',
            ];
           
            $curl = curl_init();
          
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                    "Cache-Control: no-cache",
                    "Content-Type: application/x-www-form-urlencoded",
                ),
            ));

            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            
            $response = curl_exec($curl);
            $err = curl_error($curl);
            $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);              
            curl_close($curl);
            if( $code >= 200 && $code <= 299){        
                /*Recortar los datos de la respuesta que requiero*/
                $posIni = strpos($response, '<INGC011_CAT_INDICADORECONOMIC');
                $posFin = strpos($response, '</INGC011_CAT_INDICADORECONOMIC>');               
                $datos = substr($response, $posIni,($posFin-$posIni));            
                $datos = $datos.'</INGC011_CAT_INDICADORECONOMIC>';
                $datos = str_replace('diffgr:id="INGC011_CAT_INDICADORECONOMIC1"','',$datos);
                $datos = str_replace('msdata:rowOrder="0"','',$datos);
              
                $xml = simplexml_load_string($datos);
                $valor = (int)$xml->NUM_VALOR;
                Yii::$app->cache->set('tipo_cambio', $valor);
                return true;
            }else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }
    
    public function actionActualizar() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $params = FuncionesDGT::getPostGetParams();
        $model = $this->findModel($params['id']);
        if ($model) {
            $model->dgt_medioPago = $params['medioPago'];
            $model->dgt_condicion = $params['condicion'];
            $model->dgt_moneda = $params['moneda'];
            $model->receptorRapido = $params['receptorRapido'];
            if($model->dgt_moneda == 'CRC')
                $model->dgt_tipoCambio = 1;
            else{
                if($this->actualizarTipoCambio())
                    $model->dgt_tipoCambio = Yii::$app->cache->get('tipo_cambio');
                else                
                    $model->dgt_tipoCambio = $this->config['TipoCambio'];                
            }
            
            $model->idUsua = $params['usuario'];
            $_SESSION['usuario'] = $model->idUsua;
            $model->fecha = Factura::getFormatoFecha();
            if ($model->save()) {
                return ['msj' => 'El medio se ha cambiado a :{$model->dgt_medioPago} - la condicion a {$model->dgt_condicion}'];
            } else {
                return ['msj' => $model->errors];
            }
        } else {
            return ['msj' => 'no se encuetra en bd'];
        }
    }
   
    public function actionDetalle() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($_POST['accion'] == 'agregar') {
            $model = new DetalleFactura();
            //FG::logfile('Detalle');
            $impuesto = $model->setDetalle($_POST);            
            if ($model->validate()) {
                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();
                try {                   
                    if ($model->save()) {                        
                        if ($impuesto->monto != 0) {
                            $impuesto->idDetalle = $model->id;
                            if (!$impuesto->save()) {
                                return ['errors_i' => $impuesto->errors];
                            }
                        }
                        //FG::logfile('Set Detalle');
                        $model->factura0->setDetalle($model);
                        $transaction->commit();
                    } else {
                        return ['errors_d' => $model->errors];
                    }
                } catch (Exception $e) {
                    $transaction->rollback();
                    return ['errors_t' => $model->errors];
                }
            } else {
                return ['errors_v' => $model->errors];
            }
            return ['msn' => 'agregar'];
        }
        if ($_POST['accion'] == 'quitar') {
            $model = DetalleFactura::findOne((int) $_POST['id']);
            if ($model) {
                $model->factura0->getDetalle($model);
                $model->delete();
                return ['msn' => 'quitar'];
            } else {
                return ['msn' => 'no se encuetra en bd'];
            }
        }
    }

    public function actionColaborador() {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($_POST['accion'] == 'agregar') {
            $model = new FacturaColaboradores();
            $model->id_factura = $_POST['id'];
            $model->id_colaborador = $_POST['id_col'];
            $model->save();
            return ['msn' => 'Se asignó de forma correcta'];
        }
        if ($_POST['accion'] == 'quitar') {
            $model = FacturaColaboradores::find()
                    ->where(['id_factura' => $_POST['id'], 'id_colaborador' => $_POST['id_col']])
                    ->one();
            if ($model) {
                $model->delete();
                return ['msn' => 'quitar'];
            } else
                return ['msn' => 'no se encuetra en bd'];
        }
    }

    public function actionCreate() {
        //$facturaPendiente = Factura::find()->where(['estado'=>'Procesando'])->one();
        //if(!empty($facturaPendiente))
          //  return $this->redirect(['comprobante', 'id' => $facturaPendiente->id]);
        
        $model = new Factura();
        $model->fecha = Factura::getFormatoFecha();
        $model->tipo = 'venta';
        $model->dgt_tipo = Factura::CODFE; //FE
        $model->dgt_condicion = $this->config['FormaPago'];
        $model->dgt_medioPago = $this->config['MedioPago'];
        $model->idUsua = Yii::$app->user->identity->id;
        if (isset($_SESSION['usuario'])) {
            $model->idUsua = $_SESSION['usuario'];
        } else {
            $_SESSION['usuario'] = Yii::$app->user->identity->id;
        }
        $model->estado = 'Procesando';
        $model->dgt_moneda = $this->config['Moneda'];
        $model->dgt_tipoCambio = $this->config['TipoCambio'];
        $model->GuardarFechas();
        if ($model->save()) {
            if ($model->tipo == 'venta')
                Yii::$app->session->setFlash('success', 'Nueva venta o ingreso.');
            else
                Yii::$app->session->setFlash('danger', 'Nueva compra o gasto.');

            $factCol = new FacturaColaboradores();
            $factCol->id_factura = $model->id;
            $factCol->id_colaborador = $this->config['ClientePorDefecto'];
            $factCol->save();
        }else{
            Yii::$app->session->setFlash('warning', 'No se ha podido generar la factura.');            
            return $this->redirect(['index']);
        }

        return $this->redirect(['comprobante', 'id' => $model->id]);
    }
    
     public function actionGenerarPdfId($id) {
        $model = $this->findModel($id);
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'content' => $this->renderPartial($this->config['PlantillaPdf'], [
                    'model' => $model,
                    'configuraciones' => $this->config
                    ]
                ),
            'cssInline' => '
            .panel-default>.panel-heading {
                color: white!important;
                background-color: black!important;
                border-color: #ddd!important;
            }
          ',
           // 'cssFile' => '@web/css/pdf.css',
            'options' => [
                'title' => 'Comprobante de factura',
                'subject' => ''
            ],
            'methods' => [
                'SetHeader' => [CodigosComprobantes::TIPOCOMPROBANTES[($model->comprobante)?$model->comprobante->tipoComprobante:'01']],
                'SetFooter' => [ (($this->config['MostrarSloganEnFactura']) ? $this->config['Slogan'] : '') .' || <a hr="http://webcomcostarica.com/">WEBCOMCOSTARICA.COM</a>'],
            ]
        ]);
        return $pdf->render();
        
    }

    public function generarPdf($model,$config){
        if(!empty($model->comprobante)){
             $ambiente = FuncionesDGT::getAmbienteSistema($config);
             $comprobante = $model->comprobante;
             $comprobante->pathComprobantePdf = Yii::getAlias('@webroot') . $ambiente['paths'][$comprobante->tipoComprobante] . 'pdf_' .$comprobante->numConce . '.pdf';
             $comprobante->save();
             if(!file_exists($comprobante->pathComprobantePdf)){
                 $viewPlantilla = "/factura/" . $config['PlantillaPdf'];
                  $content = $this->renderPartial($viewPlantilla, [
                     'model' => $model,
                     'configuraciones' => $config
                 ]);
                 $css = file_get_contents(Yii::getAlias('@webroot'). '/css/lite_on.css');
                $pdf = new Pdf([
                    'mode' => Pdf::MODE_UTF8, 
                    'format' => Pdf::FORMAT_A4, 
                    'orientation' => Pdf::ORIENT_PORTRAIT, 
                    'destination' => 'F',
                  //  'cssFile' => ,
                  'cssInline' => $css
                ]);
    
                return $pdf->output(
                $content,
                $comprobante->pathComprobantePdf,
                'F'
                ); 
            }
        }
     }

    public function actionImprimir($id) {
        $model = $this->findModel($id);
        if(!isset($model->comprobante))
            return $this->redirect(['index','sort'=>'-id']);
        
        $model->VerFechas();
        $this->generarPdf($model,$this->config);
        return $this->render('imprimir', [
            'model' => $model,
        ]);
    }

    public function actionEnviarFacturas() {       
        $comprobantes_enviar = DgtFe::find()
        ->andWhere(['or',
        ['envioEstado'=>0],
        ['envioEstado'=>522],
        ['envioEstado'=>500],
        ])      
        ->andWhere(['=','estadoEstado',0])
        ->with('comprobante')   
        ->all();
        
        echo 'Faltan de enviar '.count($comprobantes_enviar).'<br>'; 
            
        foreach($comprobantes_enviar as $compro){
            if($this->enviarComproDgt($compro->comprobante))               
                break;
        }
    }
    
   public static function enviarComproDgt($comprobante){
    if (!empty($comprobante->comprobante)) {                
        $pathComprobante = $comprobante->comprobante->pathComprobanteXml;              
        $ambiente = FuncionesDGT::getAmbienteSistema(SisConfiguraciones::getParametrosGenerales());
        if(!file_exists($pathComprobante)) {                    
            $numeracion = $comprobante->comprobante;
            $numeracion->pathComprobanteXml = 
            $pathComprobante =                     
                Yii::getAlias('@webroot') . 
                $ambiente['paths']['01'] . 
                $numeracion->numConce . 
                '_comprobante' .
                '.xml';
            if(!$numeracion->update()){
                Yii::$app->session->setFlash('warning', 'No hay comprobante XML generado.');
                return false;
            }                        
        }
        $xml = simplexml_load_file($pathComprobante);
        $data['Emisor']['Identificacion']['Tipo'] = $xml->Emisor->Identificacion->Tipo.'';
        $data['Emisor']['Identificacion']['Numero'] = $xml->Emisor->Identificacion->Numero.'';
        $data['FechaEmision'] = $xml->FechaEmision.'';

        if (isset($xml->Receptor)){
            $data['Receptor']['Identificacion']['Tipo'] = $xml->Emisor->Identificacion->Tipo.'';
            $data['Receptor']['Identificacion']['Numero'] = $xml->Emisor->Identificacion->Numero.'';
        }
        
        $result = FuncionesDGT::sendComprobante(
            file_get_contents($pathComprobante),
            FuncionesDGT::getToken(),
            $data,
            $comprobante->comprobante,
            $ambiente
        );                        
                          

    }else{
        Yii::$app->session->setFlash('danger', 'Debe generar un comprobante.');
        return false;
    }
   }
    
    public function actionEnviarComprobanteGenerado() {
        if(!isset($_POST['id']))
            return $this->redirect(['index']);   
              
        $comprobante = Factura::findOne($_POST['id']);
        if (FuncionesDGT::ValidarConexionHacienda()) {
            if($this->enviarComproDgt($comprobante)) 
                return $this->render('imprimir', [
                    'model' => $comprobante,
                ]);
            else
                return $this->redirect(['index']);
        }else{
            Yii::$app->session->setFlash('danger', 'Hacienda no responde.');
            return $this->redirect([
            'imprimir', 
            'id' =>$_POST['id']]);                    
        }
    }
    
    public function actionEnviarComprobante() {
        $id = Yii::$app->request->queryParams['id'];
        $comprobante = Factura::findOne($id);
        
            if (empty($comprobante->comprobante)) {
                $config = $this->config;
                $datos = [];
                $datos['emi_nombre']    = $config['NombreComercial'];
                $datos['emi_tipo']      = sprintf("%02d",$config['Persona']);
                $datos['emi_numeroId']  = $config['Cedula'];
                $datos['emi_provincia'] = $config['Provincia'];
                $datos['emi_canton']    = $config['Canton'];
                $datos['emi_distrito']  = $config['Distrito'];
                $datos['emi_barrio']    = $config['Barrio'];
                $datos['emi_otrasenas'] = $config['OtrasSenas'];
                $datos['emi_codPais']   = $config['CodPais'];
                $datos['emi_numTel']    = $config['Telefono'];
                $datos['emi_correo']    = $config['Correo'];

                if (isset($comprobante->idColaboradors[0])) {
                    if ($comprobante->idColaboradors[0]->id != $this->config['ClientePorDefecto']) {
                        $datos['rec_nombre'] = $comprobante->idColaboradors[0]->getNombreCompleto();
                        $datos['rec_tipo'] = sprintf("%02d",$comprobante->idColaboradors[0]->tipoIdentificacion);
                        $datos['rec_numeroId'] = $comprobante->idColaboradors[0]->cedula;
                        
                        if(isset($comprobante->idColaboradors[0]->pais))
                            $datos['rec_codPais'] = $comprobante->idColaboradors[0]->pais;                    
                        if(isset($comprobante->idColaboradors[0]->telefono))
                            $datos['rec_telefono'] = $comprobante->idColaboradors[0]->telefono;
                    
                        if(isset($comprobante->idColaboradors[0]->correo))
                        $datos['rec_correo'] = $comprobante->idColaboradors[0]->correo;
                    }
                }

                $datos['com_condicionVenta'] = sprintf("%02d", $comprobante->dgt_condicion); //Credito, contado, apartado
                if ($comprobante->dgt_condicion == '02') {
                    $datos['com_plazoCredito'] = $comprobante->dgt_plazoCredito;
                }
                $datos['com_medioPago'] = $comprobante->dgt_medioPago;
                $datos['com_tipMoneda'] = $comprobante->dgt_moneda;
                $datos['com_tipCambio'] = $comprobante->dgt_tipoCambio;
                $datos['com_totSerGra'] = $comprobante->dgt_serviciosGravados;
                $datos['com_totSerExe'] = $comprobante->dgt_serviciosExentos;
                $datos['com_totMerGra'] = $comprobante->dgt_mercaGravada;
                $datos['com_totMerExe'] = $comprobante->dgt_mercaExenta;
                $datos['com_totGravado'] = $comprobante->dgt_gravado;
                $datos['com_totExento'] = $comprobante->dgt_totExento;
                $datos['com_totVenta'] = $comprobante->dgt_totVenta;
                $datos['com_totDescu'] = $comprobante->dgt_totDescuento;
                $datos['com_totVenNet'] = $comprobante->dgt_totVentaNeta;
              //  FG::logfile('Facturar');
            //    FG::logfile('Arr: '.$datos['com_totVenNet']);
                $datos['com_monImp'] = $comprobante->dgt_totImpuesto;
                $datos['com_totCompro'] = $comprobante->dgt_total;
                $datos['com_resolucion'] = $config['Resolucion'];
                $datos['com_FechaResolucion'] = $config['FechaResolucion'];

                $detalles = [];
                foreach ($comprobante->detalleFacturas as $key => $detalle) {
                    $key++;

                    $row = [
                        'NumeroLinea' => $key,
                        'Codigo' => [
                            'Tipo' => sprintf("%02d", $detalle->tipo), //nota 12
                            'Codigo' => ($detalle->lote) ? $detalle->lote : $detalle->id,
                        ],
                        'Cantidad' => $detalle->cantidad,
                        'UnidadMedida' => $detalle->unidad,
                        'Detalle' => $detalle->descripcion,
                        'PrecioUnitario' => $detalle->lote_precio,
                        'MontoTotal' => $detalle->montoTotal,
                    ];


                    if ($detalle->montoDescuento > 0) {
                        $row['MontoDescuento'] = $detalle->montoDescuento;
                        $row['NaturalezaDescuento'] = $detalle->naturalezaDescuento;
                    }


                    $row['SubTotal'] = $detalle->subTotal;

                    if (count($detalle->impuestosaplicados) > 0) {
                        foreach ($detalle->impuestosaplicados as $impuesto) {
                            $row['Impuesto'] = [
                                'Codigo' => $impuesto['codigo'],
                                'Tarifa' => $impuesto['tarifa'],
                                'Monto' => $impuesto['monto']
                            ];
                            //TODO Exoneraciones
                        }
                    }

                    $row['MontoTotalLinea'] = $detalle->montoTotalLinea;
                    $detalles[$key] = $row;
                }

                $config['tipoComprobante'] = '01';
                $datos['detalles'] = $detalles;

                $numeracion = FuncionesDGT::ClaveNumerica($datos, $config, $comprobante);
                if (!empty($numeracion)) {
                    $datos['clave'] = $numeracion->clave;
                    $datos['numConce'] = $numeracion->numConce;
                    $datos['fechaEmision'] = $numeracion->fechaEmision;
                    $datosFinales = FuncionesDGT::ComprobanteXML($datos, $config, $numeracion);

                    if (!empty($datosFinales)) {
                        return $this->redirect(['imprimir', 'id' => $comprobante->id]);
                    }
                }else{
					Yii::$app->session->setFlash('danger', '003 La numeracion no se genero correctamente.');
				}
            } else {
                Yii::$app->session->setFlash('danger', 'Este documento ya posee un comprobante de Hacienda');
            }
                return $this->redirect(['imprimir', 'id' => $comprobante->id]);
                    
        }

    public function actionEstado($id) {
        $numeracion = DgtFe::find()->where(['idComprobante' => $id])->with('comprobante')->one();
        if(FuncionesDGT::ValidarConexionHacienda())
            FuncionesDGT::getEstado([
                'Numeracion' => $numeracion
            ],$this->config);
            
        if(isset($_GET['destination']))
            return $this->redirect([$_GET['destination'], 'sort'=>'-id']);
        
        return $this->redirect(['imprimir', 'id' => $id]);
    }

    public function actionDelete($id) {
        $model = $this->findModel($id);
        if ($model->estado == 'Procesando') {
            $detalles = DetalleFactura::find()->where(['factura' => $id])->all();
            foreach ($detalles as $deta) {
                $deta->delete();
            }
            $colaboradores = FacturaColaboradores::find()
                    ->where(['id_factura' => $id])
                    ->all();
            foreach ($colaboradores as $deta) {
                $deta->delete();
            }
            $model->delete();
        }
        
        if (AccessHelpers::getAcceso('factura-borrar')){

            $notas = NcComprobante::find()->where(['factura'=>$id])->all();
            if(!empty($notas)){
                foreach ($notas as $nota) {
                    $connection = Yii::$app->getDb();
                    $command = $connection->createCommand("
                    DELETE from  nc_colaboradores_comprobante 
                    WHERE nc_colaboradores_comprobante.id_factura=:id;                    
                    DELETE from  nc_detalles 
                    WHERE nc_detalles.factura=:id;                    
                    DELETE FROM  nc_dgt_comprobante 
                    WHERE nc_dgt_comprobante.idComprobante=:id;                    
                    DELETE FROM  nc_comprobante 
                    WHERE nc_comprobante.id=:id", 
                    [
                        ':id' => $nota->id
                    ]);
                    $result = $command->execute();
                }
            }

            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("
            DELETE from fe_colaboradores_comprobante 
            WHERE fe_colaboradores_comprobante.id_factura=:id;            
            DELETE from fe_detalles 
            WHERE fe_detalles.factura=:id;            
            DELETE FROM fe_dgt_comprobante 
            WHERE fe_dgt_comprobante.idComprobante=:id;
            DELETE FROM `fe_comprobante` WHERE `fe_comprobante`.`id`=:id", 
            [
                ':id' => $id
            ]);

            $result = $command->execute();
        }


        return $this->redirect(['index', 'sort' => '-id']);
    }

    protected function findModel($id) {
        $model = Factura::find()->where(['id'=>$id])->with(['comprobante','idColaboradors'])->one();
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use backend\models\Factura;
use backend\models\NcComprobante;
use backend\models\NcDetalles;
use backend\models\NcColaboradoresComprobante;
use backend\models\NcDgtComprobante;
use backend\models\base\DgtComprobante;
use backend\models\Configuraciones;
use common\components\FuncionesDGT;
use backend\controllers\BaseController;
use backend\models\search\NcComprobanteSearch;
use kartik\mpdf\Pdf;
use backend\models\Lote;

use common\models\FG;
/**
 * NotacreditoController implements the CRUD actions for NcDetalles model.
 */
class NotacreditoController extends BaseController
{
    
     public function actionEnviarComprobanteGenerado() {
        $id = $_POST['id'];
        $comprobante = NcComprobante::findOne($id);
        if (FuncionesDGT::ValidarConexionHacienda()) {
            if (!empty($comprobante->comprobante)) {                
                $ambiente = FuncionesDGT::getAmbienteSistema($this->config);  
                $pathComprobante = Yii::getAlias('@webroot') . $ambiente['paths'][$comprobante->comprobante->tipoComprobante] .$comprobante->comprobante->numConce . '_comprobante'.  '.xml';
               
                if (file_exists($pathComprobante)) {
                            $xml = simplexml_load_file($pathComprobante);                            
                            $data['Emisor']['Identificacion']['Tipo'] = $xml->Emisor->Identificacion->Tipo.'';
                            $data['Emisor']['Identificacion']['Numero'] = $xml->Emisor->Identificacion->Numero.'';
                            $data['FechaEmision'] = $xml->FechaEmision.'';
                            
                            if (isset($xml->Receptor)){
                                $data['Receptor']['Identificacion']['Tipo'] = $xml->Emisor->Identificacion->Tipo.'';
                                $data['Receptor']['Identificacion']['Numero'] = $xml->Emisor->Identificacion->Numero.'';
                            }                   
                        }                          
        
                 $result = FuncionesDGT::sendComprobante(
                        file_get_contents($pathComprobante),
                        FuncionesDGT::getToken(),
                        $data,
                        $comprobante->comprobante,
                        $ambiente
                );
                
             return $this->redirect(['imprimir', 'id' => $id]);
           
            }
        }
    }
    
    public function actionGenerarPdf($id) {
        
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'content' => $this->renderPartial('/factura/documento_pdf.php', [
                    'model' => $this->findModel($id),
                    'configuraciones' => $this->config
                    ]
                ),
            'cssInline' => '.esconde-print{ display: none; }',
            'options' => [
                'title' => 'Comprobante de factura',
                'subject' => ''
            ],
            'methods' => [
                'SetHeader' => ['¡ conectamos su negocio a la red !'],
                'SetFooter' => ['<a hr="http://webcomcostarica.com/">WEBCOMCOSTARICA.COM</a>'],
            ]
        ]);
        return $pdf->render();
        
    }
    
    public function actionAnular($id) {
        $model = new NcDetalles(); //Solo para recoger razón de anulación.
        $model->scenario = 'Anulacion';
        $model->factura = $id;
        if ($model->load(Yii::$app->request->post())) {            
            $factura = Factura::findOne($id);
            $notaCredito = new NcComprobante();
            $notaCredito->setAnulacion($factura);
            $notaCredito->dgt_razon = $model->descripcion;
            if ($notaCredito->save()){
                $ncrCol = new NcColaboradoresComprobante();
                $ncrCol->id_factura = $notaCredito->id;
                $ncrCol->id_colaborador = $factura->idColaboradors[0]->id;                
                if(!$ncrCol->save()){
                    echo 'NotaColaborador<br>';
                    var_dump($ncrCol->errors);
                    die;
                }
                
                foreach($factura->detalleFacturas as $detalle){
                    $ncNuevoDetalle = new NcDetalles();
                    $ncNuevoDetalle->descripcion = $detalle->descripcion;
                    $ncNuevoDetalle->factura = $notaCredito->id;
                    $ncNuevoDetalle->setNcDetalle($detalle);
                    if(!$ncNuevoDetalle->save()){
                        echo 'NotaDetalle<br>';
                        var_dump($ncNuevoDetalle->errors);
                        die;
                    }
                    //devolución de inventario
                    if($detalle->lote != 0){
                        $detalle->lote0->cantidad += $detalle->cantidad;
                        $detalle->lote0->cantidadVendida -= $detalle->cantidad;
                        $detalle->lote0->update();    
                    }
                }
                
                $factura->estado = 'Anulada';
                $factura->update();
                
                return $this->redirect(['enviar-comprobante', 'id' => $notaCredito->id]);
            
            } else {
                 echo 'NotaDetalle<br>';
                var_dump($notaCredito->errors);
                die;
            }
        }
        
        return $this->render('create', [
            'model' => $model,
        ]);
     }
     
    public function actionEnviarComprobante($id) {
        $id = Yii::$app->request->queryParams['id'];
        $comprobante = NcComprobante::findOne($id);
        if (empty($comprobante->comprobante)) {
            $config = $this->config;
            $datos = [];
            $datos['emi_nombre'] = $config['NombreComercial'];
            $datos['emi_tipo']      = sprintf("%02d",$config['Persona']);
            $datos['emi_numeroId']  = $config['Cedula'];
            $datos['emi_provincia'] = $config['Provincia'];
            $datos['emi_canton'] = $config['Canton'];
            $datos['emi_distrito'] = $config['Distrito'];
            $datos['emi_barrio'] = $config['Barrio'];
            $datos['emi_otrasenas'] = $config['OtrasSenas'];
            $datos['emi_codPais'] = $config['CodPais'];
            $datos['emi_numTel'] = $config['Telefono'];
            $datos['emi_correo'] = $config['Correo'];
            
            if (isset($comprobante->idColaboradors[0])) {
                if ($comprobante->idColaboradors[0]->id != $this->config['ClientePorDefecto']) {
                    $datos['rec_nombre'] = $comprobante->idColaboradors[0]->getNombreCompleto();
                    $datos['rec_tipo'] = sprintf("%02d",$comprobante->idColaboradors[0]->tipoIdentificacion);
                    $datos['rec_numeroId'] = $comprobante->idColaboradors[0]->cedula;
                    $datos['rec_codPais'] = $comprobante->idColaboradors[0]->pais;
                    $datos['rec_telefono'] = $comprobante->idColaboradors[0]->telefono;
                    $datos['rec_correo'] = $comprobante->idColaboradors[0]->correo;
                }
            }

            $datos['com_condicionVenta'] = sprintf("%02d", $comprobante->dgt_condicion); //Credito, contado, apartado
            if ($comprobante->dgt_condicion == '02') {
                $datos['com_plazoCredito'] = $comprobante->dgt_plazoCredito;
            }
            $datos['com_medioPago'] = $comprobante->dgt_medioPago;
            $datos['com_tipMoneda'] = $comprobante->dgt_moneda;
            $datos['com_tipCambio'] = $comprobante->dgt_tipoCambio;
            $datos['com_totSerGra'] = $comprobante->dgt_serviciosGravados;
            $datos['com_totSerExe'] = $comprobante->dgt_serviciosExentos;
            $datos['com_totMerGra'] = $comprobante->dgt_mercaGravada;
            $datos['com_totMerExe'] = $comprobante->dgt_mercaExenta;
            $datos['com_totGravado'] = $comprobante->dgt_gravado;
            $datos['com_totExento'] = $comprobante->dgt_totExento;
            $datos['com_totVenta'] = $comprobante->dgt_totVenta;
            $datos['com_totDescu'] = $comprobante->dgt_totDescuento;
            $datos['com_totVenNet'] = $comprobante->dgt_totVentaNeta;
            $datos['com_monImp'] = $comprobante->dgt_totImpuesto;
            $datos['com_totCompro'] = $comprobante->dgt_total;
            $datos['com_resolucion'] = $config['Resolucion'];
            $datos['com_FechaResolucion'] = $config['FechaResolucion'];

            $detalles = [];
            foreach ($comprobante->detalleFacturas as $key => $detalle) {
                $key++;

                $row = [
                    'NumeroLinea' => $key,
                    'Codigo' => [
                        'Tipo' => sprintf("%02d", $detalle->tipo), //nota 12
                        'Codigo' => ($detalle->lote) ? $detalle->lote : $detalle->id,
                    ],
                    'Cantidad' => $detalle->cantidad,
                    'UnidadMedida' => $detalle->unidad,
                    'Detalle' => $detalle->descripcion,
                    'PrecioUnitario' => $detalle->lote_precio,
                    'MontoTotal' => $detalle->montoTotal,
                ];


                if ($detalle->montoDescuento > 0) {
                    $row['MontoDescuento'] = $detalle->montoDescuento;
                    $row['NaturalezaDescuento'] = $detalle->naturalezaDescuento;
                }


                $row['SubTotal'] = $detalle->subTotal;

                if (count($detalle->impuestosaplicados) > 0) {
                    foreach ($detalle->impuestosaplicados as $impuesto) {
                        $row['Impuesto'] = [
                            'Codigo' => $impuesto['codigo'],
                            'Tarifa' => $impuesto['tarifa'],
                            'Monto' => $impuesto['monto']
                        ];
                        //TODO Exoneraciones
                    }
                }

                $row['MontoTotalLinea'] = $detalle->montoTotalLinea;
                $detalles[$key] = $row;
            }

            $config['tipoComprobante'] = '03';
            $datos['detalles'] = $detalles;

            $comprobanteAnulado = $comprobante->comprobanteAnulado->comprobante;
            $datos['ref_tipoDoc'] = '03';//Anular doc ref
            $datos['ref_clave'] = $comprobanteAnulado->clave;
            $datos['ref_fechaEmision'] = $comprobanteAnulado->fechaEmision;
            $datos['ref_codigoComprobante'] = $comprobante->comprobanteAnulado->dgt_tipo;
            $datos['ref_razon'] = 'Anular Factura electronica';	
            
            $numeracion = FuncionesDGT::ClaveNumerica($datos, $config, $comprobante);
            if (!empty($numeracion)) {
                $datos['clave'] = $numeracion->clave;
                $datos['numConce'] = $numeracion->numConce;
                $datos['fechaEmision'] = $numeracion->fechaEmision;
                $datosFinales = FuncionesDGT::ComprobanteXML($datos, $config, $numeracion);

                if (!empty($datosFinales)) {
                    return $this->redirect(['imprimir', 'id' => $comprobante->id]);
                }
            }
        } else {
            Yii::$app->session->setFlash('danger', 'Este documento ya posee un comprobante de Hacienda');
        }

        return $this->redirect(['imprimir', 'id' => $comprobante->id]);
    }

    public function actionEstado($id)
    {
        $model = NcDgtComprobante::find()->where(['idComprobante' => $id])->one();
        FuncionesDGT::getEstado([
            'Numeracion' => $model
        ],$this->config);
        if($model->estadoEstado == 1){
            $comprobanteAnulado = $model->idFactura0->comprobanteAnulado->comprobante;
            $comprobanteAnulado->estadoEstado = 800;
            $comprobanteAnulado->save();
        }
        return $this->redirect(['imprimir', 'id' => $id]);
    }
    public function generarPdf($model,$config){
        if(!empty($model->comprobante)){
             $ambiente = FuncionesDGT::getAmbienteSistema($config);
             $comprobante = $model->comprobante;
             $comprobante->pathComprobantePdf = Yii::getAlias('@webroot') . $ambiente['paths'][$comprobante->tipoComprobante] . 'pdf_' .$comprobante->numConce . '.pdf';
             $comprobante->save();
             if(!file_exists($comprobante->pathComprobantePdf)){
                 $viewPlantilla = "/factura/" . $config['PlantillaPdf'];
                  $content = $this->renderPartial($viewPlantilla, [
                     'model' => $model,
                     'configuraciones' => $config
                 ]);
                $pdf = new Pdf([
                    'mode' => Pdf::MODE_UTF8, 
                    'format' => Pdf::FORMAT_A4, 
                    'orientation' => Pdf::ORIENT_PORTRAIT, 
                    'destination' => 'F',
                ]);
    
                return $pdf->output(
                $content,
                $comprobante->pathComprobantePdf,
                'F'
                ); 
            }
        }
     }

    public function actionImprimir($id) {
        $model = NcComprobante::find()->where(['id'=>$id])->one();
        $this->generarPdf($model,$this->config);   
        return $this->render('imprimir', [
            'model' => $model,
        ]);
    }
    
   public function actionIndex()
   {
       $searchModel = new NcComprobanteSearch();
       $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
       $searchModel->consultarEstados($this->config);
       return $this->render('index',[
           'searchModel' => $searchModel,
           'dataProvider' => $dataProvider,
           'config' => $this->config
       ]);
   }
   
   public function actionDelete($id) {
        $model = $this->findModel($id);
               
        if (AccessHelpers::getAcceso('notacredito-borrar')){
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("
                DELETE from  nc_colaboradores_comprobante 
                WHERE nc_colaboradores_comprobante.id_factura=:id;                    
                DELETE from  nc_detalles 
                WHERE nc_detalles.factura=:id;                    
                DELETE FROM  nc_dgt_comprobante 
                WHERE nc_dgt_comprobante.idComprobante=:id;                    
                DELETE FROM  nc_comprobante 
                WHERE nc_comprobante.id=:id", 
            [
                ':id' => $id
            ]);

            $result = $command->queryAll();
        }

        return $this->redirect(['index', 'sort' => '-id']);
    }
    
    protected function findModel($id) {
        if (($model = NcComprobante::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

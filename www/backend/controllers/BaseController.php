<?php 
namespace backend\controllers; 
use Yii; 
use yii\web\Controller; 
use common\models\AccessHelpers;
use backend\models\Configuraciones;
use backend\models\SisConfiguraciones;
 
class BaseController extends Controller { 
 
    public $config;
 
    public function beforeAction($action) {
       $operacion = str_replace("/", "-", Yii::$app->controller->route);  
       $permitirSiempre = ['site-ayuda','site-dashboard', 'site-index', 'site-error', 'site-about', 'site-login', 'site-logout'];
     
       if (Yii::$app->user->identity){      
           if (\Yii::$app->user->isGuest) {
                $this->layout = 'visitante';
                if($operacion != 'site-index' && $operacion != 'site-login')
                    $this->redirect('/site/index');
            }
                    
             
            $this->config = SisConfiguraciones::getParametrosGenerales();
            if($this->config['contingencia'])
                Yii::$app->session->setFlash('warning', 'MODO DE CONTINGENCIA ACTIVADO.');
            
            if(!strpos($operacion,'site')){
             if($this->config['Moroso'])
                Yii::$app->session->setFlash('danger', 'Hay facturas pendientes.');
             if(!$this->config['Ambiente'])
                Yii::$app->session->setFlash('warning', 'MODO DE PRUEBAS ACTIVADO.');       
            }
            if (!parent::beforeAction($action)) { 
                 return false; 
            } 
            
           
            if (in_array($operacion, $permitirSiempre)) {
                return true;
            }

            if (!AccessHelpers::getAcceso($operacion)) {               
                echo $this->render('/site/nopermitido',
                        ['operacion'=>$operacion]);
                return false;
            }
            
            return true;
       }else{
           if (in_array($operacion, $permitirSiempre)) {
                return true;
           }
           return $this->redirect(['/site/login']);
       }
    }
}


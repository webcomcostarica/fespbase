<?php

namespace backend\controllers;

use Yii;
use backend\models\Colaborador;
use backend\models\Contactos;
use backend\models\search\ColaboradorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\FacturaColaboradores;
use backend\models\Factura;


use yii\web\Response;

/**
 * ColaboradorController implements the CRUD actions for Colaborador model.
 */
class ColaboradorController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionCorreo(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($_POST['id']);
        $model->correo = $_POST['cor'];
        if($model->update()){
            return [
                'mensaje'=>'Correo actualizado'
            ];
        }else{
            return $model->errors;
        }
    }
    /**
     * Lists all Colaborador models.
     * @return mixed
     */
    public function actionCodigo($cod)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model=  Colaborador::find()->where(
                ['like', 'id', "$cod"]
                )->one();
        if($model){
            return $model;
        }else{
            return 0;
        }     
    }


    public function getCedulaGometa($ced){
        $curl = curl_init(); 
        $url = "https://apis.gometa.org/cedulas/$ced";      
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(               
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded",
            ),
        ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
      
        $response = curl_exec($curl);
        $err = curl_error($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        $response = json_decode($response);
        if($response->resultcount > 0){
            $datos = $response->results[0];
           
            $modelCola = new Colaborador();
            $modelCola->GuardarFechas(); 
            $modelCola->VerFechas();
            $modelCola->tipo = 1; //Cliente
            if($datos->type == 'F'){
                $modelCola->tipoIdentificacion = 1;
                $modelCola->nombre = utf8_decode($datos->firstname1).' '.utf8_decode($datos->firstname2);
                $modelCola->apellido = utf8_decode($datos->lastname1);
                $modelCola->sapellido = utf8_decode($datos->lastname2);
                $modelCola->nombreCompleto =  $modelCola->nombre .' '.
                    $modelCola->apellido .' '.
                    $modelCola->sapellido;
            }else{
                $modelCola->tipoIdentificacion = 2;
                $modelCola->nombreCompleto = utf8_decode($datos->fullname);
                $modelCola->nombre = '.';
                $modelCola->apellido = '.';
                $modelCola->sapellido = '.';
            }
            $modelCola->pais = 506;
            $modelCola->cedula = $datos->cedula;
            $modelCola->nacimiento = date('Y-m-d');            

          
            if ($modelCola->validate()){
                if($modelCola->save())
                    return $modelCola;
            }else{
                var_dump($modelCola->errors);
                die;
            }
        }
        return null;
    }
    
    /**
     * Lists all Colaborador models.
     * @return mixed
     */
    public function actionCedula()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = null;
        if(!empty($_POST['ced'])){
            $model =  Colaborador::find()->where(['=', 'cedula', $_POST["ced"]])->one();
            if(empty($model)){
                $model = $this->getCedulaGometa($_POST['ced']);
            }
        }
        /* //Para pruebas desde get
        if(!empty($_GET['ced'])){

            $model =  Colaborador::find()->where(['=', 'cedula', $_GET["ced"]])->one();
            if(empty($model)){
                $model = $this->getCedulaGometa($_GET['ced']);
            }
        }*/
        if(!empty($_POST['id']))
            $model =  Colaborador::find()->where(['like', 'id', $_POST["id"]])->one();
         
        if(!empty($model)){
            $modelFC = FacturaColaboradores::find()->where(['id_factura' => $_POST['fac']])->one();
            if ($modelFC)
                $modelFC->delete();
            
            $modelFC = new FacturaColaboradores();
            $modelFC->id_factura = $_POST['fac'];
            $modelFC->id_colaborador = $model->id;
            $modelFC->save();
            $factura = Factura::find()->where(['id'=>$_POST['fac']])->one();
            
            $html = $this->renderAjax('/factura/factura/d_colaboradores', [
                'model' => $factura,
                'config' => $this->config
            ]               
            );       
       
       
            return [
                'html'=>$html,
                'model'=>$model];
        }
            return ['id'=>0];
             
    }
    
    /**
     * Lists all Colaborador models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(isset($_GET['venta']) && isset($_GET['id']) && isset($_GET['anterior'])){
            $model = FacturaColaboradores::find()
                    ->where(['id_factura' => $_GET['venta'], 'id_colaborador' => $_GET['anterior']])
                    ->one();
            if ($model)
                $model->delete();
            
            $model = new FacturaColaboradores();
            $model->id_factura = $_GET['venta'];
            $model->id_colaborador = $_GET['id'];
            $model->save();
                
            return $this->redirect(['/factura/comprobante', 'id' => $_GET['venta']]);
        }        
        
        $searchModel = new ColaboradorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * Lists all Colaborador models.
     * @return mixed
     */
    public function actionAdd_fact($id_fact)
    {
        $searchModel = new ColaboradorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->renderPartial('add_fact', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ],false,true);
    }

    /**
     * Displays a single Colaborador model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = Colaborador::find()->where(['id'=>$id])->with('idFacturas')->one();
        $model->VerFechas();
        return $this->render('view', [
            'model' => $model,
        ]);
    }

     /**
     * Creates a new Colaborador model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreatemodal()
    {
        Yii::$app->response->format = Response::FORMAT_JSON; 
		$model = new Colaborador(); 
		if($_POST["Colaborador"]["nacimiento"]){
			$model->nacimiento = $_POST["Colaborador"]["nacimiento"].'-2017';			
		}
       if($model->load(Yii::$app->request->post()) && $model->validate() ){
            $model->GuardarFechas();
            $model->save();  
            if($_POST["Colaborador"]["telefono"]){
                $contacto=new Contactos();
                $contacto->tipo="tel";
                $contacto->datos=$_POST["Colaborador"]["telefono"];
                $contacto->titulo="TELEFONO";
                $contacto->colaborador=$model->id;				
                $contacto->save();
            }
			  if($_POST["Colaborador"]["correo"]){
                $contacto=new Contactos();
                $contacto->tipo="correo";
                $contacto->datos = $_POST["Colaborador"]["correo"];
                $contacto->titulo="Correo";
                $contacto->colaborador=$model->id;				
                $contacto->save();
            }
            return $model;
       }  else {
           return $model->errors;
           //return 0;
       }
    }
    
    /**
     * Creates a new Colaborador model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Colaborador();
        $model->GuardarFechas(); 
        $model->VerFechas();
        $model->pais = 506;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {            
            
            $model->save();
            /*
            $conta = new Contactos();
            $conta->titulo = 'Telefono principal';
            $conta->datos = $model->telefono;
            $conta->tipo = 'tel';        
            $conta->colaborador = $model->id;
            $conta->save();
            
            $conta = new Contactos();
            $conta->titulo = 'Correo';
            $conta->datos = $model->correo;
            $conta->tipo = 'correo';        
            $conta->colaborador = $model->id;
            $conta->save();
            */
            
            if(isset($_GET['venta'])){
                $factCol = new FacturaColaboradores();
                $factCol->id_factura = $_GET['venta'];
                $factCol->id_colaborador = $model->id;
                $factCol->save();
                return $this->redirect(['/factura/comprobante', 'id' => $_GET['venta']]);                
            
            }
            
            if(isset($_GET['lote'])){
                return $this->redirect(['/lote/update', 'id' => $_GET['lote']]);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Colaborador model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->VerFechas();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {     		
            $model->GuardarFechas();
            $model->save();
            if(isset($_GET['venta'])){
                return $this->redirect(['/factura/comprobante', 'id' => $_GET['venta']]);                
            
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Colaborador model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if($id!=1){
			$model=$this->findModel($id);
            /*
			 foreach ($model->tbContactos as $fact){
				$fact->delete();
			}*/
            $model->delete();
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Colaborador model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Colaborador the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Colaborador::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	/**
     * Deletes an existing Colaborador model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionMejores()
    {
		$datos = Colaborador::find()->all();
		date_default_timezone_set('America/Costa_Rica');
		
		foreach($datos as $colaborador){
			$total =0;
			$totalCant=0;
			$colaborador->totalcompras = 0;
			if($colaborador->id!="20881"){
				$facturas = $colaborador->idFacturas;
				foreach($facturas as $fact){
					$string = '2017-07-01';
					$time1 = strtotime($string);
					$time2 = strtotime($fact->fecha);
					if($time2>$time1){						
						$total += $fact->total;
						$totalCant += 1;
					}
				}
				$colaborador->totalcompras += $total;
				$colaborador->cantidadcompras = $totalCant;
				$colaborador->save();
			}
		}
		echo 'listo';die;
		//recorrer todos los clientes != 20881
			//Recorrer todas sus facturas y sumar
				//facturadas
			
			//Desde hasta
    }

}

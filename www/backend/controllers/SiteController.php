<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;
use common\models\AccessHelpers;


use backend\models\Shortcuts;
/**
 * Site controller
 */
class SiteController extends BaseController
{
    
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }


    public function actionIndex()
    {
        if (Yii::$app->user->identity){
            $this->layout = 'main';
            $valor = Yii::$app->cache->get('tipo_cambio');
            if(empty($valor))
                Yii::$app->cache->set('tipo_cambio', $this->config['TipoCambio']);
           
            return $this->render('index');
        }else{
            return $this->redirect('login');
           
            /*          
            $this->layout = 'visitante';
            return $this->render('visitante');
            */
        }
    }

    public function actionAyuda(){
         return $this->render('ayuda');
    }

    public function actionPerfil(){
		echo 'Perfil';
		die;
	}
	
    public function actionLogin()
    {
        $this->layout = 'main';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
 
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}

<?php

namespace backend\controllers;

use Yii;
use backend\models\Lote;
use backend\models\search\LoteSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use backend\models\LotePrecio;
use yii\web\Response;

/**
 * LoteController implements the CRUD actions for Lote model.
 */
class LoteController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionRetirar(){ 
        $i=0;$resultados=[];
        if (isset($_POST["retirar"])) {
            if(trim($_POST["retirar"]['lotes'])!=""){
                $lotes=  explode(',', $_POST["retirar"]['lotes']);               
                foreach ($lotes as $lote){
                    if((int)$lote){
                        $Olote=Lote::findOne($lote);
                        $inicial=$Olote->cantidad;                        
                        $Olote->cantidad-=1;      
                        $Olote->cant_inicial-=1;
                        if($Olote->update()){
                            $resultados[$i]=[
                                'lote'=>$lote,
                                'inicial'=>$inicial,
                                'final'=>$Olote->cantidad,
                                'mns'=>'Correcto'
                                ];
                        }else{
                            $resultados[$i]=[
                                'lote'=>$lote,
                                'inicial'=>$inicial,
                                'final'=>$Olote->cantidad,
                                'mns'=>'NO SE QUITÓ'
                                ];                    
                        }                        
                    }
                    $i++;
                }
            }
        } 
        
        return $this->render('_form_retirar',[
            'resultados'=>$resultados
        ]);       
    }
    /**
     * Lists all Colaborador models.
     * @return mixed
     */
    public function actionCodigo($cod)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($_GET['tipo'] == 'inp_cod_int_lot')
            $model= Lote::findOne(['id'=>"$cod"]);
        else
            $model= Lote::findOne(['cod_inte_producto'=>"$cod"]);
        if($model){
        //$model->precio="Sin precio vigente";   
            /*
            foreach($model->lotePrecios as $precio){
                if($precio->vigente){
                    $model->precio=$precio->costo;
                }
            }
            */
            return $model;
        }else{
            return 0;
        }     
    }
    
    /**
     * Lists all Lote models.
     * @return mixed
     */
    public function actionIndex($id=null)
    {
        if(isset($_GET['loteSet'])){
            $_SESSION['loteSet'] = $_GET['loteSet'];
             return $this->redirect(['/factura/comprobante', 'id' => $_GET['venta']]);
        }
        
        $searchModel = new LoteSearch();
        if((int)$id!==0){
            $searchModel->paca = $id;
        }else{
            $searchModel->paca = null;
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'config'=>$this->config
        ]);
    }

    /**
     * Displays a single Lote model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model=  $this->findModel($id);
        $model->VerFechas();
        
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Lote model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new Lote();
        $model->paca=(int)$id;
        $model->calidad='...';
        $model->GuardarFechas();
        if(isset($_GET['venta'])){
            $_SESSION['loteSet'] = $model->id;
        }
        
        if($model->load(Yii::$app->request->post())){
			$model->cant_inicial = $model->cantidad;
            if ($model->validate()) {                
                $model->save();
                
                $precio= new LotePrecio();
                $precio->lote=$model->id;
                $precio->GuardarFechas();
                $precio->usuario=Yii::$app->user->identity->id;
                $precio->piezas=0;
                $precio->vigente="1";
                $precio->descripcion='Precio inicial';
                $precio->costo=$model->precio;
                if($precio->validate()){
                    $precio->save();
                }else{
                     return $this->render('create', [
                        'model' => $model,
                        'config'=>$this->config
                    ]);
                }
                
                if(isset($_GET['venta'])){
                    $_SESSION['loteSet'] = $model->id;    
                    return $this->redirect(['/factura/comprobante', 'id' => $_GET['venta']]);
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }else{
                 return $this->render('create', [
                    'model' => $model,
                    'config'=>$this->config
                ]);
            }        
        } else {
            
            return $this->render('create', [
                'model' => $model,
                'config'=>$this->config
            ]);
        }
    }

    /**
     * Lists all LotePrecio models.
     * @return mixed
     */
    public function actionPrecios($id)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => LotePrecio::find($id),
        ]);
        return $this->renderPartial('index', [
            'dataProvider' => $dataProvider,
        ],false,true);
    }
    
    /**
     * Updates an existing Lote model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {        
        $model = $this->findModel($id);
       // $calidades=  Diccionario::find()->where(['id_taxonomia'=>3])->all();        
        $model->VerFechas();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {            
           /*
			if(isset($_POST['Lote']['precio'])){
				$precios = LotePrecio::find()->where(['lote'=>$model->id])->all();
				foreach ($precios as $prace){
					if($prace->id==(int)$_POST['Lote']['precio']){
						$prace->vigente='1';
					}
					$prace->update();
				}
			}
			*/
            $model->GuardarFechas();
            $model->save();
            if(isset($_GET['venta'])){
                $_SESSION['loteSet'] = $model->id;
                return $this->redirect(['/factura/comprobante', 'id' => $_GET['venta']]);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'config'=>$this->config
            ]);
        }
    }

    /**
     * Deletes an existing Lote model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        foreach($model->lotePrecios as $precio){
            $precio->delete();
        }
        $model->delete();

        return $this->redirect(['index','id'=>$model->paca]);
    }

    /**
     * Finds the Lote model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Lote the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Lote::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

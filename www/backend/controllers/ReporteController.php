<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;
use common\models\AccessHelpers;
use backend\models\search\FacturaSearch;
use yii2tech\spreadsheet\Spreadsheet;
use yii\data\ArrayDataProvider;
use backend\models\VentasDiarias;
use backend\models\search\VentasDiariasSearch;

use backend\models\Factura;
/**
 * Reportes controller
 */
class ReporteController extends BaseController
{
    
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    
    public function actionIndex(){
         return $this->render('index');
    }
    //Hoy por defecto 
    //Desde - Hasta
    //d-m-y      
    public function actionDiarioDetalle(){       
        return $this->render('diario-detalle',[
            'config'=>$this->config
        ]);
    }
    public function actionDiarioResumen(){
        return $this->render('diario-resumen',[
            'config'=>$this->config
        ]);
    }
    
    //Mes actual por defecto
    //Desde - Hasta
    //m-y
    public function actionMesDetalle(){
        return $this->render('mes-detalle',[
            'config'=>$this->config
        ]);
    }

    //Año actual por defecto
    //Desde - Hasta
    //y    
    public function actionMesResumen(){
        //seleccionar el ultimo registro de dia resumido
        //Calcular los dias resumidos
        //Presentar reporte
        
        VentasDiarias::GenerarResumen();
        $searchModel = new VentasDiariasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('mes-resumen',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'config'=>$this->config,
        ]);
    }
    
    public function actionAnualDetalle(){
        echo 'Anual';
    }
    
    public function actionToExcel(){
        $searchModel = new FacturaSearch();
        $dataProvider = $searchModel->diarioDeVentas(Yii::$app->request->queryParams);
        
        $exporter = new Spreadsheet([
            'dataProvider' => $dataProvider,
            'columns' => [
                'fecha',
                
                [
                    'attribute'=>'receptorNombre',
                    'value'=>function($model){
                        return $model->getReceptorNombre();   
                    }
                ],
                'comprobante.numConce',
                'receptorCedula',
                'receptorNombre',
                'dgt_gravado',
                'dgt_totExento',
                'dgt_totDescuento',
                'dgt_totImpuesto',
                'dgt_total',
                
            ],
        ]);             
        $exporter->save(Yii::getAlias('@webroot').'/fileexcel.xls');
    }
    
    public function actionDiarioDeVentas()
    {            
        return $this->render('diario-de-ventas',['config'=>$this->config]);     
    }
    
    public function actionDiarioDeVentasPersonalizado()
    {
        $searchModel = new FacturaSearch();
        $dataProvider = $searchModel->diarioDeVentas(Yii::$app->request->queryParams);
           
        return $this->render('diario-de-ventas-personalizado', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);     
    }
}

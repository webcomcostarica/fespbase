<?php

namespace backend\controllers;

use Yii;
use backend\models\Horas;
use backend\models\search\HorasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\web\Response;
use backend\models\Mensajes;

/**
 * HorasController implements the CRUD actions for Horas model.
 */
class AccionesController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

	public function actionPerfil(){
		return $this->render('../colaborador/perfil');
	}
	
	
	
    /**
     * Lists all Horas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HorasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

	 /**
     * Enviar sms de cobro a un cliente
	 * 11 no se envio nada
	 * 3 solo numero sin mensaje
	 * 
     */
    public function actionSms()
    {		
        Yii::$app->response->format = Response::FORMAT_JSON;
		if(isset($_POST['telefono']))
			$numero =(int)$_POST["telefono"];
		else
			$numero = '83168582';
		
        if($numero){
			$txt = 'Estimado cliente usted posee un *apartado* pendiente en la Americana Maria Jose, estamos frente a super Bermanos Mora, BA. Gracias por poner al dia sus cuentas.';			
			$respuesta = file_get_contents("https://api.sms506.com/sms/25e141399d4bde983f4d3d4bebbdb732/t=$numero&m=$txt");
			$respuesta = trim($respuesta);
			$respuesta = (int)$respuesta;
			$model = new Mensajes();
			$model->cliente = $_POST['colabora'];
			$model->mensaje = $txt;
			$model->numero = $_POST['telefono'];
			$model->apartado = $_POST['factura'];
			$model->estado = $respuesta;	
			date_default_timezone_set('America/Costa_Rica'); 
			$model->fecha = date('Y-m-d h:i:s', strtotime('now'));		
			$model->save();
						
			return ["msn"=>$respuesta];
        }else{
            return ["msn"=>"No hay numero asociados"];
        }
    }
}

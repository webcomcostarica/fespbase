<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;
use common\models\AccessHelpers;
use backend\models\search\FacturaSearch;
use backend\models\Factura;
use backend\models\CrmOportunidades;
use backend\models\search\CrmOportunidadesSearch;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * CRM controller
 */
class CrmController extends BaseController
{
    
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionCambiarEtapa($ids,$etapa){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $errors = [];
        foreach (CrmOportunidades::find()->where(['in','id',$ids])->all() as $oportunidad){
            $oportunidad->etapa = $etapa;
            if(!$oportunidad->save()){
                return [
                    'message' => 'Error al modificar',
                    'code' => 304,
                    'data'=>$oportunidad->errors
                ];
            }
        }
        return [
                'message' => 'Oportunidades modificadas',
                'code' => 200,
            ];
        
    }

    public function actionSetTab(){
         $_SESSION['tab_crm'] = $_POST['tab'];
    }
    
    public function actionPanel()
    {
        $data = CrmOportunidades::porPrioridad();
        $searchModel = new CrmOportunidadesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                  
        return $this->render('panel', [
            'datos' => $data,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);     
    }

    /**
     * Creates a new CrmOportunidades model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CrmOportunidades();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['panel']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CrmOportunidades model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
             return $this->redirect(['panel']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CrmOportunidades model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CrmOportunidades model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CrmOportunidades the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CrmOportunidades::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

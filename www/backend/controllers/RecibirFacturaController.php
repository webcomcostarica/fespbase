<?php

namespace backend\controllers;

use Yii;
use backend\models\MrComprobante;
use backend\models\search\MrComprobanteSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use backend\controllers\BaseController;
use common\components\FuncionesDGT;
use backend\models\SisConfiguraciones;
use backend\models\DomValidator;

/**
 * RecibirFacturaController implements the CRUD actions for MrComprobante model.
 */
class RecibirFacturaController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
     public function actionNuevo(){
        return $this->render('nuevo', [
            //'model' => $model,
        ]);
    }
    
    /**
     *   Router para acciones de mensajes
     */
    public function actionUpdate($id){
        $model = $this->findModel($id);
        if($model->paso <= 1)
            return $this->redirect(['view', 'id' => $model->id]);
        if($model->paso == 2)
            if($model->tipoCarga == 'mc')
                return $this->redirect(['crear-mcdos', 'id' => $model->id]);
        if($model->paso == 3)
            return $this->redirect(['respuesta', 'id' => $model->id]);
        if($model->paso == 4)
            return $this->redirect(['view', 'id' => $model->id]);
        
        return $this->redirect(['view', 'id' => $model->id]);
    }

     /**
     * Crear mensaje a partir de mensaje
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCrearMc()
    {
        $model = new MrComprobante();
        $model->tipoCarga = 'mc';
        $model->conf = $this->config;
        $model->paso = $model->scenario = '1';
        $info_post = Yii::$app->request->post();
        if (!empty($_FILES['MrComprobante']['name'])) {
            if(!empty($_FILES['MrComprobante']['name']['mensajeDGT'])){
                $extension = explode('.', $_FILES['MrComprobante']['name']['mensajeDGT']);
                if(count($extension)>0){
                    $mensajeDGT = $model->mensajeDGT = $extension[0] . '.' . $extension[1];
                    $archivoMensajeDGT = UploadedFile::getInstance($model, 'mensajeDGT');
                    if($model->validarXML('ms_h',$archivoMensajeDGT->tempName)){                      
                        $xml = simplexml_load_file($archivoMensajeDGT->tempName);
                        
                        $model->clave = $xml->Clave;
                        $model->mensajeDGTcode = $xml->Mensaje;
                        $model->numeroCedulaEmisor = $xml->NumeroCedulaEmisor;                       
                        $model->montoTotalImpuesto = $xml->MontoTotalImpuesto;
                        $model->totalFactura = $xml->TotalFactura;
                        $model->numeroCedulaReceptor = $xml->NumeroCedulaReceptor;
                        $model->tipoComprobante = substr($model->clave.'', 29, 2);
                        $model->tipoEmisor = $xml->TipoIdentificacionEmisor;
                      
                        if($model->validate()){
                            $model->paso = $model->scenario = '2';
                            $ambiente = FuncionesDGT::getAmbienteSistema($this->config);  
                            $model->pathMah = Yii::getAlias('@webroot') . $ambiente['paths']['05'] .$model->clave . '_mah'.  '.xml';
                            $xml = file_get_contents($archivoMensajeDGT->tempName); 
                            file_put_contents($model->pathMah,$xml);
                            $model->save();
                            return $this->redirect(['crear-mcdos', 'id' => $model->id]);
                        }                            
                    }else{
                        Yii::$app->session->setFlash('danger', 'El archivo cargado, no es un XML válido, no representa la estructura de un mensaje de aprobación de Hacienda.');
                    }
                }
            }
            
        }
      
        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    
    /**
     * Carga y compara el mensaje y el comprobante electronico
     * @param integer $id
     * @return mixed
     */
    public function actionCrearMcdos($id)
    {
        $model = $this->findModel($id);
        if($model->paso == 3)            
            return $this->redirect(['respuesta', 'id' => $model->id]);
        
         if (!empty($_FILES['MrComprobante']['name'])) {            
            if(!empty($_FILES['MrComprobante']['name']['adjunto'])){
                $extension = explode('.', $_FILES['MrComprobante']['name']['adjunto']);                
                if(count($extension)>0){
                    $adjunto = $model->adjunto = $extension[0] . '.' . $extension[1];
                    $archivoMensajeDGT = UploadedFile::getInstance($model, 'adjunto');
                    //$model->tipoComprobante = '01';
                    if($model->validarXML($model->tipoComprobante,$archivoMensajeDGT->tempName)){
                        $ambiente = FuncionesDGT::getAmbienteSistema($this->config);  
                        $model->pathComprobanteXml = Yii::getAlias('@webroot') . $ambiente['paths'][$model->tipoComprobante] .$model->clave . '_comprobante'.  '.xml';
                        $xml = file_get_contents($archivoMensajeDGT->tempName); 
                        file_put_contents($model->pathComprobanteXml,$xml);
                        $xml = simplexml_load_file($archivoMensajeDGT->tempName);
                        if($xml->Clave != $model->clave)
                            $model->addError('clave', 'La clave del mensaje y la del comprobante son diferentes.');                                
                        
                        if(isset($xml->Emisor->Identificacion)){
                            if($xml->Emisor->Identificacion->Numero != $model->numeroCedulaEmisor)
                                $model->addError('numeroCedulaEmisor', 'El emisor del mensaje y el del comprobante son diferentes.');                                
                        }else
                            $model->addError('numeroCedulaEmisor', 'Falta la cedula del receptor en el comprobante.');                                
                        if(isset($xml->ResumenFactura->TotalImpuesto)){                     
                            if($xml->ResumenFactura->TotalImpuesto != $model->montoTotalImpuesto)
                                $model->addError('montoTotalImpuesto', 'El impuesto del mensaje y el del comprobante son diferentes.');                                
                        }else
                            $model->montoTotalImpuesto = -1;                                
                        
                        if($xml->ResumenFactura->TotalComprobante != $model->totalFactura)
                            $model->addError('totalFactura', 'El total del mensaje y el del comprobante son diferentes.');                                
                        
                     
                        if(isset($xml->Receptor)){
                            if($xml->Receptor->Identificacion->Numero != $model->numeroCedulaReceptor)
                                $model->addError('numeroCedulaReceptor', 'Tu cédula en el mensaje y el comprobante son diferentes.');                                
                        }else
                            $model->addError('numeroCedulaReceptor', 'Tu cédula no aparece en este comprobante.');                                
                        
                        if(empty($model->errors)){
                            $model->scenario = 2;
                            if($model->validate()){
                                $model->paso = '3';
                                $model->save();
                                
                                return $this->redirect(['respuesta', 'id' => $model->id]);
                            }
                        } 
                        
                    }else{
                        Yii::$app->session->setFlash('danger', 'El archivo cargado, no es un XML válido, no representa la estructura de un mensaje de aprobación de Hacienda.');
                    }
                    
                }
            }
        }     
        
        return $this->render('create', [
            'model' => $model,
        ]);
        
    }
    
    
    public function actionCrearCc(){
        $model = new MrComprobante();
        $model->tipoCarga = 'cc';
        $model->paso = '2';
         
         if (!empty($_FILES['MrComprobante']['name'])) {
            if(!empty($_FILES['MrComprobante']['name']['adjunto'])){
                $extension = explode('.', $_FILES['MrComprobante']['name']['adjunto']);
                if(count($extension)>0){
                    $adjunto = $model->adjunto = $extension[0] . '.' . $extension[1];
                    $archivoMensajeDGT = UploadedFile::getInstance($model, 'adjunto');
                    //TODO : cargar tipo de comprobante a validar
                    $model->tipoComprobante = '01';
                    
                    if($model->validarXML($model->tipoComprobante,$archivoMensajeDGT->tempName)){
                        $ambiente = FuncionesDGT::getAmbienteSistema($this->config);
                        $model->tipoComprobante = '05';
                        
                        $xmlStr = file_get_contents($archivoMensajeDGT->tempName);
                        $xml = simplexml_load_file($archivoMensajeDGT->tempName);
                        $model->clave = $xml->Clave;
                        $model->pathComprobanteXml = $ambiente['paths'][$model->tipoComprobante] .$model->clave . '_comprobante'.  '.xml';
                       
                        file_put_contents(Yii::getAlias('@webroot') . $model->pathComprobanteXml,$xmlStr);
                        
                        
                        if(isset($xml->Emisor->Identificacion)){
                            $model->numeroCedulaEmisor = $xml->Emisor->Identificacion->Numero;
                            $model->tipoEmisor = $xml->Emisor->Identificacion->Tipo;
                        }else
                            $model->addError('numeroCedulaEmisor', 'Falta la cedula del receptor en el comprobante.');                                
                                                
                        if(isset($xml->ResumenFactura->TotalImpuesto)){                     
                            $model->montoTotalImpuesto = $xml->ResumenFactura->TotalImpuesto;
                        }else
                            $model->montoTotalImpuesto = -1;                                
                        
                        $model->totalFactura = $xml->ResumenFactura->TotalComprobante;
                        
                        if(isset($xml->Receptor))
                            $model->numeroCedulaReceptor = $xml->Receptor->Identificacion->Numero;
                        else
                            $model->addError('numeroCedulaReceptor', 'Tu cédula no aparece en este comprobante.');                                
                        echo $model->numeroCedulaReceptor;
                        
                                             
                        $model->uniqueClave();
                        
                        if(empty($model->errors)){
                            $model->scenario = 2;
                            if($model->validate()){
                                $model->paso = '3';
                                $model->save();
                                
                                return $this->redirect(['respuesta', 'id' => $model->id]);
                            }
                        }                        
                    }else{
                        Yii::$app->session->setFlash('danger', 'El archivo cargado, no es un XML válido, no representa la estructura de un mensaje de aprobación de Hacienda.');
                    }
                }
            }else{
               Yii::$app->session->setFlash('warning', 'Debes cargar un comprobante.');
            }
        }     
        
        return $this->render('create_cc', [
            'model' => $model,
        ]);
        
    }
    
    
    
    /**
     * Lista de mensajes de respuesta
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MrComprobanteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'config' => $this->config,
        ]);
    }
    
    

    public function actionConsultarEstado($id)
    {
        $model = $this->findModel($id);
        $this->getEstado($model);
        return $this->redirect(['view', 'id' => $model->id]);
    }
    
   /**
     * Displays a single MrComprobante model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

   

   
    
        
    public function actionRespuesta($id){
        $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post())) {
            $tipoRespuesta = $_POST['MrComprobante']['tipoRespuesta'];
            $model->scenario = $model->paso;
            if($tipoRespuesta == '05'){
                $model->mensaje = '1';
                $model->detalleMensaje = 'Aceptado';
                $model->tipoComprobante = '05';
            }

            if($tipoRespuesta == '06'){
                $model->mensaje = '2';
                $model->detalleMensaje = 'Aceptado parcialmente';
                $model->tipoComprobante = '06';
            }

            if($tipoRespuesta == '07'){
                $model->mensaje = '3';
                $model->detalleMensaje = 'Rechazado';
                $model->tipoComprobante = '07';
            }
              
            $model->numeroConcecutivo = sprintf("%03d", $this->config['Sucursal']) .
                                        sprintf("%05d", $this->config['Terminal']) .
                                        $model->tipoComprobante .
                                        sprintf("%010d", $model->id);
            $model->normativa = $this->config['Resolucion'];
            $model->fechaNormativa = $this->config['FechaResolucion'];
            $model->fechaEmisionDoc = FuncionesDGT::getFormatoFecha();
          
            if($model->validate()){
                $model->paso = '4';
                $xml_sin_firma = $this->generarXml($model);
                FuncionesDGT::MrFirmarEnviarComprobante(
                    $xml_sin_firma,
                    $this->config,
                    $model
                );
                
                return $this->redirect(['consultar-estado', 'id' => $model->id]);
            }
        }
      
        
            if($model->tipoCarga == 'cc')
                return $this->render('create_cc', [
                    'model' => $model,
                ]);    
            return $this->render('create', [
                'model' => $model,
            ]);                
    }    
    
     
   
       
    /**
     * Deletes an existing MrComprobante model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**FUNCIONES**/
    
    
     public function cerosAlFrente($string){
        return str_pad($string,12,'0',STR_PAD_LEFT);
    }
    
    
     private function getEstado($numeracion) {
        $curl = curl_init();
        $token = FuncionesDGT::getToken();
        $ambiente = FuncionesDGT::getAmbienteSistema($this->config);
        if(empty($numeracion->urlConsulta)){
            $urlConsulta= 'https://api.comprobanteselectronicos.go.cr/recepcion/v1/recepcion/'
            .$numeracion->clave
            .'-'
            .$numeracion->numeroConcecutivo;
        }else{
            $urlConsulta = trim(str_replace('Location: ','',$numeracion->urlConsulta));
        }       
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => $urlConsulta,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer " . $token->access_token,
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded",
            ),
        ));

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);

        $response = curl_exec($curl);
        $err = curl_error($curl);      
        curl_close($curl);
        if ($err) {
            $numeracion->estadoMensaje = $err;
            $numeracion->estadoEstado = 404;
        } else {
            $responseT = json_decode($response);
           
            if($responseT){
                $array = get_object_vars($responseT);

                if (isset($array['respuesta-xml'])) {
                    $configs = $this->config;
                   
                    $mensaje = base64_decode($array['respuesta-xml']);
               
                    file_put_contents(
                        Yii::getAlias('@webroot') . $ambiente['paths'][$numeracion->tipoComprobante] . $numeracion->clave . '_mensaje_hacienda.xml', $mensaje
                    );
                    
                    $xml = new \SimpleXMLElement($mensaje);
                    $mensajeDetalle = str_replace("'", '', utf8_decode($xml->DetalleMensaje));
                    ;
                    $mensajeDetalle = utf8_encode($mensajeDetalle);
                    $mensajeDetalle = str_replace("'", '', $mensajeDetalle);
                    if ($xml->Mensaje != 1) {
                        $mensajeDetalle = str_replace('Este comprobante fue aceptado en el ambiente de pruebas, por lo cual no tiene validez para fines tributarios', '', $mensajeDetalle);
                    }
                    $numeracion->estadoMensaje = $mensajeDetalle;
                    $numeracion->estadoEstado = $xml->Mensaje;
                } else {
                    $mensajeDetalle = 'No hay respuesta de la DGT, verifique el token<br>';
                }
            }else{
                Yii::$app->session->setFlash('danger', 'Hacienda no responde, por favor espere...');
            }
        }
        $numeracion->scenario = '4';
        if ($numeracion->save()) {
            if($numeracion->estadoEstado != 0)
                Yii::$app->session->setFlash('success', 'Se ha verificado el estado correctamente');
            else
                Yii::$app->session->setFlash('warning', 'No se ha verificado el estado, intente mas tarde.');
           
        } else {
            var_dump($numeracion->errors);die;
            Yii::$app->session->setFlash('danger', 'Ha ocurrido un error al envar el mensaje.');
        }
    }
    
     
    /**
     * Retorna el xml formado sin firmar del mensaje de aceptación.
     * @param objetct $model
     * @return string
     */ 
    public function generarXml($model){
        $xmlDoc = new \DOMDocument('1.0' , 'UTF-8');
        $facturacion = $xmlDoc->appendChild($xmlDoc->createElement("MensajeReceptor"));
        $facturacion->appendChild($xmlDoc->createAttribute("xmlns"))->appendChild(
        $xmlDoc->createTextNode('https://tribunet.hacienda.go.cr/docs/esquemas/2017/v4.2/mensajeReceptor'));

        //ENCABEZADO DEL XML
        $facturacion->appendChild($xmlDoc->createAttribute("xmlns:xsd"))->appendChild(
            $xmlDoc->createTextNode('http://www.w3.org/2001/XMLSchema'));
        $facturacion->appendChild($xmlDoc->createAttribute("xmlns:xsi"))->appendChild(
            $xmlDoc->createTextNode('http://www.w3.org/2001/XMLSchema-instance'));
        //FIN ENCABEZADO DEL XML

        //DATOS DEL MENSAJE
        $facturacion->appendChild($xmlDoc->createElement("Clave", $model->clave));
        $facturacion->appendChild($xmlDoc->createElement("NumeroCedulaEmisor", str_pad($model->numeroCedulaEmisor,12,'0',STR_PAD_LEFT)));
        $facturacion->appendChild($xmlDoc->createElement("FechaEmisionDoc", $model->fechaEmisionDoc));
        $facturacion->appendChild($xmlDoc->createElement("Mensaje", $model->mensaje));
        $facturacion->appendChild($xmlDoc->createElement("DetalleMensaje", $model->detalleMensaje));
       
        if($model->montoTotalImpuesto >= 0)           
            $facturacion->appendChild($xmlDoc->createElement("MontoTotalImpuesto", $model->montoTotalImpuesto));
        
        $facturacion->appendChild($xmlDoc->createElement("TotalFactura", $model->totalFactura));
        $facturacion->appendChild($xmlDoc->createElement("NumeroCedulaReceptor", str_pad($model->numeroCedulaReceptor,12,'0',STR_PAD_LEFT)));
        $facturacion->appendChild($xmlDoc->createElement("NumeroConsecutivoReceptor", $model->numeroConcecutivo));

        $xmlDoc->formatOutput = true;
        $xml_sin_firma = $xmlDoc->saveXML();
        return $xml_sin_firma;
    }
    
    
    
    

    /**
     * Finds the MrComprobante model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MrComprobante the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MrComprobante::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

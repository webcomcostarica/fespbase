<?php

namespace backend\controllers;

use Yii;
use backend\models\Localizaciongeografica;
use backend\models\search\LocalizaciongeograficaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * LocalizacionController implements the CRUD actions for Localizaciongeografica model.
 */
class LocalizacionController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Localizaciongeografica models.
     * @return mixed
     */
    public function actionIndex($padre = null)
    {        
        $searchModel = new LocalizaciongeograficaSearch();
        $searchModel->idPadre = $padre;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Localizaciongeografica model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Localizaciongeografica model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($padre,$nivel)
    {
        $model = new Localizaciongeografica();
		$model->idPadre = $padre;
		$model->nivel = $nivel-1;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'padre' => $padre,'nivel'=>$nivel]);
			
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Localizaciongeografica model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idlocalizacion]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Localizaciongeografica model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
	
	   /**
     * Esta funcion va reconocer el tipo de parametro de la localización.
     * Va retonar una lista segun el tipo de localización.
     * @param integer $tipo POST 
     * @return mixed
     */
    public function actionTipo()
    {
		 Yii::$app->response->format = Response::FORMAT_JSON;
		
		$localizaciones = Localizaciongeografica::find()
        ->where('nivel=:p',[':p' => (int)$_POST['tipo']+1 ])	
        ->orderBy('nombre DESC')
        ->all();
		
		return ['localizaciones'=>$localizaciones];
		
    }
	
	

    /**
     * Finds the Localizaciongeografica model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Localizaciongeografica the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Localizaciongeografica::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

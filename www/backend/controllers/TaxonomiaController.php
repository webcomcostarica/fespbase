<?php

namespace backend\controllers;

use Yii;
use backend\models\Taxonomia;
use backend\models\search\TaxonomiaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


use backend\models\Diccionario;
use yii\web\Response;

/**
 * TaxonomiaController implements the CRUD actions for Taxonomia model.
 */
class TaxonomiaController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Taxonomia models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TaxonomiaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Taxonomia model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
		
    public function actionDeletetermino($pIdTax){		
			//Yii::$app->response->format = Response::FORMAT_JSON;		
			Diccionario::findOne($pIdTax)->delete();			
			return "#tr_term_".$pIdTax;		
	}
	/**"
     * Creates a new Termino de diccionario para vocaculario taxonomico.
     * If creation is successful, return un tr con datos creados
     * @return json
     */		
    public function actionCreatetermino($pNombre,$pIdTax){
		//Yii::$app->response->format = Response::FORMAT_JSON;
		$model=new Diccionario();
		$model->nombre=$pNombre;	
		$model->id_taxonomia=$pIdTax;	
		$model->descripcion='...';	
		if($model->save()){	
			return $this->renderPartial('termino',['termino'=>$model],false,true);			
		}
	}

    /**
     * Creates a new Taxonomia model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Taxonomia();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Taxonomia model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Taxonomia model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Taxonomia model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Taxonomia the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Taxonomia::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

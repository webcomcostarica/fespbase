<?php

namespace backend\controllers;

use Yii;
use backend\models\LotePrecio;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LoteprecioController implements the CRUD actions for LotePrecio model.
 */
class LoteprecioController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all LotePrecio models.
     * @return mixed
     */
    public function actionIndex($id)
    {
//        $data=LotePrecio::find()->where(['lote'=>$id])->all();
      //  echo count($data);die;
        $dataProvider = new ActiveDataProvider([
            'query' => LotePrecio::find()->where(['lote'=>$id]),
        ]);
        return $this->renderPartial('index', [
            'dataProvider' => $dataProvider,
            'lote_id' => $id,            
        ],false,true);
    }

    /**
     * Displays a single LotePrecio model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LotePrecio model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($idlote)
    {
        $model = new LotePrecio();
        $model->lote=$idlote;
        $model->GuardarFechas();
        $model->usuario=Yii::$app->user->identity->id;
        $model->piezas=0;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if($model->vigente==1){
                $precios=  LotePrecio::find()->where(["lote"=>$model->lote])->all();
                foreach ($precios as $prec){
                    if($model->id!=$prec->id){
                        $prec->vigente='0';
                        if($prec->save()){}else{
                            var_dump($prec->errors);die;
                        }
                    }
                } 
                $lote=  \backend\models\Lote::findOne($model->lote);
                $lote->precio=$model->costo;
                $lote->save();
            }
            return $this->redirect(['/lote/update', 'id' => $model->lote]);
           // return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing LotePrecio model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if($model->vigente==1){
                $precios=  LotePrecio::find()->where(["lote"=>$model->lote])->all();
                foreach ($precios as $prec){
                    if($model->id!=$prec->id){
                        $prec->vigente='0';
                        if($prec->save()){}else{
                            var_dump($prec->errors);die;
                        }
                    }
                } 
                $lote=  \backend\models\Lote::findOne($model->lote);
                $lote->precio=$model->costo;
                $lote->update();
            }
            return $this->redirect(['/lote/update', 'id' => $model->lote]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing LotePrecio model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model=$this->findModel($id);
            $idlote=$model->lote;
        if($model->piezas==0){
            $model->delete();
                return $this->redirect(['/lote/update', 'id' => $idlote]);
        }else{
                return $this->redirect(['/lote/update', 'id' => $idlote]);            
        }
        //return $this->redirect(['index']);
    }

    /**
     * Finds the LotePrecio model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LotePrecio the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LotePrecio::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use backend\models\Caja;
use backend\models\search\CajaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\PackagePi;
use backend\models\Package;

/**
 * CajaController implements the CRUD actions for Caja model.
 */
class CajaController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                   // 'delete' => ['post'],
                ],
            ],
        ];
    }

	
	  /**
     * Creates a new Caja model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAbrir()
    {
        $hoy = date('Y-m-d',strtotime('now'));
        $caja = Caja::find()->where(['like','fecha',$hoy])->andWhere(['tipo'=>'Abrir'])->one();      
		if($caja){
		   Yii::$app->session->setFlash("success", "El dia de hoy ya cuenta con una caja abierta");
			
            return $this->redirect(['view', 'id' => $caja->id]);
		}
        $model = new Caja();
        $model->fecha = $hoy;

        if ($model->load(Yii::$app->request->post()) ) {
			$this->calcular($model);	
			$model->tipo = "Abrir";		
			if($model->save())			
				return $this->redirect(['view', 'id' => $model->id]);
        }
            return $this->render('create', [
                'model' => $model,
				'titulo'=> 'Abrir',
                'config' => $this->config
            ]);
        
		
		
    }
	  /**
     * Creates a new Caja model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCerrar()
    {
        $hoy = date('Y-m-d',strtotime('now'));
        $caja = Caja::find()->where(['like','fecha',$hoy])->andWhere(['tipo'=>'Cerrar'])->one();      
		if($caja){
		   Yii::$app->session->setFlash("success", "El dia de hoy ya cuenta con una caja cerrada");
			
            return $this->redirect(['view', 'id' => $caja->id]);
		}
        $model = new Caja();
        $model->fecha = $hoy;

        if ($model->load(Yii::$app->request->post())) {
			$model = $this->calcular($model);
			$model->tipo = 'Cerrar';
			if($model->save())			
				return $this->redirect(['view', 'id' => $model->id]);
        }
		
		return $this->render('create', [
			'model' => $model,
			'titulo'=> 'Cerrar',
            'config' => $this->config
		]);
        
    }
	
	
	function calcular($mod){
		//$mod->fecha = date('Y-m-d h:i:s',strtotime('now'));
		$mod->usuario = Yii::$app->user->identity->id;
		$mod->total = 
		$mod->cincuentamil * 50000 +
		$mod->veitemil * 20000 +
		$mod->diesmil * 10000 +
		$mod->cincomil * 5000 +
		$mod->dosmil * 2000 +
		$mod->mil * 1000 +
		$mod->quinientos * 500 +
		$mod->cien * 100 +
		$mod->cincuenta *50 +
		$mod->vienticinco * 25 +
		$mod->veinte * 20 +
		$mod->dies *10 +
		$mod->cinco * 5;
		return $mod;
	}
    /**
     * Lists all Caja models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CajaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Caja model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Caja model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Caja();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$this->calcular($model);	
			$model->save();	
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
	

    /**
     * Updates an existing Caja model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$this->calcular($model);	
			$model->save();	
		   return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'config' => $this->config
            ]);
        }
    }

    /**
     * Deletes an existing Caja model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

	/**
     * Cerrar dia del sistema
	 * Cierra cajas los domingos
	 * Control de inversiones
	 * Notificaciones
     */
    public function actionCierresistema()
    {
		date_default_timezone_set('America/Costa_Rica'); 
		//Cierra de caja los domingos
		$date = date('N'); 
		$hora = date('G'); 
		if($date == 7){ //domingo
			 $model = new Caja();
			 $model->scenario = 'domingo';
			 $model->tipo = 'Abrir';			 
			 $model->fecha = date('Y-m-d',  strtotime('now'));
			 $model->usuario = User::ID_ADMIN;
			 $model->save();
			 
			 $model = new Caja();
			 $model->scenario = 'domingo';
			 $model->tipo = 'Cerrar';			 
			 $model->fecha = date('Y-m-d',  strtotime('now'));
			 $model->usuario = User::ID_ADMIN;
			 $model->save();
		}
		
		$date = date('Y-m-d',strtotime('-1 day'));	
		$ayer = PackagePi::find()->andWhere(['=','fecha',$date])->one();
		if($ayer && $hora >= 20){		
			$paca = Package::findOne(233);
			$nuevaPI = new PackagePi();
			$venEfectivo = $paca->ValorDeVentaEfe;
			$venApartados = $paca->ValorDeVentaAparVendidos;
			$cobApartados = $paca->ValorDeVentaApar;
			
			$totVenPI = $venEfectivo+$venApartados;	
			$nuevaPI->tienDiarioNeto = (($totVenPI*$paca->porcentajeTienda)/100 ) - $ayer->tienda;
			$nuevaPI->tienda = $nuevaPI->tienDiarioNeto + $ayer->tienda ;
						
			$nuevaPI->invDiarioNeto = (($totVenPI*$paca->porcentajeInversor)/100) - $ayer->inversor;			
			$nuevaPI->inversor = $nuevaPI->tienDiarioNeto + $ayer->inversor;
						
			$nuevaPI->paca = 233;
			$nuevaPI->fecha = date('Y-m-d',strtotime('now'));
			$nuevaPI->monto = $nuevaPI->tienda + $nuevaPI->inversor;
			$nuevaPI->save();
			if($nuevaPI->errors){
				var_dump($nuevaPI->errors);
			}
		}
		die;
		
		
		
    }

    /**
     * Finds the Caja model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Caja the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Caja::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

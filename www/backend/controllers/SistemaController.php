<?php

namespace backend\controllers;

use Yii;
use backend\models\SisConfiguraciones;
use backend\models\search\SisConfiguracionesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


use backend\models\LocDistrito;
use backend\models\LocBarrio;
use backend\models\LocCanton;
use backend\models\Localizaciongeografica;
use yii\web\Response;
use common\components\FuncionesDGT;
use backend\controllers\BaseController;
use common\models\User;
use backend\models\base\SendMail;

class SistemaController extends BaseController
{
    
    public function actionTipoCambio(){
        
       Yii::$app->cache->set('tipo_cambio', $valor);
    }
    public function actionSendMail($id){
        $model = New SendMail();
        //$model->comprobante = $id;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->plantilla = $this->render(
             'enviar_comprobantes', [
                'cliente'=>'Crisman',
                'cantidad'=>12,
                'mensaje'=>'hola'
             ]);
             echo 'enviar';die;
            //if($model->send())
            return $this->redirect(['_form_send_mail', 'id' => $model->id]);
        } else {
            return $this->render('_form_send_mail', [
                'model' => $model
            ]);
        }
        
     
   }
    
   
   public function actionLimpiar(){      
       if($this->config['Ambiente'] == 0){
           $sql = file_get_contents('../bd/limpiar.sql');
           $command = Yii::$app->db->createCommand($sql);
           echo $command->execute();
       }else{
           echo 'Debes estar en modo de pruebas';
       }
   }
    
    public function actionSetTab(){
        $_SESSION['tab'] = $_POST['tab'];
        //permite recordar cual es el ultimo tab seleccionado dentro de los configs
    }
    
    public function actionView($id){
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
       
    public function actionUbicaciones(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $nivel = $_POST['nivel'];
        $provincia = $_POST['provincia'];
        $canton = $_POST['canton'];
        $distrito = $_POST['distrito'];
        $barrio = $_POST['barrio'];
        
        $cantones = $distritos = $barrios = [];
            
        if($nivel=='cantones')
            $cantones = $this->toOptions(LocCanton::getCantones($provincia));
        if($nivel=='distritos')
            $distritos = $this->toOptions(LocDistrito::getDistritos($canton,$provincia));
        if($nivel=='barrios')
            $barrios = $this->toOptions(LocBarrio::getBarrios($distrito,$canton,$provincia));
        
        return [
            'cantones'=>$cantones,
            'distritos'=>$distritos,
            'barrios'=>$barrios,
        ];

    }
    
    private function toOptions($array){
        $html = '';
        foreach($array as $key=>$item){
           $html .= "<option value='$key' >$item</option>";
        }
        return $html;
    }
   
    public function actionCreate()
    {
        $model = new SisConfiguraciones();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->Persona]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionUpdate()
    {
        $user = User::findOne(Yii::$app->user->id);
        $id = $user->configs;
        if($id == 0)
           return $this->redirect(['create']);
       
        $model = $this->findModel($id);
        if(empty($model))
           return $this->redirect(['create']);
       
        if ($model->load(Yii::$app->request->post())) {   
        
            $model = $this->RevisarEstado($model);           
            Yii::$app->cache->set('tipo_cambio',$model->TipoCambio);
            $ambiente = FuncionesDGT::getAmbienteSistema($this->config);
            if(file_exists(Yii::getAlias('@webroot') . $ambiente['urlP12'])){
                if (!$pfx = file_get_contents(Yii::getAlias('@webroot') . $ambiente['urlP12'])) {
                    $model->mensajeFirma = 'danger';
                }            
                if (openssl_pkcs12_read($pfx, $key, $ambiente['pinP12'])) {
                    $model->mensajeFirma = 'success';
                }else{
                     $model->mensajeFirma = 'danger';
                }
            }else
                 $model->mensajeFirma = 'danger';
            
            $model->save();
            
            if(!Yii::$app->session->getFlash('success')) 
                Yii::$app->session->setFlash('success', 'Se han actualizado correctamente los datos del sistema.');
         
        }
        
        return $this->render('update', [
            'model' => $model,
        ]);
        
    }

         
     public function RevisarEstado($model){
        if($model->Ambiente == SisConfiguraciones::PRUEBAS)
            $model->estadoStg = 'danger';
        else 
            $model->estadoProd = 'danger';
        
         if (FuncionesDGT::ValidarConexionHacienda()) {
            $ambiente = FuncionesDGT::getAmbienteSistema($this->config);     
            $token = FuncionesDGT::TokenGenerarNuevo($ambiente);
           
            if(is_object($token)){
                 if($model->Ambiente == SisConfiguraciones::PRUEBAS)
                    $model->estadoStg = 'success';
                else 
                    $model->estadoProd = 'success';
                
                $model->mensajeApi = 'Acceso correto al sistema.';
                               
                return $model;
            }
            $model->mensajeApi = $token;           
        }
        return $model;
    }
  
    protected function findModel($id)
    {
        if (($model = SisConfiguraciones::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php

namespace backend\controllers;

use Yii;
use backend\models\Package;
use backend\models\search\packageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\behaviors\TimestampBehavior;
use backend\models\Diccionario;
use backend\models\Colaborador;
/**
 * PackageController implements the CRUD actions for Package model.
 */
class PackageController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Package models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new packageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Package model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model=$this->findModel($id);
        $model->VerFechas();
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Package model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Package();
        $model->creacion=date('now');
        $calidad= Diccionario::find()->where(['id_taxonomia'=>'1'])->all();
        $estado= Diccionario::find()->where(['id_taxonomia'=>'3'])->all();
        $categorias= Diccionario::find()->where(['id_taxonomia'=>'4'])->all();
        $proveedor= Colaborador::find()->where(['tipo'=>'2'])->all();

                $model->proveedor='1';
        if ($model->load(Yii::$app->request->post())) {
            $model->GuardarFechas();
            if($model->validate()){
                $model->save();
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } 
            return $this->render('create', [
                'model' => $model,
                'categorias' => $categorias,
                'calidad' => $calidad,
                'estado' => $estado,
                'proveedor' => $proveedor,
            ]);        
    }

    /**
     * Updates an existing Package model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $calidad= Diccionario::find()->where(['id_taxonomia'=>'1'])->all();
        $categorias= Diccionario::find()->where(['id_taxonomia'=>'4'])->all();
        $estado= Diccionario::find()->where(['id_taxonomia'=>'3'])->all();
        $proveedor= Diccionario::find()->where(['id_taxonomia'=>'5'])->all();
        
        $model=$this->findModel($id);
        $model->VerFechas();
      //  var_dump($model->desempacada);die;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) { 
            $model->GuardarFechas();
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'categorias' => $categorias,
                'calidad' => $calidad,
                'estado' => $estado,
                'proveedor' => $proveedor,
            ]);
        }
    }

    /**
     * Deletes an existing Package model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Package model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Package the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Package::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

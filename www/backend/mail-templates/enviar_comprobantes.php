<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $cliente \ Nombre de la persona a entregar */
/* @var $mensaje \ Mensaje personalizado para el cliente */
/* @var $despedida \ Mensaje personalizado para la despedida */
/* @var Firma de entrega */

/* plantilla de correo para presentar comprobantes electronicos */

?>
<h1>Nuevo comprobante electrónico</h1>
<p>
    Estimado, <?= (isset($cliente))?$cliente:'' ?><br>
</p>
<p>
    <?php if(isset($mensaje)){
        echo $mensaje;
    }else{ ?>
    El emisor autorizado <?= $emisor ?>, ha creado un comprobante electrónico para usted.<br>
    <br>
    Verifique la información adjunta.
    <ul>
        <li>Comprobante PDF (De facil lectura)</li>
        <li>Comprobante XML (Comprobante legal)</li>
        <li>Mensaje de hacienda confirmado que todo está en orden</li>
    </ul>
    <?php } ?>
</p>

<p>
    <?php  if(isset($despedida)){ ?>
        <?= $despedida ?>
    <?php  }else{ ?>
        Saludos.
    <?php } ?>
</p>

<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $cliente \ Nombre de la persona a entregar */
/* @var $mensaje \ Mensaje personalizado para el cliente */
/* @var $despedida \ Mensaje personalizado para la despedida */
/* @var Firma de entrega */

/* plantilla de correo para presentar comprobantes electronicos */

?>
<h1>Entrega de comprobante electrónico</h1>

<p> Estimado, <?= (isset($cliente))?$cliente:'' ?></p>
<p>
    <?php  if(isset($mensaje)){ ?>
       <?= $mensaje ?>
    <?php  }else{ ?>
        Puedes utilizar los archivos XML para presentar aceptar un gasto deducible.
    <?php } ?>
</p>
<p>
    <?php  if(isset($despedida)){ ?>
        <?= $despedida ?>
    <?php  }else{ ?>
        Saludos.
    <?php } ?>
</p>

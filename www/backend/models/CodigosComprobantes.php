<?php

namespace backend\models;

use Yii;

class CodigosComprobantes
{
    const CONDICIONES = [
		'01'=>'Contado',
	//	'02'=>'Crédito',
	//	'03'=>'Consignación',
//		'04'=>'Apartado',
		//'05'=>'Arrendamiento con opción de compra',
	//	'05'=>'Arrendamiento en función finaciera',
		//'99'=>'Otros'
	];

    const TIPOCOMPROBANTES = [
		'01'=>'FACTURA ELECTRÓNICA',
		'02'=>'NOTA DE DEBITO ELECTRÓNICA',
		'03'=>'NOTA DE CRÉDITO ELECTRÓNICA',
		'04'=>'TIQUETE ELECTRÓNICO',
	]; 
    
    const MEDIOSDEPAGO = [
        '01'=>'Efectivo',
        '02'=>'Tarjeta',
        '03'=>'Cheque',
        '04'=>'Transferencia - depósito bancario',
        //'05'=>'Recaudado por terceros',
        //'99'=>'Otros',                
        ]; 
    const ESTADOVALIDACION = [
        0=>'',
        1=>'Aceptado',
        2=>'Parcial',
        3=>'Rechazado',
        null=>'Esperando',
        100=>'Revisar',
        200=>'Revisar',
        300=>'Revisar',
        400=>'Revisar',
        500=>'Revisar',
    ];
     const ESTADOENTREGA = [
        0=>'',
        1=>'Aceptado',
        2=>'Parcial',
        3=>'Rechazado',
        null=>'',
        100=>'Revisar',
        200=>'Enviado',
        300=>'Revisar',
        400=>'Revisar',
        500=>'Revisar',
    ];
}

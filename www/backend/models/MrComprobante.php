<?php

namespace backend\models;

use Yii;
use backend\models\DOMValidator;
use backend\models\base\DgtComprobante;

class MrComprobante extends DgtComprobante
{
    const URI_RECIB_FACT = 'mr/comprobantes/';
    public $mensajeDGT;
    public $conf;
    public $mensajeDGTcode;
    public $tipoRespuesta;
    
    public static function tableName()
    {
        return 'mr_comprobante';
    }

    public function rules()
    {
        return [
            [['clave'], 'uniqueClave'],
            [['mensajeDGTcode'], 'valMensajeDGTcode'],
            [['numeroCedulaReceptor'], 'valCed'],
            [['paso','tipoComprobante', 'clave', 'numeroCedulaEmisor', 'fechaEmisionDoc', 'mensaje', 'detalleMensaje', 'montoTotalImpuesto', 'totalFactura', 'numeroCedulaReceptor', 'numeroConcecutivo'], 'required'],
            [['mensaje'], 'integer'],
            [['montoTotalImpuesto', 'totalFactura'], 'number']
        ];
    }
    
    public function valMensajeDGTcode(){        
        if($this->mensajeDGTcode == 3)
            $this->addError('mensajeDGTcode', 'Este comprobante no ha sido aceptado por hacienda');
    } 
    
    public function valCed(){
        $value = Yii::$app->cache->get('cuenta_cedula');
        if($this->numeroCedulaReceptor != $value)
            $this->addError('numeroCedulaReceptor', 'Usted no es el receptor de este comprobante.');
    }

    public function uniqueClave(){
        if (!empty($this->clave)) {          
            $claveRepetida = MrComprobante::find()
                    ->andWhere(['clave' => $this->clave])
                    ->one();
            if ($claveRepetida) {
                 Yii::$app->session->setFlash('warning', 'Este comprobante ya lo cargaste.');
                $this->addError('clave', 'Este comprobante ya lo cargaste.');
            }
        }
    }
    
    
     public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['1'] = [
            'clave',
            'mensajeDGTcode',
            'numeroCedulaEmisor',
            'numeroCedulaReceptor',
            'montoTotalImpuesto',
            'totalFactura',
            'totalFactura'
        ];
        
        $scenarios['2'] = ['adjunto'];
                
        $scenarios['3'] = [
            'numeroConcecutivo',
            'mensaje',
            'detalleMensaje',
            'tipoComprobante',
            'normativa',
            'fechaNormativa',
            'fechaEmisionDoc',
        ];
        $scenarios['4'] = [
            'pathComprobanteXml',
            'pathComprobantePdf',
            'pathMensajeXml',
            'pathMensajePdf',
            'pathMah',
        ];
        return $scenarios;
    }
    
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'tipoComprobante' => Yii::t('backend', 'Tipo Comprobante'),
            'clave' => Yii::t('backend', 'Clave'),
            'numeroCedulaEmisor' => Yii::t('backend', 'Numero Cedula Emisor'),
            'fechaEmisionDoc' => Yii::t('backend', 'Fecha Emision Doc'),
            'mensaje' => Yii::t('backend', 'Mensaje'),
            'detalleMensaje' => Yii::t('backend', 'Detalle Mensaje'),
            'montoTotalImpuesto' => Yii::t('backend', 'Monto Total Impuesto'),
            'totalFactura' => Yii::t('backend', 'Total Factura'),
            'numeroCedulaReceptor' => Yii::t('backend', 'Numero Cedula Receptor'),
            'numeroConcecutivo' => Yii::t('backend', 'Numero Concecutivo'),
            'adjunto' => Yii::t('backend', 'Adjunto'),
        ];
    }
    
    public function validarXML($tipo,$pathXml){
        $validator = new DOMValidator;
        $validator->feedSchema = __DIR__  .  "/../web/xsd/$tipo.xsd";
       
        $validated = $validator->validateFeeds($pathXml);
        if ($validated) {
            return true;
        } else {
            //print_r($validator->displayErrors());
            return false;
        }
    }
}

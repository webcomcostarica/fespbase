<?php

namespace backend\models;

use backend\models\ImpuestosAplicados;
use backend\models\Lote;
use Yii;
use common\models\FG;

/**
 * This is the model class for table "detalle_factura".
 *
  $id
  $factura
  $tipo
  $lote
  $lote_precio
  $cantidad
  $unidad
  $descripcion
  $montoDescuento
  $impuesto
  $tipoDescuento
  $naturalezaDescuento
  $montoTotal
  $subTotal
  $montoTotalLinea
 *
 * @property Factura $factura0
 * @property Impuestosaplicados[] $impuestosaplicados
 */
class DetalleFactura extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fe_detalles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['factura', 'tipo', 'lote_precio', 'cantidad', 'unidad', 'descripcion', 'montoDescuento', 'impuesto', 'tipoDescuento', 'montoTotal', 'subTotal', 'montoTotalLinea'], 'required'],
            [['factura', 'tipo', 'lote'], 'integer'],
            [['cantidad', 'montoDescuento','impuesto', 'montoTotal', 'subTotal', 'montoTotalLinea', 'lote_precio'], 'number'],
            [['unidad', 'descripcion',  'tipoDescuento', 'naturalezaDescuento'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'factura' => Yii::t('backend', 'Factura'),
            'tipo' => Yii::t('backend', 'Tipo'),
            'lote' => Yii::t('backend', 'Lote'),
            'lote_precio' => Yii::t('backend', 'Lote Precio'),
            'cantidad' => Yii::t('backend', 'Cantidad'),
            'unidad' => Yii::t('backend', 'Unidad'),
            'descripcion' => Yii::t('backend', 'Descripcion'),           
            'montoDescuento' => Yii::t('backend', 'Monto Descuento'),       
            'impuesto' => Yii::t('backend', 'Impuesto'),
            'tipoDescuento' => Yii::t('backend', 'Tipo Descuento'),
            'naturalezaDescuento' => Yii::t('backend', 'Naturaleza Descuento'),
            'montoTotal' => Yii::t('backend', 'Monto Total'),
            'subTotal' => Yii::t('backend', 'Sub Total'),
            'montoTotalLinea' => Yii::t('backend', 'Monto Total Linea'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLote0()
    {
        return $this->hasOne(Lote::className(), ['id' => 'lote']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFactura0()
    {
        return $this->hasOne(Factura::className(), ['id' => 'factura']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImpuestosaplicados()
    {
        return $this->hasMany(ImpuestosAplicados::className(), ['idDetalle' => 'id']);
    }
    
    
    /**
     * Funciones
     */
    
    private function setTipo($post) {
            $this->lote_precio = $post['costo'];
            $this->descripcion = $post['descripcion'];
        if ($post['tipo'] == 0) {
            //$precio = LotePrecio::find()->where(['lote' => $post['codigo_lote'], 'vigente' => '1'])->one();
           // $this->lote_precio = $precio->costo;
            $this->lote = $post['codigo_lote'];
        } else {
            $this->lote = 0;
        }
    }
    
    private function setDescuento($post){
        $this->montoDescuento = FG::dc($post['descuento']);
        $this->tipoDescuento = $post['tipo_descuento'];
        if ($this->montoDescuento > 0){
            $this->naturalezaDescuento = 'Descuento Promocional';
        }
    }
    
     private function setImpuesto($post){
        $impuesto = new ImpuestosAplicados();
        $impuesto->monto = 0;
        if ($this->impuesto == 'G') {
            $impuesto->fecha = date('Y-m-d h:i:s', strtotime('now'));
            $impuesto->nombre = 'Impuesto General Sobre las Ventas';
            $impuesto->codigo = '01';
            $impuesto->tarifa = 13.00;
            $impuesto->tag = 'tag';
            
            $tot = 
                 $this->subTotal *
                 $impuesto->tarifa / 100;
                
            $impuesto->monto = FG::dc($tot);     
            $this->impuesto = $impuesto->monto;
            FG::logfile("i: ".$tot);
            
        }else{
            $this->impuesto = 0;
        }
        return $impuesto;
    }
            
    public function setDetalle($post) {
        $this->factura = $post['id'];
        $this->setTipo($post);
        $this->tipo = '04'; //Codigo de uso interno
        $this->impuesto = $post['impuesto'];
        if(isset($post['unidad']))
            $this->unidad = $post['unidad'];
        else{
            $lote = Lote::findOne($post['codigo_lote']);
            $this->unidad = $lote->unidad;
        }
        
        $this->cantidad = $post['cantidad'];
            FG::logfile('C: '.$this->cantidad.' ');
       
        $this->montoTotal = FG::dc($post['costo'] * $post['cantidad']);
        
        $this->setDescuento($post);        
        $this->subTotal = FG::dc($this->montoTotal - $this->montoDescuento);
            FG::logfile('P: '.$post['costo'].' ');
            FG::logfile('D: '.$this->montoDescuento.' ');
            FG::logfile('S: '.$this->subTotal.' ');
        
        $impuesto = $this->setImpuesto($post);         
        $this->montoTotalLinea = FG::dc($this->subTotal) + FG::dc($impuesto->monto);
            FG::logfile('T: '.$this->montoTotalLinea.' ');
        
        return $impuesto;
        
        
    }
    
    
    public function getImpuestosAplicadosCalc(){
        $total = 0;
        foreach($this->impuestosaplicados as $impuesto){
          $total += $impuesto->monto;
        }
        return FG::dc($total);
    }
}

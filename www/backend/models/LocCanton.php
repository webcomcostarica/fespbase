<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "loc_canton".
 *
 * @property integer $id
 * @property integer $codigoProvincia
 * @property integer $codigo
 * @property string $canton
 */
class LocCanton extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'loc_canton';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codigoProvincia', 'codigo'], 'required'],
            [['codigoProvincia', 'codigo'], 'integer'],
            [['canton'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigoProvincia' => 'Codigo Provincia',
            'codigo' => 'Codigo',
            'canton' => 'Canton',
        ];
    }
    
    
    public static function getCantones($id){
        $data = [];
        foreach(LocCanton::find()->where(['codigoProvincia' => $id])->all() as $prov){
            $data[$prov->codigo] = $prov->canton;
        }
        return $data;
    }
}

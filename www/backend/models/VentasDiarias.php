<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ventas_diarias".
 *
 * @property int $id
 * @property string $fecha
 * @property string $condicion
 * @property string $medioPago
 * @property double $tipoCambio
 * @property double $totGravado
 * @property double $totExento
 * @property double $totDescuento
 * @property double $totVentaNeta
 * @property double $totImpuesto
 * @property double $total
 */
class VentasDiarias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ventas_diarias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha', 'totGravado', 'totExento', 'totDescuento', 'totVentaNeta', 'totImpuesto', 'total'], 'required'],
            [['fecha'], 'string'],
            [['totGravado', 'totExento', 'totDescuento', 'totVentaNeta', 'totImpuesto', 'total'], 'number'],
         ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha' => 'Fecha',
            'condicion' => 'Condicion',
            'medioPago' => 'Medio Pago',
            'tipoCambio' => 'Tipo Cambio',
            'totGravado' => 'Tot Gravado',
            'totExento' => 'Tot Exento',
            'totDescuento' => 'Tot Descuento',
            'totVentaNeta' => 'Tot Venta Neta',
            'totImpuesto' => 'Tot Impuesto',
            'total' => 'Total',
        ];
    }
    
    public static function GenerarResumen(){
        $model = VentasDiarias::find()->orderBy(['id' => SORT_DESC])->one();
        if(isset($model)){ //Ultimo registro existe
        
            $sql = "            
                select `creacion`, 
                    TRUNCATE(SUM(`dgt_mercaExenta`),2) as exento, 
                    TRUNCATE(SUM(`dgt_gravado`),2) as gravado, 
                    TRUNCATE(SUM(`dgt_totDescuento`),2) as descuentos, 
                    TRUNCATE(SUM(`dgt_totVentaNeta`),2) as totalVenta, 
                    TRUNCATE(SUM(`dgt_totImpuesto`),2) as totImpuesto, 
                    TRUNCATE(SUM(`dgt_total`),2) as totalpordia 
                from fe_comprobante 
                where `creacion`>'".$model->fecha."' 
                group by `creacion`;
            
            ";
        }else{
            $sql = "            
                select `creacion`, 
                    TRUNCATE(SUM(`dgt_mercaExenta`),2) as exento, 
                    TRUNCATE(SUM(`dgt_gravado`),2) as gravado, 
                    TRUNCATE(SUM(`dgt_totDescuento`),2) as descuentos, 
                    TRUNCATE(SUM(`dgt_totVentaNeta`),2) as totalVenta, 
                    TRUNCATE(SUM(`dgt_totImpuesto`),2) as totImpuesto, 
                    TRUNCATE(SUM(`dgt_total`),2) as totalpordia 
                from fe_comprobante                
                group by `creacion`;";
        }
        
        $datos = Yii::$app->db->createCommand($sql)->queryAll();
        
        foreach($datos as $dato){
            $NuevoModelo = new VentasDiarias();
            $NuevoModelo->fecha = $dato['creacion'];
            $NuevoModelo->totGravado = $dato['gravado'];
            $NuevoModelo->totExento = $dato['exento'];
            $NuevoModelo->totDescuento = $dato['descuentos'];
            $NuevoModelo->totVentaNeta = $dato['totalVenta'];
            $NuevoModelo->totImpuesto = $dato['totImpuesto'];
            $NuevoModelo->total = $dato['totalpordia'];
            if(!$NuevoModelo->save()){
                var_dump($NuevoModelo->errors);
                die;
            }
        }        

    }
    
    public function moneda_reporte($pMonto,$s=null){
        
        return  number_format((double)$pMonto, 2, ',', '');
      
              
    }
}

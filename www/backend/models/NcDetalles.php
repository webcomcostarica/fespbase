<?php

namespace backend\models;
use backend\models\ImpuestosAplicados;
use Yii;
use common\models\FG;

/**
 * This is the model class for table "nc_detalles".
 *
 * @property integer $id
 * @property integer $factura
 * @property integer $tipo
 * @property integer $lote
 * @property integer $lote_precio
 * @property double $cantidad
 * @property string $unidad
 * @property string $descripcion
 * @property double $montoDescuento
 * @property string $impuesto
 * @property string $tipoDescuento
 * @property string $naturalezaDescuento
 * @property double $montoTotal
 * @property double $subTotal
 * @property double $montoTotalLinea
 *
 * @property NcComprobante $factura0
 */
class NcDetalles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nc_detalles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['factura', 'tipo', 'cantidad', 'unidad', 'descripcion', 'montoDescuento', 'tipoDescuento', 'montoTotal', 'subTotal', 'montoTotalLinea'], 'required'],
            [['factura', 'tipo', 'lote'], 'integer'],
            [['impuesto', 'lote_precio'], 'number'],
            [['cantidad', 'montoDescuento', 'montoTotal', 'subTotal', 'montoTotalLinea'], 'number'],
            [['unidad', 'descripcion', 'tipoDescuento', 'naturalezaDescuento'], 'string']
        ];
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['Anulacion'] = ['descripcion','factura'];
        return $scenarios;
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'factura' => Yii::t('backend', 'Factura'),
            'tipo' => Yii::t('backend', 'Tipo'),
            'lote' => Yii::t('backend', 'Lote'),
            'lote_precio' => Yii::t('backend', 'Lote Precio'),
            'cantidad' => Yii::t('backend', 'Cantidad'),
            'unidad' => Yii::t('backend', 'Unidad'),
            'descripcion' => Yii::t('backend', 'Descripcion'),
            'montoDescuento' => Yii::t('backend', 'Monto Descuento'),
            'impuesto' => Yii::t('backend', 'Impuesto'),
            'tipoDescuento' => Yii::t('backend', 'Tipo Descuento'),
            'naturalezaDescuento' => Yii::t('backend', 'Naturaleza Descuento'),
            'montoTotal' => Yii::t('backend', 'Monto Total'),
            'subTotal' => Yii::t('backend', 'Sub Total'),
            'montoTotalLinea' => Yii::t('backend', 'Monto Total Linea'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFactura0()
    {
        return $this->hasOne(NcComprobante::className(), ['id' => 'factura']);
    }
    
     /**
     * @return \yii\db\ActiveQuery
     */
    public function getImpuestosaplicados()
    {
        return $this->hasMany(Impuestosaplicados::className(), ['idDetalle' => 'id']);
    }
    
     /**
     * @return \yii\db\ActiveQuery
     */
    public function getLote0()
    {
        return $this->hasOne(Lote::className(), ['id' => 'lote']);
    }
    
    /**
     * Funciones
     */    
     public function setNcDetalle($detalle) {     
        $this->tipo = $detalle->tipo;
        $this->lote = $detalle->lote;       
        $this->lote_precio = $detalle->lote_precio;
        $this->cantidad = $detalle->cantidad;
        $this->unidad = $detalle->unidad;
        $this->montoDescuento = FG::dc($detalle->montoDescuento);
        $this->impuesto = FG::dc($detalle->impuesto);
        $this->tipoDescuento = $detalle->tipoDescuento;
        $this->naturalezaDescuento = $detalle->naturalezaDescuento;
        $this->montoTotal = FG::dc($detalle->montoTotal);
        $this->subTotal = FG::dc($detalle->subTotal);
        $this->montoTotalLinea = FG::dc($detalle->montoTotalLinea);
    }
}

<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "impuestosaplicados".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $codigo
 * @property double $monto
 * @property double $tarifa
 * @property integer $idDetalle
 * @property string $fecha
 * @property string $tag
 *
 * @property DetalleFactura $idDetalle0
 */
class ImpuestosAplicados extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'impuestosaplicados';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'codigo', 'monto', 'tarifa', 'idDetalle', 'fecha', 'tag'], 'required'],
            [['nombre', 'codigo', 'fecha', 'tag'], 'string'],
            [['monto', 'tarifa'], 'number'],
            [['idDetalle'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'nombre' => Yii::t('backend', 'Nombre'),
            'codigo' => Yii::t('backend', 'Codigo'),
            'monto' => Yii::t('backend', 'Monto'),
            'tarifa' => Yii::t('backend', 'Tarifa'),
            'idDetalle' => Yii::t('backend', 'Id Detalle'),
            'fecha' => Yii::t('backend', 'Fecha'),
            'tag' => Yii::t('backend', 'Tag'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdDetalle0()
    {
        return $this->hasOne(DetalleFactura::className(), ['id' => 'idDetalle']);
    }
}

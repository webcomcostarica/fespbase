<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "loc_barrio".
 *
 * @property integer $id
 * @property integer $codigoProvincia
 * @property integer $codigoCanton
 * @property integer $codigoDistrito
 * @property integer $codigo
 * @property string $barrio
 */
class LocBarrio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'loc_barrio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codigoProvincia', 'codigoCanton', 'codigoDistrito', 'codigo'], 'integer'],
            [['barrio'], 'string', 'max' => 80],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigoProvincia' => 'Codigo Provincia',
            'codigoCanton' => 'Codigo Canton',
            'codigoDistrito' => 'Codigo Distrito',
            'codigo' => 'Codigo',
            'barrio' => 'Barrio',
        ];
    }
    
    public static function getBarrios($distrito,$canton,$provincia){
        $data = [];
        foreach(LocBarrio::find()
        ->where(['codigoDistrito' => $distrito])
        ->andWhere(['codigoProvincia' => $provincia])
        ->andWhere(['codigoCanton' => $canton])
        ->all() as $prov){
            $data[$prov->codigo] = $prov->barrio;
        }
        return $data;
    }
}

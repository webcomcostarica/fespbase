<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "contacto".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $correo
 * @property string $fecha
 * @property string $tipo
 * @property string $mensaje
 */
class Contacto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tiquetes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'fecha', 'tipo', 'mensaje'], 'required'],
            [['nombre', 'correo', 'tipo', 'mensaje'], 'string'],
            [['fecha'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'nombre' => Yii::t('backend', 'Nombre'),
            'correo' => Yii::t('backend', 'Correo'),
            'fecha' => Yii::t('backend', 'Fecha'),
            'tipo' => Yii::t('backend', 'Tipo'),
            'mensaje' => Yii::t('backend', 'Mensaje'),
        ];
    }
}

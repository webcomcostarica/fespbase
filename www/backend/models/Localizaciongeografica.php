<?php

namespace backend\models;

use Yii;
use backend\models\base\BaseLocalizacion;

class Localizaciongeografica extends BaseLocalizacion
{ 
    public function getLocalizacionsuperior()
    {
        return $this->hasOne(Localizaciongeografica::className(), ['idlocalizacion' => 'idPadre']);
    }
    
    const NIVELES = [
        null=>'ninguno',
        0=>'Continentes',
        1=>'Paises',
        2=>'Provincias',
        3=>'Cantones',
        4=>'Distritos',
    ];
    const NIVEL = [
        null=>'ninguno',
        0=>'Continente',
        1=>'País',
        2=>'Provincia',
        3=>'Cantón',
        4=>'Distrito',
    ];
    
    public function getTipo(){
        return self::NIVEL[$this->nivel];
    }
    
    public static function getLocalizacionesPorSuperior($superior){
        $localizaciones = Localizaciongeografica::find()
        ->where('idPadre=:p',[':p' => $superior ])	
        ->orderBy('idLocalizacion ASC')
        ->all();
        $data=[];
        foreach($localizaciones as $loc){
            $data[$loc->idLocalizacion] = $loc->nombre;
        }
        return $data;
    }
}

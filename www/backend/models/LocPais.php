<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "loc_pais".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $iso2
 * @property string $iso3
 * @property integer $codigoTelefono
 */
class LocPais extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'loc_pais';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codigoTelefono'], 'integer'],
            [['nombre'], 'string', 'max' => 100],
            [['iso2'], 'string', 'max' => 2],
            [['iso3'], 'string', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'iso2' => 'Iso2',
            'iso3' => 'Iso3',
            'codigoTelefono' => 'Codigo Telefono',
        ];
    }
}

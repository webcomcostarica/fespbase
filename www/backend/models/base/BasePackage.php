<?php

namespace backend\models\base;

use Yii;

/**
 * This is the model class for table "package".
 *
 * @property integer $id
 * @property string $categoria
 * @property string $compra
 * @property string $desempacada
 * @property string $proveedor
 * @property string $estado
 * @property double $precio
 * @property string $calidad
 * @property string $descripcion
 * @property string $creacion
 * @property string $modificacion
 * @property integer $num_factura
 */
class BasePackage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'package';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['categoria', 'compra', 'desempacada', 'proveedor', 'estado', 'precio', 'calidad', 'descripcion', 'creacion', 'modificacion', 'num_factura'], 'required'],
            [['categoria', 'proveedor', 'estado', 'calidad', 'descripcion'], 'string'],
            [['compra', 'desempacada', 'creacion', 'modificacion'], 'safe'],
            [['precio'], 'number'],
            [['num_factura'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'categoria' => Yii::t('app', 'Categoria'),
            'compra' => Yii::t('app', 'Compra'),
            'desempacada' => Yii::t('app', 'Desempacada'),
            'proveedor' => Yii::t('app', 'Proveedor'),
            'estado' => Yii::t('app', 'Estado'),
            'precio' => Yii::t('app', 'Precio'),
            'calidad' => Yii::t('app', 'Calidad'),
            'descripcion' => Yii::t('app', 'Descripcion'),
            'creacion' => Yii::t('app', 'Creacion'),
            'modificacion' => Yii::t('app', 'Modificacion'),
            'num_factura' => Yii::t('app', 'Num Factura'),
        ];
    }
}

<?php

namespace backend\models\base;

use Yii;
use backend\models\DetalleFactura;
/**
 * This is the model class for table "factura".
 *
$id
$fecha
$vence
$idUsua
$creacion
$modificacion
$tipo
$forma_pago
$estado
$colaboradores
$dgt_condicion
$dgt_plazoCredito
$dgt_medioPago
$dgt_moneda
$dgt_tipoCambio
$dgt_serviciosGravados
$dgt_serviciosExentos
$dgt_mercaGravada
$dgt_mercaExenta
$dgt_gravado
$dgt_totExento
$dgt_totVenta
$dgt_totDescuento
$dgt_totVentaNeta
$dgt_totImpuesto
$dgt_total
 *
 * @property DetalleFactura[] $detalleFacturas
 * @property DgtFe[] $dgtves
 */
class BaseFactura extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fe_comprobante';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha', 'vence', 'idUsua', 'creacion', 'modificacion', 'tipo', 'estado', 'dgt_condicion', 'dgt_medioPago', 'dgt_moneda', 'dgt_tipoCambio', ], 'required'],
            [['fecha', 'tipo', 'forma_pago', 'estado', 'dgt_plazoCredito'], 'string'],
            [['vence', 'creacion', 'modificacion'], 'safe'],
            [['dgt_tipoCambio', 'dgt_serviciosGravados', 'dgt_serviciosExentos', 'dgt_mercaGravada', 'dgt_mercaExenta', 'dgt_gravado', 'dgt_totExento', 'dgt_totVenta', 'dgt_totDescuento', 'dgt_totVentaNeta', 'dgt_totImpuesto', 'dgt_total'], 'number'],
            [['idUsua', 'colaboradores', 'dgt_condicion'], 'integer'],
            [['dgt_medioPago'], 'string', 'max' => 2],
            [['dgt_moneda'], 'string', 'max' => 3]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'fecha' => Yii::t('backend', 'Fecha'),
            'vence' => Yii::t('backend', 'Vence'),
            'idUsua' => Yii::t('backend', 'Id Usua'),
            'creacion' => Yii::t('backend', 'Creacion'),
            'modificacion' => Yii::t('backend', 'Modificacion'),
            'tipo' => Yii::t('backend', 'Tipo'),
            'forma_pago' => Yii::t('backend', 'Forma Pago'),
            'estado' => Yii::t('backend', 'Estado'),
            'colaboradores' => Yii::t('backend', 'Colaboradores'),
            'dgt_condicion' => Yii::t('backend', 'Dgt Condicion'),
            'dgt_plazoCredito' => Yii::t('backend', 'Dgt Plazo Credito'),
            'dgt_medioPago' => Yii::t('backend', 'Dgt Medio Pago'),
            'dgt_moneda' => Yii::t('backend', 'Dgt Moneda'),
            'dgt_tipoCambio' => Yii::t('backend', 'Dgt Tipo Cambio'),
            'dgt_serviciosGravados' => Yii::t('backend', 'Dgt Servicios Gravados'),
            'dgt_serviciosExentos' => Yii::t('backend', 'Dgt Servicios Exentos'),
            'dgt_mercaGravada' => Yii::t('backend', 'Dgt Merca Gravada'),
            'dgt_mercaExenta' => Yii::t('backend', 'Dgt Merca Exenta'),
            'dgt_gravado' => Yii::t('backend', 'Dgt Gravado'),
            'dgt_totExento' => Yii::t('backend', 'Dgt Tot Exento'),
            'dgt_totVenta' => Yii::t('backend', 'Dgt Tot Venta'),
            'dgt_totDescuento' => Yii::t('backend', 'Dgt Tot Descuento'),
            'dgt_totVentaNeta' => Yii::t('backend', 'Dgt Tot Venta Neta'),
            'dgt_totImpuesto' => Yii::t('backend', 'Dgt Tot Impuesto'),
            'dgt_total' => Yii::t('backend', 'Dgt Total'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetalleFacturas()
    {
        return $this->hasMany(DetalleFactura::className(), ['factura' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDgtves()
    {
        return $this->hasMany(DgtFe::className(), ['idFactura' => 'id']);
    }
}

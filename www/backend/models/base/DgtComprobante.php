<?php

namespace backend\models\base;

use yii\helpers\Html;
use Yii;
use common\components\FuncionesDGT;
use backend\models\NcComprobante;

class DgtComprobante extends \yii\db\ActiveRecord
{
    
   
    
    public function getEnvioEstadoL(){
        if($this->envioEstado == 0)
            return 'Enviar';
        
        if($this->envioEstado >= 200 && $this->envioEstado <= 299 || ($this->envioEstado >= 400 && $this->envioEstado <= 499))
            return 'Recibido';
        
        if($this->envioEstado >= 300 && $this->envioEstado <= 899)
            return 'Error';
        
        return $this->envioEstado;
       
    }
    
    public function getEstadoEstadoL(){
        if($this->getNotaCredito())
            return 'Anulada';
        
        if($this->envioEstado == 202 || ($this->envioEstado >= 400 && $this->envioEstado <= 499)){
            if($this->estadoEstado == 1)
                return 'Aceptado';
            if($this->estadoEstado == 2)
                return '(Rechazado Parcialmente) <a  href="/factura/nota-credito?id=' . $this->idComprobante . '">Nota Crédito</a>';
             if($this->estadoEstado == 3){
                return '(Rechazado) '.
                            Html::a('Nota Crédito',
                                ['/factura/nota-credito'],
                                [
                                    'data' => [
                                        'method' => 'post',
                                        'params' => ['id' => $this->idComprobante],
                                    ],                                   
                                ]
                            );
             }
             if($this->estadoEstado == 0)
                return '(Pendiente) <a  href="/factura/estado?id=' . $this->idComprobante . '&destination=index">Consultar</a>';
        }
        
        return 'Error';
   }
   
   
   public function getEnvioEstadoD(){        
        if($this->envioEstado >= 200 && $this->envioEstado <= 299)
            return "({$this->envioEstado}) Aceptado";
        if($this->envioEstado >= 400 && $this->envioEstado <= 499)
            return "({$this->envioEstado}) Revisar";
    
        return '<a  href="/index.php?r=dgt%2Fenviar&tipo=01?id=' . $this->id . '">Enviar</a>';
    }
    
    public function getEstadoEstadoD(){
        if($this->envioEstado == 202){
            if($this->estadoEstado == 1)
                return "({$this->estadoEstado}) Aceptado";
            if($this->estadoEstado == 2)
                return "({$this->estadoEstado}) Rechazado Parcialmente";
             if($this->estadoEstado == 3)
                return "({$this->estadoEstado}) Rechazado";           
        }
        if($this->envioEstado >= 400 && $this->envioEstado <= 499)
            return 'Revisar';
        return 'Error';
   }
   
   public function getNotaCredito(){
      return NcComprobante::find()
      ->where(['factura'=>$this->idComprobante])
      ->one();
   }
   
   public function getClase(){
       if($this->envioEstado > 99 && $this->envioEstado < 200)
           return 100;
       
       if($this->envioEstado > 199 && $this->envioEstado < 300)
           return 200;
       
       if($this->envioEstado > 299 && $this->envioEstado < 400)
           return 300; 
       
       if($this->envioEstado > 399 && $this->envioEstado < 500)
           return 400;
       
       if($this->envioEstado > 499 && $this->envioEstado < 600)
           return 500;
       return $this->envioEstado;
       
   }
  
   
     public function getClaseEstado(){
       if($this->estadoEstado > 99 && $this->estadoEstado < 200)
           return 100;
       
       if($this->estadoEstado > 199 && $this->estadoEstado < 300)
           return 200;
       
       if($this->estadoEstado > 299 && $this->estadoEstado < 400)
           return 300; 
       
       if($this->estadoEstado > 399 && $this->estadoEstado < 500)
           return 400;
       
       if($this->estadoEstado > 499 && $this->estadoEstado < 600)
           return 500;
       return $this->estadoEstado;
       
   }
   
   public function getConsultarDeNuevo(){
       if($this->estadoEstado > 0 && $this->estadoEstado < 4 )
           return false;
       return true;
   }

}

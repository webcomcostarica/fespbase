<?php

namespace backend\models\base;

use Yii;

/**
 * This is the model class for table "localizaciongeografica".
 *
 * @property integer $idLocalizacion
 * @property string $nombre
 * @property string $codigo
 * @property integer $orden
 * @property string $iso2
 * @property string $iso3
 * @property string $postal
 * @property integer $idPadre
 * @property string $memoria
 */
class BaseLocalizacion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'localizaciongeografica';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codigo', 'orden', 'idPadre'], 'required'],
            [['orden', 'idPadre'], 'integer'],
            [['nombre'], 'string', 'max' => 20],
            [['codigo'], 'string', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idLocalizacion' => 'Id Localizacion',
            'nombre' => 'Nombre',
            'codigo' => 'Codigo',
            'orden' => 'Orden',
            'iso2' => 'Iso2',
            'iso3' => 'Iso3',
            'postal' => 'Postal',
            'idPadre' => 'Id Padre',
            'memoria' => 'Memoria',
        ];
    }
}

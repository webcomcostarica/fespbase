<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "sis_parametros".
 *
 * @property integer $id
 * @property string $dato
 * @property string $valor
 */
class SisParametros extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sis_parametros';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dato', 'valor'], 'required'],
            [['dato', 'valor'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dato' => 'Dato',
            'valor' => 'Valor',
        ];
    }
}

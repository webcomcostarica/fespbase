<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "dgt_colas".
 *
 * @property integer $id
 * @property integer $comprobante
 * @property integer $tipo
 * @property integer $estado
 * @property integer $terminal
 */
class DgtColas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dgt_colas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comprobante', 'tipo', 'estado', 'terminal'], 'required'],
            [['comprobante', 'tipo', 'estado', 'terminal'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'comprobante' => 'Comprobante',
            'tipo' => 'Tipo',
            'estado' => 'Estado',
            'terminal' => 'Terminal',
        ];
    }
    
    public static function setItem($comprobante,$terminal, $tipo='01'){
        if($tipo === '01'){
            $modelCola = new DgtColas();
            $modelCola->comprobante = $comprobante->id;
            $modelCola->tipo = $tipo;
            $modelCola->terminal = $terminal;
            $modelCola->estado = 0;
            if(!$modelCola->save()){
                var_dump($modelCola->errors);
                die;
            }
        }
    }
}

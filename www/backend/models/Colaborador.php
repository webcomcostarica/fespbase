<?php

namespace backend\models;

use Yii;

use backend\models\ColaboradorNotificacion;
/**
 * This is the model class for table "colaborador".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $apellido
 * @property string $sapellido
 * @property string $cedula
 * @property integer $tipo
 * @property string $nacimiento
 *
 * @property Factura[] $facturas
 */
class Colaborador extends \yii\db\ActiveRecord
{
    const TIPOS = [
        1 => 'Fisica',
        2 => 'Juridica',
        999 => 'N/A',
        0 => 'N/A',
    ];
	public $notificacionesActivas;
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'colaborador';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre','pais','tipoIdentificacion', 'cedula', 'tipo','nombreCompleto'], 'required'],
            [['nombre', 'apellido', 'sapellido','nombreCompleto','cod_provedor'], 'string'],
            [['nombre', 'apellido', 'sapellido'], 'filter','filter'=>'strtoupper'],
            [['tipo','tipoIdentificacion'], 'integer'],
            [['cedula'], 'unique'],
            [['correo'], 'email'],
            [['telefono'],'number',
                'min'=>10000000,
                'max'=>99999999999999999999,
                'tooBig'=>'Eje: 80080002008899',
                'tooSmall'=>'Eje: 27718080'
                ],
            [['cedula'], 'match', 'pattern' => '/\d{9,12}/','message'=>'Formato incorrecto (Número de 9 a 12 dígitos sin guiones y sin espacios).'],
            [['cedula'],'number',
                'min'=>100000000,
                'max'=>999999999999,
                'tooBig'=>'Eje: 114930394 o 3101435443',
                'tooSmall'=>'Eje: 114930394 o 3101435443'
                ],
            [['nacimiento'], 'safe']
        ];
    }
	
	

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'pais' => Yii::t('backend', 'Codigo país (506)'),
            'nombre' => Yii::t('backend', 'Nombre'),
            'apellido' => Yii::t('backend', '1er Apellido'),
            'sapellido' => Yii::t('backend', '2do Apellido'),
            'cedula' => Yii::t('backend', 'Cédula'),
            'tipo' => Yii::t('backend', 'Rol'),
            'nacimiento' => Yii::t('backend', 'Nacimiento'),
            'tipoIdentificacionletras' => Yii::t('backend', 'Tipo'),
        ];
    }

	public function afterSave($insert, $changedAttributes){
		
        Yii::$app->db->createCommand()->delete('col_notificaciones', 'col_id = '.(int) $this->id)->execute();

		if(isset($_POST["Colaborador"])){
			if(isset($_POST["Colaborador"]["notificacionesActivas"]))
				foreach($_POST["Colaborador"]["notificacionesActivas"] as $id) {					
					$ro = new ColaboradorNotificacion();
					$ro->col_id = $this->id;
					$ro->cat_notif_id = $id;
					$ro->save();
				}				
		}
	}

	
	public function getNotificaciones()
    {
        return $this->hasMany(ColaboradorNotificacion::className(), ['col_id' => 'id']);
    }
	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacturas()
    {
        return $this->hasMany(Factura::className(), ['cliente' => 'id']);
    }
           
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdFacturas()
    {
        return $this
        ->hasMany(Factura::className(), ['id' => 'id_factura'])
        ->viaTable('fe_colaboradores_comprobante', ['id_colaborador' => 'id']);
    }
    
    public function getRol(){
        $tipo=[
            1=>'(C) Cliente',
            2=>'(E) Empleado',
            3=>'(I) Inversionista',
            4=>'(P) Proveedor',
            ];
        if(isset($this->tipo))
            return $tipo[$this->tipo];
        return null;
    }
     /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbContactos()
    {
        return $this->hasMany(Contactos::className(), ['colaborador' => 'id']);
    }
    
    public function GuardarFechas(){        
        date_default_timezone_set('America/Costa_Rica');
        $this->nacimiento =  date('Y-m-d',strtotime($this->nacimiento)); 
    }
    /*
     * Muestra las fechas en formato d-m-y
     */
    public function VerFechas(){        
        date_default_timezone_set('America/Costa_Rica');
        $this->nacimiento =  date('d-m-Y',strtotime($this->nacimiento));       
    }
    
    public function getNombreCompleto(){
        $nombre =  $this->nombre;
        $nombre .= (trim($this->apellido!=''))?' '.$this->apellido:'';
        $nombre .= (trim($this->sapellido!=''))?' '.$this->sapellido:'';
            
        if($this->tipoIdentificacion == 2){
            if(!empty($this->nombreCompleto))
                $nombre = substr($this->nombreCompleto,0,50);
        }
        return $nombre;
    }
    
    public function getDireccion(){
        return '';
    }
    
    
    public function getTipoIdentificacionletras(){
        return self::TIPOS[$this->tipoIdentificacion];
    }
    
    public static function getProveedores(){
        return Colaborador::find()
        ->where(['=','tipo',4])
        ->all();
    }
}

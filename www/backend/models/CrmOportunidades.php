<?php

namespace backend\models;
use common\models\User;
use backend\models\Colaborador;

use Yii;

/**
 * This is the model class for table "crm_oportunidades".
 *
 * @property integer $id
 * @property string $nombre
 * @property double $monto
 * @property integer $etapa
 * @property string $detalles
 * @property integer $prioridad
 * @property integer $orden
 * @property integer $estado
 * @property string $creacion
 * @property string $modificacion
 * @property string $cierreProyectado
 * @property integer $encargado
 * @property integer $cliente
 *
 * @property Colaborador $cliente0
 * @property User $encargado0
 */
class CrmOportunidades extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'crm_oportunidades';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'monto', 'etapa', 'prioridad','encargado', 'cliente'], 'required'],
            [['nombre', 'detalles'], 'string'],
            [['monto'], 'number'],
            [['etapa', 'prioridad', 'orden', 'estado', 'encargado', 'cliente'], 'integer'],
            [['creacion', 'modificacion', 'cierreProyectado'], 'safe'],
            [['cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Colaborador::className(), 'targetAttribute' => ['cliente' => 'id']],
            [['encargado'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['encargado' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'monto' => 'Monto',
            'etapa' => 'Etapa',
            'detalles' => 'Detalles',
            'prioridad' => 'Prioridad',
            'orden' => 'Orden',
            'estado' => 'Estado',
            'creacion' => 'Creacion',
            'modificacion' => 'Modificacion',
            'cierreProyectado' => 'Cierre Proyectado',
            'encargado' => 'Encargado',
            'cliente' => 'Cliente',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCliente0()
    {
        return $this->hasOne(Colaborador::className(), ['id' => 'cliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEncargado0()
    {
        return $this->hasOne(User::className(), ['id' => 'encargado']);
    }
    
    public static function porPrioridad(){
        return CrmOportunidades::find()
        ->orderBy([
            'prioridad' => SORT_DESC 
        ])->all();
    }
}

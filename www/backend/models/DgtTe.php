<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "dgt_te".
 *
 * @property string $clave
 * @property string $numConce
 * @property string $fechaEmision
 * @property string $normativa
 * @property string $fechaNormativa
 */
class DgtTe extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dgt_te';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fechaEmision', 'normativa', 'fechaNormativa'], 'required'],
            [['fechaEmision', 'normativa', 'fechaNormativa'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'clave' => Yii::t('backend', 'Clave'),
            'numConce' => Yii::t('backend', 'Num Conce'),
            'fechaEmision' => Yii::t('backend', 'Fecha Emision'),
            'normativa' => Yii::t('backend', 'Normativa'),
            'fechaNormativa' => Yii::t('backend', 'Fecha Normativa'),
        ];
    }
}

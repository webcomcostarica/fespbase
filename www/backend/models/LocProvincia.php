<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "loc_provincia".
 *
 * @property integer $id
 * @property integer $codigo
 * @property string $provincia
 */
class LocProvincia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'loc_provincia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codigo'], 'integer'],
            [['provincia'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo' => 'Codigo',
            'provincia' => 'Provincia',
        ];
    }
    
    public static function getProvincias(){
        $provincias = [];
        foreach(LocProvincia::find()->all() as $prov){
            $provincias[$prov->codigo] = $prov->provincia;
        }
        return $provincias;
    }
}

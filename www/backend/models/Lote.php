<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "lote".
 *
 * @property integer $id
 * @property integer $paca
 * @property double $precio
 * @property string $calidad
 * @property integer $cantidad
 * @property string $descripcion
 * @property string $creacion
 * @property string $modificacion
 * @property integer $cant_inicial
 * @property integer $imp_ven_i
 *
 * @property Package $paca0
 * @property LotePrecio[] $lotePrecios
 */
class Lote extends \yii\db\ActiveRecord
{
	public $EXENTO = 0;
	public $AGRAVADO = 1;
	
    private $creacion_fecha;    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lote';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['paca', 'precio',  'cantidad', 'descripcion', 'creacion', 
            'modificacion', 'cant_inicial','unidad'], 'required'],
            [['paca','imp_ven_i'], 'integer'],
            [['precio','precioCompra','utilidad','minimo','alerta'], 'number'],
            [['cod_barras','cod_inte_producto','cod_prod_proveedor'], 'unique'],
            [['cantidad', 'cant_inicial','minimo','alerta'], 'double'],
            [['calidad', 'descripcion','unidad','cod_barras','cod_provedor_a',
            'cod_inte_producto','cod_prod_proveedor'], 'string'],
            [['creacion', 'modificacion'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'paca' => Yii::t('backend', 'Paca'),
            'precio' => Yii::t('backend', 'Precio de venta'),
            'calidad' => Yii::t('backend', 'Calidad'),
            'unidad' => Yii::t('backend', 'Unidad de medida'),
            'cantidad' => Yii::t('backend', 'Cantidad Actual'),
            'descripcion' => Yii::t('backend', 'Descripcion'),
            'creacion' => Yii::t('backend', 'Creacion'),
            'modificacion' => Yii::t('backend', 'Modificacion'),
            'cant_inicial' => Yii::t('backend', 'Cant Inicial'),
            'impGetText' => Yii::t('backend', 'Impuesto'),
            'cod_provedor_a' => Yii::t('backend', 'Cod Proveedor'),
            'cod_inte_producto' => Yii::t('backend', 'CPI / Código Prod Interno'),
            'cod_prod_proveedor' => Yii::t('backend', 'CPP / Código Prod Proveedor'),
        ];
    }

	 /**
     * @return \yii\db\ActiveQuery
     */
    public function getImpGetText()
    {
        if(isset($this->imp_ven_i))
			if($this->imp_ven_i==1)
				return 'AGRAVADO';
			else
				return 'EXENTO';
		else
			return 'N/D';
    }
	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaca0()
    {
        return $this->hasOne(Package::className(), ['id' => 'paca']);
    }
    
    public function getProveedor0()
    {
        return $this->hasOne(Colaborador::className(), ['cod_provedor' => 'cod_provedor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLotePrecios()
    {
        return $this->hasMany(LotePrecio::className(), ['lote' => 'id']);
    }
    
    
    public function getPrecioVigente(){        
        $precio=  LotePrecio::find()->where(['lote' => $this->id,'vigente'=>'1'])->one();   
        if(empty($precio))
            return 0;
        $this->precio = $precio->costo;
        return $this->precio;
    }
    /*
     * Muestra las fechas en formato d-m-y
     */
    public function VerFechas(){        
        date_default_timezone_set('America/Costa_Rica');
        $this->creacion_fecha=$this->creacion;
        $this->creacion =  date('d-m-Y  H:i:s',strtotime($this->creacion));    
        $this->modificacion =  date('d-m-Y  H:i:s',strtotime($this->modificacion));        
    }
    /*
     * Guarda las fechas en BD con formato Y-m-d
     */
    public function GuardarFechas(){        
        date_default_timezone_set('America/Costa_Rica');
        $this->modificacion =  date('Y-m-d H:i:s',  strtotime('now'));  
        $this->creacion =  $this->creacion_fecha;
              
        if($this->isNewRecord){                    
            $this->creacion =  date('Y-m-d H:i:s',strtotime('now'));                
        }
    }
}

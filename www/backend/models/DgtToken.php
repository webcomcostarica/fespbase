<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "dgt_token".
 *
 * @property integer $id
 * @property string $access_token
 * @property string $expires_in
 * @property string $refresh_expires_in
 * @property string $refresh_token
 * @property string $refresh_token_limit
 * @property string $token_type
 * @property string $id_token
 * @property string $session_state
 */
class DgtToken extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dgt_token';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['access_token', 'refresh_token',  'token_type', 'id_token', 'session_state'], 'required'],
            [['access_token', 'refresh_token', 'token_type', 'id_token', 'session_state'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'access_token' => Yii::t('backend', 'Access Token'),
            'expires_in' => Yii::t('backend', 'Expires In'),
            'refresh_expires_in' => Yii::t('backend', 'Refresh Expires In'),
            'refresh_token' => Yii::t('backend', 'Refresh Token'),
            'refresh_token_limit' => Yii::t('backend', 'Refresh Token Limit'),
            'token_type' => Yii::t('backend', 'Token Type'),
            'id_token' => Yii::t('backend', 'Id Token'),
            'session_state' => Yii::t('backend', 'Session State'),
           
        ];
    }
    
    public function getAmbiente(){
        if($this->id == 1)
            return 'Pruebas';
        else
            return 'Producción';  	
    } 
    
    public function getEstado(){
        $ahora = strtotime(date('Y-m-d H:i:s'));                        
        if((int)$ahora < (int)($this->refresh_token_limit)){	
            return 'Activo';
        }else{	
            return 'Inactivo';                        
        }	
    }
}

<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "caja".
 *
 * @property integer $id
 * @property integer $cincuentamil
 * @property integer $veitemil
 * @property integer $diesmil
 * @property integer $cincomil
 * @property integer $dosmil
 * @property integer $mil
 * @property integer $quinientos
 * @property integer $cien
 * @property integer $cincuenta
 * @property integer $vienticinco
 * @property integer $dies
 * @property integer $cinco
 * @property string $fecha
 * @property integer $usuario
 */
class Caja extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'caja';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cincuentamil', 'veitemil', 'diesmil', 'cincomil', 'dosmil', 'mil', 'quinientos', 'cien', 'cincuenta', 'vienticinco', 'veinte', 'dies', 'cinco', 'fecha', 'usuario'], 'required'],
            [['cincuentamil','sucursal','terminal', 'veitemil', 'diesmil', 'cincomil', 'dosmil', 'mil', 'quinientos', 'cien', 'cincuenta', 'vienticinco', 'veinte', 'dies', 'cinco', 'usuario'], 'integer'],
            [['fecha'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'cincuentamil' => Yii::t('backend', '50 mil'),
            'veitemil' => Yii::t('backend', '20 mil'),
            'diesmil' => Yii::t('backend', '10 mil'),
            'cincomil' => Yii::t('backend', '5 mil'),
            'dosmil' => Yii::t('backend', '2 mil'),
            'mil' => Yii::t('backend', 'Mil'),
            'quinientos' => Yii::t('backend', '500'),
            'cien' => Yii::t('backend', '100'),
            'cincuenta' => Yii::t('backend', '50'),
            'vienticinco' => Yii::t('backend', '25'),
            'viente' => Yii::t('backend', '20'),
            'dies' => Yii::t('backend', '10'),
            'cinco' => Yii::t('backend', '5'),
            'fecha' => Yii::t('backend', 'Fecha'),
            'usuario' => Yii::t('backend', 'Usuario'),
        ];
    }
}

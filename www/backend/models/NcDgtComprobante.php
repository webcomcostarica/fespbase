<?php

namespace backend\models;

use Yii;
use backend\models\NcComprobante;

/**
 * This is the model class for table "nc_dgt_comprobante".
 *
 * @property integer $id
 * @property string $estadoMensaje
 * @property integer $estadoEstado
 * @property string $envioMensaje
 * @property integer $envioEstado
 * @property integer $idComprobante
 * @property string $clave
 * @property string $numConce
 * @property string $fechaEmision
 * @property string $normativa
 * @property string $fechaNormativa
 *
 * @property NcComprobante $idFactura0
 */
class NcDgtComprobante extends \yii\db\ActiveRecord
{
    const ESTADOS = [
        null=>'Enviar',
        0=>'Enviar',
        1=>'Aceptado',
        2=>'Parcialmente Aceptado',
        3=>'Rechazado',
        100=>'Enviar',
        200=>'Recibido',
        300=>'<a href="#" data-toggle="tooltip" data-placement="right" title="300 No hay accceso en Hacienda - Avise al administrador">
            Error                
            </a>',
        400=>'<a href="#" data-toggle="tooltip" data-placement="right" title="400 No hay accceso en Hacienda - Avise al administrador">
            Error                
            </a>',
        500=>'<a href="#" data-toggle="tooltip" data-placement="right" title="500 No hay accceso en Hacienda - Avise al administrador">
            Error                
            </a>',
        800=>'Consultar',
    ];

     const CLASES = [
        null=>'',
        0=>'',
        1=>'success',
        2=>'warning',
        3=>'danger',
        100=>'warning',
        200=>'success',
        300=>'danger',
        400=>'danger',
        500=>'danger',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nc_dgt_comprobante';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['estadoEstado', 'envioEstado', 'idComprobante', 'clave', 'numConce', 'fechaEmision', 'normativa', 'fechaNormativa'], 'required'],
            [['estadoEstado', 'envioEstado', 'idComprobante'], 'integer'],
            [['clave', 'numConce', 'fechaEmision', 'normativa', 'fechaNormativa'], 'string'],
            [['estadoMensaje'], 'string', 'max' => 5000],
            [['envioMensaje'], 'string', 'max' => 500]
        ];
    }

     public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['Generacion'] = ['fechaEmision','normativa','fechaNormativa','idComprobante'];
        return $scenarios;
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'estadoMensaje' => Yii::t('backend', 'Estado Mensaje'),
            'estadoEstado' => Yii::t('backend', 'Estado Estado'),
            'envioMensaje' => Yii::t('backend', 'Envio Mensaje'),
            'envioEstado' => Yii::t('backend', 'Envio Estado'),
            'idComprobante' => Yii::t('backend', 'Factura Electrónica'),
            'clave' => Yii::t('backend', 'Clave'),
            'numConce' => Yii::t('backend', 'N° Consecutivo'),
            'fechaEmision' => Yii::t('backend', 'Fecha Emision'),
            'normativa' => Yii::t('backend', 'Normativa'),
            'fechaNormativa' => Yii::t('backend', 'Fecha Normativa'),
            'estadoEstadoL' => Yii::t('backend', 'Estado DGT'),
            'envioEstadoL' => Yii::t('backend', 'Estado DGT'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdFactura0()
    {
        return $this->hasOne(NcComprobante::className(), ['id' => 'idComprobante']);
    }
    public function getComprobante() {
        return $this->hasOne(NcComprobante::className(), ['id' => 'idComprobante']);
    }
    
    public function getClase(){
       if($this->envioEstado > 99 && $this->envioEstado < 200)
           return 100;
       
       if($this->envioEstado > 199 && $this->envioEstado < 300)
           return 200;
       
       if($this->envioEstado > 299 && $this->envioEstado < 400)
           return 300; 
       
       if($this->envioEstado > 399 && $this->envioEstado < 500)
           return 400;
       
       if($this->envioEstado > 499 && $this->envioEstado < 600)
           return 500;
       
   }

    public function getEstadoEstadoL(){
        
        if($this->estadoEstado>=0 && $this->estadoEstado <=3)
            return self::ESTADOS[$this->estadoEstado];
        return $this->estadoEstado;
    }
    public function getEnvioEstadoL(){
            return self::ESTADOS[$this->getClase()];
    }
}

<?php

namespace backend\models;
use common\components\Moneda;
use Yii;
/**
 * This is the model class for table "package".
 *
 * @property integer $id
 * @property string $categoria
 * @property string $compra
 * @property string $desempacada
 * @property string $proveedor
 * @property string $estado
 * @property double $precio
 * @property string $calidad
 * @property string $descripcion
 * @property string $creacion
 * @property string $modificacion
 * @property integer $num_factura
 */
class Package extends \yii\db\ActiveRecord
{   
    public $cantPacas;
    public $creacion_fecha;
    //public $ValorDeVenta=0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'package';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['categoria', 'compra', 'desempacada', 'proveedor', 'estado', 'precio', 'calidad', 'descripcion', 'creacion', 'modificacion', 'num_factura'], 'required'],
            [['categoria', 'proveedor', 'estado', 'calidad', 'descripcion'], 'string'],
            [['compra', 'desempacada', 'creacion', 'modificacion'], 'safe'],
            [['precio'], 'number'],
            [['num_factura'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'categoria' => Yii::t('backend', 'Categoria'),
            'compra' => Yii::t('backend', 'Compra'),
            'desempacada' => Yii::t('backend', 'Desempacada'),
            'proveedor' => Yii::t('backend', 'Proveedor'),
            'estado' => Yii::t('backend', 'Estado'),
            'precio' => Yii::t('backend', 'Costo Compra'),
            'calidad' => Yii::t('backend', 'Calidad'),
            'descripcion' => Yii::t('backend', 'Descripcion'),
            'creacion' => Yii::t('backend', 'Creacion'),
            'modificacion' => Yii::t('backend', 'Modificacion'),
            'ValorDeVenta' => Yii::t('backend', 'Vendido'),
            'ValorDeTienda' => Yii::t('backend', 'En Tienda'),
            'ValorDeVentaInt' => Yii::t('backend', 'Vendido'),
            'num_factura' => Yii::t('backend', 'Num Factura'),
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    
        
    public function getLaCalidad()
    {
        return $this->hasOne(Diccionario::className(), ['id' => 'calidad']);
    }
               
    public function getElEstado()
    {
        return $this->hasOne(Diccionario::className(), ['id' => 'estado']);
    }        
    public function getLaCategoria()
    {
        return $this->hasOne(Diccionario::className(), ['id' => 'categoria']);
    }    
    public function getElProveedor()
    {
        return $this->hasOne(Colaborador::className(), ['id' => 'proveedor']);
    }       
    public function getLotes(){
        return $this->hasMany(Lote::className(), ['paca' => 'id']);
    }    

	public function getValorDeVenta(){
            $data=$this->lotes;
            $this->cantPacas=count($data);
            $cost=0;
            foreach ($data as $dat){
                $cost+=$dat->precio*($dat->cant_inicial-$dat->cantidad);
            }
            return $cost;
    }   
	
	public function getValorDeVentaInt(){
            $data=$this->lotes;
            $this->cantPacas=count($data);
            $cost=0;
            foreach ($data as $dat){
                $cost += $dat->precio*($dat->cant_inicial-$dat->cantidad);
            }
            return $cost;
    }   
	
	public function getValorDeTienda(){
            $data=$this->lotes;
            $this->cantPacas=count($data);
            $cost=0;
            foreach ($data as $dat){
                $cost+=$dat->precio*$dat->cantidad;
            }
            return $cost;
    }
	
    public function getNombre(){
        $calidad=$this->laCalidad();        
        $categoria=$this->laCategoria();        
        $cod=$this->id();        
        $compra=$this->compra();
    }
	
    
    
    
    /*
     * Muestra las fechas en formato d-m-y
     */
    public function VerFechas(){        
        date_default_timezone_set('America/Costa_Rica');
        $this->compra =  date('d-m-Y',strtotime($this->compra));
        $this->creacion_fecha=$this->creacion;    
        $this->desempacada =  date('d-m-Y',strtotime($this->desempacada));        
    }
    /*
     * Guarda las fechas en BD con formato Y-m-d
     */
    public function GuardarFechas(){        
        date_default_timezone_set('America/Costa_Rica');
        $this->compra =  date('Y-m-d',strtotime($this->compra)); 
        $this->desempacada =  date('Y-m-d',strtotime($this->desempacada));      
        $this->modificacion =  date('Y-m-d',  strtotime('now'));
        $this->creacion =  $this->creacion_fecha;  
              
        if($this->isNewRecord){                   
            $this->creacion =  date('Y-m-d',strtotime('now'));                
        }
    }
}

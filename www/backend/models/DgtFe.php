<?php

namespace backend\models;
use backend\models\NcComprobante;
use yii\helpers\Html;
use Yii;
use backend\models\base\DgtComprobante;
use backend\models\Factura;

/**
 * This is the model class for table "dgt_fe".
 *
 * @property integer $id
 * @property integer $idFactura
 * @property string $clave
 * @property string $numConce
 * @property string $fechaEmision
 * @property string $normativa
 * @property string $fechaNormativa
 * @property integer $envioEstado
 * @property string $envioMensaje
 * @property integer $estadoEstado
 * @property string $estadoMensaje
 */
class DgtFe extends DgtComprobante
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fe_dgt_comprobante';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    { 
        return [
            [['fechaEmision', 'normativa', 'fechaNormativa'], 'required'],
            [['clave', 'numConce','fechaEmision', 'normativa', 'fechaNormativa'], 'string'],
            [['responseEstado','jsonEstado','json','response','pathComprobantePdf','pathMensajeXml','pathComprobanteXml'],'string'],
            [['estadoMensaje', 'envioMensaje'], 'string', 'max' => 5000],
            ['correoNotificacion','integer']
        ];
    }

     public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['Generacion'] = ['fechaEmision','normativa','fechaNormativa','idFactura'];
        return $scenarios;
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'idFactura' => Yii::t('backend', 'Id Factura'),
            'clave' => Yii::t('backend', 'Clave'),
            'numConce' => Yii::t('backend', 'Consecutivo'),
            'fechaEmision' => Yii::t('backend', 'Fecha Emision'),
            'normativa' => Yii::t('backend', 'Normativa'),
            'fechaNormativa' => Yii::t('backend', 'Fecha Normativa'),
            'envioEstado' => Yii::t('backend', 'FE Envio'),
            'envioEstadoL' => Yii::t('backend', 'FE Envio'),
            'envioEstadoD' => Yii::t('backend', 'FE Envio'),
            'envioMensaje' => Yii::t('backend', 'Mensaje de entrega'),
            'estadoEstado' => Yii::t('backend', 'FE Estado'),
            'estadoEstadoL' => Yii::t('backend', 'FE Estado'),
            'estadoEstadoD' => Yii::t('backend', 'FE Estado'),
            'estadoMensaje' => Yii::t('backend', 'Mensaje de estado'),
        ];
    }
    
   public function getNotaCredito(){
      return NcComprobante::find()
      ->where(['factura'=>$this->idComprobante])
      ->one();
   }
   
    public function getComprobante() {
        return $this->hasOne(Factura::className(), ['id' => 'idComprobante']);
    }

  
   
}

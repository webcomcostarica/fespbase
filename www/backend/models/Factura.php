<?php

namespace backend\models;

use Yii;
use common\models\User;
use backend\models\Mensajes;
use backend\models\DgtFe;
use backend\models\base\BaseFactura;
use common\models\FG;

class Factura extends \backend\models\base\BaseFactura {

    const CODFE = '01';
    
    const Facturada = 'Facturada';
    const Procesando = 'Procesando';
    const Anulada = 'Anulada';
    
    public $desde = '';
    public $hasta = '';

    /**
     * Atributos
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('backend', 'ID'),
            'fecha' => Yii::t('backend', 'Fecha'),
            'vence' => Yii::t('backend', 'Vence'),
            'idUsua' => Yii::t('backend', 'Usuario'),
            'creacion' => Yii::t('backend', 'Creación'),
            'modificacion' => Yii::t('backend', 'Modificación'),
            'tipo' => Yii::t('backend', 'Tipo'),
            'forma_pago' => Yii::t('backend', 'Forma Pago'),
            'estado' => Yii::t('backend', 'Estado'),
            'colaboradores' => Yii::t('backend', 'Colaboradores'),
            'dgt_condicion' => Yii::t('backend', 'Condición de venta'),
            'dgt_plazoCredito' => Yii::t('backend', 'Plazo Credito'),
            'dgt_medioPago' => Yii::t('backend', 'Medio Pago'),
            'dgt_moneda' => Yii::t('backend', 'Moneda'),
            'dgt_tipoCambio' => Yii::t('backend', 'Tipo Cambio'),
            'dgt_serviciosGravados' => Yii::t('backend', 'Servicios Gravados'),
            'dgt_serviciosExentos' => Yii::t('backend', 'Servicios Exentos'),
            'dgt_mercaGravada' => Yii::t('backend', 'Merca Gravada'),
            'dgt_mercaExenta' => Yii::t('backend', 'Merca Exenta'),
            'dgt_gravado' => Yii::t('backend', 'Gravado'),
            'dgt_totExento' => Yii::t('backend', 'Tot Exento'),
            'dgt_totVenta' => Yii::t('backend', 'Tot Venta'),
            'dgt_totDescuento' => Yii::t('backend', 'Tot Descuento'),
            'dgt_totVentaNeta' => Yii::t('backend', 'Tot Venta Neta'),
            'dgt_totImpuesto' => Yii::t('backend', 'Tot Impuesto'),
            'dgt_total' => Yii::t('backend', 'Total'),
        ];
    }

    /**
     * Relaciones
     */
   

    public function getSms() {
        return $this->hasMany(Mensajes::className(), ['apartado' => 'id']);
    }

    public function getIdUsua0() {
        return $this->hasOne(User::className(), ['id' => 'idUsua']);
    }

    public function getComprobante() {
        return $this->hasOne(DgtFe::className(), ['idComprobante' => 'id']);
    }

    public function getFacturaColaboradores() {
        return $this->hasMany(FacturaColaboradores::className(), ['id_factura' => 'id']);
    }

    public function getIdColaboradors() {
        return $this->hasMany(Colaborador::className(), ['id' => 'id_colaborador'])->viaTable('fe_colaboradores_comprobante', ['id_factura' => 'id']);
    }

    /**
     * Funciones
     */
    public function getVence() {
        $i = strtotime($this->creacion);
        $fechaFin = '';
        $Cantador = 45; //Busca desde la fecha inicial hasta mes y medio para calcular el dia de cierre de apartado
        for (; $Cantador > 0; $i+=86400) {
            $fechIndex = date('d-m-Y', $i);
            $fechaFin = date($fechIndex); //dia de que finaliza en caso de ser jornada Acumulativa
            $Cantador--;
        }
        return $fechaFin;
    }

    public function getTelefono() {
        foreach ($this->idColaboradors as $data) {
            foreach ($data->tbContactos as $conta) {
                if (strtolower($conta->tipo) == 'tel') {
                    return $conta->datos;
                }
            }
        }
    }

  

    public function VerFechas() {
        $this->fecha = date('d-m-Y h:i:s a', strtotime($this->fecha));
        $this->creacion = date('d-m-Y h:i:s a', strtotime('now'));
        $this->modificacion = date('d-m-Y h:i:s a', strtotime('now'));
    }

    public function GuardarFechas() {
        if ($this->isNewRecord) {
            $this->creacion = date('Y-m-d h:i:s', strtotime('now'));
        }
        $this->modificacion = date('Y-m-d h:i:s', strtotime('now'));
        $this->vence = date('Y-m-d h:i:s', strtotime($this->getVence()));
    }
       
    public function actualizarInventario($detalles) {
        foreach($detalles as $detalle){
            if($detalle->lote != 0){//detalle de inventario
                $lote = $detalle->lote0;
                $lote->cantidadVendida += $detalle->cantidad;
                $lote->cantidad -= $detalle->cantidad;
                if(!$lote->save()){
                    var_dump($lote->errors);                   
                }
            }
        }
    }
    
    public function actualizarInventarioNotaAnulacion($detalles) {
        foreach($detalles as $detalle){
            if($detalle->lote != 0){//detalle de inventario
                $lote = $detalle->lote0;
                $lote->cantidadVendida -= $detalle->cantidad;
                $lote->cantidad += $detalle->cantidad;
                $lote->save();
            }
        }
    }
    
         
    public function setDetalle($detalle) {
        // det subTotal = Precio - descuentos
        // det montoTotalLinea = Precio - descuentos + impuestos
        if($detalle->impuesto != 0){
            $this->dgt_mercaGravada += $detalle->montoTotal;
        }
        else{
            $this->dgt_mercaExenta += $detalle->montoTotal;
        }
        
        $this->dgt_gravado = $this->dgt_serviciosGravados + $this->dgt_mercaGravada;
        $this->dgt_totExento = $this->dgt_serviciosExentos + $this->dgt_mercaExenta;
        $this->dgt_totVenta = FG::dc($this->dgt_gravado) + FG::dc($this->dgt_totExento);
        $this->dgt_totDescuento += $detalle->montoDescuento;
        $this->dgt_totVentaNeta = $this->dgt_totVenta - $this->dgt_totDescuento;
        $this->dgt_totImpuesto += FG::dc($detalle->impuesto);
        $this->dgt_total = $this->dgt_totVentaNeta + $this->dgt_totImpuesto;
        $this->save();
        
        /*
        if($detalle->impuesto != 0){
            $this->dgt_mercaGravada += $detalle->montoTotal;
            FG::logfile('dgt_mg: '.$this->dgt_mercaGravada.' ');
        }
        else{
            $this->dgt_mercaExenta += $detalle->subTotal;
            FG::logfile('dgt_me: '.$this->dgt_mercaExenta.' ');
        }
        
        $this->dgt_gravado = $this->dgt_serviciosGravados + $this->dgt_mercaGravada;
            FG::logfile('dgt_tg: '.$this->dgt_gravado.' ');
        $this->dgt_totExento = $this->dgt_serviciosExentos + $this->dgt_mercaExenta;
            FG::logfile('dgt_te: '.$this->dgt_totExento.' ');
                
        $this->dgt_totVenta = FG::dc($this->dgt_gravado) + FG::dc($this->dgt_totExento);
            FG::logfile('dgt_tv: '.$this->dgt_totVenta.' ');
        
        $this->dgt_totDescuento += $detalle->montoDescuento;
            FG::logfile('dgt_td: '.$this->dgt_totDescuento.' ');
        
        $this->dgt_totVentaNeta = $this->dgt_totVenta-$this->dgt_totDescuento;
            FG::logfile('dgt_tvn: '.$this->dgt_totVentaNeta.' ');
        
        $this->dgt_totImpuesto += FG::dc($detalle->impuestosAplicadosCalc);
            FG::logfile('dgt_tvi: '.$this->dgt_totImpuesto.' ');
        
        $this->dgt_total = $this->dgt_totVentaNeta + $this->dgt_totImpuesto;
            FG::logfile('dgt_tf: '.$this->dgt_total.' ');
        $this->save();
        */
    }
    
     public function getDetalle($detalle) {
         
        if($detalle->impuesto != 0)
            $this->dgt_mercaGravada -= $detalle->subTotal;
        else
            $this->dgt_mercaExenta -= $detalle->subTotal;
        
        $this->dgt_gravado = $this->dgt_serviciosGravados + $this->dgt_mercaGravada;
        $this->dgt_totExento = $this->dgt_serviciosExentos + $this->dgt_mercaExenta;
                
        $this->dgt_totVenta = 
            FG::dc($this->dgt_gravado) + 
            FG::dc($this->dgt_totExento);
        
        $this->dgt_totDescuento -= $detalle->montoDescuento;
        $this->dgt_totVentaNeta = $this->dgt_totVenta;
        $this->dgt_totImpuesto -= $detalle->impuestosAplicadosCalc;
        $this->dgt_total = $this->dgt_totVentaNeta + $this->dgt_totImpuesto;
        $this->save();
    }
    
    public static function getFormatoFecha(){
        return date('Y-m-d h:i:s a', strtotime('now'));
    }
    
    public function getCondicionWords(){
        $condition = [
            '01' => 'Contado',
            '02' => 'Credito',
            '04' => 'Apartado'
        ];
        return $condition[$this->dgt_condicion];
    }

    public function getCondiciones(){
        $condition = [
            '01' => 'Contado',
            '02' => 'Credito',
            '04' => 'Apartado'
        ];
        return $condition;
    }    
    
      public function moneda($pMonto,$s=null){
        if(!empty($s))
            return  $s.' '.number_format((double)$pMonto, 2, ',', ' ');
      
        if($this->dgt_moneda == 'CRC')
            return  '₡ '.number_format((double)$pMonto, 2, ',', ' ');
        else
            return  '$ '.number_format((double)$pMonto, 0, ',', ' ');       
    }

    
 public function getReceptorNombre() {
    foreach ($this->idColaboradors as $key => $item) {
        return $item->getNombreCompleto();
    }
 }
 
 public function getReceptorCedula() {
    foreach ($this->idColaboradors as $key => $item) {
        return $item->cedula;
    }
 }
 
 public function getReceptores() {
        $vendor_arry = [];
        foreach ($this->idColaboradors as $key => $item) {
            array_push($vendor_arry, $item->getNombreCompleto());
        }

        sort($vendor_arry);
        return implode(array_unique($vendor_arry, SORT_STRING), ", ");
    }    
    

}

<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "nc_colaboradores_comprobante".
 *
 * @property integer $id_factura
 * @property integer $id_colaborador
 *
 * @property NcComprobante $idFactura
 */
class NcColaboradoresComprobante extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nc_colaboradores_comprobante';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_factura', 'id_colaborador'], 'required'],
            [['id_factura', 'id_colaborador'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_factura' => Yii::t('backend', 'Id Factura'),
            'id_colaborador' => Yii::t('backend', 'Id Colaborador'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdFactura()
    {
        return $this->hasOne(NcComprobante::className(), ['id' => 'id_factura']);
    }
}

<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tb_contacto".
 *
 * @property integer $id
 * @property integer $colaborador
 * @property string $titulo
 * @property string $datos
 * @property string $tipo
 *
 * @property Colaborador $colaborador0
 */
class Contactos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_contacto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['colaborador', 'titulo', 'datos', 'tipo'], 'required'],
            [['colaborador'], 'integer'],
            [['titulo', 'datos', 'tipo'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'colaborador' => Yii::t('backend', 'Colaborador'),
            'titulo' => Yii::t('backend', 'Titulo'),
            'datos' => Yii::t('backend', 'Datos'),
            'tipo' => Yii::t('backend', 'Tipo'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getColaborador0()
    {
        return $this->hasOne(Colaborador::className(), ['id' => 'colaborador']);
    }
}

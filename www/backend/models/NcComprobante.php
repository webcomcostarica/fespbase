<?php

namespace backend\models;
use common\models\FG;

use Yii;

/**
 * This is the model class for table "nc_comprobante".
 *
 * @property integer $id
 * @property string $fecha
 * @property string $vence
 * @property integer $idUsua
 * @property string $creacion
 * @property string $modificacion
 * @property string $tipo
 * @property string $forma_pago
 * @property string $estado
 * @property integer $colaboradores
 * @property string $dgt_condicion
 * @property string $dgt_plazoCredito
 * @property string $dgt_medioPago
 * @property string $dgt_moneda
 * @property double $dgt_tipoCambio
 * @property double $dgt_serviciosGravados
 * @property double $dgt_serviciosExentos
 * @property double $dgt_mercaGravada
 * @property double $dgt_mercaExenta
 * @property double $dgt_gravado
 * @property double $dgt_totExento
 * @property double $dgt_totVenta
 * @property double $dgt_totDescuento
 * @property double $dgt_totVentaNeta
 * @property double $dgt_totImpuesto
 * @property double $dgt_total
 *
 * @property NcColaboradoresComprobante[] $ncColaboradoresComprobantes
 * @property NcDetalles[] $ncDetalles
 * @property NcDgtComprobante[] $ncDgtComprobantes
 */
class NcComprobante extends \yii\db\ActiveRecord
{
    const CODNC = '03';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nc_comprobante';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha', 'vence', 'idUsua', 'creacion', 'modificacion', 'tipo', 'estado', 'dgt_condicion', 'dgt_medioPago', 'dgt_moneda', 'dgt_tipoCambio', 'dgt_serviciosGravados', 'dgt_serviciosExentos', 'dgt_mercaGravada', 'dgt_mercaExenta', 'dgt_gravado', 'dgt_totExento', 'dgt_totVenta', 'dgt_totDescuento', 'dgt_totVentaNeta', 'dgt_totImpuesto', 'dgt_total'], 'required'],
            [['fecha', 'tipo', 'forma_pago', 'estado', 'dgt_plazoCredito','dgt_razon'], 'string'],
            [['vence', 'creacion', 'modificacion'], 'safe'],
            [['dgt_tipoCambio', 'dgt_serviciosGravados', 'dgt_serviciosExentos', 'dgt_mercaGravada', 'dgt_mercaExenta', 'dgt_gravado', 'dgt_totExento', 'dgt_totVenta', 'dgt_totDescuento', 'dgt_totVentaNeta', 'dgt_totImpuesto', 'dgt_total'], 'number'],
            [['idUsua', 'colaboradores'], 'integer'],
            [['dgt_condicion', 'dgt_medioPago'], 'string', 'max' => 2],
            [['dgt_moneda'], 'string', 'max' => 3]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'fecha' => Yii::t('backend', 'Fecha'),
            'vence' => Yii::t('backend', 'Vence'),
            'idUsua' => Yii::t('backend', 'Id Usua'),
            'creacion' => Yii::t('backend', 'Creacion'),
            'modificacion' => Yii::t('backend', 'Modificacion'),
            'tipo' => Yii::t('backend', 'Tipo'),
            'forma_pago' => Yii::t('backend', 'Forma Pago'),
            'estado' => Yii::t('backend', 'Estado'),
            'colaboradores' => Yii::t('backend', 'Colaboradores'),
            'dgt_condicion' => Yii::t('backend', 'Dgt Condicion'),
            'dgt_plazoCredito' => Yii::t('backend', 'Dgt Plazo Credito'),
            'dgt_medioPago' => Yii::t('backend', 'Dgt Medio Pago'),
            'dgt_moneda' => Yii::t('backend', 'Dgt Moneda'),
            'dgt_tipoCambio' => Yii::t('backend', 'Dgt Tipo Cambio'),
            'dgt_serviciosGravados' => Yii::t('backend', 'Dgt Servicios Gravados'),
            'dgt_serviciosExentos' => Yii::t('backend', 'Dgt Servicios Exentos'),
            'dgt_mercaGravada' => Yii::t('backend', 'Dgt Merca Gravada'),
            'dgt_mercaExenta' => Yii::t('backend', 'Dgt Merca Exenta'),
            'dgt_gravado' => Yii::t('backend', 'Dgt Gravado'),
            'dgt_totExento' => Yii::t('backend', 'Dgt Tot Exento'),
            'dgt_totVenta' => Yii::t('backend', 'Dgt Tot Venta'),
            'dgt_totDescuento' => Yii::t('backend', 'Dgt Tot Descuento'),
            'dgt_totVentaNeta' => Yii::t('backend', 'Dgt Tot Venta Neta'),
            'dgt_totImpuesto' => Yii::t('backend', 'Dgt Tot Impuesto'),
            'dgt_total' => Yii::t('backend', 'Dgt Total'),
        ];
    }

    public function getComprobanteAnulado() {
        return $this->hasOne(Factura::className(), ['id' => 'factura']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNcColaboradoresComprobantes()
    {
        return $this->hasMany(NcColaboradoresComprobante::className(), ['id_factura' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetalleFacturas()
    {
        return $this->hasMany(NcDetalles::className(), ['factura' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComprobante()
    {
        return $this->hasOne(NcDgtComprobante::className(), ['idComprobante' => 'id']);
    }
    
     public function getIdColaboradors() {
        return $this->hasMany(Colaborador::className(), ['id' => 'id_colaborador'])->viaTable('nc_colaboradores_comprobante', ['id_factura' => 'id']);
    }
    
    public function GuardarFechas() {
        if ($this->isNewRecord) {
            $this->creacion = date('Y-m-d h:i:s', strtotime('now'));
        }
        $this->modificacion = date('Y-m-d h:i:s', strtotime('now'));
        $this->vence = date('Y-m-d h:i:s', strtotime('now'));
    }
    
    public function setAnulacion($factura){
        $this->GuardarFechas();
        $this->fecha = self::getFormatoFecha();
        $this->factura = $factura->id;
        $this->idUsua = $factura->idUsua;
        $this->tipo = $factura->tipo;
        $this->forma_pago = $factura->forma_pago;
        $this->estado = $factura->estado;
        $this->dgt_condicion = $factura->dgt_condicion;
        $this->dgt_plazoCredito = $factura->dgt_plazoCredito;
        $this->dgt_medioPago = $factura->dgt_medioPago;
        $this->dgt_moneda = $factura->dgt_moneda;
        $this->dgt_tipoCambio = $factura->dgt_tipoCambio;
        $this->dgt_serviciosGravados = FG::dc($factura->dgt_serviciosGravados);
        $this->dgt_serviciosExentos = FG::dc($factura->dgt_serviciosExentos);
        $this->dgt_mercaGravada = FG::dc($factura->dgt_mercaGravada);
        $this->dgt_mercaExenta = FG::dc($factura->dgt_mercaExenta);
        $this->dgt_gravado = FG::dc($factura->dgt_gravado);
        $this->dgt_totExento = FG::dc($factura->dgt_totExento);
        $this->dgt_totVenta = FG::dc($factura->dgt_totVenta);
        $this->dgt_totDescuento = FG::dc($factura->dgt_totDescuento);
        $this->dgt_totVentaNeta = FG::dc($factura->dgt_totVentaNeta);
        $this->dgt_totImpuesto = FG::dc($factura->dgt_totImpuesto);
        $this->dgt_total = FG::dc($factura->dgt_total);
    }
    public function getClase(){
       if($this->envioEstado > 99 && $this->envioEstado < 200)
           return 100;
       
       if($this->envioEstado > 199 && $this->envioEstado < 300)
           return 200;
       
       if($this->envioEstado > 299 && $this->envioEstado < 400)
           return 300; 
       
       if($this->envioEstado > 399 && $this->envioEstado < 500)
           return 400;
       
       if($this->envioEstado > 499 && $this->envioEstado < 600)
           return 500;
       
   }
     public function moneda($pMonto){
        if($this->dgt_moneda == 'CRC')
            return  '₡ '.number_format($pMonto, 0, ',', ' ');
        else
            return  '$ '.number_format($pMonto, 0, ',', ' ');       
    } 

     public static function getFormatoFecha(){
        return date('Y-m-d h:i:s a', strtotime('now'));
    }
}

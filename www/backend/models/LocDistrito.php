<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "loc_distrito".
 *
 * @property integer $id
 * @property integer $codigoProvincia
 * @property integer $codigoCanton
 * @property integer $codigo
 * @property string $distrito
 */
class LocDistrito extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'loc_distrito';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codigoProvincia', 'codigoCanton', 'codigo'], 'integer'],
            [['distrito'], 'string', 'max' => 70],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigoProvincia' => 'Codigo Provincia',
            'codigoCanton' => 'Codigo Canton',
            'codigo' => 'Codigo',
            'distrito' => 'Distrito',
        ];
    }
    
    public static function getDistritos($canton,$provincia){
        $data = [];
        foreach(LocDistrito::find()
        ->where(['codigoCanton' => $canton])
        ->andWhere(['codigoProvincia' =>$provincia])
        ->all() as $prov){
            $data[$prov->codigo] = $prov->distrito;
        }
        return $data;
    }
}

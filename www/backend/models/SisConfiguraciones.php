<?php

namespace backend\models;

use Yii;
use backend\models\SisParametros;

/**
 * This is the model class for table "sis_configuraciones".
 *
 * @property string $NombreComercial
 * @property string $Cedula
 * @property integer $Persona
 * @property integer $CodPais
 * @property integer $Provincia
 * @property string $Canton
 * @property string $Distrito
 * @property string $Barrio
 * @property string $OtrasSenas
 * @property integer $MostrarSloganEnFactura
 * @property integer $ClientePorDefecto
 * @property string $Telefono
 * @property string $Correo
 * @property integer $Ambiente
 * @property integer $StgPinArchivoP12
 * @property string $StgArchivoP12
 * @property string $StgClaveDGT
 * @property string $StgUsuarioDGT
 * @property integer $ProdPinArchivoP12
 * @property string $ProdArchivoP12
 * @property string $ProdClaveDGT
 * @property string $ProdUsuarioDGT
 * @property string $Fax
 * @property string $GrupoDeActivos
 * @property string $NombreDeLotes
 * @property string $Logo
 * @property integer $Inventario
 * @property integer $Moroso
 * @property string $FormaPago
 * @property string $MedioPago
 * @property string $Moneda
 * @property double $TipoCambio
 * @property integer $Sucursal
 * @property integer $Terminal
 * @property string $estadoStg
 * @property string $estadoProd
 */
class SisConfiguraciones extends \yii\db\ActiveRecord
{
    const PRUEBAS = 0;
    const PRODUCCION = 1;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sis_configuraciones';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NombreComercial', 'Cedula', 'Persona', 'Provincia', 'Canton', 'Distrito', 'Barrio', 'OtrasSenas', 'MostrarSloganEnFactura', 'ClientePorDefecto', 'Telefono', 'Correo', 'Ambiente', 'StgPinArchivoP12', 'StgArchivoP12', 'StgClaveDGT', 'StgUsuarioDGT', 'ProdPinArchivoP12', 'ProdArchivoP12', 'ProdClaveDGT', 'ProdUsuarioDGT', 'Fax', 'GrupoDeActivos', 'NombreDeLotes',  'Moneda', 'TipoCambio', 'Sucursal', 'Terminal'], 'required'],
            [['PlantillaPdf','Slogan','NombreComercial', 'Cedula', 'OtrasSenas', 'Telefono', 'Correo', 'StgArchivoP12', 'StgClaveDGT', 'StgUsuarioDGT', 'ProdArchivoP12', 'ProdClaveDGT', 'ProdUsuarioDGT', 'Fax', 'GrupoDeActivos', 'NombreDeLotes', 'Logo', 'FormaPago', 'MedioPago', 'Moneda', 'estadoStg', 'estadoProd'], 'string'],
            [['Persona', 'CodPais', 'Provincia', 'MostrarSloganEnFactura', 'ClientePorDefecto', 'Ambiente', 'StgPinArchivoP12', 'ProdPinArchivoP12', 'Inventario', 'Moroso', 'Sucursal', 'Terminal'], 'integer'],
            [['TipoCambio'], 'number'],
            [['Canton', 'Distrito', 'Barrio'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'NombreComercial' => 'Nombre Comercial',
            'Cedula' => 'Cedula',
            'Persona' => 'Persona',
            'CodPais' => 'Cod Pais',
            'Provincia' => 'Provincia',
            'Canton' => 'Canton',
            'Distrito' => 'Distrito',
            'Barrio' => 'Barrio',
            'OtrasSenas' => 'Otras Senas',
            'MostrarSloganEnFactura' => 'Mostrar Slogan En Factura',
            'ClientePorDefecto' => 'Cliente Por Defecto',
            'Telefono' => 'Telefono',
            'Correo' => 'Correo',
            'Ambiente' => 'Ambiente',
            'StgPinArchivoP12' => 'Stg Pin Archivo P12',
            'StgArchivoP12' => 'Stg Archivo P12',
            'StgClaveDGT' => 'Stg Clave Dgt',
            'StgUsuarioDGT' => 'Stg Usuario Dgt',
            'ProdPinArchivoP12' => 'Prod Pin Archivo P12',
            'ProdArchivoP12' => 'Prod Archivo P12',
            'ProdClaveDGT' => 'Prod Clave Dgt',
            'ProdUsuarioDGT' => 'Prod Usuario Dgt',
            'Fax' => 'Fax',
            'GrupoDeActivos' => 'Grupo De Activos',
            'NombreDeLotes' => 'Nombre De Lotes',
            'Logo' => 'Logo',
            'Inventario' => 'Inventario',
            'Moroso' => 'Moroso',
            'FormaPago' => 'Forma Pago',
            'MedioPago' => 'Medio Pago',
            'Moneda' => 'Moneda',
            'TipoCambio' => 'Tipo Cambio',
            'Sucursal' => 'Sucursal',
            'Terminal' => 'Terminal',
            'estadoStg' => 'Estado Stg',
            'estadoProd' => 'Estado Prod',
        ];
    }
    
    public static function getParametrosGenerales(){ 
       $model = SisConfiguraciones::findOne(1); 
       $datos = $model->attributes; 
       $parametrosAmbiente = SisParametros::find()->all();
       foreach($parametrosAmbiente as $param){
           $datos[$param->dato] = $param->valor;
       }
       Yii::$app->cache->set('cuenta_cedula', $datos['Cedula']);
       
       return $datos; 
   } 
}

<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "lote_precio".
 *
 * @property integer $id
 * @property string $fecha
 * @property integer $usuario
 * @property integer $lote
 * @property string $descripcion
 * @property string $vigente
 * @property string $costo
 * @property string $piezas
 *
 * @property Lote $lote0
 * @property User $usuario0
 */
class LotePrecio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lote_precio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha', 'usuario', 'lote', 'descripcion', 'costo','piezas'], 'required'],
            [['usuario', 'vigente','lote','piezas'], 'integer'],
            [['fecha'], 'safe'],
            [['descripcion'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'fecha' => Yii::t('backend', 'Fecha'),
            'usuario' => Yii::t('backend', 'Usuario'),
            'lote' => Yii::t('backend', 'Lote'),
            'descripcion' => Yii::t('backend', 'Descripcion'),
            'vigente' => Yii::t('backend', 'Vigente'),
            'costo' => Yii::t('backend', 'Costo'),
            'piezas' => Yii::t('backend', 'Piezas Vendidas'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLote0()
    {
        return $this->hasOne(Lote::className(), ['id' => 'lote']);
    }
    public function getEstado(){
        $estado=[0=>'Inactivo',1=>'Vigente'];
        return $estado[$this->vigente];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario0()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'usuario']);
    }
    
     /*
     * Guarda las fechas en BD con formato Y-m-d H:i:s
     */
    public function GuardarFechas(){        
        date_default_timezone_set('America/Costa_Rica');
        $this->fecha =  date('Y-m-d H:i:s',  strtotime('now')); 
    }
}

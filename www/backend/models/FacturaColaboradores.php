<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "factura_colaboradores".
 *
 * @property integer $id_factura
 * @property integer $id_colaborador
 *
 * @property Colaborador $idColaborador
 * @property Factura $idFactura
 */
class FacturaColaboradores extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fe_colaboradores_comprobante';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_factura', 'id_colaborador'], 'required'],
            [['id_factura', 'id_colaborador'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_factura' => Yii::t('backend', 'Id Factura'),
            'id_colaborador' => Yii::t('backend', 'Id Colaborador'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdColaborador()
    {
        return $this->hasOne(Colaborador::className(), ['id' => 'id_colaborador']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdFactura()
    {
        return $this->hasOne(Factura::className(), ['id' => 'id_factura']);
    }
    
}

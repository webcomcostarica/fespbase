<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\NcComprobante;
use backend\models\NcDgtComprobante;
use common\components\FuncionesDGT;
/**
 * NcComprobanteSearch represents the model behind the search form about `backend\models\NcComprobante`.
 */
class NcComprobanteSearch extends NcComprobante
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'idUsua', 'colaboradores'], 'integer'],
            [['fecha', 'vence', 'creacion', 'modificacion', 'tipo', 'forma_pago', 'estado', 'dgt_condicion', 'dgt_plazoCredito', 'dgt_medioPago', 'dgt_moneda'], 'safe'],
            [['dgt_tipoCambio', 'dgt_serviciosGravados', 'dgt_serviciosExentos', 'dgt_mercaGravada', 'dgt_mercaExenta', 'dgt_gravado', 'dgt_totExento', 'dgt_totVenta', 'dgt_totDescuento', 'dgt_totVentaNeta', 'dgt_totImpuesto', 'dgt_total'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NcComprobante::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'vence' => $this->vence,
            'idUsua' => $this->idUsua,
            'creacion' => $this->creacion,
            'modificacion' => $this->modificacion,
            'colaboradores' => $this->colaboradores,
            'dgt_tipoCambio' => $this->dgt_tipoCambio,
            'dgt_serviciosGravados' => $this->dgt_serviciosGravados,
            'dgt_serviciosExentos' => $this->dgt_serviciosExentos,
            'dgt_mercaGravada' => $this->dgt_mercaGravada,
            'dgt_mercaExenta' => $this->dgt_mercaExenta,
            'dgt_gravado' => $this->dgt_gravado,
            'dgt_totExento' => $this->dgt_totExento,
            'dgt_totVenta' => $this->dgt_totVenta,
            'dgt_totDescuento' => $this->dgt_totDescuento,
            'dgt_totVentaNeta' => $this->dgt_totVentaNeta,
            'dgt_totImpuesto' => $this->dgt_totImpuesto,
            'dgt_total' => $this->dgt_total,
        ]);

        $query->andFilterWhere(['like', 'fecha', $this->fecha])
            ->andFilterWhere(['like', 'tipo', $this->tipo])
            ->andFilterWhere(['like', 'forma_pago', $this->forma_pago])
            ->andFilterWhere(['like', 'estado', $this->estado])
            ->andFilterWhere(['like', 'dgt_condicion', $this->dgt_condicion])
            ->andFilterWhere(['like', 'dgt_plazoCredito', $this->dgt_plazoCredito])
            ->andFilterWhere(['like', 'dgt_medioPago', $this->dgt_medioPago])
            ->andFilterWhere(['like', 'dgt_moneda', $this->dgt_moneda]);

        return $dataProvider;
    }
    
    public function consultarEstados($config){
        $comprobantes_pendientes = NcDgtComprobante::find()
        ->andWhere(['!=','correoNotificacion',1])
        ->andWhere(['=','envioEstado',202])
        ->andWhere(['=','estadoEstado',1])
        ->all();
       
        foreach($comprobantes_pendientes as $compro){
            if(isset($compro->comprobante) && $compro->comprobante->idColaboradors)
            FuncionesDGT::enviarCorreo($compro->comprobante);
        }

        $comprobantes_pendientes = NcDgtComprobante::find()
         ->andWhere(['or',
           ['estadoEstado'=>0],
           ['estadoEstado'=>200]
        ])
        ->all();
                
        foreach($comprobantes_pendientes as $compro){
            if(FuncionesDGT::ValidarConexionHacienda())
                FuncionesDGT::getEstado([
                    'Numeracion' => NcDgtComprobante::find()->where(['idComprobante' => $compro->idComprobante])->one()
                ],$config);
        }

       
    }
}

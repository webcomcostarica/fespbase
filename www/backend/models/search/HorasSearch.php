<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Horas;

/**
 * HorasSearch represents the model behind the search form about `backend\models\Horas`.
 */
class HorasSearch extends Horas
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'dia', 'usuario', 'estado', 'costo', 'total'], 'integer'],
            [['desde','hasta'], 'safe'],
            [['cantidad'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Horas::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'dia' => $this->dia,
            'usuario' => $this->usuario,
            'desde' => $this->desde,
            'hasta' => $this->hasta,
            'estado' => $this->estado,
            'cantidad' => $this->cantidad,
            'costo' => $this->costo,
            'total' => $this->total,
        ]);
		
        return $dataProvider;
    }
}

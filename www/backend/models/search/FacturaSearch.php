<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Factura;
use backend\models\base\BaseFactura;
use common\models\AccessHelpers; 
use backend\models\Colaborador;
use backend\models\DgtFe;
use common\components\FuncionesDGT;
use common\models\FG;


/**
 * FacturaSearch represents the model behind the search form about `backend\models\Factura`.
 */
class FacturaSearch extends BaseFactura
{
    public $permiso;
    public $colaborador_nombre;
    public $concecutivo;
    public $feEnvio;
    public $feEstado;
   
    public function rules()
    {
        return [
            [['colaborador_nombre','dgt_condicion','fecha', 'tipo', 'forma_pago', 'estado', 'dgt_plazoCredito'], 'string'],
            [['vence', 'creacion','colaboradores', 'modificacion'], 'safe'],
            [['dgt_tipoCambio', 'dgt_serviciosGravados', 'dgt_serviciosExentos', 'dgt_mercaGravada', 'dgt_mercaExenta', 'dgt_gravado', 'dgt_totExento', 'dgt_totVenta', 'dgt_totDescuento', 'dgt_totVentaNeta', 'dgt_totImpuesto', 'dgt_total'], 'number'],
            [['id','idUsua', 'colaboradores'], 'integer'],
            [['dgt_medioPago'], 'string', 'max' => 2],
            [['dgt_moneda'], 'string', 'max' => 3]
        ];
    }
    
   
    
    public function search($params)
    {
        $query = Factura::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
             'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            var_dump($this->errors);
            die;
            return $dataProvider;
        }
        
         $query->joinWith('idColaboradors');
         $query->joinWith('comprobante');

        $query->andFilterWhere([
            'id' => $this->id,
            'fecha' => $this->fecha,        
            'idUsua' => $this->idUsua,
            'creacion' => $this->creacion,
            'vence' => $this->vence,
            'modificacion' => $this->modificacion,
            'colaboradores' => $this->colaboradores,
            'dgt_condicion' => $this->dgt_condicion,            
            'dgt_medioPago' => $this->dgt_medioPago,
        ]);
        
        
        $query->andFilterWhere(['like', 'colaborador.nombreCompleto', $this->colaborador_nombre]);
        $query->andFilterWhere(['like', 'comprobante.numConce', $this->concecutivo]);
        $query->andFilterWhere(['like', 'comprobante.envioEstado', $this->feEnvio]);
        $query->andFilterWhere(['like', 'comprobante.estadoEstado', $this->feEstado]);
       
       
        return $dataProvider;
    }
	
    public function consultarEstados($config){
        
        
    }
    
    public function diarioDeVentas($params)
    {
        $query = Factura::find()
        ->joinWith(['idColaboradors']);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            var_dump($this->errors);
            die;
            return $dataProvider;
        }
        
        //$date = date('Y-m-d', strtotime('now'));

        $query->andFilterWhere([
            'id' => $this->id,
           // 'fecha' => $this->fecha,        
            'idUsua' => $this->idUsua,
            'creacion' => $this->creacion,
            'vence' => $this->vence,
            'modificacion' => $this->modificacion,
            'colaboradores' => $this->colaboradores,
            'dgt_condicion' => $this->dgt_condicion,            
            'dgt_medioPago' => $this->dgt_medioPago,
            'estado' => 'Facturada',
        ]);
                
        if(Yii::$app->request->post()){
            
       date_default_timezone_set('America/Costa_Rica'); 
            $desde =  date('Y-m-d H:i:s', strtotime($_POST['Factura']['desde']) );
            $hasta =  date('Y-m-d H:i:s', strtotime($_POST['Factura']['hasta']) );
            
            $query->andFilterWhere([
                    'between', 
                    'fecha',
                    $_POST['Factura']['desde'],
                    $_POST['Factura']['hasta']
                   // $desde,
                   // $hasta
            ]);
           // $query->andFilterWhere(['<=', 'fecha', date($_POST['Factura']['hasta'])]);
        }else{
            $query->andFilterWhere(['like', 'fecha', $this->fecha]);
        }

        $query->andFilterWhere(['like', 'colaborador.nombreCompleto', $this->colaborador_nombre]);
        
        return $dataProvider;
        
    }
    
     public function diarioDeVentasTipo($tipo)
    {
        $query = Factura::find()
        ->joinWith(['idColaboradors']);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->andFilterWhere([
            'id' => $this->id,     
            'idUsua' => $this->idUsua,
            'creacion' => $this->creacion,
            'vence' => $this->vence,
            'modificacion' => $this->modificacion,
            'colaboradores' => $this->colaboradores,
            'dgt_condicion' => $this->dgt_condicion,
            'dgt_medioPago' => $this->dgt_medioPago,
            'estado' => 'Facturada',
        ]);
                
       date_default_timezone_set('America/Costa_Rica');  
       if($tipo == 'hoy')
		   if(isset($_POST['Factura']['desde']))
				$query->andFilterWhere(['like', 'fecha', $_POST['Factura']['desde']]);
			else
				$query->andFilterWhere(['like', 'fecha', date('Y-m-d')]);
        
        if($tipo == 'mes'){    
            $hasta = $desde = date('Y-m-', strtotime('now'));
            $desde .= '01 00:00:00';			
            $hasta .= FG::ultimo_dia().' 23:59:59';
			
             if(isset($_POST['Factura']['desde']))
				$desde = $_POST['Factura']['desde'].' 00:00:00';
             if(isset($_POST['Factura']['hasta']))
				$hasta = $_POST['Factura']['desde'].' 23:59:59';
			
            $query->andFilterWhere([
                    'between', 
                    'fecha',
                    $desde,
                    $hasta
            ]);
        }
        
        $query->andFilterWhere(['like', 'colaborador.nombreCompleto', $this->colaborador_nombre]);
        
        return $dataProvider;
        
    }
   	
	public function getColaborafact() {
		return Colaborador::find()
						->select(['nombre', 'id'])
						->indexBy('id')
						->column();
      
    }
}

<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Caja;

/**
 * CajaSearch represents the model behind the search form about `backend\models\Caja`.
 */
class CajaSearch extends Caja
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'cincuentamil', 'veitemil', 'diesmil', 'cincomil', 'dosmil', 'mil', 'quinientos', 'cien', 'cincuenta', 'vienticinco', 'dies', 'cinco', 'usuario'], 'integer'],
            [['fecha'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Caja::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'cincuentamil' => $this->cincuentamil,
            'veitemil' => $this->veitemil,
            'diesmil' => $this->diesmil,
            'cincomil' => $this->cincomil,
            'dosmil' => $this->dosmil,
            'mil' => $this->mil,
            'quinientos' => $this->quinientos,
            'cien' => $this->cien,
            'cincuenta' => $this->cincuenta,
            'vienticinco' => $this->vienticinco,
            'dies' => $this->dies,
            'cinco' => $this->cinco,
            'fecha' => $this->fecha,
            'usuario' => $this->usuario,
        ]);

        return $dataProvider;
    }
}

<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Lote;

/**
 * LoteSearch represents the model behind the search form about `backend\models\Lote`.
 */
class LoteSearch extends Lote
{
    public $id_paca;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'paca', 'cantidad', 'cant_inicial'], 'integer'],
            [['precio'], 'number'],
            [['cod_barras','cod_provedor_a','cod_inte_producto','cod_prod_proveedor'], 'string'],
            [['calidad', 'descripcion', 'creacion', 'modificacion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Lote::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            //'cod_barras' => $this->cod_barras,
            'paca' => $this->paca,
            //'precio' => $this->precio,
            //'cod_provedor_a' => $this->cod_provedor_a,
            'cantidad' => $this->cantidad,
            'creacion' => $this->creacion,
            'modificacion' => $this->modificacion,
            'cant_inicial' => $this->cant_inicial,
        ]);

        $query->andFilterWhere(['like', 'calidad', $this->calidad])
            ->andFilterWhere(['like', 'precio', $this->precio])
            ->andFilterWhere(['like', 'cod_barras', $this->cod_barras])
            ->andFilterWhere(['like', 'cod_prod_proveedor', $this->cod_prod_proveedor])
            ->andFilterWhere(['like', 'cod_inte_producto', $this->cod_inte_producto])
            ->andFilterWhere(['like', 'cod_provedor_a', $this->cod_provedor_a])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion])
            ;

        return $dataProvider;
    }
}

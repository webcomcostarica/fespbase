<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Localizaciongeografica;

class LocalizaciongeograficaSearch extends Localizaciongeografica
{
   public $nivelSuperior;
   public function rules()
    {
        return [
            [['idLocalizacion', 'orden','nivel', 'idPadre'], 'integer'],
            [['nombre', 'codigo', 'iso2', 'iso3', 'postal', 'memoria','nivelSuperior'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }
    
   public function search($params)
    {
        $query = Localizaciongeografica::find();

       // $query->joinWith(['localizacionsuperior']);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        
        $query->andFilterWhere([
            'idLocalizacion' => $this->idLocalizacion,
            'orden' => $this->orden,
            'idPadre' => $this->idPadre,
            'nivel' => $this->nivel,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'codigo', $this->codigo])
            ->andFilterWhere(['like', 'iso2', $this->iso2])
            ->andFilterWhere(['like', 'iso3', $this->iso3])
            ->andFilterWhere(['like', 'postal', $this->postal])
            ->andFilterWhere(['like', 'localizaciongeografica.nombre', $this->nivelSuperior])
            ->andFilterWhere(['like', 'memoria', $this->memoria]);

        return $dataProvider;
    }
}

<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Package;

/**
 * packageSearch represents the model behind the search form about `backend\models\Package`.
 */
class packageSearch extends Package
{
    /**
     * Variables publicas
     */
      public $laCategoria;
      public $elEstado;
      public $laCalidad;
              
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'num_factura'], 'integer'],
            [['laCategoria','categoria', 'compra', 'desempacada', 'proveedor', 'estado', 'calidad', 'descripcion', 'creacion', 'modificacion'], 'safe'],
            [['precio'], 'number'],
            [['laCategoria'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Package::find();
     /*   $query->joinWith(['laCategoria'
          //  ,'elEstado','laCalidad'
            ]);
       */ 
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        // Lets do the same with country now
        $dataProvider->sort->attributes['laCategoria'] = [
            'asc' => ['laCategoria.nombre' => SORT_ASC],
            'desc' => ['laCategoria.nombre' => SORT_DESC],
        ];
        // No search? Then return data Provider
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        
        $query->andFilterWhere([
            'id' => $this->id,
            'compra' => $this->compra,
            'desempacada' => $this->desempacada,
            'precio' => $this->precio,
            'creacion' => $this->creacion,
            'modificacion' => $this->modificacion,
            'num_factura' => $this->num_factura,
           // 'ValorDeVenta' => $this->ValorDeVenta,
        ]);

        $query->andFilterWhere(['like', 'categoria', $this->categoria])
            ->andFilterWhere(['like', 'proveedor', $this->proveedor])
            ->andFilterWhere(['like', 'estado', $this->estado])
            ->andFilterWhere(['like', 'calidad', $this->calidad])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion])          
           // ->andFilterWhere(['like', 'ValorDeVenta', $this->ValorDeVenta])
           // ->andFilterWhere(['like', 'laCategoria.nombre', $this->laCategoria])
           // ->andFilterWhere(['like', 'elEstado.nombre', $this->elEstado])
           // ->andFilterWhere(['like', 'laCalidad.nombre', $this->laCalidad])
                ;
                var_dump(count($dataProvider));die;
        return $dataProvider;
    }
}

<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\CrmOportunidades;

/**
 * CrmOportunidadesSearch represents the model behind the search form about `backend\models\CrmOportunidades`.
 */
class CrmOportunidadesSearch extends CrmOportunidades
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'etapa', 'prioridad', 'orden', 'estado', 'encargado', 'cliente'], 'integer'],
            [['monto'], 'number'],
            [['detalles', 'creacion', 'modificacion', 'cierreProyectado'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
   public function search($params)
    {
        $query = CrmOportunidades::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
           /* 'monto' => $this->monto,
            'etapa' => $this->etapa,
            'prioridad' => $this->prioridad,
            'orden' => $this->orden,
            'estado' => $this->estado,
            'creacion' => $this->creacion,
            'modificacion' => $this->modificacion,
            'cierreProyectado' => $this->cierreProyectado,
            'encargado' => $this->encargado,
            'cliente' => $this->cliente,*/
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'detalles', $this->detalles]);

        return $dataProvider;
    }
}

<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\MrComprobante;

/**
 * MrComprobanteSearch represents the model behind the search form about `backend\models\MrComprobante`.
 */
class MrComprobanteSearch extends MrComprobante
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'fechaEmisionDoc', 'mensaje', 'detalleMensaje'], 'integer'],
            [['tipoComprobante', 'clave', 'numeroConcecutivo', 'numeroCedulaEmisor'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MrComprobante::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'fechaEmisionDoc' => $this->fechaEmisionDoc,
            'mensaje' => $this->mensaje,
            'detalleMensaje' => $this->detalleMensaje,
        ]);

        $query->andFilterWhere(['like', 'tipoComprobante', $this->tipoComprobante])
            ->andFilterWhere(['like', 'clave', $this->clave])
            ->andFilterWhere(['like', 'numeroConcecutivo', $this->numeroConcecutivo])
            ->andFilterWhere(['like', 'numeroCedulaEmisor', $this->numeroCedulaEmisor]);

        return $dataProvider;
    }
}

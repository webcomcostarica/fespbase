<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\VentasDiarias;

/**
 * VentasDiariasSearch represents the model behind the search form of `backend\models\VentasDiarias`.
 */
class VentasDiariasSearch extends VentasDiarias
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['fecha', 'condicion', 'medioPago'], 'safe'],
            [['tipoCambio', 'totGravado', 'totExento', 'totDescuento', 'totVentaNeta', 'totImpuesto', 'total'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VentasDiarias::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['fecha'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tipoCambio' => $this->tipoCambio,
            'totGravado' => $this->totGravado,
            'totExento' => $this->totExento,
            'totDescuento' => $this->totDescuento,
            'totVentaNeta' => $this->totVentaNeta,
            'totImpuesto' => $this->totImpuesto,
            'total' => $this->total,
        ]);

        if(isset($_POST['Factura'])){
            date_default_timezone_set('America/Costa_Rica');
            $desde =  date('Y-m-d H:i:s', strtotime($_POST['Factura']['desde']) );
            $hasta =  date('Y-m-d H:i:s', strtotime($_POST['Factura']['hasta']) );
            
            $query->andFilterWhere([
                    'between', 
                    'fecha',
                   // $_POST['Factura']['desde'],
                   // $_POST['Factura']['hasta']
                    $desde,
                    $hasta
            ]);
            
        }
        $query ->andFilterWhere(['like', 'condicion', $this->condicion])
        //andFilterWhere(['like', 'fecha', $this->fecha])
            ->andFilterWhere(['like', 'medioPago', $this->medioPago]);

        return $dataProvider;
    }
}
